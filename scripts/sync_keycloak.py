#!/usr/bin/env python3

import json
import os
from requests import Session

admin_session = Session()
user_session = Session()
realm = os.getenv('KEYCLOAK_REALM')
keycloak_url = os.getenv('KEYCLOAK_URL')
# dest_api = "https://devapi.ncaecybergames.org"
dest_api = "http://localhost:5000"


auth = {
    "client_id": os.getenv("KEYCLOAK_CLIENT_ID"),
    "client_secret": os.getenv("KEYCLOAK_SECRET"),
    "grant_type": "client_credentials",
}

resp = admin_session.post(
    f"{keycloak_url}/realms/{realm}/protocol/openid-connect/token",
    auth,
)
resp.raise_for_status()
admin_token = resp.json()["access_token"]
admin_session.headers.update({"Authorization": f"Bearer {admin_token}"})

resp = admin_session.get(
    f"{keycloak_url}/admin/realms/{realm}/users?max=2000&enabled=true"
)
resp.raise_for_status()
users = resp.json()

print(len(users))

for user in users:
    try:
        resp = admin_session.post(
            f"{keycloak_url}/realms/{realm}/protocol/openid-connect/token",
            data={
                "client_id": auth["client_id"],
                "client_secret": auth["client_secret"],
                "requested_subject": user["id"],
                "grant_type": "urn:ietf:params:oauth:grant-type:token-exchange",
                "subject_token": admin_token,
                "scope": "openid",
            },
        )
        resp.raise_for_status()
        user_session.headers.update(
            {"Authorization": f"Bearer {resp.json()['access_token']}"}
        )

        resp = user_session.get(f"{dest_api}/v1/auth/whoami")
        resp.raise_for_status()
    except:
        continue

    print(json.dumps(resp.json()))