/**
 * Copy-paste the region object and the function into a browser to get a list of schools
 * https://www.caecommunity.org/cae-map
 */

const Regions = {
  southeast: {
    label: 'Southeast Region',
    states: [
      'Kentucky',
      'North Carolina',
      'South Carolina',
      'Tennessee',
      'Georgia',
      'Alabama',
      'Mississippi',
      'Georgia',
      'Florida',
      'Puerto Rico',
    ],
  },
  northeast: {
    label: 'Northeast Region',
    states: [
      'Maine',
      'Vermont',
      'Massachusetts',
      'Connecticut',
      'New Hampshire',
      'Pennsylvania',
      'West Virginia',
      'Virginia',
      'New York',
      'Delaware',
      'Rhode Island',
      'New Jersey',
      'District of Columbia',
      'Maryland',
    ],
  },
  midwest: {
    label: 'Midwest Region',
    states: [
      'Ohio',
      'Michigan',
      'Indiana',
      'Illinois',
      'Wisconsin',
      'Minnesota',
      'Iowa',
      'Missouri',
      'Kansas',
      'Nebraska',
    ],
  },
  southwest: {
    label: 'Southwest Region',
    states: [
      //
      'Arkansas',
      'Louisiana',
      'Oklahoma',
      'Texas',
      'New Mexico',
      'Arizona',
      'California',
      'Nevada',
      'Hawaii',
    ],
  },
  northwest: {
    label: 'Northwest Region',
    states: [
      'North Dakota',
      'South Dakota',
      'Wyoming',
      'Montana',
      'Colorado',
      'Utah',
      'Idaho',
      'Washington',
      'Oregon',
      'Alaska',
    ],
  },
};

function parseSchoolsBrowser() {
  const schools = [];
  // $ is jquery from the page here
  const data = $(
    '#block-jango-sub-content--2 > article > div > div > div > div > div > div > footer > div > table > tbody tr',
  );
  for (const row of data) {
    const school = {
      name: row.querySelector('.cae-map--title').textContent.trim(),
      domain: new URL(row.querySelector('.cae-map--designations a').href).host.split('.').slice(-2).join('.'),
      state: row.querySelector('.views-field-field-address-administrative-area').textContent.trim(),
    };
    for (const [region, { states }] of Object.entries(Regions)) {
      if (states.includes(school.state)) school.region = region;
    }
    if (!school.region) console.warn('region not found for ', school);
    schools.push(school);
  }

  return schools;
}
