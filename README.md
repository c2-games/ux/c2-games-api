# Tracking UX API

Central API for tracking users across the entire C2Games platform.

## Setup

```sh
npm install --legacy-peer-deps
```

Add the following properties to `.env` file at root level:
Note: values shown are for development environment.

```ini
POSTGRES_DB=tracking-api
POSTGRES_HOST=localhost
POSTGRES_PORT=5432
POSTGRES_USER=postgres
POSTGRES_PASSWORD=postgres
KEYCLOAK_URL=https://auth.c2games.org
KEYCLOAK_REALM=dev
KEYCLOAK_CLIENT_ID=tracking-api
KEYCLOAK_SECRET=[secret]
```

Note: `[secret]` can be found on the Tracking API client page under the 'Credentials' tab of the Keycloak admin console. If you do not have admin access to keycloak, ask an administrator to share the client secret with you.

## Database

Use the following commands to start and stop the database:

```sh
# start
npm run start:db

# stop
npm run stop:db
```

Once the database is running you should run migrations to ensure you have the latest DB state

```sh
npm run migrate:db
```

### Syncing Keycloak Users

To sync keycloak users to your local database for testing, run the command:

```shell
export $(cat .env | xargs) && python3 scripts/sync_keycloak.py
```

## Running the app

```sh
# development
npm run start

# watch mode
npm run start:dev

# production mode
npm run start:prod
```

## Test

```sh
# unit tests
npm run test

# e2e tests
npm run test:e2e

# test coverage
npm run test:cov
```

## TypeORM

This application uses [TypeORM](https://typeorm.io/) for ORM utilities.

Note: typeorm commands are run against the latest in `./dist` so be sure to run `npm run build` first.

```sh
# check configuration
npm run typeorm:cli

# show all migrations
npm run typeorm:cli migration:show

# generate migration (generates based on changes made to entities)
npm run typeorm:cli -- migration:generate -d src/db/migrations -n MigrationName

# create migration (generates blank template)
npm run typeorm:cli -- migration:create -d src/db/migrations -n MigrationName

# run migration
npm run typeorm:cli -- migration:run

# revert last migration
npm run typeorm:cli -- migration:revert
```

## Swagger

Swagger Docs are available at /api when the application is running. Ex: http://localhost:5000/api

## Intercom Feature

The Intercom feature provides a sitewide intercom banner for displaying messages. Currently, modifying the banner requires direct changes to the database. The available fields for modification are listed in the table below:

| Field                  | Description                                                                                                                                                   |
| ---------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **id**                 | Auto-generated.                                                                                                                                               |
| **message**            | The message you want to display. Special message: `Finals start in, countdown(:eventId)` or `Don't forget to sign up for the new event!` for event countdown. |
| **enabled**            | `true` or `false`.                                                                                                                                            |
| **created_by_user_id** | Auto-generated.                                                                                                                                               |
| **intercom_color**     | Choose from: `red`, `blue`, `green`, `yellow`, or `purple`.                                                                                                   |
| **created_date**       | Auto-generated.                                                                                                                                               |
| **updated_by_user_id** | Auto-generated.                                                                                                                                               |
| **updated_date**       | Auto-generated.                                                                                                                                               |

### Usage Example

To display a countdown to an event, set the `message` field to `Finals starts in, countdown(5)` where `5` is the ID of the event for which you want to display the countdown. The banner will then show the remaining time until the event starts.
