import { registerDecorator, ValidationOptions } from 'class-validator';
import { StateInRegionValidator } from '../validators/state-in-region.validator';

export function StateInRegion(validationOptions?: ValidationOptions) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      name: 'StateInRegion',
      target: object.prototype,
      propertyName: propertyName,
      options: validationOptions,
      validator: StateInRegionValidator,
    });
  };
}
