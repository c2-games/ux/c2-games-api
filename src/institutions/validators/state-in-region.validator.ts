import { Injectable } from '@nestjs/common';
import { ValidatorConstraint, ValidatorConstraintInterface } from 'class-validator';
import { Regions } from '../entities/regions';
import { CreateInstitutionDto } from '../dto/create-institution.dto';
import { UpdateInstitutionDto } from '../dto/update-institution.dto';

@ValidatorConstraint({ name: 'StateInRegion' })
@Injectable()
export class StateInRegionValidator implements ValidatorConstraintInterface {
  validate(object: CreateInstitutionDto | UpdateInstitutionDto) {
    const { state, region } = object as { state: string; region: string };
    return !!Regions[region]?.states?.includes(state);
  }

  defaultMessage() {
    return `State does not belong to the specified region`;
  }
}
