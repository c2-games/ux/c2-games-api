import { Logger } from '@nestjs/common';
import {
  AfterInsert,
  AfterRemove,
  AfterUpdate,
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Region, Regions } from './regions';
import { State, States } from './states';

@Entity({ name: 'institutions' })
export class Institution {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'name', unique: true })
  name: string;

  // todo do we need to support multiple domains? Ex, sunyit.edu & sunypoly.edu
  @Column({ name: 'domain' })
  domain: string;

  @Column({ name: 'state', enum: States })
  state: State;

  @Column({ name: 'region', enum: Object.keys(Regions) })
  region: Region;

  @UpdateDateColumn({
    name: 'updated_date',
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
  })
  updatedDate: Date;

  @CreateDateColumn({
    name: 'created_date',
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
  })
  createdDate: Date;

  @AfterInsert()
  logInsert() {
    Logger.log(`Inserted Institution with id ${this.id}`);
  }

  @AfterUpdate()
  logUpdate() {
    Logger.log(`Updated Institution with id ${this.id}`);
  }

  @AfterRemove()
  logRemove() {
    Logger.log('Removed Institution');
  }
}
