import { BadRequestException, Body, Controller, Get, Param, Patch, Post, Query, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiParam, ApiTags } from '@nestjs/swagger';
import { UserRolesGuard } from '../auth/guards/user-roles.guard';
import { InstitutionsService } from './institutions.service';
import { UserRoles } from '../auth/decorators/roles.decorator';
import { UserRole } from '../auth/decorators/user-role.enum';
import { CreateInstitutionDto } from './dto/create-institution.dto';
import { QueryInstitutionDto } from './dto/query-institution.dto';
import { UpdateInstitutionDto } from './dto/update-institution.dto';
import { Region, Regions } from './entities/regions';
import { Unprotected } from 'nest-keycloak-connect';
import { StateInRegionValidator } from './validators/state-in-region.validator';

@ApiTags('Institutions')
@Controller('institutions')
@ApiBearerAuth()
@UseGuards(UserRolesGuard)
export class InstitutionsController {
  constructor(private readonly institutionsService: InstitutionsService) {}

  @Get()
  @Unprotected()
  @ApiOperation({ summary: 'Find all institutions' })
  @ApiOkResponse({ description: 'Institutions found' })
  findAll(@Query() params?: QueryInstitutionDto) {
    return this.institutionsService.findAll(params);
  }

  @Post()
  @ApiOperation({ summary: 'Create New Institution' })
  @ApiOkResponse({ description: 'Institution Created' })
  @UserRoles(UserRole.EventAdmin)
  // @StateInRegion()
  create(@Body() createDto: CreateInstitutionDto) {
    // todo stop using validator manually
    const validator = new StateInRegionValidator();
    if (!validator.validate(createDto)) {
      throw new BadRequestException(validator.defaultMessage());
    }
    return this.institutionsService.create(createDto);
  }

  @Patch(':id')
  @ApiOperation({ summary: 'Update Institution' })
  @ApiOkResponse({ description: 'Institution Updated' })
  @UserRoles(UserRole.EventAdmin)
  // @StateInRegion()
  update(@Param('id') id: string, @Body() updateDto: UpdateInstitutionDto) {
    // todo stop using validator manually
    const validator = new StateInRegionValidator();
    if (!validator.validate(updateDto)) {
      throw new BadRequestException(validator.defaultMessage());
    }
    return this.institutionsService.update(+id, updateDto);
  }

  @Get('/by-region/:region')
  @Unprotected()
  @ApiOperation({ summary: 'Find institutions' })
  @ApiOkResponse({ description: 'Institutions found' })
  @ApiParam({ name: 'region', enum: Object.keys(Regions) })
  findByRegion(@Param('region') region: Region) {
    return this.institutionsService.findAll({ region });
  }

  @Get('/by-domain/:domain')
  @Unprotected()
  @ApiOperation({ summary: 'Find institutions' })
  @ApiOkResponse({ description: 'Institutions found' })
  findByDomain(@Param('domain') domain: string) {
    return this.institutionsService.findAll({ domain });
  }
}
