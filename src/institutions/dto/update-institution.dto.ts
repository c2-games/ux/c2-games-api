import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';
import { States } from '../entities/states';
import { Region, Regions } from '../entities/regions';

export class UpdateInstitutionDto {
  @ApiProperty({
    type: 'string',
    required: false,
    example: 'Mohawk Valley Community College',
  })
  @IsOptional()
  @IsString()
  name: string;

  @ApiProperty({
    type: 'string',
    required: false,
    description: "Domain used to verify a user's email address",
    example: 'mvcc.edu',
  })
  @IsOptional()
  @IsString()
  domain: string;

  @ApiProperty({
    type: 'string',
    enum: States,
    required: false,
    example: 'New York',
  })
  @IsOptional()
  @IsString()
  state: string;

  @ApiProperty({
    type: 'string',
    enum: Regions,
    required: false,
    example: 'northeast',
  })
  @IsOptional()
  @IsString()
  region: Region;
}
