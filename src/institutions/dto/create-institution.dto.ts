import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import { States } from '../entities/states';
import { Region, Regions } from '../entities/regions';

export class CreateInstitutionDto {
  @ApiProperty({
    type: 'string',
    required: true,
    example: 'Mohawk Valley Community College',
  })
  @IsString()
  name: string;

  @ApiProperty({
    type: 'string',
    required: true,
    description: "Domain used to verify a user's email address",
    example: 'mvcc.edu',
  })
  @IsString()
  domain: string;

  @ApiProperty({
    type: 'string',
    enum: States,
    required: true,
    example: 'New York',
  })
  @IsString()
  state: string;

  @ApiProperty({
    type: 'string',
    enum: Object.keys(Regions),
    required: true,
    example: 'northeast',
  })
  @IsString()
  region: Region;
}
