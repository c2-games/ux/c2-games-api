import { ApiProperty } from '@nestjs/swagger';
import { IsDate, IsOptional, IsString } from 'class-validator';
import { State, States } from '../entities/states';
import { Region, Regions } from '../entities/regions';

export class QueryInstitutionDto {
  @ApiProperty({
    type: 'string',
    required: false,
    example: 'Mohawk Valley Community College',
  })
  @IsOptional()
  @IsString()
  name?: string;

  @ApiProperty({
    type: 'string',
    required: false,
    description: "Domain used to verify a user's email address",
    example: 'mvcc.edu',
  })
  @IsOptional()
  @IsString()
  domain?: string;

  @ApiProperty({
    type: 'string',
    enum: States,
    required: false,
    example: 'New York',
  })
  @IsOptional()
  @IsString()
  state?: State;

  @ApiProperty({
    type: 'string',
    enum: Object.keys(Regions),
    required: false,
    example: 'northeast',
  })
  @IsOptional()
  @IsString()
  region?: Region;
}
