import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { KeycloakModule } from '../keycloak/keycloak.module';
import { InstitutionsController } from './institutions.controller';
import { InstitutionsService } from './institutions.service';
import { StateInRegionValidator } from './validators/state-in-region.validator';
import { Institution } from './entities/institution.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Institution]), KeycloakModule],
  controllers: [InstitutionsController],
  providers: [InstitutionsService, StateInRegionValidator],
})
export class InstitutionsModule {}
