import { BadRequestException, ConflictException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { QueryFailedError, Repository } from 'typeorm';
import { CreateInstitutionDto } from './dto/create-institution.dto';
import { Institution } from './entities/institution.entity';
import { QueryInstitutionDto } from './dto/query-institution.dto';
import { UpdateInstitutionDto } from './dto/update-institution.dto';

@Injectable()
export class InstitutionsService {
  constructor(@InjectRepository(Institution) private repo: Repository<Institution>) {}

  async save(inputDto: CreateInstitutionDto | UpdateInstitutionDto) {
    try {
      return await this.repo.save(inputDto);
    } catch (err) {
      if (err instanceof QueryFailedError) {
        const Exc = err.driverError.routine === '_bt_check_unique' ? ConflictException : BadRequestException;
        throw new Exc(err.driverError.detail);
      }
      throw err;
    }
  }

  async create(createDto: CreateInstitutionDto) {
    const data = this.repo.create(createDto);
    await this.save(data);
    return data;
  }

  findAll(params?: QueryInstitutionDto) {
    let query = this.repo.createQueryBuilder();
    if (params.state)
      query = query.andWhere('state = :state', {
        state: params.state,
      });
    if (params.region)
      query = query.andWhere('region = :region', {
        region: params.region,
      });
    if (params.domain)
      query = query.andWhere('domain = :domain', {
        domain: params.domain,
      });
    if (params.name)
      query = query.andWhere('name = :name', {
        name: params.name,
      });
    return query.getMany();
  }

  async findOne(id: number) {
    if (!id) {
      throw new BadRequestException('ID not provided');
    }
    const data = await this.repo.findOne(id);
    if (!data) {
      throw new NotFoundException('Institution not found');
    }
    return data;
  }

  async update(id: number, updateMessageDto: UpdateInstitutionDto) {
    const data = await this.findOne(id);
    Object.assign(data, updateMessageDto);
    await this.save(data);
    return data;
  }

  async remove(id: number) {
    const data = await this.findOne(id);
    await this.repo.remove(data);
    return data;
  }
}
