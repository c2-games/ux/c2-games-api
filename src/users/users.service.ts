import { BadRequestException, ForbiddenException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { UserQuery } from './dto/user-query';
import { User } from './entities/user.entity';
import { UserRole } from '../auth/decorators/user-role.enum';
import { UserRolesGuard } from '../auth/guards/user-roles.guard';
import { parseEmailDomains } from '../utils';

@Injectable()
export class UsersService {
  constructor(@InjectRepository(User) private repo: Repository<User>, private readonly roleGuard: UserRolesGuard) {}

  canAccessEntity(currentUser: User, user: User) {
    if (user.id === currentUser.id || this.roleGuard.getRoles(currentUser).includes(UserRole.Staff)) {
      // If user is retrieving their own account, or user is staff, then allow
      return user;
    }

    const currentUserDomains = parseEmailDomains(currentUser.email);
    const userDomains = parseEmailDomains(user.email);

    if (currentUserDomains.institutionDomain === userDomains.institutionDomain) {
      // if user is from the same school (same email domain), then allow
      return user;
    }

    // otherwise, deny
    throw new ForbiddenException('You cannot query users from other institutions');
  }

  async create(createUserDto: CreateUserDto, currentUserId = 0) {
    const user = this.repo.create({
      ...createUserDto,
      createdByUserId: currentUserId,
      updatedByUserId: currentUserId,
    });
    await this.repo.save(user);
    return user;
  }

  async findAll() {
    return this.repo.find();
  }

  async findOneById(id: number, autoError = true) {
    if (!id) {
      throw new BadRequestException();
    }
    const user = await this.repo.findOne(id);
    if (!user && autoError) {
      throw new NotFoundException('User not found');
    }
    return user;
  }

  async findOne(query: UserQuery) {
    console.log('finding one with query ', query);
    const user = await this.repo.findOne({ ...query });
    if (!user) {
      throw new NotFoundException('User not found');
    }
    return user;
  }

  async findOneByKeycloakSid(keycloakSid: string) {
    if (!keycloakSid) {
      return null;
    }
    return this.repo.findOne({ keycloakSid });
  }
}
