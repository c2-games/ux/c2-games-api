import { createParamDecorator, ExecutionContext } from '@nestjs/common';

export const CurrentUser = createParamDecorator((data: never, context: ExecutionContext) => {
  const request = context.switchToHttp().getRequest();
  // NOTE: this is the database user, NOT the keycloak user.
  // The KC c2games claims and realm access have been manually added in current-user.interceptor.ts
  return request.currentUser;
});
