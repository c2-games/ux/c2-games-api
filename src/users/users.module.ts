import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { KeycloakModule } from '../keycloak/keycloak.module';
import { User } from './entities/user.entity';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { UserExistsRule } from './validators/user-exists-rule.validator';
import { UserRolesGuard } from '../auth/guards/user-roles.guard';

@Module({
  imports: [TypeOrmModule.forFeature([User]), KeycloakModule],
  controllers: [UsersController],
  providers: [UsersService, UserExistsRule, UserRolesGuard],
  exports: [UsersService],
})
export class UsersModule {}
