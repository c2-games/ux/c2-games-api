import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsInt, IsOptional } from 'class-validator';

export class UserQuery {
  @ApiProperty({
    type: 'number',
    required: false,
    example: '123',
  })
  @IsInt()
  @IsOptional()
  id?: number;

  @ApiProperty({
    type: 'email',
    required: false,
    example: 'joe@example.com',
  })
  @IsEmail()
  @IsOptional()
  email?: string;
}
