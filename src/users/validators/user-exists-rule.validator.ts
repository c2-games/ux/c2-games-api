import { Injectable } from '@nestjs/common';
import { ValidatorConstraint, ValidatorConstraintInterface } from 'class-validator';
import { UsersService } from '../users.service';

@ValidatorConstraint({ name: 'UserExists', async: true })
@Injectable()
export class UserExistsRule implements ValidatorConstraintInterface {
  constructor(private readonly usersService: UsersService) {}

  async validate(id: number) {
    try {
      await this.usersService.findOneById(id);
    } catch (error) {
      return false;
    }
    return true;
  }

  defaultMessage() {
    return `User does not exist`;
  }
}
