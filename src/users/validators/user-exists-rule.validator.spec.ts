import { NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { createFakeUser } from '../../utils/test-helpers/fakes/user-faker';
import { UsersService } from '../users.service';
import { UserExistsRule } from './user-exists-rule.validator';

const fakeUser = createFakeUser();

describe('UserExistsRule', () => {
  let userExistsRule: UserExistsRule;
  let service: UsersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UserExistsRule,
        {
          provide: UsersService,
          useValue: {
            findOneById: jest.fn().mockImplementation((id: number) => Promise.resolve(fakeUser)),
          },
        },
      ],
    }).compile();

    userExistsRule = module.get<UserExistsRule>(UserExistsRule);
    service = module.get<UsersService>(UsersService);
  });

  it('should return true when user exists', async () => {
    expect(userExistsRule.validate(fakeUser.id)).resolves.toEqual(true);
    expect(service.findOneById).toBeCalledTimes(1);
    expect(service.findOneById).toBeCalledWith(fakeUser.id);
  });

  it('should return false when user does not exist', async () => {
    const findOneByIdSpy = jest.spyOn(service, 'findOneById').mockRejectedValueOnce(NotFoundException);
    expect(userExistsRule.validate(fakeUser.id)).resolves.toEqual(false);
    expect(findOneByIdSpy).toBeCalledTimes(1);
    expect(findOneByIdSpy).toBeCalledWith(fakeUser.id);
  });

  it('should return correct default message', () => {
    expect(userExistsRule.defaultMessage()).toEqual('User does not exist');
  });
});
