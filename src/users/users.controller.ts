import { Controller, ForbiddenException, Get, Param, Query, UseGuards, ValidationPipe } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { UserQuery } from './dto/user-query';
import { UsersService } from './users.service';
import { UserRolesGuard } from '../auth/guards/user-roles.guard';
import { UserRole } from '../auth/decorators/user-role.enum';
import { UserRoles } from '../auth/decorators/roles.decorator';
import { CurrentUser } from './decorators/current-user.decorator';
import { User } from './entities/user.entity';

@ApiTags('Users')
@ApiBearerAuth()
@Controller('users')
@UseGuards(UserRolesGuard)
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Get('/find')
  @ApiOperation({ summary: 'Find user' })
  @ApiOkResponse({ description: 'User found' })
  async findOne(
    @Query(
      new ValidationPipe({
        transform: true,
        transformOptions: { enableImplicitConversion: true },
        whitelist: true,
        forbidNonWhitelisted: true,
      }),
    )
    query: UserQuery,
    @CurrentUser() currentUser: User,
  ) {
    return this.usersService.canAccessEntity(currentUser, await this.usersService.findOne(query));
  }

  @Get(':id')
  @ApiOperation({ summary: 'Find user by ID' })
  @ApiOkResponse({ description: 'User found' })
  async findOneById(@Param('id') id: string, @CurrentUser() currentUser: User) {
    const user = await this.usersService.findOneById(+id);
    return this.usersService.canAccessEntity(currentUser, user);
  }

  @Get()
  @ApiOperation({ summary: 'Find all users' })
  @ApiOkResponse({ description: 'Users found' })
  @UserRoles(UserRole.Staff)
  findAll() {
    return this.usersService.findAll();
  }
}
