import { faker } from '@faker-js/faker';
import { BadRequestException, NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UsersService } from './users.service';
import { User } from './entities/user.entity';
import { createFakeUser } from '../utils/test-helpers/fakes/user-faker';

const currentUserId = faker.datatype.number();

const testUser = createFakeUser(currentUserId);

describe('UsersService', () => {
  let service: UsersService;
  let repo: Repository<User>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UsersService,
        {
          provide: getRepositoryToken(User),
          useValue: {
            create: jest.fn().mockReturnValue(testUser),
            save: jest.fn(),
            find: jest.fn().mockResolvedValue([testUser]),
            findOne: jest.fn().mockResolvedValue(testUser),
          },
        },
      ],
    }).compile();

    service = module.get<UsersService>(UsersService);
    repo = module.get<Repository<User>>(getRepositoryToken(User));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should create a new user when provided with valid createUserDto', async () => {
    const createUserDto = {
      email: testUser.email,
      keycloakSid: testUser.keycloakSid,
    };

    const user = await service.create(createUserDto, currentUserId);

    expect(user).toEqual(testUser);
    expect(repo.create).toBeCalledTimes(1);
    expect(repo.create).toBeCalledWith({
      ...createUserDto,
      createdByUserId: currentUserId,
      updatedByUserId: currentUserId,
    });
    expect(repo.save).toBeCalledTimes(1);
  });

  it('should find all users', async () => {
    const users = await service.findAll();

    expect(users).toEqual([testUser]);
  });

  it('should find one user', async () => {
    const user = await service.findOneById(faker.datatype.number());

    expect(user).toEqual(testUser);
  });

  it('should throw bad request exception on findOneById if id not provided', async () => {
    await expect(() => service.findOneById(null)).rejects.toThrow(BadRequestException);
  });

  it('should throw not found exception on findOneById if user not found', async () => {
    const userId = faker.datatype.number();
    const repoSpy = jest.spyOn(repo, 'findOne').mockReturnValue(undefined);

    await expect(() => service.findOneById(userId)).rejects.toThrow(new NotFoundException('User not found'));
    expect(repoSpy).toBeCalledTimes(1);
    expect(repoSpy).toBeCalledWith(userId);
  });

  it('should find one user by keycloakSid', async () => {
    const user = await service.findOneByKeycloakSid(faker.datatype.string());

    expect(user).toEqual(testUser);
  });
});
