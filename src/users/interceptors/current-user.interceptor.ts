import { CallHandler, ExecutionContext, Inject, Injectable, Logger, NestInterceptor } from '@nestjs/common';
import KeycloakConnect from 'keycloak-connect';
import { KEYCLOAK_INSTANCE } from 'nest-keycloak-connect';
import { Observable } from 'rxjs';
import { User } from '../entities/user.entity';
import { UsersService } from '../users.service';

/* eslint-disable @typescript-eslint/no-namespace */
declare global {
  namespace Express {
    interface Request {
      currentUser?: User;
    }
  }
}

@Injectable()
export class CurrentUserInterceptor implements NestInterceptor {
  constructor(
    @Inject(KEYCLOAK_INSTANCE) private keycloak: KeycloakConnect.Keycloak,
    private usersService: UsersService,
  ) {}

  async intercept(context: ExecutionContext, next: CallHandler): Promise<Observable<any>> {
    const request = context.switchToHttp().getRequest();
    try {
      const authHeader = request.headers.authorization;
      if (authHeader && authHeader.startsWith('Bearer ')) {
        const token = authHeader.substring(7);
        const { email, sub: keycloakSid } = (await this.keycloak.grantManager.userInfo(token)) as any;
        if (keycloakSid) {
          let user = await this.usersService.findOneByKeycloakSid(keycloakSid);
          if (!user) {
            Logger.log('creating user for the first time');
            user = await this.usersService.create({ email, keycloakSid });
          }
          Logger.log(`user is ${JSON.stringify(user)}`);
          request.currentUser = {
            ...user,
            realm_access: request.user.realm_access,
            'https://c2games.org/jwt/claims': request.user['https://c2games.org/jwt/claims'],
          };
        }
      }
    } catch (error) {
      Logger.warn('Unable to fetch current user; setting currentUser to 0');
      request.currentUser = { id: 0 };
    } finally {
      return next.handle();
    }
  }
}
