import { Logger } from '@nestjs/common';
import {
  AfterInsert,
  AfterRemove,
  AfterUpdate,
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { EventTeamUser } from '../../event-team-users/entities/event-team-user.entity';
import { UserAchievement } from '../../user-achievements/entities/user-achievement.entity';
import { UserBadge } from '../../user-badges/entities/user-badge.entity';
import { UserChallengeMilestone } from '../../user-challenge-milestones/entities/user-challenge-milestone.entity';
import { UserMessage } from '../../user-messages/entities/user-message.entity';
import { UserUserChecklistItem } from '../../user-user-checklist-items/entities/user-user-checklist-item.entity';

@Entity({ name: 'users' })
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'email', unique: true })
  email: string;

  @Column({ name: 'keycloak_sid', unique: true })
  keycloakSid: string;

  @Column({ name: 'enabled', default: true })
  enabled: boolean;

  @Column({ name: 'created_by_user_id', default: 0 })
  createdByUserId: number;

  @CreateDateColumn({
    name: 'created_date',
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
  })
  createdDate: Date;

  @Column({ name: 'updated_by_user_id', default: 0 })
  updatedByUserId: number;

  @UpdateDateColumn({
    name: 'updated_date',
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
  })
  updatedDate: Date;

  @OneToMany(() => UserUserChecklistItem, (userUserChecklistItem) => userUserChecklistItem.user)
  userUserChecklistItems: UserUserChecklistItem[];

  @OneToMany(() => EventTeamUser, (eventTeamUser) => eventTeamUser.user)
  eventTeamUsers: EventTeamUser[];

  @OneToMany(() => UserAchievement, (userAchievement) => userAchievement.user)
  userAchievements: UserAchievement[];

  @OneToMany(() => UserBadge, (userBadge) => userBadge.user)
  userBadges: UserBadge[];

  @OneToMany(() => UserChallengeMilestone, (userChallengeMilestone) => userChallengeMilestone.user)
  userChallengeMilestones: UserChallengeMilestone[];

  @OneToMany(() => UserMessage, (userMessage) => userMessage.user)
  userMessages: UserMessage[];

  // Virtual Columns, populated from JWT
  realm_access: { roles: string[] };
  'https://c2games.org/jwt/claims': { 'x-tracking-api-roles': string[]; [key: string]: unknown };

  @AfterInsert()
  logInsert() {
    Logger.log(`Inserted User with id ${this.id}`);
  }

  @AfterUpdate()
  logUpdate() {
    Logger.log(`Updated User with id ${this.id}`);
  }

  @AfterRemove()
  logRemove() {
    Logger.log('Removed User');
  }
}
