import { faker } from '@faker-js/faker';
import { Test, TestingModule } from '@nestjs/testing';
import { KeycloakModule } from '../keycloak/keycloak.module';
import { createFakeUser } from '../utils/test-helpers/fakes/user-faker';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';

const currentUserId = faker.datatype.number();

const fakeUser = createFakeUser(currentUserId);

describe('UsersController', () => {
  let controller: UsersController;
  let service: UsersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [KeycloakModule],
      controllers: [UsersController],
      providers: [
        {
          provide: UsersService,
          useValue: {
            findAll: jest.fn().mockResolvedValue([fakeUser]),
            findOneById: jest.fn().mockImplementation((id: number) => Promise.resolve({ ...fakeUser, id })),
          },
        },
      ],
    }).compile();

    controller = module.get<UsersController>(UsersController);
    service = module.get<UsersService>(UsersService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should find all users', async () => {
    const users = await controller.findAll();

    expect(users).toEqual([fakeUser]);
    expect(service.findAll).toBeCalledTimes(1);
  });

  it('should find one user by id', async () => {
    const user = await controller.findOneById(`${fakeUser.id}`);

    expect(user).toEqual(fakeUser);
    expect(service.findOneById).toBeCalledTimes(1);
    expect(service.findOneById).toBeCalledWith(fakeUser.id);
  });
});
