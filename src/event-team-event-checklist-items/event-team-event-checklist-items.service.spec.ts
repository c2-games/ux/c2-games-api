import { faker } from '@faker-js/faker';
import { BadRequestException, NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { EventTeamsService } from '../event-teams/event-teams.service';
import { createFakeEventTeamEventChecklistItem } from '../utils/test-helpers/fakes/event-team-event-checklist-item-faker';
import { createFakeEventTeam } from '../utils/test-helpers/fakes/event-team-faker';
import { GetEventTeamEventChecklistItemRequestParamsDto } from './dto/get-event-team-event-checklist-item-request-params.dto';
import { EventTeamEventChecklistItem } from './entities/event-team-event-checklist-item.entity';
import { EventTeamEventChecklistItemsService } from './event-team-event-checklist-items.service';

const currentUserId = faker.datatype.number();

const testEventTeam = createFakeEventTeam(currentUserId);

const testEventTeamEventChecklistItem = createFakeEventTeamEventChecklistItem({
  eventId: testEventTeam.eventId,
  eventTeamId: testEventTeam.id,
  userId: currentUserId,
});

const getEventTeamEventChecklistItemRequestParamsDto = new GetEventTeamEventChecklistItemRequestParamsDto();

describe('EventTeamEventChecklistItemsService', () => {
  let eventTeamsService: EventTeamsService;
  let service: EventTeamEventChecklistItemsService;
  let repo: Repository<EventTeamEventChecklistItem>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        EventTeamEventChecklistItemsService,
        {
          provide: EventTeamsService,
          useValue: { findOne: jest.fn().mockResolvedValue(testEventTeam) },
        },
        {
          provide: getRepositoryToken(EventTeamEventChecklistItem),
          useValue: {
            create: jest.fn().mockReturnValue(testEventTeamEventChecklistItem),
            save: jest.fn(),
            findAll: jest.fn().mockResolvedValue([testEventTeamEventChecklistItem]),
            createQueryBuilder: jest.fn(() => ({
              where: jest.fn().mockReturnThis(),
              andWhere: jest.fn().mockReturnThis(),
              getMany: jest.fn().mockResolvedValue([testEventTeamEventChecklistItem]),
            })),
            findOne: jest.fn().mockResolvedValue(testEventTeamEventChecklistItem),
            update: jest.fn().mockResolvedValue(true),
            remove: jest.fn().mockResolvedValue(true),
          },
        },
      ],
    }).compile();

    eventTeamsService = module.get<EventTeamsService>(EventTeamsService);
    service = module.get<EventTeamEventChecklistItemsService>(EventTeamEventChecklistItemsService);
    repo = module.get<Repository<EventTeamEventChecklistItem>>(getRepositoryToken(EventTeamEventChecklistItem));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should create a new event team event checklist item when provided with valid createEventTeamEventChecklistItemDto that contains eventId', async () => {
    const createEventTeamEventChecklistItemDto = {
      eventId: testEventTeamEventChecklistItem.eventId,
      eventTeamId: testEventTeamEventChecklistItem.eventTeamId,
      eventChecklistItemId: testEventTeamEventChecklistItem.eventChecklistItemId,
    };

    const eventTeamEventChecklistItem = await service.create(createEventTeamEventChecklistItemDto, currentUserId);

    expect(eventTeamEventChecklistItem).toEqual(testEventTeamEventChecklistItem);
    expect(repo.create).toBeCalledTimes(1);
    expect(repo.create).toBeCalledWith({
      ...createEventTeamEventChecklistItemDto,
      createdByUserId: currentUserId,
      updatedByUserId: currentUserId,
    });
    expect(repo.save).toBeCalledTimes(1);
    expect(eventTeamsService.findOneById).toBeCalledTimes(0);
  });

  it('should create a new event team event checklist item when provided with valid createEventTeamEventChecklistItemDto that does not contain eventId', async () => {
    const createEventTeamEventChecklistItemDto = {
      eventTeamId: testEventTeamEventChecklistItem.eventTeamId,
      eventChecklistItemId: testEventTeamEventChecklistItem.eventChecklistItemId,
    };

    const eventTeamEventChecklistItem = await service.create(createEventTeamEventChecklistItemDto, currentUserId);

    expect(eventTeamEventChecklistItem).toEqual(testEventTeamEventChecklistItem);
    expect(repo.create).toBeCalledTimes(1);
    expect(repo.create).toBeCalledWith({
      ...createEventTeamEventChecklistItemDto,
      createdByUserId: currentUserId,
      updatedByUserId: currentUserId,
    });
    expect(repo.save).toBeCalledTimes(1);
    expect(eventTeamsService.findOneById).toBeCalledTimes(1);
    expect(eventTeamsService.findOneById).toBeCalledWith(createEventTeamEventChecklistItemDto.eventTeamId);
  });

  it('should find all event team event checklist items', async () => {
    const eventTeamEventChecklistItems = await service.findAll(getEventTeamEventChecklistItemRequestParamsDto);

    expect(eventTeamEventChecklistItems).toEqual([testEventTeamEventChecklistItem]);
  });

  it('should find one event team event checklist item', async () => {
    const eventTeamEventChecklistItem = await service.findOne(faker.datatype.number());

    expect(eventTeamEventChecklistItem).toEqual(testEventTeamEventChecklistItem);
  });

  it('should throw bad request exception on findOne if id not provided', async () => {
    await expect(() => service.findOne(null)).rejects.toThrow(BadRequestException);
  });

  it('should throw not found exception on findOne if event team event checklist item not found', async () => {
    const eventTeamEventChecklistItemId = faker.datatype.number();
    const repoSpy = jest.spyOn(repo, 'findOne').mockReturnValue(undefined);

    await expect(() => service.findOne(eventTeamEventChecklistItemId)).rejects.toThrow(
      new NotFoundException('Event Team Event Checklist Item not found'),
    );
    expect(repoSpy).toBeCalledTimes(1);
    expect(repoSpy).toBeCalledWith(eventTeamEventChecklistItemId);
  });

  it('should update event team event checklist item', async () => {
    const eventTeamEventChecklistItemId = faker.datatype.number();
    const updateEventTeamEventChecklistItemDto = {
      completedOn: faker.datatype.datetime(),
    };

    const eventTeamEventChecklistItem = await service.update(
      eventTeamEventChecklistItemId,
      updateEventTeamEventChecklistItemDto,
      currentUserId,
    );

    expect(eventTeamEventChecklistItem).toEqual({
      ...testEventTeamEventChecklistItem,
      ...updateEventTeamEventChecklistItemDto,
    });
    expect(repo.findOne).toBeCalledTimes(1);
    expect(repo.findOne).toBeCalledWith(eventTeamEventChecklistItemId);
    expect(repo.save).toBeCalledTimes(1);
  });

  it('should remove event team event checklist item', async () => {
    const eventTeamEventChecklistItemId = faker.datatype.number();

    const eventTeamEventChecklistItem = await service.remove(eventTeamEventChecklistItemId);

    expect(eventTeamEventChecklistItem).toEqual(testEventTeamEventChecklistItem);
  });
});
