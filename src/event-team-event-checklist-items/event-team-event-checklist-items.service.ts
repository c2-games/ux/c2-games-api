import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { EventTeamsService } from '../event-teams/event-teams.service';
import { CreateEventTeamEventChecklistItemDto } from './dto/create-event-team-event-checklist-item.dto';
import { GetEventTeamEventChecklistItemRequestParamsDto } from './dto/get-event-team-event-checklist-item-request-params.dto';
import { UpdateEventTeamEventChecklistItemDto } from './dto/update-event-team-event-checklist-item.dto';
import { EventTeamEventChecklistItem } from './entities/event-team-event-checklist-item.entity';

@Injectable()
export class EventTeamEventChecklistItemsService {
  constructor(
    private eventTeamsService: EventTeamsService,
    @InjectRepository(EventTeamEventChecklistItem)
    private repo: Repository<EventTeamEventChecklistItem>,
  ) {}

  async create(createEventTeamEventChecklistItemDto: CreateEventTeamEventChecklistItemDto, currentUserId = 0) {
    // If event_id was not provided, grab it from the Event Team
    if (!createEventTeamEventChecklistItemDto.eventId) {
      const eventTeam = await this.eventTeamsService.findOneById(createEventTeamEventChecklistItemDto.eventTeamId);
      createEventTeamEventChecklistItemDto.eventId = eventTeam.eventId;
    }
    const eventTeamEventChecklistItem = this.repo.create({
      ...createEventTeamEventChecklistItemDto,
      createdByUserId: currentUserId,
      updatedByUserId: currentUserId,
    });
    await this.repo.save(eventTeamEventChecklistItem);
    return eventTeamEventChecklistItem;
  }

  findAll(params?: GetEventTeamEventChecklistItemRequestParamsDto) {
    let query = this.repo.createQueryBuilder('event_teams_x_event_checklist_items').where('1=1');
    query = params.eventTeamId
      ? query.andWhere('event_teams_x_event_checklist_items.eventTeamId = :eventTeamId', {
          eventTeamId: params.eventTeamId,
        })
      : query;
    query = params.eventId
      ? query.andWhere('event_teams_x_event_checklist_items.eventId = :eventId', {
          eventId: params.eventId,
        })
      : query;
    query = params.eventChecklistItemId
      ? query.andWhere('event_teams_x_event_checklist_items.eventChecklistItemId = :eventChecklistItemId', {
          eventChecklistItemId: params.eventChecklistItemId,
        })
      : query;
    query = params.completedOn
      ? query.andWhere('event_teams_x_event_checklist_items.completedOn = :completedOn', {
          completedOn: params.completedOn,
        })
      : query;
    return query.getMany();
  }

  async findOne(id: number) {
    if (!id) {
      throw new BadRequestException();
    }
    const eventTeamEventChecklistItem = this.repo.findOne(id);
    if (!eventTeamEventChecklistItem) {
      throw new NotFoundException('Event Team Event Checklist Item not found');
    }
    return eventTeamEventChecklistItem;
  }

  async update(
    id: number,
    updateEventTeamEventChecklistItemDto: UpdateEventTeamEventChecklistItemDto,
    currentUserId = 0,
  ) {
    const eventTeamEventChecklistItem = await this.findOne(id);
    Object.assign(updateEventTeamEventChecklistItemDto, {
      updatedByUserId: currentUserId,
    });
    Object.assign(eventTeamEventChecklistItem, updateEventTeamEventChecklistItemDto);
    await this.repo.save(eventTeamEventChecklistItem);
    return eventTeamEventChecklistItem;
  }

  async remove(id: number) {
    const eventTeamEventChecklistItem = await this.findOne(id);
    await this.repo.remove(eventTeamEventChecklistItem);
    return eventTeamEventChecklistItem;
  }
}
