import { Logger } from '@nestjs/common';
import {
  AfterInsert,
  AfterRemove,
  AfterUpdate,
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  Unique,
  UpdateDateColumn,
} from 'typeorm';
import { EventChecklistItem } from '../../event-checklist-items/entities/event-checklist-item.entity';
import { EventTeam } from '../../event-teams/entities/event-team.entity';
import { Event } from '../../events/entities/event.entity';

@Entity({ name: 'event_teams_x_event_checklist_items' })
@Unique(['eventId', 'eventTeamId', 'eventChecklistItemId'])
export class EventTeamEventChecklistItem {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'event_id' })
  eventId: number;

  @Column({ name: 'event_team_id' })
  eventTeamId: number;

  @Column({ name: 'event_checklist_item_id' })
  eventChecklistItemId: number;

  @Column({ name: 'completed_on', nullable: true })
  completedOn: Date;

  @Column({ name: 'enabled', default: true })
  enabled: boolean;

  @Column({ name: 'created_by_user_id', default: 0 })
  createdByUserId: number;

  @CreateDateColumn({
    name: 'created_date',
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
  })
  createdDate: Date;

  @Column({ name: 'updated_by_user_id', default: 0 })
  updatedByUserId: number;

  @UpdateDateColumn({
    name: 'updated_date',
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
  })
  updatedDate: Date;

  @ManyToOne(() => Event)
  @JoinColumn({ name: 'event_id', referencedColumnName: 'id' })
  event: Event;

  @ManyToOne(() => EventTeam)
  @JoinColumn({ name: 'event_team_id', referencedColumnName: 'id' })
  eventTeam: EventTeam;

  @ManyToOne(() => EventChecklistItem)
  @JoinColumn({ name: 'event_checklist_item_id', referencedColumnName: 'id' })
  eventChecklistItem: EventChecklistItem;

  @AfterInsert()
  logInsert() {
    Logger.log(`Inserted Event Team Event Checklist Item with id ${this.id}`);
  }

  @AfterUpdate()
  logUpdate() {
    Logger.log(`Updated Event Team Event Checklist Item with id ${this.id}`);
  }

  @AfterRemove()
  logRemove() {
    Logger.log('Removed Event Team Event Checklist Item');
  }
}
