import { faker } from '@faker-js/faker';
import { Test, TestingModule } from '@nestjs/testing';
import { KeycloakModule } from '../keycloak/keycloak.module';
import { createFakeEventTeamEventChecklistItem } from '../utils/test-helpers/fakes/event-team-event-checklist-item-faker';
import { createFakeUser } from '../utils/test-helpers/fakes/user-faker';
import { CreateEventTeamEventChecklistItemDto } from './dto/create-event-team-event-checklist-item.dto';
import { GetEventTeamEventChecklistItemRequestParamsDto } from './dto/get-event-team-event-checklist-item-request-params.dto';
import { PostEventTeamEventChecklistItemBodyDto } from './dto/post-event-team-event-checklist-item-body.dto';
import { PostEventTeamEventChecklistItemParamsDto } from './dto/post-event-team-event-checklist-item-params.dto';
import { UpdateEventTeamEventChecklistItemDto } from './dto/update-event-team-event-checklist-item.dto';
import { EventTeamEventChecklistItemsController } from './event-team-event-checklist-items.controller';
import { EventTeamEventChecklistItemsService } from './event-team-event-checklist-items.service';

const currentUserId = faker.datatype.number();

const fakeEventTeamEventChecklistItem = createFakeEventTeamEventChecklistItem({
  userId: currentUserId,
});
const fakeUser = createFakeUser(currentUserId);

const postEventTeamEventChecklistItemParamsDto: PostEventTeamEventChecklistItemParamsDto = {
  eventTeamId: fakeEventTeamEventChecklistItem.eventTeamId,
  eventChecklistItemId: fakeEventTeamEventChecklistItem.eventChecklistItemId,
};
const postEventTeamEventChecklistItemBodyDto: PostEventTeamEventChecklistItemBodyDto = {
  eventId: fakeEventTeamEventChecklistItem.eventId,
};
const getEventTeamEventChecklistItemRequestParamsDto: GetEventTeamEventChecklistItemRequestParamsDto =
  new GetEventTeamEventChecklistItemRequestParamsDto();

describe('EventTeamEventChecklistItemsController', () => {
  let controller: EventTeamEventChecklistItemsController;
  let service: EventTeamEventChecklistItemsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [KeycloakModule],
      controllers: [EventTeamEventChecklistItemsController],
      providers: [
        {
          provide: EventTeamEventChecklistItemsService,
          useValue: {
            create: jest
              .fn()
              .mockImplementation(
                (eventTeamEventChecklistItem: CreateEventTeamEventChecklistItemDto, currentUserId: number) =>
                  Promise.resolve({
                    ...fakeEventTeamEventChecklistItem,
                    ...eventTeamEventChecklistItem,
                  }),
              ),
            findAll: jest.fn().mockResolvedValue([fakeEventTeamEventChecklistItem]),
            findOne: jest
              .fn()
              .mockImplementation((id: number) => Promise.resolve({ ...fakeEventTeamEventChecklistItem, id })),
            update: jest
              .fn()
              .mockImplementation(
                (
                  id: number,
                  eventTeamEventChecklistItem: UpdateEventTeamEventChecklistItemDto,
                  currentUserId: number,
                ) => Promise.resolve({ ...fakeEventTeamEventChecklistItem, id }),
              ),
            completeItem: jest
              .fn()
              .mockImplementation(
                (
                  id: number,
                  eventTeamEventChecklistItem: UpdateEventTeamEventChecklistItemDto,
                  currentUserId: number,
                ) => Promise.resolve({ ...fakeEventTeamEventChecklistItem, id }),
              ),
            incompleteItem: jest
              .fn()
              .mockImplementation(
                (
                  id: number,
                  eventTeamEventChecklistItem: UpdateEventTeamEventChecklistItemDto,
                  currentUserId: number,
                ) => Promise.resolve({ ...fakeEventTeamEventChecklistItem, id }),
              ),
            remove: jest.fn().mockResolvedValue(fakeEventTeamEventChecklistItem),
          },
        },
      ],
    }).compile();

    controller = module.get<EventTeamEventChecklistItemsController>(EventTeamEventChecklistItemsController);
    service = module.get<EventTeamEventChecklistItemsService>(EventTeamEventChecklistItemsService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should create event team event checklist item', async () => {
    const createEventTeamEventChecklistItemDto: CreateEventTeamEventChecklistItemDto = {
      eventId: fakeEventTeamEventChecklistItem.eventId,
      eventTeamId: fakeEventTeamEventChecklistItem.eventTeamId,
      eventChecklistItemId: fakeEventTeamEventChecklistItem.eventChecklistItemId,
    };
    const eventTeamEventChecklistItem = await controller.create(
      postEventTeamEventChecklistItemParamsDto,
      postEventTeamEventChecklistItemBodyDto,
      fakeUser,
    );

    expect(eventTeamEventChecklistItem).toEqual({
      ...fakeEventTeamEventChecklistItem,
    });
    expect(service.create).toBeCalledTimes(1);
    expect(service.create).toBeCalledWith(createEventTeamEventChecklistItemDto, fakeUser.id);
  });

  it('should find all event team event checklist items', async () => {
    const eventTeamEventChecklistItems = await controller.findAllByEventTeam(
      getEventTeamEventChecklistItemRequestParamsDto,
    );

    expect(eventTeamEventChecklistItems).toEqual([fakeEventTeamEventChecklistItem]);
    expect(service.findAll).toBeCalledTimes(1);
  });

  it('should find one event team event checklist item by id', async () => {
    const eventTeamEventChecklistItem = await controller.findOne(`${fakeEventTeamEventChecklistItem.id}`);

    expect(eventTeamEventChecklistItem).toEqual(fakeEventTeamEventChecklistItem);
    expect(service.findOne).toBeCalledTimes(1);
    expect(service.findOne).toBeCalledWith(fakeEventTeamEventChecklistItem.id);
  });

  it('should update event team event checklist item', async () => {
    const updateEventTeamEventChecklistItemDto = {
      completedOn: faker.datatype.datetime(),
    };
    const eventTeamEventChecklistItem = await controller.update(
      `${fakeEventTeamEventChecklistItem.id}`,
      updateEventTeamEventChecklistItemDto,
      fakeUser,
    );

    expect(eventTeamEventChecklistItem).toEqual(fakeEventTeamEventChecklistItem);
    expect(service.update).toBeCalledTimes(1);
    expect(service.update).toBeCalledWith(
      fakeEventTeamEventChecklistItem.id,
      updateEventTeamEventChecklistItemDto,
      fakeUser.id,
    );
  });

  it('should update event team event checklist item to completed', async () => {
    const updateEventTeamEventChecklistItemDto = {
      completedOn: new Date(),
    };
    const eventTeamEventChecklistItem = await controller.completeItem(
      `${fakeEventTeamEventChecklistItem.id}`,
      fakeUser,
    );

    expect(eventTeamEventChecklistItem).toEqual(fakeEventTeamEventChecklistItem);
    expect(service.update).toBeCalledTimes(1);
    expect(service.update).toBeCalledWith(
      fakeEventTeamEventChecklistItem.id,
      updateEventTeamEventChecklistItemDto,
      fakeUser.id,
    );
  });

  it('should update event team event checklist item to incomplete', async () => {
    const updateEventTeamEventChecklistItemDto = {
      completedOn: null,
    };
    const eventTeamEventChecklistItem = await controller.incompleteItem(
      `${fakeEventTeamEventChecklistItem.id}`,
      fakeUser,
    );

    expect(eventTeamEventChecklistItem).toEqual(fakeEventTeamEventChecklistItem);
    expect(service.update).toBeCalledTimes(1);
    expect(service.update).toBeCalledWith(
      fakeEventTeamEventChecklistItem.id,
      updateEventTeamEventChecklistItemDto,
      fakeUser.id,
    );
  });

  it('should remove event team event checklist item', async () => {
    const eventTeamEventChecklistItem = await controller.remove(`${fakeEventTeamEventChecklistItem.id}`);

    expect(eventTeamEventChecklistItem).toEqual(fakeEventTeamEventChecklistItem);
    expect(service.remove).toBeCalledTimes(1);
    expect(service.remove).toBeCalledWith(fakeEventTeamEventChecklistItem.id);
  });
});
