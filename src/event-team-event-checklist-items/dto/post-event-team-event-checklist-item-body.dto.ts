import { ApiProperty } from '@nestjs/swagger';
import { IsDate, IsInt, IsOptional } from 'class-validator';
import { EventExists } from '../../events/decorators/event-exists.decorator';

export class PostEventTeamEventChecklistItemBodyDto {
  @ApiProperty({
    type: 'number',
    required: true,
    example: '123',
  })
  @IsInt()
  @EventExists()
  @IsOptional()
  eventId?: number;

  @ApiProperty({
    type: 'date',
    required: false,
    example: '2022-08-01T20:29:56.561Z',
  })
  @IsDate()
  @IsOptional()
  completedOn?: Date;
}
