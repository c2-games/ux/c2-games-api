import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsOptional } from 'class-validator';
import { EventTeamExists } from '../../event-teams/decorators/event-team-exists.decorator';

export class GetEventTeamEventChecklistItemsByEventTeamDto {
  @ApiProperty({
    type: 'number',
    required: true,
    example: '123',
  })
  @IsInt()
  @EventTeamExists()
  @IsOptional()
  eventTeamId?: number;
}
