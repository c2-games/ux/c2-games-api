import { ApiProperty } from '@nestjs/swagger';
import { IsDate, IsInt, IsOptional } from 'class-validator';
import { EventChecklistItemExists } from '../../event-checklist-items/decorators/event-checklist-item-exists.decorator';
import { EventTeamExists } from '../../event-teams/decorators/event-team-exists.decorator';
import { EventExists } from '../../events/decorators/event-exists.decorator';

export class GetEventTeamEventChecklistItemRequestParamsDto {
  @ApiProperty({
    type: 'number',
    required: false,
    example: '123',
  })
  @IsInt()
  @EventTeamExists()
  @IsOptional()
  eventTeamId?: number;

  @ApiProperty({
    type: 'number',
    required: false,
    example: '123',
  })
  @IsInt()
  @EventExists()
  @IsOptional()
  eventId?: number;

  @ApiProperty({
    type: 'number',
    required: false,
    example: '123',
  })
  @IsInt()
  @EventChecklistItemExists()
  @IsOptional()
  eventChecklistItemId?: number;

  @ApiProperty({
    type: 'date',
    required: false,
    example: '2022-08-01T20:29:56.561Z',
  })
  @IsDate()
  @IsOptional()
  completedOn?: Date;
}
