import { Body, Controller, Delete, Get, Param, Patch, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiCreatedResponse, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { UserRoles } from 'src/auth/decorators/roles.decorator';
import { UserRole } from 'src/auth/decorators/user-role.enum';
import { UserRolesGuard } from 'src/auth/guards/user-roles.guard';
import { CurrentUser } from '../users/decorators/current-user.decorator';
import { User } from '../users/entities/user.entity';
import { GetEventTeamEventChecklistItemRequestParamsDto } from './dto/get-event-team-event-checklist-item-request-params.dto';
import { PostEventTeamEventChecklistItemBodyDto } from './dto/post-event-team-event-checklist-item-body.dto';
import { PostEventTeamEventChecklistItemParamsDto } from './dto/post-event-team-event-checklist-item-params.dto';
import { UpdateEventTeamEventChecklistItemDto } from './dto/update-event-team-event-checklist-item.dto';
import { EventTeamEventChecklistItemsService } from './event-team-event-checklist-items.service';

@ApiTags('Event Team Event Checklist Items')
@ApiBearerAuth()
@Controller()
@UseGuards(UserRolesGuard)
export class EventTeamEventChecklistItemsController {
  constructor(private readonly eventTeamEventChecklistItemsService: EventTeamEventChecklistItemsService) {}

  @Post('event-teams/:eventTeamId/event-checklist-items/:eventChecklistItemId')
  @ApiOperation({ summary: 'Create event team event checklist item' })
  @ApiCreatedResponse({
    description: 'Event team event checklist item created',
  })
  @UserRoles(UserRole.EventAdmin)
  create(
    @Param() params: PostEventTeamEventChecklistItemParamsDto,
    @Body() body: PostEventTeamEventChecklistItemBodyDto,
    @CurrentUser() currentUser: User,
  ) {
    const createEventTeamEventChecklistItemDto = { ...params, ...body };
    return this.eventTeamEventChecklistItemsService.create(createEventTeamEventChecklistItemDto, currentUser.id);
  }

  @Get('event-teams/:eventTeamId/event-checklist-items')
  @ApiOperation({
    summary: 'Find all event team event checklist items by event team',
  })
  @ApiOkResponse({ description: 'Event team event checklist items found' })
  findAllByEventTeam(@Param() params?: GetEventTeamEventChecklistItemRequestParamsDto) {
    return this.eventTeamEventChecklistItemsService.findAll(params);
  }

  @Get('event-team-event-checklist-items/:id')
  @ApiOperation({ summary: 'Find event team event checklist item by ID' })
  @ApiOkResponse({ description: 'Event team event checklist item found' })
  findOne(@Param('id') id: string) {
    return this.eventTeamEventChecklistItemsService.findOne(+id);
  }

  @Patch('event-team-event-checklist-items/:id')
  @ApiOperation({ summary: 'Update event team event checklist item' })
  @ApiOkResponse({ description: 'Event team event checklist item updated' })
  @UserRoles(UserRole.EventAdmin)
  update(
    @Param('id') id: string,
    @Body()
    updateEventTeamEventChecklistItemDto: UpdateEventTeamEventChecklistItemDto,
    @CurrentUser() currentUser: User,
  ) {
    return this.eventTeamEventChecklistItemsService.update(+id, updateEventTeamEventChecklistItemDto, currentUser.id);
  }

  @Patch('event-team-event-checklist-items/:id/mark-complete')
  @ApiOperation({
    summary: 'Update event team event checklist item to completed',
  })
  @ApiOkResponse({
    description: 'Event team event checklist item updated to completed',
  })
  @UserRoles(UserRole.EventAdmin)
  completeItem(@Param('id') id: string, @CurrentUser() currentUser: User) {
    const updateEventTeamEventChecklistItemDto = { completedOn: new Date() };
    return this.eventTeamEventChecklistItemsService.update(+id, updateEventTeamEventChecklistItemDto, currentUser.id);
  }

  @Patch('event-team-event-checklist-items/:id/mark-incomplete')
  @ApiOperation({
    summary: 'Update event team event checklist item to incomplete',
  })
  @ApiOkResponse({
    description: 'Event team event checklist item updated to incomplete',
  })
  @UserRoles(UserRole.EventAdmin)
  incompleteItem(@Param('id') id: string, @CurrentUser() currentUser: User) {
    const updateEventTeamEventChecklistItemDto = { completedOn: null };
    return this.eventTeamEventChecklistItemsService.update(+id, updateEventTeamEventChecklistItemDto, currentUser.id);
  }

  @Delete('event-team-event-checklist-items/:id')
  @ApiOperation({ summary: 'Remove event team event checklist item' })
  @ApiOkResponse({ description: 'Event team event checklist item removed' })
  @UserRoles(UserRole.EventAdmin)
  remove(@Param('id') id: string) {
    return this.eventTeamEventChecklistItemsService.remove(+id);
  }
}
