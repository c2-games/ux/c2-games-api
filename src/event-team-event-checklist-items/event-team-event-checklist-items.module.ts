import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EventTeamsModule } from '../event-teams/event-teams.module';
import { KeycloakModule } from '../keycloak/keycloak.module';
import { EventTeamEventChecklistItem } from './entities/event-team-event-checklist-item.entity';
import { EventTeamEventChecklistItemsController } from './event-team-event-checklist-items.controller';
import { EventTeamEventChecklistItemsService } from './event-team-event-checklist-items.service';

@Module({
  imports: [TypeOrmModule.forFeature([EventTeamEventChecklistItem]), EventTeamsModule, KeycloakModule],
  controllers: [EventTeamEventChecklistItemsController],
  providers: [EventTeamEventChecklistItemsService],
  exports: [EventTeamEventChecklistItemsService],
})
export class EventTeamEventChecklistItemsModule {}
