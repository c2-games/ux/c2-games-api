import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserUserChecklistItemDto } from './dto/create-user-user-checklist-item.dto';
import { UpdateUserUserChecklistItemDto } from './dto/update-user-user-checklist-item.dto';
import { UserUserChecklistItemsQueryDto } from './dto/user-user-checklist-items-query.dto';
import { UserUserChecklistItem } from './entities/user-user-checklist-item.entity';

@Injectable()
export class UserUserChecklistItemsService {
  constructor(
    @InjectRepository(UserUserChecklistItem)
    private repo: Repository<UserUserChecklistItem>,
  ) {}

  async create(createUserUserChecklistItemDto: CreateUserUserChecklistItemDto, currentUserId = 0) {
    const userUserChecklistItem = this.repo.create({
      ...createUserUserChecklistItemDto,
      createdByUserId: currentUserId,
      updatedByUserId: currentUserId,
    });
    await this.repo.save(userUserChecklistItem);
    return userUserChecklistItem;
  }

  findAll(params?: UserUserChecklistItemsQueryDto) {
    let query = this.repo.createQueryBuilder('users_x_user_checklist_items').where('1=1');
    query = params.userId
      ? query.andWhere('users_x_user_checklist_items.userId = :userId', {
          userId: params.userId,
        })
      : query;
    query = params.userChecklistItemId
      ? query.andWhere('users_x_user_checklist_items.userChecklistItemId = :userChecklistItemId', {
          userChecklistItemId: params.userChecklistItemId,
        })
      : query;
    query = params.completedOn
      ? query.andWhere('users_x_user_checklist_items.completedOn = :completedOn', {
          completedOn: params.completedOn,
        })
      : query;
    return query.getMany();
  }

  async findOne(id: number) {
    if (!id) {
      throw new BadRequestException();
    }
    const userUserChecklistItem = await this.repo.findOne(id);
    if (!userUserChecklistItem) {
      throw new NotFoundException('User User Checklist Item not found');
    }
    return userUserChecklistItem;
  }

  async update(id: number, updateUserUserChecklistItemDto: UpdateUserUserChecklistItemDto, currentUserId = 0) {
    const userUserChecklistItem = await this.findOne(id);
    Object.assign(updateUserUserChecklistItemDto, {
      updatedByUserId: currentUserId,
    });
    Object.assign(userUserChecklistItem, updateUserUserChecklistItemDto);
    await this.repo.save(userUserChecklistItem);
    return userUserChecklistItem;
  }

  async remove(id: number) {
    const userUserChecklistItem = await this.findOne(id);
    await this.repo.remove(userUserChecklistItem);
    return userUserChecklistItem;
  }
}
