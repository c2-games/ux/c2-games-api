import { faker } from '@faker-js/faker';
import { BadRequestException, NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { createFakeUser } from '../utils/test-helpers/fakes/user-faker';
import { createFakeUserUserChecklistItem } from '../utils/test-helpers/fakes/user-user-checklist-item-faker';
import { UserUserChecklistItemsQueryDto } from './dto/user-user-checklist-items-query.dto';
import { UserUserChecklistItem } from './entities/user-user-checklist-item.entity';
import { UserUserChecklistItemsService } from './user-user-checklist-items.service';

const currentUserId = faker.datatype.number();

const testUser = createFakeUser(currentUserId);

const testUserUserChecklistItem = createFakeUserUserChecklistItem({
  userId: testUser.id,
});

const userUserChecklistItemQueryDto = new UserUserChecklistItemsQueryDto();

describe('UserUserChecklistItemsService', () => {
  let service: UserUserChecklistItemsService;
  let repo: Repository<UserUserChecklistItem>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UserUserChecklistItemsService,
        {
          provide: getRepositoryToken(UserUserChecklistItem),
          useValue: {
            create: jest.fn().mockReturnValue(testUserUserChecklistItem),
            save: jest.fn(),
            findAll: jest.fn().mockResolvedValue([testUserUserChecklistItem]),
            createQueryBuilder: jest.fn(() => ({
              where: jest.fn().mockReturnThis(),
              andWhere: jest.fn().mockReturnThis(),
              getMany: jest.fn().mockResolvedValue([testUserUserChecklistItem]),
            })),
            findOne: jest.fn().mockResolvedValue(testUserUserChecklistItem),
            update: jest.fn().mockResolvedValue(true),
            remove: jest.fn().mockResolvedValue(true),
          },
        },
      ],
    }).compile();

    service = module.get<UserUserChecklistItemsService>(UserUserChecklistItemsService);
    repo = module.get<Repository<UserUserChecklistItem>>(getRepositoryToken(UserUserChecklistItem));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should create a new user user checklist item when provided with valid createUserUserChecklistItemDto', async () => {
    const createUserUserChecklistItemDto = {
      userId: testUserUserChecklistItem.userId,
      userChecklistItemId: testUserUserChecklistItem.userChecklistItemId,
    };

    const userUserChecklistItem = await service.create(createUserUserChecklistItemDto, currentUserId);

    expect(userUserChecklistItem).toEqual(testUserUserChecklistItem);
    expect(repo.create).toBeCalledTimes(1);
    expect(repo.create).toBeCalledWith({
      ...createUserUserChecklistItemDto,
      createdByUserId: currentUserId,
      updatedByUserId: currentUserId,
    });
    expect(repo.save).toBeCalledTimes(1);
  });

  it('should find all user user checklist items', async () => {
    const userUserChecklistItems = await service.findAll(userUserChecklistItemQueryDto);

    expect(userUserChecklistItems).toEqual([testUserUserChecklistItem]);
  });

  it('should find one user user checklist item', async () => {
    const userUserChecklistItem = await service.findOne(faker.datatype.number());

    expect(userUserChecklistItem).toEqual(testUserUserChecklistItem);
  });

  it('should throw bad request exception on findOne if id not provided', async () => {
    await expect(() => service.findOne(null)).rejects.toThrow(BadRequestException);
  });

  it('should throw not found exception on findOne if user user checklist item not found', async () => {
    const userUserChecklistItemId = faker.datatype.number();
    const repoSpy = jest.spyOn(repo, 'findOne').mockReturnValue(undefined);

    await expect(() => service.findOne(userUserChecklistItemId)).rejects.toThrow(
      new NotFoundException('User User Checklist Item not found'),
    );
    expect(repoSpy).toBeCalledTimes(1);
    expect(repoSpy).toBeCalledWith(userUserChecklistItemId);
  });

  it('should update user user checklist item', async () => {
    const userUserChecklistItemId = faker.datatype.number();
    const updateUserUserChecklistItemDto = {
      completedOn: faker.datatype.datetime(),
    };

    const userUserChecklistItem = await service.update(
      userUserChecklistItemId,
      updateUserUserChecklistItemDto,
      currentUserId,
    );

    expect(userUserChecklistItem).toEqual({
      ...testUserUserChecklistItem,
      ...updateUserUserChecklistItemDto,
    });
    expect(repo.findOne).toBeCalledTimes(1);
    expect(repo.findOne).toBeCalledWith(userUserChecklistItemId);
    expect(repo.save).toBeCalledTimes(1);
  });

  it('should remove user user checklist item', async () => {
    const userUserChecklistItemId = faker.datatype.number();

    const userUserChecklistItem = await service.remove(userUserChecklistItemId);

    expect(userUserChecklistItem).toEqual(testUserUserChecklistItem);
  });
});
