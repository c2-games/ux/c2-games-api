import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { KeycloakModule } from '../keycloak/keycloak.module';
import { UserUserChecklistItem } from './entities/user-user-checklist-item.entity';
import { UserUserChecklistItemsController } from './user-user-checklist-items.controller';
import { UserUserChecklistItemsService } from './user-user-checklist-items.service';

@Module({
  imports: [TypeOrmModule.forFeature([UserUserChecklistItem]), KeycloakModule],
  controllers: [UserUserChecklistItemsController],
  providers: [UserUserChecklistItemsService],
})
export class UserUserChecklistItemsModule {}
