import { faker } from '@faker-js/faker';
import { Test, TestingModule } from '@nestjs/testing';
import { KeycloakModule } from '../keycloak/keycloak.module';
import { createFakeUser } from '../utils/test-helpers/fakes/user-faker';
import { createFakeUserUserChecklistItem } from '../utils/test-helpers/fakes/user-user-checklist-item-faker';
import { CreateUserUserChecklistItemDto } from './dto/create-user-user-checklist-item.dto';
import { GetUserUserChecklistItemsByUserRequestParamsDto } from './dto/get-user-user-checklist-items-by-user-request-params.dto';
import { PostUserUserChecklistItemBodyDto } from './dto/post-user-user-checklist-item-body.dto';
import { PostUserUserChecklistItemParamsDto } from './dto/post-user-user-checklist-item-params.dto';
import { UpdateUserUserChecklistItemDto } from './dto/update-user-user-checklist-item.dto';
import { UserUserChecklistItemsController } from './user-user-checklist-items.controller';
import { UserUserChecklistItemsService } from './user-user-checklist-items.service';

const currentUserId = faker.datatype.number();

const fakeUserUserChecklistItem = createFakeUserUserChecklistItem({
  userId: currentUserId,
});
const fakeUser = createFakeUser(currentUserId);

const postUserUserChecklistItemParamsDto: PostUserUserChecklistItemParamsDto = {
  userId: fakeUserUserChecklistItem.userId,
  userChecklistItemId: fakeUserUserChecklistItem.userChecklistItemId,
};
const postUserUserChecklistItemBodyDto: PostUserUserChecklistItemBodyDto = {
  completedOn: fakeUserUserChecklistItem.completedOn,
};
const getUserUserChecklistItemsByUserRequestParamsDto: GetUserUserChecklistItemsByUserRequestParamsDto = {
  userId: fakeUserUserChecklistItem.userId,
};

describe('UserUserChecklistItemsController', () => {
  let controller: UserUserChecklistItemsController;
  let service: UserUserChecklistItemsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [KeycloakModule],
      controllers: [UserUserChecklistItemsController],
      providers: [
        {
          provide: UserUserChecklistItemsService,
          useValue: {
            create: jest
              .fn()
              .mockImplementation((userUserChecklistItem: CreateUserUserChecklistItemDto, currentUserId: number) =>
                Promise.resolve({
                  ...fakeUserUserChecklistItem,
                  ...userUserChecklistItem,
                }),
              ),
            findAll: jest.fn().mockResolvedValue([fakeUserUserChecklistItem]),
            findOne: jest
              .fn()
              .mockImplementation((id: number) => Promise.resolve({ ...fakeUserUserChecklistItem, id })),
            update: jest
              .fn()
              .mockImplementation(
                (id: number, userUserChecklistItem: UpdateUserUserChecklistItemDto, currentUserId: number) =>
                  Promise.resolve({ ...fakeUserUserChecklistItem, id }),
              ),
            completeItem: jest
              .fn()
              .mockImplementation(
                (id: number, userUserChecklistItem: UpdateUserUserChecklistItemDto, currentUserId: number) =>
                  Promise.resolve({ ...fakeUserUserChecklistItem, id }),
              ),
            incompleteItem: jest
              .fn()
              .mockImplementation(
                (id: number, userUserChecklistItem: UpdateUserUserChecklistItemDto, currentUserId: number) =>
                  Promise.resolve({ ...fakeUserUserChecklistItem, id }),
              ),
            remove: jest.fn().mockResolvedValue(fakeUserUserChecklistItem),
          },
        },
      ],
    }).compile();

    controller = module.get<UserUserChecklistItemsController>(UserUserChecklistItemsController);
    service = module.get<UserUserChecklistItemsService>(UserUserChecklistItemsService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should create user user checklist item', async () => {
    const createUserUserChecklistItemDto: CreateUserUserChecklistItemDto = {
      userId: fakeUserUserChecklistItem.userId,
      userChecklistItemId: fakeUserUserChecklistItem.userChecklistItemId,
      completedOn: fakeUserUserChecklistItem.completedOn,
    };
    const userUserChecklistItem = await controller.create(
      postUserUserChecklistItemParamsDto,
      postUserUserChecklistItemBodyDto,
      fakeUser,
    );

    expect(userUserChecklistItem).toEqual({
      ...fakeUserUserChecklistItem,
    });
    expect(service.create).toBeCalledTimes(1);
    expect(service.create).toBeCalledWith(createUserUserChecklistItemDto, fakeUser.id);
  });

  it('should find all user user checklist items', async () => {
    const userUserChecklistItems = await controller.findAllByUser(getUserUserChecklistItemsByUserRequestParamsDto);

    expect(userUserChecklistItems).toEqual([fakeUserUserChecklistItem]);
    expect(service.findAll).toBeCalledTimes(1);
  });

  it('should find one user user checklist item by id', async () => {
    const userUserChecklistItem = await controller.findOne(`${fakeUserUserChecklistItem.id}`);

    expect(userUserChecklistItem).toEqual(fakeUserUserChecklistItem);
    expect(service.findOne).toBeCalledTimes(1);
    expect(service.findOne).toBeCalledWith(fakeUserUserChecklistItem.id);
  });

  it('should update user user checklist item', async () => {
    const updateUserUserChecklistItemDto = {
      completedOn: fakeUserUserChecklistItem.completedOn,
    };
    const userUserChecklistItem = await controller.update(
      `${fakeUserUserChecklistItem.id}`,
      updateUserUserChecklistItemDto,
      fakeUser,
    );

    expect(userUserChecklistItem).toEqual(fakeUserUserChecklistItem);
    expect(service.update).toBeCalledTimes(1);
    expect(service.update).toBeCalledWith(fakeUserUserChecklistItem.id, updateUserUserChecklistItemDto, fakeUser.id);
  });

  it('should update user user checklist item to completed', async () => {
    const updateUserUserChecklistItemDto = {
      completedOn: new Date(),
    };
    const userUserChecklistItem = await controller.completeItem(`${fakeUserUserChecklistItem.id}`, fakeUser);

    expect(userUserChecklistItem).toEqual(fakeUserUserChecklistItem);
    expect(service.update).toBeCalledTimes(1);
    expect(service.update).toBeCalledWith(fakeUserUserChecklistItem.id, updateUserUserChecklistItemDto, fakeUser.id);
  });

  it('should update user user checklist item to incomplete', async () => {
    const updateUserUserChecklistItemDto = {
      completedOn: null,
    };
    const userUserChecklistItem = await controller.incompleteItem(`${fakeUserUserChecklistItem.id}`, fakeUser);

    expect(userUserChecklistItem).toEqual(fakeUserUserChecklistItem);
    expect(service.update).toBeCalledTimes(1);
    expect(service.update).toBeCalledWith(fakeUserUserChecklistItem.id, updateUserUserChecklistItemDto, fakeUser.id);
  });

  it('should remove user user checklist item', async () => {
    const userUserChecklistItem = await controller.remove(`${fakeUserUserChecklistItem.id}`);

    expect(userUserChecklistItem).toEqual(fakeUserUserChecklistItem);
    expect(service.remove).toBeCalledTimes(1);
    expect(service.remove).toBeCalledWith(fakeUserUserChecklistItem.id);
  });
});
