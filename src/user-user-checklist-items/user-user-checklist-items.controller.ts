import { Body, Controller, Delete, Get, Param, Patch, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiCreatedResponse, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { UserRoles } from 'src/auth/decorators/roles.decorator';
import { UserRole } from 'src/auth/decorators/user-role.enum';
import { UserRolesGuard } from 'src/auth/guards/user-roles.guard';
import { CurrentUser } from '../users/decorators/current-user.decorator';
import { User } from '../users/entities/user.entity';
import { GetUserUserChecklistItemsByUserRequestParamsDto } from './dto/get-user-user-checklist-items-by-user-request-params.dto';
import { PostUserUserChecklistItemBodyDto } from './dto/post-user-user-checklist-item-body.dto';
import { PostUserUserChecklistItemParamsDto } from './dto/post-user-user-checklist-item-params.dto';
import { UpdateUserUserChecklistItemDto } from './dto/update-user-user-checklist-item.dto';
import { UserUserChecklistItemsService } from './user-user-checklist-items.service';

@ApiTags('User User Checklist Items')
@ApiBearerAuth()
@Controller()
@UseGuards(UserRolesGuard)
export class UserUserChecklistItemsController {
  constructor(private readonly userUserChecklistItemsService: UserUserChecklistItemsService) {}

  @Post('users/:userId/user-checklist-items/:userChecklistItemId')
  @ApiOperation({ summary: 'Create user checklist item for user' })
  @ApiCreatedResponse({
    description: 'User user checklist item created',
  })
  @UserRoles(UserRole.AwardAdmin)
  create(
    @Param() params: PostUserUserChecklistItemParamsDto,
    @Body() body: PostUserUserChecklistItemBodyDto,
    @CurrentUser() currentUser: User,
  ) {
    const createUserUserChecklistItemDto = { ...params, ...body };
    return this.userUserChecklistItemsService.create(createUserUserChecklistItemDto, currentUser.id);
  }

  @Get('users/:userId/user-checklist-items')
  @ApiOperation({ summary: 'Find all user checklist item for users by user' })
  @ApiOkResponse({ description: 'User checklist items found for users' })
  findAllByUser(@Param() params?: GetUserUserChecklistItemsByUserRequestParamsDto) {
    return this.userUserChecklistItemsService.findAll(params);
  }

  @Get('user-user-checklist-items/:id')
  @ApiOperation({ summary: 'Find user checklist item for user by ID' })
  @ApiOkResponse({ description: 'User checklist item for user found' })
  findOne(@Param('id') id: string) {
    return this.userUserChecklistItemsService.findOne(+id);
  }

  @Patch('user-user-checklist-items/:id')
  @ApiOperation({ summary: 'Update user checklist item for user' })
  @ApiOkResponse({ description: 'User checklist item for user updated' })
  @UserRoles(UserRole.AwardAdmin)
  update(
    @Param('id') id: string,
    @Body()
    updateUserUserChecklistItemDto: UpdateUserUserChecklistItemDto,
    @CurrentUser() currentUser: User,
  ) {
    return this.userUserChecklistItemsService.update(+id, updateUserUserChecklistItemDto, currentUser.id);
  }

  @Patch('user-user-checklist-items/:id/mark-complete')
  @ApiOperation({
    summary: 'Update user checklist item for user to completed',
  })
  @ApiOkResponse({
    description: 'User checklist item for user updated to completed',
  })
  @UserRoles(UserRole.AwardAdmin)
  completeItem(@Param('id') id: string, @CurrentUser() currentUser: User) {
    const updateUserUserChecklistItemDto = { completedOn: new Date() };
    return this.userUserChecklistItemsService.update(+id, updateUserUserChecklistItemDto, currentUser.id);
  }

  @Patch('user-user-checklist-items/:id/mark-incomplete')
  @ApiOperation({
    summary: 'Update user checklist item for user to incomplete',
  })
  @ApiOkResponse({
    description: 'User checklist item for user updated to incomplete',
  })
  @UserRoles(UserRole.AwardAdmin)
  incompleteItem(@Param('id') id: string, @CurrentUser() currentUser: User) {
    const updateUserUserChecklistItemDto = { completedOn: null };
    return this.userUserChecklistItemsService.update(+id, updateUserUserChecklistItemDto, currentUser.id);
  }

  @Delete('/user-user-checklist-items/:id')
  @ApiOperation({ summary: 'Remove user checklist item for user' })
  @ApiOkResponse({ description: 'User checklist item for user removed' })
  @UserRoles(UserRole.AwardAdmin)
  remove(@Param('id') id: string) {
    return this.userUserChecklistItemsService.remove(+id);
  }
}
