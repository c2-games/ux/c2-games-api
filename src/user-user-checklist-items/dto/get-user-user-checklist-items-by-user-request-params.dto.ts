import { ApiProperty } from '@nestjs/swagger';
import { IsInt } from 'class-validator';
import { UserExists } from '../../users/decorators/user-exists.decorator';

export class GetUserUserChecklistItemsByUserRequestParamsDto {
  @ApiProperty({
    type: 'number',
    required: false,
    example: '123',
  })
  @IsInt()
  @UserExists()
  userId: number;
}
