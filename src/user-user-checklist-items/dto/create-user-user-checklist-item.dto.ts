import { ApiProperty } from '@nestjs/swagger';
import { IsDate, IsInt, IsOptional } from 'class-validator';
import { UserChecklistItemExists } from '../../user-checklist-items/decorators/user-checklist-item-exists.decorator';
import { UserExists } from '../../users/decorators/user-exists.decorator';

export class CreateUserUserChecklistItemDto {
  @ApiProperty({
    type: 'number',
    required: true,
    example: '123',
  })
  @IsInt()
  @UserExists()
  userId: number;

  @ApiProperty({
    type: 'number',
    required: true,
    example: '123',
  })
  @IsInt()
  @UserChecklistItemExists()
  userChecklistItemId: number;

  @ApiProperty({
    type: 'date',
    required: false,
    example: '2022-08-01T20:29:56.561Z',
  })
  @IsDate()
  @IsOptional()
  completedOn?: Date;
}
