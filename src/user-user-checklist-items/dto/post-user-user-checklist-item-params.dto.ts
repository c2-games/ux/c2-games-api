import { ApiProperty } from '@nestjs/swagger';
import { IsInt } from 'class-validator';
import { UserChecklistItemExists } from '../../user-checklist-items/decorators/user-checklist-item-exists.decorator';
import { UserExists } from '../../users/decorators/user-exists.decorator';

export class PostUserUserChecklistItemParamsDto {
  @ApiProperty({
    type: 'number',
    required: true,
    example: '123',
  })
  @IsInt()
  @UserExists()
  userId: number;

  @ApiProperty({
    type: 'number',
    required: true,
    example: '123',
  })
  @IsInt()
  @UserChecklistItemExists()
  userChecklistItemId: number;
}
