import { ApiProperty } from '@nestjs/swagger';
import { IsDate, IsInt, IsOptional } from 'class-validator';

export class UserUserChecklistItemsQueryDto {
  @ApiProperty({
    type: 'number',
    required: false,
    example: '123',
  })
  @IsInt()
  @IsOptional()
  userId?: number;

  @ApiProperty({
    type: 'number',
    required: false,
    example: '123',
  })
  @IsInt()
  @IsOptional()
  userChecklistItemId?: number;

  @ApiProperty({
    type: 'date',
    required: false,
    example: '2022-08-01T20:29:56.561Z',
  })
  @IsDate()
  @IsOptional()
  completedOn?: Date;
}
