import { Logger } from '@nestjs/common';
import {
  AfterInsert,
  AfterRemove,
  AfterUpdate,
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  Unique,
  UpdateDateColumn,
} from 'typeorm';
import { UserChecklistItem } from '../../user-checklist-items/entities/user-checklist-item.entity';
import { User } from '../../users/entities/user.entity';

@Entity({ name: 'users_x_user_checklist_items' })
@Unique(['userId', 'userChecklistItemId'])
export class UserUserChecklistItem {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'user_id' })
  userId: number;

  @Column({ name: 'user_checklist_item_id' })
  userChecklistItemId: number;

  @Column({ name: 'completed_on', nullable: true })
  completedOn: Date;

  @Column({ name: 'enabled', default: true })
  enabled: boolean;

  @Column({ name: 'created_by_user_id', default: 0 })
  createdByUserId: number;

  @CreateDateColumn({
    name: 'created_date',
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
  })
  createdDate: Date;

  @Column({ name: 'updated_by_user_id', default: 0 })
  updatedByUserId: number;

  @UpdateDateColumn({
    name: 'updated_date',
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
  })
  updatedDate: Date;

  @ManyToOne(() => User)
  @JoinColumn({ name: 'user_id', referencedColumnName: 'id' })
  user: User;

  @ManyToOne(() => UserChecklistItem)
  @JoinColumn({ name: 'user_checklist_item_id', referencedColumnName: 'id' })
  userChecklistItem: UserChecklistItem;

  @AfterInsert()
  logInsert() {
    Logger.log(`Inserted User User Checklist Item with id ${this.id}`);
  }

  @AfterUpdate()
  logUpdate() {
    Logger.log(`Updated User User Checklist Item with id ${this.id}`);
  }

  @AfterRemove()
  logRemove() {
    Logger.log('Removed User User Checklist Item');
  }
}
