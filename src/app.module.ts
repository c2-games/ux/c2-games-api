import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AchievementsModule } from './achievements/achievements.module';
import { AuthModule } from './auth/auth.module';
import { BadgesModule } from './badges/badges.module';
import { ChallengeMilestonesModule } from './challenge-milestones/challenge-milestones.module';
import { ChallengesModule } from './challenges/challenges.module';
import { typeOrmAsyncConfig } from './config/typeorm.config';
import { EventChecklistItemsModule } from './event-checklist-items/event-checklist-items.module';
import { EventTeamEventChecklistItemsModule } from './event-team-event-checklist-items/event-team-event-checklist-items.module';
import { EventTeamsModule } from './event-teams/event-teams.module';
import { EventsModule } from './events/events.module';
import { KeycloakModule } from './keycloak/keycloak.module';
import { UserAchievementsModule } from './user-achievements/user-achievements.module';
import { UserBadgesModule } from './user-badges/user-badges.module';
import { UserChallengeMilestonesModule } from './user-challenge-milestones/user-challenge-milestones.module';
import { UserChecklistItemsModule } from './user-checklist-items/user-checklist-items.module';
import { CurrentUserInterceptor } from './users/interceptors/current-user.interceptor';
import { UsersModule } from './users/users.module';
import { UserUserChecklistItemsModule } from './user-user-checklist-items/user-user-checklist-items.module';
import { EventTeamUsersModule } from './event-team-users/event-team-users.module';
import { MessagesModule } from './messages/messages.module';
import { UserMessagesModule } from './user-messages/user-messages.module';
import { InstitutionsModule } from './institutions/institutions.module';
import { IntercomModule } from './intercom/intercom.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: ['.env'],
      isGlobal: true,
      cache: true,
    }),
    TypeOrmModule.forRootAsync(typeOrmAsyncConfig),
    AchievementsModule,
    AuthModule,
    BadgesModule,
    ChallengesModule,
    ChallengeMilestonesModule,
    KeycloakModule,
    UserAchievementsModule,
    UserBadgesModule,
    UserChallengeMilestonesModule,
    UsersModule,
    EventsModule,
    EventChecklistItemsModule,
    EventTeamEventChecklistItemsModule,
    EventTeamsModule,
    UserChecklistItemsModule,
    UserUserChecklistItemsModule,
    EventTeamUsersModule,
    MessagesModule,
    UserMessagesModule,
    InstitutionsModule,
    IntercomModule,
  ],
  providers: [
    {
      provide: APP_INTERCEPTOR,
      useClass: CurrentUserInterceptor,
    },
  ],
})
export class AppModule {}
