import { faker } from '@faker-js/faker';
import { EventTeam } from '../../../event-teams/entities/event-team.entity';

export const createFakeEventTeam = (userId = faker.datatype.number()) => {
  return {
    id: faker.datatype.number(),
    eventId: faker.datatype.number(),
    name: faker.datatype.string(),
    enabled: faker.datatype.boolean(),
    createdByUserId: userId,
    createdDate: faker.datatype.datetime(),
    updatedByUserId: userId,
    updatedDate: faker.datatype.datetime(),
  } as EventTeam;
};
