import { faker } from '@faker-js/faker';
import { UserChallengeMilestone } from '../../../user-challenge-milestones/entities/user-challenge-milestone.entity';

export const createFakeUserChallengeMilestone = ({
  id = faker.datatype.number(),
  userId = faker.datatype.number(),
  challengeMilestoneId = faker.datatype.number(),
  currentUserId = faker.datatype.number(),
}) => {
  return {
    id: id,
    userId: userId,
    challengeMilestoneId: challengeMilestoneId,
    completedOn: faker.datatype.datetime(),
    enabled: faker.datatype.boolean(),
    createdByUserId: currentUserId,
    createdDate: faker.datatype.datetime(),
    updatedByUserId: currentUserId,
    updatedDate: faker.datatype.datetime(),
  } as UserChallengeMilestone;
};
