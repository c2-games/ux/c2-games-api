import { faker } from '@faker-js/faker';
import { ChallengeMilestone } from '../../../challenge-milestones/entities/challenge-milestone.entity';

export const createFakeChallengeMilestone = (userId = faker.datatype.number()) => {
  return {
    id: userId,
    title: faker.datatype.string(),
    description: faker.datatype.string(),
    challengeId: faker.datatype.number(),
    enabled: faker.datatype.boolean(),
    createdByUserId: userId,
    createdDate: faker.datatype.datetime(),
    updatedByUserId: userId,
    updatedDate: faker.datatype.datetime(),
  } as ChallengeMilestone;
};
