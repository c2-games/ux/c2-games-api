import { faker } from '@faker-js/faker';
import { UserAchievement } from '../../../user-achievements/entities/user-achievement.entity';

export const createFakeUserAchievement = ({
  id = faker.datatype.number(),
  userId = faker.datatype.number(),
  achievementId = faker.datatype.number(),
  currentUserId = faker.datatype.number(),
}) => {
  return {
    id: id,
    userId: userId,
    achievementId: achievementId,
    earnedOn: faker.datatype.datetime(),
    enabled: faker.datatype.boolean(),
    createdByUserId: currentUserId,
    createdDate: faker.datatype.datetime(),
    updatedByUserId: currentUserId,
    updatedDate: faker.datatype.datetime(),
  } as UserAchievement;
};
