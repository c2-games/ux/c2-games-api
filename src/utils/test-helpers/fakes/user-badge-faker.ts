import { faker } from '@faker-js/faker';
import { UserBadge } from '../../../user-badges/entities/user-badge.entity';

export const createFakeUserBadge = ({
  id = faker.datatype.number(),
  userId = faker.datatype.number(),
  badgeId = faker.datatype.number(),
  currentUserId = faker.datatype.number(),
}) => {
  return {
    id: id,
    userId: userId,
    badgeId: badgeId,
    earnedOn: faker.datatype.datetime(),
    enabled: faker.datatype.boolean(),
    createdByUserId: currentUserId,
    createdDate: faker.datatype.datetime(),
    updatedByUserId: currentUserId,
    updatedDate: faker.datatype.datetime(),
  } as UserBadge;
};
