import { faker } from '@faker-js/faker';
import { User } from '../../../users/entities/user.entity';

export const createFakeUser = (id?: number, email?: string) => {
  const userId = id ? id : faker.datatype.number();
  return {
    id: userId,
    email: email || `${faker.datatype.string()}@${faker.datatype.string()}.edu`,
    keycloakSid: faker.datatype.string(),
    enabled: faker.datatype.boolean(),
    createdByUserId: userId,
    createdDate: faker.datatype.datetime(),
    updatedByUserId: userId,
    updatedDate: faker.datatype.datetime(),
  } as User;
};
