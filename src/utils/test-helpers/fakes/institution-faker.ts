import { faker } from '@faker-js/faker';
import { Institution } from '../../../institutions/entities/institution.entity';
import { Region } from '../../../institutions/entities/regions';

export const createFakeInstitution = (domain: string, region: Region = 'northeast') => {
  return {
    id: faker.datatype.number(),
    name: faker.datatype.string(),
    domain: domain,
    state: faker.datatype.string(),
    region: region,
    createdDate: faker.datatype.datetime(),
    updatedDate: faker.datatype.datetime(),
  } as Institution;
};
