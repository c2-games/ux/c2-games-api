import { faker } from '@faker-js/faker';
import { UserChecklistItem } from '../../../user-checklist-items/entities/user-checklist-item.entity';

export const createFakeUserChecklistItem = (userId = faker.datatype.number()) => {
  return {
    id: faker.datatype.number(),
    title: faker.datatype.string(),
    description: faker.datatype.string(),
    enabled: faker.datatype.boolean(),
    createdByUserId: userId,
    createdDate: faker.datatype.datetime(),
    updatedByUserId: userId,
    updatedDate: faker.datatype.datetime(),
  } as UserChecklistItem;
};
