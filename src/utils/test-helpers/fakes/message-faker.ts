import { faker } from '@faker-js/faker';
import { Message } from '../../../messages/entities/message.entity';

export const createFakeMessage = (userId = faker.datatype.number()) => {
  return {
    id: faker.datatype.number(),
    content: faker.datatype.string(),
    announcedOn: faker.datatype.datetime(),
    enabled: faker.datatype.boolean(),
    createdByUserId: userId,
    createdDate: faker.datatype.datetime(),
    updatedByUserId: userId,
    updatedDate: faker.datatype.datetime(),
  } as Message;
};
