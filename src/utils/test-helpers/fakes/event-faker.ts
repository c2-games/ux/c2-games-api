import { faker } from '@faker-js/faker';
import { Event } from '../../../events/entities/event.entity';
import { Regions } from '../../../institutions/entities/regions';

export const createFakeEvent = (userId = faker.datatype.number()) => {
  return {
    id: faker.datatype.number(),
    name: faker.datatype.string(),
    startsOn: faker.datatype.datetime(),
    enabled: faker.datatype.boolean(),
    createdByUserId: userId,
    createdDate: faker.datatype.datetime(),
    updatedByUserId: userId,
    updatedDate: faker.datatype.datetime(),
    regions: Object.keys(Regions),
  } as Event;
};
