import { faker } from '@faker-js/faker';
import { EventTeamEventChecklistItem } from '../../../event-team-event-checklist-items/entities/event-team-event-checklist-item.entity';

export const createFakeEventTeamEventChecklistItem = ({
  eventId = faker.datatype.number(),
  eventTeamId = faker.datatype.number(),
  userId = faker.datatype.number(),
}) => {
  return {
    id: faker.datatype.number(),
    eventId: eventId,
    eventTeamId: eventTeamId,
    eventChecklistItemId: faker.datatype.number(),
    completedOn: faker.datatype.datetime(),
    enabled: faker.datatype.boolean(),
    createdByUserId: userId,
    createdDate: faker.datatype.datetime(),
    updatedByUserId: userId,
    updatedDate: faker.datatype.datetime(),
  } as EventTeamEventChecklistItem;
};
