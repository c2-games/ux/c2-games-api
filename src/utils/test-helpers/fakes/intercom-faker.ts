import { faker } from '@faker-js/faker';
import { Intercom } from '../../../intercom/entities/intercom.entity';

export const createFakeIntercom = (enabled: boolean, message: string) => {
  return {
    id: faker.datatype.number(),
    message: message,
    enabled: enabled,
    createdDate: faker.datatype.datetime(),
    updatedDate: faker.datatype.datetime(),
  } as Intercom;
};
