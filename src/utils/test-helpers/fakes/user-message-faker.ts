import { faker } from '@faker-js/faker';
import { UserMessage } from '../../../user-messages/entities/user-message.entity';

export const createFakeUserMessage = ({
  id = faker.datatype.number(),
  userId = faker.datatype.number(),
  messageId = faker.datatype.number(),
  currentUserId = faker.datatype.number(),
}) => {
  return {
    id: id,
    userId: userId,
    messageId: messageId,
    seenOn: faker.datatype.datetime(),
    enabled: faker.datatype.boolean(),
    createdByUserId: currentUserId,
    createdDate: faker.datatype.datetime(),
    updatedByUserId: currentUserId,
    updatedDate: faker.datatype.datetime(),
  } as UserMessage;
};
