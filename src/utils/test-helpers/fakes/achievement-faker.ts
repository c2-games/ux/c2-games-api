import { faker } from '@faker-js/faker';
import { Achievement } from '../../../achievements/entities/achievement.entity';

export const createFakeAchievement = (userId = faker.datatype.number()) => {
  return {
    id: userId,
    title: faker.datatype.string(),
    description: faker.datatype.string(),
    enabled: faker.datatype.boolean(),
    createdByUserId: userId,
    createdDate: faker.datatype.datetime(),
    updatedByUserId: userId,
    updatedDate: faker.datatype.datetime(),
  } as Achievement;
};
