import { faker } from '@faker-js/faker';
import { EventChecklistItem } from '../../../event-checklist-items/entities/event-checklist-item.entity';

export const createFakeEventChecklistItem = (userId = faker.datatype.number()) => {
  return {
    id: faker.datatype.number(),
    title: faker.datatype.string(),
    description: faker.datatype.string(),
    enabled: faker.datatype.boolean(),
    createdByUserId: userId,
    createdDate: faker.datatype.datetime(),
    updatedByUserId: userId,
    updatedDate: faker.datatype.datetime(),
  } as EventChecklistItem;
};
