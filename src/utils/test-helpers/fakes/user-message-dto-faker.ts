import { faker } from '@faker-js/faker';
import { UserMessageDto } from '../../../user-messages/dto/user-message.dto';

export const createFakeUserMessageDto = ({
  id = faker.datatype.number(),
  userId = faker.datatype.number(),
  messageId = faker.datatype.number(),
  currentUserId = faker.datatype.number(),
}) => {
  return {
    id: id,
    userId: userId,
    messageId: messageId,
    content: faker.datatype.string(),
    announcedOn: faker.datatype.datetime(),
    seenOn: faker.datatype.datetime(),
    enabled: faker.datatype.boolean(),
  } as UserMessageDto;
};
