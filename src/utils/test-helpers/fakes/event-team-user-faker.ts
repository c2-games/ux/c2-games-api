import { faker } from '@faker-js/faker';
import { EventTeamUser } from '../../../event-team-users/entities/event-team-user.entity';

export const createFakeEventTeamUser = ({
  currentUserId = faker.datatype.number(),
  eventTeamId = faker.datatype.number(),
  userId = faker.datatype.number(),
}) => {
  return {
    id: faker.datatype.number(),
    eventTeamId: eventTeamId,
    userId: userId,
    addedOn: faker.datatype.datetime(),
    enabled: faker.datatype.boolean(),
    createdByUserId: currentUserId,
    createdDate: faker.datatype.datetime(),
    updatedByUserId: currentUserId,
    updatedDate: faker.datatype.datetime(),
  } as EventTeamUser;
};
