import { faker } from '@faker-js/faker';
import { UserUserChecklistItem } from '../../../user-user-checklist-items/entities/user-user-checklist-item.entity';

export const createFakeUserUserChecklistItem = ({
  userId = faker.datatype.number(),
  userChecklistItemId = faker.datatype.number(),
}) => {
  return {
    id: faker.datatype.number(),
    userId: userId,
    userChecklistItemId: userChecklistItemId,
    completedOn: faker.datatype.datetime(),
    enabled: faker.datatype.boolean(),
    createdByUserId: userId,
    createdDate: faker.datatype.datetime(),
    updatedByUserId: userId,
    updatedDate: faker.datatype.datetime(),
  } as UserUserChecklistItem;
};
