import { BadRequestException } from '@nestjs/common';

/**
 * Get the user's email and institution email from a string representing an email
 * @param email Email to parse domains from
 */
export function parseEmailDomains(email: string): { emailDomain: string; institutionDomain: string } {
  try {
    // get domain from email
    const emailDomain = email.split('@')[1];
    // get primary domain from email domain
    const institutionDomain = emailDomain.split('.').slice(-2).join('.');
    return { emailDomain, institutionDomain };
  } catch (e) {
    const msg = `failed to parse email domain ${email}`;
    console.error(msg, e);
    throw new BadRequestException(msg + e.toString() || '');
  }
}
