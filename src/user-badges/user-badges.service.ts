import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserBadgeDto } from './dto/create-user-badge.dto';
import { GetUserBadgeRequestParamsDto } from './dto/get-user-badge-request-params.dto';
import { UpdateUserBadgeDto } from './dto/update-user-badge.dto';
import { UserBadge } from './entities/user-badge.entity';

@Injectable()
export class UserBadgesService {
  constructor(@InjectRepository(UserBadge) private repo: Repository<UserBadge>) {}

  async create(createUserBadgeDto: CreateUserBadgeDto, currentUserId = 0) {
    const userBadge = this.repo.create({
      ...createUserBadgeDto,
      createdByUserId: currentUserId,
      updatedByUserId: currentUserId,
    });
    await this.repo.save(userBadge);
    return userBadge;
  }

  findAll(params?: GetUserBadgeRequestParamsDto) {
    // return this.repo.findBy({ ...params });
    let query = this.repo.createQueryBuilder('users_x_badges').where('1=1');
    query = params.userId
      ? query.andWhere('users_x_badges.userId = :userId', {
          userId: params.userId,
        })
      : query;
    query = params.badgeId
      ? query.andWhere('users_x_badges.badgeId = :badgeId', {
          badgeId: params.badgeId,
        })
      : query;
    query = params.earnedOn
      ? query.andWhere('users_x_badges.earnedOn = :earnedOn', {
          earnedOn: params.earnedOn,
        })
      : query;
    return query.getMany();
  }

  async findOne(id: number) {
    if (!id) {
      throw new BadRequestException();
    }
    const userBadge = await this.repo.findOne(id);
    if (!userBadge) {
      throw new NotFoundException('User Badge not found');
    }
    return userBadge;
  }

  async update(id: number, updateUserBadgeDto: UpdateUserBadgeDto, currentUserId = 0) {
    const userBadge = await this.findOne(id);
    Object.assign(updateUserBadgeDto, { updatedByUserId: currentUserId });
    Object.assign(userBadge, updateUserBadgeDto);
    await this.repo.save(userBadge);
    return userBadge;
  }

  async remove(id: number) {
    const userBadge = await this.findOne(id);
    await this.repo.remove(userBadge);
    return userBadge;
  }
}
