import { ApiProperty } from '@nestjs/swagger';
import { IsInt } from 'class-validator';
import { BadgeExists } from '../../badges/decorators/badge-exists.decorator';
import { UserExists } from '../../users/decorators/user-exists.decorator';

export class PostUserBadgeRequestParamsDto {
  @ApiProperty({
    type: 'number',
    required: true,
    example: '123',
  })
  @IsInt()
  @UserExists()
  userId: number;

  @ApiProperty({
    type: 'number',
    required: true,
    example: '123',
  })
  @IsInt()
  @BadgeExists()
  badgeId: number;
}
