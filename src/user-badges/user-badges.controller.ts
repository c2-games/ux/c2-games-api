import { Controller, Get, Param, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiCreatedResponse, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { UserRoles } from 'src/auth/decorators/roles.decorator';
import { UserRole } from 'src/auth/decorators/user-role.enum';
import { UserRolesGuard } from 'src/auth/guards/user-roles.guard';
import { CurrentUser } from '../users/decorators/current-user.decorator';
import { User } from '../users/entities/user.entity';
import { GetUserBadgeByUserRequestParamsDto } from './dto/get-user-badge-by-user-request-params.dto';
import { PostUserBadgeRequestParamsDto } from './dto/post-user-badge-request-params.dto';
import { UserBadgesService } from './user-badges.service';

@ApiTags('User Badges')
@ApiBearerAuth()
@Controller()
@UseGuards(UserRolesGuard)
export class UserBadgesController {
  constructor(private readonly userBadgesService: UserBadgesService) {}

  @Post('users/:userId/badges/earned/:badgeId')
  @ApiOperation({ summary: 'Create user badge' })
  @ApiCreatedResponse({ description: 'User badge created' })
  @UserRoles(UserRole.AwardAdmin)
  create(@Param() params: PostUserBadgeRequestParamsDto, @CurrentUser() currentUser: User) {
    const { userId, badgeId } = params;
    const createUserBadgeDto = { userId, badgeId };
    return this.userBadgesService.create(createUserBadgeDto, currentUser.id);
  }

  @Get('users/:userId/badges')
  @ApiOperation({ summary: 'Find user badges by user ID' })
  @ApiOkResponse({ description: 'User badges found' })
  findAllByUserId(@Param() params: GetUserBadgeByUserRequestParamsDto) {
    const { userId } = params;
    return this.userBadgesService.findAll({ userId });
  }

  // TODO: Remove endpoints if not needed

  // @Get()
  // @Unprotected()
  // @ApiOperation({ summary: 'Find all user badges' })
  // @ApiOkResponse({ description: 'User badges found' })
  // findAll(@Query() params?: GetUserBadgeRequestParamsDto) {
  //   return this.userBadgesService.findAll(params);
  // }

  // @Get(':id')
  // @Unprotected()
  // @ApiOperation({ summary: 'Find user badge by ID' })
  // @ApiOkResponse({ description: 'User badge found' })
  // findOne(@Param('id') id: string) {
  //   return this.userBadgesService.findOne(+id);
  // }

  // @Patch(':id')
  // @ApiOperation({ summary: 'Update user badge' })
  // @ApiOkResponse({ description: 'User badge updated' })
  // update(
  //   @Param('id') id: string,
  //   @Body() updateUserBadgeDto: UpdateUserBadgeDto,
  //   @CurrentUser() currentUser: User,
  // ) {
  //   return this.userBadgesService.update(
  //     +id,
  //     updateUserBadgeDto,
  //     currentUser.id,
  //   );
  // }

  // @Delete(':id')
  // @ApiOperation({ summary: 'Remove user badge' })
  // @ApiOkResponse({ description: 'User badge removed' })
  // remove(@Param('id') id: string) {
  //   return this.userBadgesService.remove(+id);
  // }
}
