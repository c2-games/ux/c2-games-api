import { faker } from '@faker-js/faker';
import { Test, TestingModule } from '@nestjs/testing';
import { KeycloakModule } from '../keycloak/keycloak.module';
import { createFakeUserBadge } from '../utils/test-helpers/fakes/user-badge-faker';
import { createFakeUser } from '../utils/test-helpers/fakes/user-faker';
import { GetUserBadgeByUserRequestParamsDto } from './dto/get-user-badge-by-user-request-params.dto';
import { PostUserBadgeRequestParamsDto } from './dto/post-user-badge-request-params.dto';
import { UserBadgesController } from './user-badges.controller';
import { UserBadgesService } from './user-badges.service';

const currentUserId = faker.datatype.number();

const fakeUserBadge = createFakeUserBadge({ currentUserId });
const fakeUser = createFakeUser(currentUserId);

describe('UserBadgesController', () => {
  let controller: UserBadgesController;
  let service: UserBadgesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [KeycloakModule],
      controllers: [UserBadgesController],
      providers: [
        {
          provide: UserBadgesService,
          useValue: {
            create: jest.fn().mockImplementation((params: PostUserBadgeRequestParamsDto, currentUserId: number) =>
              Promise.resolve({
                ...fakeUserBadge,
                ...params,
              }),
            ),
            findAll: jest
              .fn()
              .mockImplementation((params: GetUserBadgeByUserRequestParamsDto) => Promise.resolve([fakeUserBadge])),
          },
        },
      ],
    }).compile();

    controller = module.get<UserBadgesController>(UserBadgesController);
    service = module.get<UserBadgesService>(UserBadgesService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should create user achievement', async () => {
    const params: PostUserBadgeRequestParamsDto = {
      userId: fakeUserBadge.userId,
      badgeId: fakeUserBadge.badgeId,
    };
    const userBadge = await controller.create(params, fakeUser);

    expect(userBadge).toEqual({ ...fakeUserBadge });
    expect(service.create).toBeCalledTimes(1);
    expect(service.create).toBeCalledWith(params, fakeUser.id);
  });

  it('should find all user achievements', async () => {
    const params: GetUserBadgeByUserRequestParamsDto = {
      userId: fakeUserBadge.userId,
    };
    const userBadges = await controller.findAllByUserId(params);

    expect(userBadges).toEqual([fakeUserBadge]);
    expect(service.findAll).toBeCalledTimes(1);
    expect(service.findAll).toBeCalledWith(params);
  });
});
