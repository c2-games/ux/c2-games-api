import { faker } from '@faker-js/faker';
import { BadRequestException, NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { createFakeUserBadge } from '../utils/test-helpers/fakes/user-badge-faker';
import { GetUserBadgeRequestParamsDto } from './dto/get-user-badge-request-params.dto';
import { UserBadge } from './entities/user-badge.entity';
import { UserBadgesService } from './user-badges.service';

const currentUserId = faker.datatype.number();

const testUserBadge = createFakeUserBadge({ currentUserId });

const getUserBadgeRequestParamsDto = new GetUserBadgeRequestParamsDto();

describe('UserBadgesService', () => {
  let service: UserBadgesService;
  let repo: Repository<UserBadge>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UserBadgesService,
        {
          provide: getRepositoryToken(UserBadge),
          useValue: {
            create: jest.fn().mockReturnValue(testUserBadge),
            save: jest.fn(),
            findAll: jest.fn().mockResolvedValue([testUserBadge]),
            createQueryBuilder: jest.fn(() => ({
              where: jest.fn().mockReturnThis(),
              andWhere: jest.fn().mockReturnThis(),
              getMany: jest.fn().mockResolvedValue([testUserBadge]),
            })),
            findOne: jest.fn().mockResolvedValue(testUserBadge),
            update: jest.fn().mockResolvedValue(true),
            remove: jest.fn().mockResolvedValue(true),
          },
        },
      ],
    }).compile();

    service = module.get<UserBadgesService>(UserBadgesService);
    repo = module.get<Repository<UserBadge>>(getRepositoryToken(UserBadge));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should create a new user badge when provided with valid createUserBadgeDto', async () => {
    const createUserBadgeDto = {
      userId: testUserBadge.userId,
      badgeId: testUserBadge.badgeId,
    };

    const userBadge = await service.create(createUserBadgeDto, currentUserId);

    expect(userBadge).toEqual(testUserBadge);
    expect(repo.create).toBeCalledTimes(1);
    expect(repo.create).toBeCalledWith({
      ...createUserBadgeDto,
      createdByUserId: currentUserId,
      updatedByUserId: currentUserId,
    });
    expect(repo.save).toBeCalledTimes(1);
  });

  it('should find all user badges', async () => {
    const userBadges = await service.findAll(getUserBadgeRequestParamsDto);

    expect(userBadges).toEqual([testUserBadge]);
  });

  it('should find one user badge', async () => {
    const userBadge = await service.findOne(faker.datatype.number());

    expect(userBadge).toEqual(testUserBadge);
  });

  it('should throw bad request exception on findOne if id not provided', async () => {
    await expect(() => service.findOne(null)).rejects.toThrow(BadRequestException);
  });

  it('should throw not found exception on findOne if user badge not found', async () => {
    const userBadgeId = faker.datatype.number();
    const repoSpy = jest.spyOn(repo, 'findOne').mockReturnValue(undefined);

    await expect(() => service.findOne(userBadgeId)).rejects.toThrow(new NotFoundException('User Badge not found'));
    expect(repoSpy).toBeCalledTimes(1);
    expect(repoSpy).toBeCalledWith(userBadgeId);
  });

  it('should update user badge', async () => {
    const userBadgeId = faker.datatype.number();
    const updateUserBadgeDto = { earnedOn: faker.datatype.datetime() };

    const userBadge = await service.update(userBadgeId, updateUserBadgeDto, currentUserId);

    expect(userBadge).toEqual({
      ...testUserBadge,
      ...updateUserBadgeDto,
    });
    expect(repo.findOne).toBeCalledTimes(1);
    expect(repo.findOne).toBeCalledWith(userBadgeId);
    expect(repo.save).toBeCalledTimes(1);
  });

  it('should remove user badge', async () => {
    const userBadgeId = faker.datatype.number();

    const userBadge = await service.remove(userBadgeId);

    expect(userBadge).toEqual(testUserBadge);
  });
});
