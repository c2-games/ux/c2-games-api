import { Logger } from '@nestjs/common';
import {
  AfterInsert,
  AfterRemove,
  AfterUpdate,
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  Unique,
  UpdateDateColumn,
} from 'typeorm';
import { Badge } from '../../badges/entities/badge.entity';
import { User } from '../../users/entities/user.entity';

@Entity({ name: 'users_x_badges' })
@Unique(['userId', 'badgeId'])
export class UserBadge {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'user_id' })
  userId: number;

  @Column({ name: 'badge_id' })
  badgeId: number;

  @Column({
    name: 'earned_on',
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
  })
  earnedOn: Date;

  @Column({ name: 'enabled', default: true })
  enabled: boolean;

  @Column({ name: 'created_by_user_id', default: 0 })
  createdByUserId: number;

  @CreateDateColumn({
    name: 'created_date',
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
  })
  createdDate: Date;

  @Column({ name: 'updated_by_user_id', default: 0 })
  updatedByUserId: number;

  @UpdateDateColumn({
    name: 'updated_date',
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
  })
  updatedDate: Date;

  @ManyToOne(() => User)
  @JoinColumn({ name: 'user_id', referencedColumnName: 'id' })
  user: User;

  @ManyToOne(() => Badge)
  @JoinColumn({ name: 'badge_id', referencedColumnName: 'id' })
  badge: Badge;

  @AfterInsert()
  logInsert() {
    Logger.log(`Inserted User Badge with id ${this.id}`);
  }

  @AfterUpdate()
  logUpdate() {
    Logger.log(`Updated User Badge with id ${this.id}`);
  }

  @AfterRemove()
  logRemove() {
    Logger.log('Removed User Badge');
  }
}
