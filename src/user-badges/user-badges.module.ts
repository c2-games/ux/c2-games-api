import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { KeycloakModule } from '../keycloak/keycloak.module';
import { UserBadge } from './entities/user-badge.entity';
import { UserBadgesController } from './user-badges.controller';
import { UserBadgesService } from './user-badges.service';

@Module({
  imports: [TypeOrmModule.forFeature([UserBadge]), KeycloakModule],
  controllers: [UserBadgesController],
  providers: [UserBadgesService],
})
export class UserBadgesModule {}
