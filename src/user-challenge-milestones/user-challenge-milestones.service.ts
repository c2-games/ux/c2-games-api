import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserChallengeMilestoneDto } from './dto/create-user-challenge-milestone.dto';
import { GetUserChallengeMilestoneRequestParamsDto } from './dto/get-user-challenge-milestone-request-params.dto';
import { UpdateUserChallengeMilestoneDto } from './dto/update-user-challenge-milestone.dto';
import { UserChallengeMilestone } from './entities/user-challenge-milestone.entity';

@Injectable()
export class UserChallengeMilestonesService {
  constructor(
    @InjectRepository(UserChallengeMilestone)
    private repo: Repository<UserChallengeMilestone>,
  ) {}

  async create(createUserChallengeMilestoneDto: CreateUserChallengeMilestoneDto, currentUserId = 0) {
    const userChallengeMilestone = this.repo.create({
      ...createUserChallengeMilestoneDto,
      createdByUserId: currentUserId,
      updatedByUserId: currentUserId,
    });
    await this.repo.save(userChallengeMilestone);
    return userChallengeMilestone;
  }

  findAll(params?: GetUserChallengeMilestoneRequestParamsDto) {
    let query = this.repo.createQueryBuilder('users_x_challenge_milestones').where('1=1');
    query = params.userId
      ? query.andWhere('users_x_challenge_milestones.userId = :userId', {
          userId: params.userId,
        })
      : query;
    query = params.challengeId
      ? query
          .leftJoin('users_x_challenge_milestones.challengeMilestone', 'challenge_milestone')
          .andWhere('challenge_milestone.challengeId = :challengeId', {
            challengeId: params.challengeId,
          })
      : query;
    query = params.challengeMilestoneId
      ? query.andWhere('users_x_challenge_milestones.challengeMilestoneId = :challengeMilestoneId', {
          challengeMilestoneId: params.challengeMilestoneId,
        })
      : query;
    query = params.completedOn
      ? query.andWhere('users_x_challenge_milestones.completedOn = :completedOn', {
          completedOn: params.completedOn,
        })
      : query;
    return query.getMany();
  }

  async findOne(id: number) {
    if (!id) {
      throw new BadRequestException();
    }
    const userChallengeMilestone = this.repo.findOne(id);
    if (!userChallengeMilestone) {
      throw new NotFoundException('User Challenge Milestone not found');
    }
    return userChallengeMilestone;
  }

  async update(id: number, updateUserChallengeMilestoneDto: UpdateUserChallengeMilestoneDto, currentUserId = 0) {
    const userChallengeMilestone = await this.findOne(id);
    Object.assign(updateUserChallengeMilestoneDto, {
      updatedByUserId: currentUserId,
    });
    Object.assign(userChallengeMilestone, updateUserChallengeMilestoneDto);
    await this.repo.save(userChallengeMilestone);
    return userChallengeMilestone;
  }

  async remove(id: number) {
    const userChallengeMilestone = await this.findOne(id);
    await this.repo.remove(userChallengeMilestone);
    return userChallengeMilestone;
  }
}
