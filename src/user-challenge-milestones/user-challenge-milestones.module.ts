import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { KeycloakModule } from '../keycloak/keycloak.module';
import { UserChallengeMilestone } from './entities/user-challenge-milestone.entity';
import { UserChallengeMilestonesController } from './user-challenge-milestones.controller';
import { UserChallengeMilestonesService } from './user-challenge-milestones.service';

@Module({
  imports: [TypeOrmModule.forFeature([UserChallengeMilestone]), KeycloakModule],
  controllers: [UserChallengeMilestonesController],
  providers: [UserChallengeMilestonesService],
})
export class UserChallengeMilestonesModule {}
