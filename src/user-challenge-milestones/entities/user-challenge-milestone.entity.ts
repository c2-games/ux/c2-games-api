import { Logger } from '@nestjs/common';
import {
  AfterInsert,
  AfterRemove,
  AfterUpdate,
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  Unique,
  UpdateDateColumn,
} from 'typeorm';
import { ChallengeMilestone } from '../../challenge-milestones/entities/challenge-milestone.entity';
import { User } from '../../users/entities/user.entity';

@Entity({ name: 'users_x_challenge_milestones' })
@Unique(['userId', 'challengeMilestoneId'])
export class UserChallengeMilestone {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'user_id' })
  userId: number;

  @Column({ name: 'challenge_milestone_id' })
  challengeMilestoneId: number;

  @Column({
    name: 'completed_on',
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
  })
  completedOn: Date;

  @Column({ name: 'enabled', default: true })
  enabled: boolean;

  @Column({ name: 'created_by_user_id', default: 0 })
  createdByUserId: number;

  @CreateDateColumn({
    name: 'created_date',
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
  })
  createdDate: Date;

  @ManyToOne(() => User)
  @JoinColumn({ name: 'user_id', referencedColumnName: 'id' })
  user: User;

  @ManyToOne(() => ChallengeMilestone)
  @JoinColumn({ name: 'challenge_milestone_id', referencedColumnName: 'id' })
  challengeMilestone: ChallengeMilestone;

  @Column({ name: 'updated_by_user_id', default: 0 })
  updatedByUserId: number;

  @UpdateDateColumn({
    name: 'updated_date',
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
  })
  updatedDate: Date;

  @AfterInsert()
  logInsert() {
    Logger.log(`Inserted User Challenge Milestone with id ${this.id}`);
  }

  @AfterUpdate()
  logUpdate() {
    Logger.log(`Updated User Challenge Milestone with id ${this.id}`);
  }

  @AfterRemove()
  logRemove() {
    Logger.log('Removed User Challenge Milestone');
  }
}
