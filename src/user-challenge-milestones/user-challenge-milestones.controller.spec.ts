import { faker } from '@faker-js/faker';
import { Test, TestingModule } from '@nestjs/testing';
import { KeycloakModule } from '../keycloak/keycloak.module';
import { createFakeUserChallengeMilestone } from '../utils/test-helpers/fakes/user-challenge-milestone-faker';
import { createFakeUser } from '../utils/test-helpers/fakes/user-faker';
import { GetUserChallengeMilestoneByUserAndChallengeRequestParamsDto } from './dto/get-user-challenge-milestone-by-user-and-challenge-request-params.dto';
import { PostUserChallengeMilestoneRequestParamsDto } from './dto/post-user-challenge-milestone-request-params.dto';
import { UserChallengeMilestonesController } from './user-challenge-milestones.controller';
import { UserChallengeMilestonesService } from './user-challenge-milestones.service';

const currentUserId = faker.datatype.number();

const fakeUserChallengeMilestone = createFakeUserChallengeMilestone({
  currentUserId,
});
const fakeUser = createFakeUser(currentUserId);

describe('UserChallengeMilestonesController', () => {
  let controller: UserChallengeMilestonesController;
  let service: UserChallengeMilestonesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [KeycloakModule],
      controllers: [UserChallengeMilestonesController],
      providers: [
        {
          provide: UserChallengeMilestonesService,
          useValue: {
            create: jest
              .fn()
              .mockImplementation((params: PostUserChallengeMilestoneRequestParamsDto, currentUserId: number) =>
                Promise.resolve({
                  ...fakeUserChallengeMilestone,
                  ...params,
                }),
              ),
            findAll: jest
              .fn()
              .mockImplementation((params: GetUserChallengeMilestoneByUserAndChallengeRequestParamsDto) =>
                Promise.resolve([fakeUserChallengeMilestone]),
              ),
          },
        },
      ],
    }).compile();

    controller = module.get<UserChallengeMilestonesController>(UserChallengeMilestonesController);
    service = module.get<UserChallengeMilestonesService>(UserChallengeMilestonesService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should create user challenge milestone', async () => {
    const params: PostUserChallengeMilestoneRequestParamsDto = {
      userId: fakeUserChallengeMilestone.userId,
      challengeMilestoneId: fakeUserChallengeMilestone.challengeMilestoneId,
    };
    const userChallengeMilestone = await controller.create(params, fakeUser);

    expect(userChallengeMilestone).toEqual({ ...fakeUserChallengeMilestone });
    expect(service.create).toBeCalledTimes(1);
    expect(service.create).toBeCalledWith(params, fakeUser.id);
  });

  it('should find all user challenge milestones by userId and challengeId', async () => {
    const params: GetUserChallengeMilestoneByUserAndChallengeRequestParamsDto = {
      userId: fakeUserChallengeMilestone.userId,
      challengeId: faker.datatype.number(),
    };
    const userChallengeMilestones = await controller.findAll(params);

    expect(userChallengeMilestones).toEqual([fakeUserChallengeMilestone]);
    expect(service.findAll).toBeCalledTimes(1);
    expect(service.findAll).toBeCalledWith(params);
  });
});
