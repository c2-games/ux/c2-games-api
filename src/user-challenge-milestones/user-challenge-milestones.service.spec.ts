import { faker } from '@faker-js/faker';
import { BadRequestException, NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { createFakeUserChallengeMilestone } from '../utils/test-helpers/fakes/user-challenge-milestone-faker';
import { GetUserChallengeMilestoneRequestParamsDto } from './dto/get-user-challenge-milestone-request-params.dto';
import { UserChallengeMilestone } from './entities/user-challenge-milestone.entity';
import { UserChallengeMilestonesService } from './user-challenge-milestones.service';

const currentUserId = faker.datatype.number();

const testUserChallengeMilestone = createFakeUserChallengeMilestone({
  currentUserId,
});

const getUserChallengeMilestoneRequestParamsDto = new GetUserChallengeMilestoneRequestParamsDto();

describe('UserChallengeMilestonesService', () => {
  let service: UserChallengeMilestonesService;
  let repo: Repository<UserChallengeMilestone>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UserChallengeMilestonesService,
        {
          provide: getRepositoryToken(UserChallengeMilestone),
          useValue: {
            create: jest.fn().mockReturnValue(testUserChallengeMilestone),
            save: jest.fn(),
            findAll: jest.fn().mockResolvedValue([testUserChallengeMilestone]),
            createQueryBuilder: jest.fn(() => ({
              where: jest.fn().mockReturnThis(),
              andWhere: jest.fn().mockReturnThis(),
              getMany: jest.fn().mockResolvedValue([testUserChallengeMilestone]),
            })),
            findOne: jest.fn().mockResolvedValue(testUserChallengeMilestone),
            update: jest.fn().mockResolvedValue(true),
            remove: jest.fn().mockResolvedValue(true),
          },
        },
      ],
    }).compile();

    service = module.get<UserChallengeMilestonesService>(UserChallengeMilestonesService);
    repo = module.get<Repository<UserChallengeMilestone>>(getRepositoryToken(UserChallengeMilestone));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should create a new user challenge milestone when provided with valid createUserChallengeMilestoneDto', async () => {
    const createUserChallengeMilestoneDto = {
      userId: testUserChallengeMilestone.userId,
      challengeMilestoneId: testUserChallengeMilestone.challengeMilestoneId,
    };

    const userChallengeMilestone = await service.create(createUserChallengeMilestoneDto, currentUserId);

    expect(userChallengeMilestone).toEqual(testUserChallengeMilestone);
    expect(repo.create).toBeCalledTimes(1);
    expect(repo.create).toBeCalledWith({
      ...createUserChallengeMilestoneDto,
      createdByUserId: currentUserId,
      updatedByUserId: currentUserId,
    });
    expect(repo.save).toBeCalledTimes(1);
  });

  it('should find all user challenge milestones', async () => {
    const userChallengeMilestones = await service.findAll(getUserChallengeMilestoneRequestParamsDto);

    expect(userChallengeMilestones).toEqual([testUserChallengeMilestone]);
  });

  it('should find one user challenge milestone', async () => {
    const userChallengeMilestone = await service.findOne(faker.datatype.number());

    expect(userChallengeMilestone).toEqual(testUserChallengeMilestone);
  });

  it('should throw bad request exception on findOne if id not provided', async () => {
    await expect(() => service.findOne(null)).rejects.toThrow(BadRequestException);
  });

  it('should throw not found exception on findOne if user challenge milestone not found', async () => {
    const userChallengeMilestoneId = faker.datatype.number();
    const repoSpy = jest.spyOn(repo, 'findOne').mockReturnValue(undefined);

    await expect(() => service.findOne(userChallengeMilestoneId)).rejects.toThrow(
      new NotFoundException('User Challenge Milestone not found'),
    );
    expect(repoSpy).toBeCalledTimes(1);
    expect(repoSpy).toBeCalledWith(userChallengeMilestoneId);
  });

  it('should update user challenge milestone', async () => {
    const userChallengeMilestoneId = faker.datatype.number();
    const updateUserChallengeMilestoneDto = {
      completedOn: faker.datatype.datetime(),
    };

    const userChallengeMilestone = await service.update(
      userChallengeMilestoneId,
      updateUserChallengeMilestoneDto,
      currentUserId,
    );

    expect(userChallengeMilestone).toEqual({
      ...testUserChallengeMilestone,
      ...updateUserChallengeMilestoneDto,
    });
    expect(repo.findOne).toBeCalledTimes(1);
    expect(repo.findOne).toBeCalledWith(userChallengeMilestoneId);
    expect(repo.save).toBeCalledTimes(1);
  });

  it('should remove user challenge milestone', async () => {
    const userChallengeMilestoneId = faker.datatype.number();

    const userChallengeMilestone = await service.remove(userChallengeMilestoneId);

    expect(userChallengeMilestone).toEqual(testUserChallengeMilestone);
  });
});
