import { Controller, Get, Param, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiCreatedResponse, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { UserRoles } from 'src/auth/decorators/roles.decorator';
import { UserRole } from 'src/auth/decorators/user-role.enum';
import { UserRolesGuard } from 'src/auth/guards/user-roles.guard';
import { CurrentUser } from '../users/decorators/current-user.decorator';
import { User } from '../users/entities/user.entity';
import { GetUserChallengeMilestoneByUserAndChallengeRequestParamsDto } from './dto/get-user-challenge-milestone-by-user-and-challenge-request-params.dto';
import { PostUserChallengeMilestoneRequestParamsDto } from './dto/post-user-challenge-milestone-request-params.dto';
import { UserChallengeMilestonesService } from './user-challenge-milestones.service';

@ApiTags('User Challenge Milestones')
@ApiBearerAuth()
@Controller()
@UseGuards(UserRolesGuard)
export class UserChallengeMilestonesController {
  constructor(private readonly userChallengeMilestonesService: UserChallengeMilestonesService) {}

  @Post('users/:userId/challenge-milestones/completed/:challengeMilestoneId')
  @ApiOperation({ summary: 'Create user challenge milestone' })
  @ApiCreatedResponse({ description: 'User challenge milestone created' })
  @UserRoles(UserRole.AwardAdmin)
  create(@Param() params: PostUserChallengeMilestoneRequestParamsDto, @CurrentUser() currentUser: User) {
    const { userId, challengeMilestoneId } = params;
    const createUserChallengeMilestoneDto = { userId, challengeMilestoneId };
    return this.userChallengeMilestonesService.create(createUserChallengeMilestoneDto, currentUser.id);
  }

  @Get('users/:userId/challenge-milestones/:challengeId')
  @ApiOperation({
    summary: 'Find all user challenge milestones by user and challenge',
  })
  @ApiOkResponse({ description: 'User challenge milestones found' })
  findAll(
    @Param()
    params?: GetUserChallengeMilestoneByUserAndChallengeRequestParamsDto,
  ) {
    const { userId, challengeId } = params;
    return this.userChallengeMilestonesService.findAll({ userId, challengeId });
  }

  // TODO: Remove endpoints if not needed

  // @Get()
  // @Unprotected()
  // @ApiOperation({ summary: 'Find all user challenge milestones' })
  // @ApiOkResponse({ description: 'User challenge milestones found' })
  // findAll(@Query() params?: GetUserChallengeMilestoneRequestParamsDto) {
  //   return this.userChallengeMilestonesService.findAll(params);
  // }

  // @Get(':id')
  // @Unprotected()
  // @ApiOperation({ summary: 'Find user challenge milestone by ID' })
  // @ApiOkResponse({ description: 'User challenge milestone found' })
  // findOne(@Param('id') id: string) {
  //   return this.userChallengeMilestonesService.findOne(+id);
  // }

  // @Patch(':id')
  // @ApiOperation({ summary: 'Update user challenge milestone' })
  // @ApiOkResponse({ description: 'User challenge milestone updated' })
  // update(
  //   @Param('id') id: string,
  //   @Body() updateUserChallengeMilestoneDto: UpdateUserChallengeMilestoneDto,
  //   @CurrentUser() currentUser: User,
  // ) {
  //   return this.userChallengeMilestonesService.update(
  //     +id,
  //     updateUserChallengeMilestoneDto,
  //     currentUser.id,
  //   );
  // }

  // @Delete(':id')
  // @ApiOperation({ summary: 'Remove user challenge milestone' })
  // @ApiOkResponse({ description: 'User challenge milestone removed' })
  // remove(@Param('id') id: string) {
  //   return this.userChallengeMilestonesService.remove(+id);
  // }
}
