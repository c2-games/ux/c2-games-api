import { ApiProperty } from '@nestjs/swagger';
import { IsInt } from 'class-validator';
import { ChallengeMilestoneExists } from '../../challenge-milestones/decorators/challenge-milestone-exists.decorator';
import { UserExists } from '../../users/decorators/user-exists.decorator';

export class PostUserChallengeMilestoneRequestParamsDto {
  @ApiProperty({
    type: 'number',
    required: true,
    example: '123',
  })
  @IsInt()
  @UserExists()
  userId: number;

  @ApiProperty({
    type: 'number',
    required: true,
    example: '123',
  })
  @IsInt()
  @ChallengeMilestoneExists()
  challengeMilestoneId: number;
}
