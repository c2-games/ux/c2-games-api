import { ApiProperty } from '@nestjs/swagger';
import { IsInt } from 'class-validator';
import { ChallengeExists } from '../../challenges/decorators/challenge-exists.decorator';
import { UserExists } from '../../users/decorators/user-exists.decorator';

export class GetUserChallengeMilestoneByUserAndChallengeRequestParamsDto {
  @ApiProperty({
    type: 'number',
    required: true,
    example: '123',
  })
  @IsInt()
  @UserExists()
  userId: number;

  @ApiProperty({
    type: 'number',
    required: true,
    example: '123',
  })
  @IsInt()
  @ChallengeExists()
  challengeId: number;
}
