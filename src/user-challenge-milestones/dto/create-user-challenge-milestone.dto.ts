import { ApiProperty } from '@nestjs/swagger';
import { IsDate, IsInt, IsOptional } from 'class-validator';
import { ChallengeMilestoneExists } from '../../challenge-milestones/decorators/challenge-milestone-exists.decorator';
import { UserExists } from '../../users/decorators/user-exists.decorator';

export class CreateUserChallengeMilestoneDto {
  @ApiProperty({
    type: 'number',
    required: true,
    example: '123',
  })
  @IsInt()
  @UserExists()
  userId: number;

  @ApiProperty({
    type: 'number',
    required: true,
    example: '123',
  })
  @IsInt()
  @ChallengeMilestoneExists()
  challengeMilestoneId: number;

  @ApiProperty({
    type: 'date',
    required: false,
    example: '2022-08-01T20:29:56.561Z',
  })
  @IsDate()
  @IsOptional()
  completedOn?: Date;
}
