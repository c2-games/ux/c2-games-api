import { ApiProperty } from '@nestjs/swagger';
import { IsDate, IsInt, IsOptional } from 'class-validator';

export class GetUserChallengeMilestoneRequestParamsDto {
  @ApiProperty({
    type: 'number',
    required: false,
    example: '123',
  })
  @IsInt()
  @IsOptional()
  userId?: number;

  @ApiProperty({
    type: 'number',
    required: false,
    example: '123',
  })
  @IsInt()
  @IsOptional()
  challengeId?: number;

  @ApiProperty({
    type: 'number',
    required: false,
    example: '123',
  })
  @IsInt()
  @IsOptional()
  challengeMilestoneId?: number;

  @ApiProperty({
    type: 'date',
    required: false,
    example: '2022-08-01T20:29:56.561Z',
  })
  @IsDate()
  @IsOptional()
  completedOn?: Date;
}
