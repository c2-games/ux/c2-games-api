import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { KeycloakModule } from '../keycloak/keycloak.module';
import { ChallengeMilestonesController } from './challenge-milestones.controller';
import { ChallengeMilestonesService } from './challenge-milestones.service';
import { ChallengeMilestone } from './entities/challenge-milestone.entity';
import { ChallengeMilestoneExistsRule } from './validators/challenge-milestone-exists-rule.validator';

@Module({
  imports: [TypeOrmModule.forFeature([ChallengeMilestone]), KeycloakModule],
  controllers: [ChallengeMilestonesController],
  providers: [ChallengeMilestonesService, ChallengeMilestoneExistsRule],
})
export class ChallengeMilestonesModule {}
