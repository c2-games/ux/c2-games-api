import { Body, Controller, Delete, Get, Param, Patch, Post, Query, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiCreatedResponse, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { UserRoles } from 'src/auth/decorators/roles.decorator';
import { UserRole } from 'src/auth/decorators/user-role.enum';
import { UserRolesGuard } from 'src/auth/guards/user-roles.guard';
import { CurrentUser } from '../users/decorators/current-user.decorator';
import { User } from '../users/entities/user.entity';
import { ChallengeMilestonesService } from './challenge-milestones.service';
import { CreateChallengeMilestoneDto } from './dto/create-challenge-milestone.dto';
import { GetChallengeMilestoneByChallengeRequestParamsDto } from './dto/get-challenge-milestone-by-challenge-request-params.dto';
import { GetChallengeMilestoneRequestParamsDto } from './dto/get-challenge-milestone-request-params.dto';
import { UpdateChallengeMilestoneDto } from './dto/update-challenge-milestone.dto';

@ApiTags('Challenge Milestones')
@ApiBearerAuth()
@Controller('challenge-milestones')
@UseGuards(UserRolesGuard)
export class ChallengeMilestonesController {
  constructor(private readonly challengeMilestonesService: ChallengeMilestonesService) {}

  @Post()
  @ApiOperation({ summary: 'Create challenge milestone' })
  @ApiCreatedResponse({ description: 'Challenge milestone created' })
  @UserRoles(UserRole.AwardAdmin)
  create(@Body() createChallengeMilestoneDto: CreateChallengeMilestoneDto, @CurrentUser() currentUser: User) {
    return this.challengeMilestonesService.create(createChallengeMilestoneDto, currentUser.id);
  }

  @Get()
  @ApiOperation({ summary: 'Find all challenge milestones' })
  @ApiOkResponse({ description: 'Challenge milestones found' })
  findAll(@Query() params?: GetChallengeMilestoneRequestParamsDto) {
    return this.challengeMilestonesService.findAll(params);
  }

  @Get(':id')
  @ApiOperation({ summary: 'Find challenge milestone by ID' })
  @ApiOkResponse({ description: 'Challenge milestone found' })
  findOne(@Param('id') id: string) {
    return this.challengeMilestonesService.findOne(+id);
  }

  @Get('/challenge/:challengeId')
  @ApiOperation({ summary: 'Find all challenge milestones by challenge ID' })
  @ApiOkResponse({ description: 'Challenge milestones found' })
  findAllByChallengeId(@Param() params: GetChallengeMilestoneByChallengeRequestParamsDto) {
    const { challengeId } = params;
    return this.challengeMilestonesService.findAll({ challengeId });
  }

  @Patch(':id')
  @ApiOperation({ summary: 'Update challenge milestone' })
  @ApiOkResponse({ description: 'Challenge milestone updated' })
  @UserRoles(UserRole.AwardAdmin)
  update(
    @Param('id') id: string,
    @Body() updateChallengeMilestoneDto: UpdateChallengeMilestoneDto,
    @CurrentUser() currentUser: User,
  ) {
    return this.challengeMilestonesService.update(+id, updateChallengeMilestoneDto, currentUser.id);
  }

  @Delete(':id')
  @ApiOperation({ summary: 'Remove challenge milestone' })
  @ApiOkResponse({ description: 'Challenge milestone removed' })
  @UserRoles(UserRole.AwardAdmin)
  remove(@Param('id') id: string) {
    return this.challengeMilestonesService.remove(+id);
  }
}
