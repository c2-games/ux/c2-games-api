import { registerDecorator, ValidationOptions } from 'class-validator';
import { ChallengeMilestoneExistsRule } from '../validators/challenge-milestone-exists-rule.validator';

export function ChallengeMilestoneExists(validationOptions?: ValidationOptions) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      name: 'ChallengeMilestoneExists',
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: ChallengeMilestoneExistsRule,
    });
  };
}
