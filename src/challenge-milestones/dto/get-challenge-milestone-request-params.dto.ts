import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsOptional, IsString } from 'class-validator';

export class GetChallengeMilestoneRequestParamsDto {
  @ApiProperty({
    type: 'string',
    required: false,
    example: 'Challenge Milestone Title',
  })
  @IsString()
  @IsOptional()
  title?: string;

  @ApiProperty({
    type: 'string',
    required: false,
    example: 'This is a challenge milestone description',
  })
  @IsString()
  @IsOptional()
  description?: string;

  @ApiProperty({
    type: 'number',
    required: false,
    example: '123',
  })
  @IsInt()
  @IsOptional()
  challengeId?: number;
}
