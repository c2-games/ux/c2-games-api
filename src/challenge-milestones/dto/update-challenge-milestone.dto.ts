import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsOptional, IsString } from 'class-validator';

export class UpdateChallengeMilestoneDto {
  @ApiProperty({
    type: 'string',
    required: false,
    example: 'Challenge Milestone Title',
  })
  @IsString()
  @IsOptional()
  title?: string;

  @ApiProperty({
    type: 'string',
    required: false,
    example: 'This is a challenge milestone description',
  })
  @IsString()
  @IsOptional()
  description?: string;

  @ApiProperty({
    type: 'boolean',
    required: false,
    example: 'true',
  })
  @IsBoolean()
  @IsOptional()
  enabled?: boolean;
}
