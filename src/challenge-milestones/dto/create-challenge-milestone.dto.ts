import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsString } from 'class-validator';
import { ChallengeExists } from '../../challenges/decorators/challenge-exists.decorator';

export class CreateChallengeMilestoneDto {
  @ApiProperty({
    type: 'string',
    required: true,
    example: 'Challenge Milestone Title',
  })
  @IsString()
  title: string;

  @ApiProperty({
    type: 'string',
    required: true,
    example: 'This is a challenge milestone description',
  })
  @IsString()
  description: string;

  @ApiProperty({
    type: 'number',
    required: true,
    example: '123',
  })
  @IsInt()
  @ChallengeExists()
  challengeId: number;
}
