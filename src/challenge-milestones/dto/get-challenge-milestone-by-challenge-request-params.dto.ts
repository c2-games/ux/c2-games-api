import { ApiProperty } from '@nestjs/swagger';
import { IsInt } from 'class-validator';
import { ChallengeExists } from '../../challenges/decorators/challenge-exists.decorator';

export class GetChallengeMilestoneByChallengeRequestParamsDto {
  @ApiProperty({
    type: 'number',
    required: false,
    example: '123',
  })
  @IsInt()
  @ChallengeExists()
  challengeId: number;
}
