import { faker } from '@faker-js/faker';
import { Test, TestingModule } from '@nestjs/testing';
import { KeycloakModule } from '../keycloak/keycloak.module';
import { createFakeChallengeMilestone } from '../utils/test-helpers/fakes/challenge-milestone-faker';
import { createFakeUser } from '../utils/test-helpers/fakes/user-faker';
import { ChallengeMilestonesController } from './challenge-milestones.controller';
import { ChallengeMilestonesService } from './challenge-milestones.service';
import { CreateChallengeMilestoneDto } from './dto/create-challenge-milestone.dto';
import { UpdateChallengeMilestoneDto } from './dto/update-challenge-milestone.dto';

const currentUserId = faker.datatype.number();

const fakeChallengeMilestone = createFakeChallengeMilestone(currentUserId);
const fakeUser = createFakeUser(currentUserId);

describe('ChallengeMilestonesController', () => {
  let controller: ChallengeMilestonesController;
  let service: ChallengeMilestonesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [KeycloakModule],
      controllers: [ChallengeMilestonesController],
      providers: [
        {
          provide: ChallengeMilestonesService,
          useValue: {
            create: jest
              .fn()
              .mockImplementation((challengeMilestone: CreateChallengeMilestoneDto, currentUserId: number) =>
                Promise.resolve({
                  ...fakeChallengeMilestone,
                  ...challengeMilestone,
                }),
              ),
            findAll: jest.fn().mockResolvedValue([fakeChallengeMilestone]),
            findOne: jest.fn().mockImplementation((id: number) => Promise.resolve({ ...fakeChallengeMilestone, id })),
            findAllByChallengeId: jest.fn().mockResolvedValue([fakeChallengeMilestone]),
            update: jest
              .fn()
              .mockImplementation(
                (id: number, challengeMilestone: UpdateChallengeMilestoneDto, currentUserId: number) =>
                  Promise.resolve({ ...fakeChallengeMilestone, id }),
              ),
            remove: jest.fn().mockResolvedValue(fakeChallengeMilestone),
          },
        },
      ],
    }).compile();

    controller = module.get<ChallengeMilestonesController>(ChallengeMilestonesController);
    service = module.get<ChallengeMilestonesService>(ChallengeMilestonesService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should create challenge milestone', async () => {
    const createChallengeMilestoneDto: CreateChallengeMilestoneDto = {
      title: fakeChallengeMilestone.title,
      description: fakeChallengeMilestone.description,
      challengeId: fakeChallengeMilestone.challengeId,
    };
    const challengeMilestone = await controller.create(createChallengeMilestoneDto, fakeUser);

    expect(challengeMilestone).toEqual({ ...fakeChallengeMilestone });
    expect(service.create).toBeCalledTimes(1);
    expect(service.create).toBeCalledWith(createChallengeMilestoneDto, fakeUser.id);
  });

  it('should find all challenge milestones', async () => {
    const challengeMilestones = await controller.findAll();

    expect(challengeMilestones).toEqual([fakeChallengeMilestone]);
    expect(service.findAll).toBeCalledTimes(1);
  });

  it('should find one challenge milestone by id', async () => {
    const challengeMilestone = await controller.findOne(`${fakeChallengeMilestone.id}`);

    expect(challengeMilestone).toEqual(fakeChallengeMilestone);
    expect(service.findOne).toBeCalledTimes(1);
    expect(service.findOne).toBeCalledWith(fakeChallengeMilestone.id);
  });

  it('should find all challenge milestones by challengeId', async () => {
    const challengeMilestones = await controller.findAllByChallengeId({
      challengeId: fakeChallengeMilestone.challengeId,
    });

    expect(challengeMilestones).toEqual([fakeChallengeMilestone]);
    expect(service.findAll).toBeCalledTimes(1);
  });

  it('should update challenge milestone', async () => {
    const updateChallengeMilestoneDto = {
      title: faker.datatype.string(),
    };
    const challengeMilestone = await controller.update(
      `${fakeChallengeMilestone.id}`,
      updateChallengeMilestoneDto,
      fakeUser,
    );

    expect(challengeMilestone).toEqual(fakeChallengeMilestone);
    expect(service.update).toBeCalledTimes(1);
    expect(service.update).toBeCalledWith(fakeChallengeMilestone.id, updateChallengeMilestoneDto, fakeUser.id);
  });

  it('should remove challenge milestone', async () => {
    const challengeMilestone = await controller.remove(`${fakeChallengeMilestone.id}`);

    expect(challengeMilestone).toEqual(fakeChallengeMilestone);
    expect(service.remove).toBeCalledTimes(1);
    expect(service.remove).toBeCalledWith(fakeChallengeMilestone.id);
  });
});
