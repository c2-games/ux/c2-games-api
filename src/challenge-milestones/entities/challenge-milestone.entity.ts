import { Logger } from '@nestjs/common';
import {
  AfterInsert,
  AfterRemove,
  AfterUpdate,
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Challenge } from '../../challenges/entities/challenge.entity';
import { UserChallengeMilestone } from '../../user-challenge-milestones/entities/user-challenge-milestone.entity';

@Entity({ name: 'challenge_milestones' })
export class ChallengeMilestone {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'title' })
  title: string;

  @Column({ name: 'description' })
  description: string;

  @Column({ name: 'challenge_id' })
  challengeId: number;

  @Column({ name: 'enabled', default: true })
  enabled: boolean;

  @Column({ name: 'created_by_user_id', default: 0 })
  createdByUserId: number;

  @CreateDateColumn({
    name: 'created_date',
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
  })
  createdDate: Date;

  @Column({ name: 'updated_by_user_id', default: 0 })
  updatedByUserId: number;

  @UpdateDateColumn({
    name: 'updated_date',
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
  })
  updatedDate: Date;

  @ManyToOne(() => Challenge)
  @JoinColumn({ name: 'challenge_id', referencedColumnName: 'id' })
  challenge: Challenge;

  @OneToMany(() => UserChallengeMilestone, (userChallengeMilestone) => userChallengeMilestone.challengeMilestone)
  userChallengeMilestones: UserChallengeMilestone[];

  @AfterInsert()
  logInsert() {
    Logger.log(`Inserted Challenge Milestone with id ${this.id}`);
  }

  @AfterUpdate()
  logUpdate() {
    Logger.log(`Updated Challenge Milestone with id ${this.id}`);
  }

  @AfterRemove()
  logRemove() {
    Logger.log('Removed Challenge Milestone');
  }
}
