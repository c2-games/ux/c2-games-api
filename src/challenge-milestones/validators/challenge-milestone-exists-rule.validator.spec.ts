import { NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { createFakeChallengeMilestone } from '../../utils/test-helpers/fakes/challenge-milestone-faker';
import { ChallengeMilestonesService } from '../challenge-milestones.service';
import { ChallengeMilestoneExistsRule } from './challenge-milestone-exists-rule.validator';

const fakeChallengeMilestone = createFakeChallengeMilestone();

describe('ChallengeMilestoneExistsRule', () => {
  let challengeMilestoneExistsRule: ChallengeMilestoneExistsRule;
  let service: ChallengeMilestonesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ChallengeMilestoneExistsRule,
        {
          provide: ChallengeMilestonesService,
          useValue: {
            findOne: jest.fn().mockImplementation((id: number) => Promise.resolve(fakeChallengeMilestone)),
          },
        },
      ],
    }).compile();

    challengeMilestoneExistsRule = module.get<ChallengeMilestoneExistsRule>(ChallengeMilestoneExistsRule);
    service = module.get<ChallengeMilestonesService>(ChallengeMilestonesService);
  });

  it('should return true when challenge milestone exists', async () => {
    expect(challengeMilestoneExistsRule.validate(fakeChallengeMilestone.id)).resolves.toEqual(true);
    expect(service.findOne).toBeCalledTimes(1);
    expect(service.findOne).toBeCalledWith(fakeChallengeMilestone.id);
  });

  it('should return false when challenge milestone does not exist', async () => {
    const findOneSpy = jest.spyOn(service, 'findOne').mockRejectedValueOnce(NotFoundException);
    expect(challengeMilestoneExistsRule.validate(fakeChallengeMilestone.id)).resolves.toEqual(false);
    expect(findOneSpy).toBeCalledTimes(1);
    expect(findOneSpy).toBeCalledWith(fakeChallengeMilestone.id);
  });

  it('should return correct default message', () => {
    expect(challengeMilestoneExistsRule.defaultMessage()).toEqual('Challenge milestone does not exist');
  });
});
