import { Injectable } from '@nestjs/common';
import { ValidatorConstraint, ValidatorConstraintInterface } from 'class-validator';
import { ChallengeMilestonesService } from '../challenge-milestones.service';

@ValidatorConstraint({ name: 'UserExists', async: true })
@Injectable()
export class ChallengeMilestoneExistsRule implements ValidatorConstraintInterface {
  constructor(private readonly challengeMilestonesService: ChallengeMilestonesService) {}

  async validate(id: number) {
    try {
      await this.challengeMilestonesService.findOne(id);
    } catch (error) {
      return false;
    }
    return true;
  }

  defaultMessage() {
    return `Challenge milestone does not exist`;
  }
}
