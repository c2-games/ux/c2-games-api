import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateChallengeMilestoneDto } from './dto/create-challenge-milestone.dto';
import { GetChallengeMilestoneRequestParamsDto } from './dto/get-challenge-milestone-request-params.dto';
import { UpdateChallengeMilestoneDto } from './dto/update-challenge-milestone.dto';
import { ChallengeMilestone } from './entities/challenge-milestone.entity';

@Injectable()
export class ChallengeMilestonesService {
  constructor(
    @InjectRepository(ChallengeMilestone)
    private repo: Repository<ChallengeMilestone>,
  ) {}

  async create(createChallengeMilestoneDto: CreateChallengeMilestoneDto, currentUserId = 0) {
    const challengeMilestone = this.repo.create({
      ...createChallengeMilestoneDto,
      createdByUserId: currentUserId,
      updatedByUserId: currentUserId,
    });
    await this.repo.save(challengeMilestone);
    return challengeMilestone;
  }

  findAll(params?: GetChallengeMilestoneRequestParamsDto) {
    let query = this.repo.createQueryBuilder('challenge_milestones').where('1=1');
    query = params.title
      ? query.andWhere('challenge_milestones.title = :title', {
          title: params.title,
        })
      : query;
    query = params.description
      ? query.andWhere('challenge_milestones.description = :description', {
          description: params.description,
        })
      : query;

    query = params.challengeId
      ? query.andWhere('challenge_milestones.challengeId = :challengeId', {
          challengeId: params.challengeId,
        })
      : query;
    return query.getMany();
  }

  async findOne(id: number) {
    if (!id) {
      throw new BadRequestException();
    }
    const badge = await this.repo.findOne(id);
    if (!badge) {
      throw new NotFoundException('Challenge milestone not found');
    }
    return badge;
  }

  async update(id: number, updateChallengeMilestoneDto: UpdateChallengeMilestoneDto, currentUserId = 0) {
    const challengeMilestone = await this.findOne(id);
    Object.assign(updateChallengeMilestoneDto, {
      updatedByUserId: currentUserId,
    });
    Object.assign(challengeMilestone, updateChallengeMilestoneDto);
    await this.repo.save(challengeMilestone);
    return challengeMilestone;
  }

  async remove(id: number) {
    const challengeMilestone = await this.findOne(id);
    await this.repo.remove(challengeMilestone);
    return challengeMilestone;
  }
}
