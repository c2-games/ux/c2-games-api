import { faker } from '@faker-js/faker';
import { BadRequestException, NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ChallengeMilestonesService } from './challenge-milestones.service';
import { GetChallengeMilestoneRequestParamsDto } from './dto/get-challenge-milestone-request-params.dto';
import { ChallengeMilestone } from './entities/challenge-milestone.entity';
import { createFakeChallengeMilestone } from '../utils/test-helpers/fakes/challenge-milestone-faker';

const currentUserId = faker.datatype.number();

const testChallengeMilestone = createFakeChallengeMilestone(currentUserId);

const getChallengeMilestoneRequestParamsDto = new GetChallengeMilestoneRequestParamsDto();

describe('ChallengeMilestonesService', () => {
  let service: ChallengeMilestonesService;
  let repo: Repository<ChallengeMilestone>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ChallengeMilestonesService,
        {
          provide: getRepositoryToken(ChallengeMilestone),
          useValue: {
            create: jest.fn().mockReturnValue(testChallengeMilestone),
            save: jest.fn(),
            findAll: jest.fn().mockResolvedValue([testChallengeMilestone]),
            createQueryBuilder: jest.fn(() => ({
              where: jest.fn().mockReturnThis(),
              andWhere: jest.fn().mockReturnThis(),
              getMany: jest.fn().mockResolvedValue([testChallengeMilestone]),
            })),
            findOne: jest.fn().mockResolvedValue(testChallengeMilestone),
            update: jest.fn().mockResolvedValue(true),
            remove: jest.fn().mockResolvedValue(true),
          },
        },
      ],
    }).compile();

    service = module.get<ChallengeMilestonesService>(ChallengeMilestonesService);
    repo = module.get<Repository<ChallengeMilestone>>(getRepositoryToken(ChallengeMilestone));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should create a new challenge milestone when provided with valid createChallengeMilestoneDto', async () => {
    const createChallengeMilestoneDto = {
      title: testChallengeMilestone.title,
      description: testChallengeMilestone.description,
      challengeId: testChallengeMilestone.challengeId,
    };

    const challengeMilestone = await service.create(createChallengeMilestoneDto, currentUserId);

    expect(challengeMilestone).toEqual(testChallengeMilestone);
    expect(repo.create).toBeCalledTimes(1);
    expect(repo.create).toBeCalledWith({
      ...createChallengeMilestoneDto,
      createdByUserId: currentUserId,
      updatedByUserId: currentUserId,
    });
    expect(repo.save).toBeCalledTimes(1);
  });

  it('should find all challenge milestones', async () => {
    const challengeMilestones = await service.findAll(getChallengeMilestoneRequestParamsDto);

    expect(challengeMilestones).toEqual([testChallengeMilestone]);
  });

  it('should find one challenge milestone', async () => {
    const challengeMilestone = await service.findOne(faker.datatype.number());

    expect(challengeMilestone).toEqual(testChallengeMilestone);
  });

  it('should throw bad request exception on findOne if id not provided', async () => {
    await expect(() => service.findOne(null)).rejects.toThrow(BadRequestException);
  });

  it('should throw not found exception on findOne if challenge milestone not found', async () => {
    const challengeMilestoneId = faker.datatype.number();
    const repoSpy = jest.spyOn(repo, 'findOne').mockReturnValue(undefined);

    await expect(() => service.findOne(challengeMilestoneId)).rejects.toThrow(
      new NotFoundException('Challenge milestone not found'),
    );
    expect(repoSpy).toBeCalledTimes(1);
    expect(repoSpy).toBeCalledWith(challengeMilestoneId);
  });

  it('should update challenge milestone', async () => {
    const challengeMilestoneId = faker.datatype.number();
    const updateChallengeMilestoneDto = {
      title: 'This title has been updated',
    };

    const challengeMilestone = await service.update(challengeMilestoneId, updateChallengeMilestoneDto, currentUserId);

    expect(challengeMilestone).toEqual({
      ...testChallengeMilestone,
      ...updateChallengeMilestoneDto,
    });
    expect(repo.findOne).toBeCalledTimes(1);
    expect(repo.findOne).toBeCalledWith(challengeMilestoneId);
    expect(repo.save).toBeCalledTimes(1);
  });

  it('should remove challenge milestone', async () => {
    const challengeMilestoneId = faker.datatype.number();

    const challengeMilestone = await service.remove(challengeMilestoneId);

    expect(challengeMilestone).toEqual(testChallengeMilestone);
  });
});
