import { NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { createFakeMessage } from '../../utils/test-helpers/fakes/message-faker';
import { MessagesService } from '../messages.service';
import { MessageExistsRule } from './message-exists-rule.validator';

const fakeMessage = createFakeMessage();

describe('MessageExistsRule', () => {
  let messageExistsRule: MessageExistsRule;
  let service: MessagesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        MessageExistsRule,
        {
          provide: MessagesService,
          useValue: {
            findOne: jest.fn().mockImplementation((id: number) => Promise.resolve(fakeMessage)),
          },
        },
      ],
    }).compile();

    messageExistsRule = module.get<MessageExistsRule>(MessageExistsRule);
    service = module.get<MessagesService>(MessagesService);
  });

  it('should return true when message exists', async () => {
    expect(messageExistsRule.validate(fakeMessage.id)).resolves.toEqual(true);
    expect(service.findOne).toBeCalledTimes(1);
    expect(service.findOne).toBeCalledWith(fakeMessage.id);
  });

  it('should return false when message does not exist', async () => {
    const findOneSpy = jest.spyOn(service, 'findOne').mockRejectedValueOnce(NotFoundException);
    expect(messageExistsRule.validate(fakeMessage.id)).resolves.toEqual(false);
    expect(findOneSpy).toBeCalledTimes(1);
    expect(findOneSpy).toBeCalledWith(fakeMessage.id);
  });

  it('should return correct default message', () => {
    expect(messageExistsRule.defaultMessage()).toEqual('Message does not exist');
  });
});
