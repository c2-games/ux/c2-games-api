import { Injectable } from '@nestjs/common';
import { ValidatorConstraint, ValidatorConstraintInterface } from 'class-validator';
import { MessagesService } from '../messages.service';

@ValidatorConstraint({ name: 'MessageExists', async: true })
@Injectable()
export class MessageExistsRule implements ValidatorConstraintInterface {
  constructor(private readonly messagesService: MessagesService) {}

  async validate(id: number) {
    try {
      await this.messagesService.findOne(id);
    } catch (error) {
      return false;
    }
    return true;
  }

  defaultMessage() {
    return `Message does not exist`;
  }
}
