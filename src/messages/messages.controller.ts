import { Body, Controller, Delete, Get, Param, Patch, Post, Query, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiCreatedResponse, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { UserRoles } from 'src/auth/decorators/roles.decorator';
import { UserRole } from 'src/auth/decorators/user-role.enum';
import { UserRolesGuard } from 'src/auth/guards/user-roles.guard';
import { CurrentUser } from '../users/decorators/current-user.decorator';
import { User } from '../users/entities/user.entity';
import { CreateMessageDto } from './dto/create-message.dto';
import { MessageQueryDto } from './dto/message-query.dto';
import { UpdateMessageDto } from './dto/update-message.dto';
import { MessagesService } from './messages.service';

@ApiTags('Messages')
@ApiBearerAuth()
@Controller('messages')
@UseGuards(UserRolesGuard)
export class MessagesController {
  constructor(private readonly messagesService: MessagesService) {}

  @Post()
  @ApiOperation({ summary: 'Create message' })
  @ApiCreatedResponse({ description: 'Message created' })
  @UserRoles(UserRole.Announcer)
  create(@Body() createMessageDto: CreateMessageDto, @CurrentUser() currentUser: User) {
    return this.messagesService.create(createMessageDto, currentUser.id);
  }

  @Get()
  @ApiOperation({ summary: 'Find all messages' })
  @ApiOkResponse({ description: 'Messages found' })
  findAll(@Query() params?: MessageQueryDto) {
    return this.messagesService.findAll(params);
  }

  @Get(':id')
  @ApiOperation({ summary: 'Find message by ID' })
  @ApiOkResponse({ description: 'Message found' })
  findOne(@Param('id') id: string) {
    return this.messagesService.findOne(+id);
  }

  @Patch(':id')
  @ApiOperation({ summary: 'Update message' })
  @ApiOkResponse({ description: 'Message updated' })
  @UserRoles(UserRole.Announcer)
  update(@Param('id') id: string, @Body() updateMessageDto: UpdateMessageDto, @CurrentUser() currentUser: User) {
    return this.messagesService.update(+id, updateMessageDto, currentUser.id);
  }

  @Delete(':id')
  @ApiOperation({ summary: 'Remove message' })
  @ApiOkResponse({ description: 'Message removed' })
  @UserRoles(UserRole.Announcer)
  remove(@Param('id') id: string) {
    return this.messagesService.remove(+id);
  }
}
