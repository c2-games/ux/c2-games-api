import { faker } from '@faker-js/faker';
import { BadRequestException, NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { createFakeMessage } from '../utils/test-helpers/fakes/message-faker';
import { MessageQueryDto } from './dto/message-query.dto';
import { Message } from './entities/message.entity';
import { MessagesService } from './messages.service';

const currentUserId = faker.datatype.number();

const testMessage = createFakeMessage(currentUserId);

const messageQueryDto = new MessageQueryDto();

describe('MessagesService', () => {
  let service: MessagesService;
  let repo: Repository<Message>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        MessagesService,
        {
          provide: getRepositoryToken(Message),
          useValue: {
            create: jest.fn().mockReturnValue(testMessage),
            save: jest.fn(),
            findAll: jest.fn().mockResolvedValue([testMessage]),
            createQueryBuilder: jest.fn(() => ({
              where: jest.fn().mockReturnThis(),
              andWhere: jest.fn().mockReturnThis(),
              getMany: jest.fn().mockResolvedValue([testMessage]),
            })),
            findOne: jest.fn().mockResolvedValue(testMessage),
            update: jest.fn().mockResolvedValue(true),
            remove: jest.fn().mockResolvedValue(true),
          },
        },
      ],
    }).compile();

    service = module.get<MessagesService>(MessagesService);
    repo = module.get<Repository<Message>>(getRepositoryToken(Message));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should create a new message when provided with valid createMessageDto', async () => {
    const createMessageDto = {
      content: testMessage.content,
      announcedOn: testMessage.announcedOn,
    };

    const message = await service.create(createMessageDto, currentUserId);

    expect(message).toEqual(testMessage);
    expect(repo.create).toBeCalledTimes(1);
    expect(repo.create).toBeCalledWith({
      ...createMessageDto,
      createdByUserId: currentUserId,
      updatedByUserId: currentUserId,
    });
    expect(repo.save).toBeCalledTimes(1);
  });

  it('should find all messages', async () => {
    const messages = await service.findAll(messageQueryDto);

    expect(messages).toEqual([testMessage]);
  });

  it('should find one message', async () => {
    const message = await service.findOne(faker.datatype.number());

    expect(message).toEqual(testMessage);
  });

  it('should throw bad request exception on findOne if id not provided', async () => {
    await expect(() => service.findOne(null)).rejects.toThrow(new BadRequestException('Message ID not provided'));
  });

  it('should throw not found exception on findOne if message not found', async () => {
    const messageId = faker.datatype.number();
    const repoSpy = jest.spyOn(repo, 'findOne').mockReturnValue(undefined);

    await expect(() => service.findOne(messageId)).rejects.toThrow(new NotFoundException('Message not found'));
    expect(repoSpy).toBeCalledTimes(1);
    expect(repoSpy).toBeCalledWith(messageId);
  });

  it('should update message', async () => {
    const messageId = faker.datatype.number();
    const updateMessageDto = { content: 'This content has been updated' };

    const message = await service.update(messageId, updateMessageDto, currentUserId);

    expect(message).toEqual({
      ...testMessage,
      ...updateMessageDto,
    });
    expect(repo.findOne).toBeCalledTimes(1);
    expect(repo.findOne).toBeCalledWith(messageId);
    expect(repo.save).toBeCalledTimes(1);
  });

  it('should remove message', async () => {
    const messageId = faker.datatype.number();

    const message = await service.remove(messageId);

    expect(message).toEqual(testMessage);
  });
});
