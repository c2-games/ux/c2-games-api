import { faker } from '@faker-js/faker';
import { Test, TestingModule } from '@nestjs/testing';
import { KeycloakModule } from '../keycloak/keycloak.module';
import { createFakeMessage } from '../utils/test-helpers/fakes/message-faker';
import { createFakeUser } from '../utils/test-helpers/fakes/user-faker';
import { CreateMessageDto } from './dto/create-message.dto';
import { UpdateMessageDto } from './dto/update-message.dto';
import { MessagesController } from './messages.controller';
import { MessagesService } from './messages.service';

const currentUserId = faker.datatype.number();

const fakeMessage = createFakeMessage(currentUserId);
const fakeUser = createFakeUser(currentUserId);

describe('MessagesController', () => {
  let controller: MessagesController;
  let service: MessagesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [KeycloakModule],
      controllers: [MessagesController],
      providers: [
        {
          provide: MessagesService,
          useValue: {
            create: jest
              .fn()
              .mockImplementation((message: CreateMessageDto, currentUserId: number) =>
                Promise.resolve({ ...fakeMessage, ...message }),
              ),
            findAll: jest.fn().mockResolvedValue([fakeMessage]),
            findOne: jest.fn().mockImplementation((id: number) => Promise.resolve({ ...fakeMessage, id })),
            update: jest
              .fn()
              .mockImplementation((id: number, message: UpdateMessageDto, currentUserId: number) =>
                Promise.resolve({ ...fakeMessage, id }),
              ),
            remove: jest.fn().mockResolvedValue(fakeMessage),
          },
        },
      ],
    }).compile();

    controller = module.get<MessagesController>(MessagesController);
    service = module.get<MessagesService>(MessagesService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should create message', async () => {
    const createMessageDto: CreateMessageDto = {
      content: fakeMessage.content,
      announcedOn: fakeMessage.announcedOn,
    };
    const message = await controller.create(createMessageDto, fakeUser);

    expect(message).toEqual({ ...fakeMessage });
    expect(service.create).toBeCalledTimes(1);
    expect(service.create).toBeCalledWith(createMessageDto, fakeUser.id);
  });

  it('should find all messages', async () => {
    const messages = await controller.findAll();

    expect(messages).toEqual([fakeMessage]);
    expect(service.findAll).toBeCalledTimes(1);
  });

  it('should find one message by id', async () => {
    const message = await controller.findOne(`${fakeMessage.id}`);

    expect(message).toEqual(fakeMessage);
    expect(service.findOne).toBeCalledTimes(1);
    expect(service.findOne).toBeCalledWith(fakeMessage.id);
  });

  it('should update message', async () => {
    const updateMessageDto = {
      content: faker.datatype.string(),
    };
    const message = await controller.update(`${fakeMessage.id}`, updateMessageDto, fakeUser);

    expect(message).toEqual(fakeMessage);
    expect(service.update).toBeCalledTimes(1);
    expect(service.update).toBeCalledWith(fakeMessage.id, updateMessageDto, fakeUser.id);
  });

  it('should remove message', async () => {
    const message = await controller.remove(`${fakeMessage.id}`);

    expect(message).toEqual(fakeMessage);
    expect(service.remove).toBeCalledTimes(1);
    expect(service.remove).toBeCalledWith(fakeMessage.id);
  });
});
