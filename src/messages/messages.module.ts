import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { KeycloakModule } from '../keycloak/keycloak.module';
import { Message } from './entities/message.entity';
import { MessagesController } from './messages.controller';
import { MessagesService } from './messages.service';
import { MessageExistsRule } from './validators/message-exists-rule.validator';

@Module({
  imports: [TypeOrmModule.forFeature([Message]), KeycloakModule],
  controllers: [MessagesController],
  providers: [MessagesService, MessageExistsRule],
})
export class MessagesModule {}
