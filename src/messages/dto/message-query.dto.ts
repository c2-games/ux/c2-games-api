import { ApiProperty } from '@nestjs/swagger';
import { IsDate, IsOptional, IsString } from 'class-validator';

export class MessageQueryDto {
  @ApiProperty({
    type: 'number',
    required: false,
    example: '123',
  })
  @IsString()
  @IsOptional()
  id?: number;

  @ApiProperty({
    type: 'string',
    required: false,
    example: 'Message content',
  })
  @IsString()
  @IsOptional()
  content?: string;

  @ApiProperty({
    type: 'date',
    required: false,
    example: '2022-08-01T20:29:56.561Z',
  })
  @IsDate()
  @IsOptional()
  announcedOn?: Date;
}
