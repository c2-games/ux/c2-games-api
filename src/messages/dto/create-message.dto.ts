import { ApiProperty } from '@nestjs/swagger';
import { IsDate, IsOptional, IsString } from 'class-validator';

export class CreateMessageDto {
  @ApiProperty({
    type: 'string',
    required: true,
    example: 'Message content',
  })
  @IsString()
  content: string;

  @ApiProperty({
    type: 'date',
    required: false,
    example: '2022-08-01T20:29:56.561Z',
  })
  @IsDate()
  @IsOptional()
  announcedOn?: Date;
}
