import { registerDecorator, ValidationOptions } from 'class-validator';
import { MessageExistsRule } from '../validators/message-exists-rule.validator';

export function MessageExists(validationOptions?: ValidationOptions) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      name: 'MessageExists',
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: MessageExistsRule,
    });
  };
}
