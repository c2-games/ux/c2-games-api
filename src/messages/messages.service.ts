import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateMessageDto } from './dto/create-message.dto';
import { MessageQueryDto } from './dto/message-query.dto';
import { UpdateMessageDto } from './dto/update-message.dto';
import { Message } from './entities/message.entity';

@Injectable()
export class MessagesService {
  constructor(@InjectRepository(Message) private repo: Repository<Message>) {}

  async create(createMessageDto: CreateMessageDto, currentUserId = 0) {
    const message = this.repo.create({
      ...createMessageDto,
      createdByUserId: currentUserId,
      updatedByUserId: currentUserId,
    });
    await this.repo.save(message);
    return message;
  }

  findAll(params?: MessageQueryDto) {
    let query = this.repo.createQueryBuilder('messages').where('1=1');
    query = params.id
      ? query.andWhere('messages.id = :id', {
          id: params.id,
        })
      : query;
    query = params.content
      ? query.andWhere('messages.content = :content', {
          content: params.content,
        })
      : query;
    query = params.announcedOn
      ? query.andWhere('messages.announcedOn = :announcedOn', {
          announcedOn: params.announcedOn,
        })
      : query;
    return query.getMany();
  }

  async findOne(id: number) {
    if (!id) {
      throw new BadRequestException('Message ID not provided');
    }
    const message = await this.repo.findOne(id);
    if (!message) {
      throw new NotFoundException('Message not found');
    }
    return message;
  }

  async update(id: number, updateMessageDto: UpdateMessageDto, currentUserId = 0) {
    const message = await this.findOne(id);
    Object.assign(updateMessageDto, { updatedByUserId: currentUserId });
    Object.assign(message, updateMessageDto);
    await this.repo.save(message);
    return message;
  }

  async remove(id: number) {
    const message = await this.findOne(id);
    await this.repo.remove(message);
    return message;
  }
}
