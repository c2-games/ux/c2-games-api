import { faker } from '@faker-js/faker';
import { Test, TestingModule } from '@nestjs/testing';
import { KeycloakModule } from '../keycloak/keycloak.module';
import { createFakeAchievement } from '../utils/test-helpers/fakes/achievement-faker';
import { createFakeUser } from '../utils/test-helpers/fakes/user-faker';
import { AchievementsController } from './achievements.controller';
import { AchievementsService } from './achievements.service';
import { CreateAchievementDto } from './dto/create-achievement.dto';
import { UpdateAchievementDto } from './dto/update-achievement.dto';

const currentUserId = faker.datatype.number();

const fakeAchievement = createFakeAchievement(currentUserId);
const fakeUser = createFakeUser(currentUserId);

describe('AchievementsController', () => {
  let controller: AchievementsController;
  let service: AchievementsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [KeycloakModule],
      controllers: [AchievementsController],
      providers: [
        {
          provide: AchievementsService,
          useValue: {
            create: jest
              .fn()
              .mockImplementation((achievement: CreateAchievementDto, currentUserId: number) =>
                Promise.resolve({ ...fakeAchievement, ...achievement }),
              ),
            findAll: jest.fn().mockResolvedValue([fakeAchievement]),
            findOne: jest.fn().mockImplementation((id: number) => Promise.resolve({ ...fakeAchievement, id })),
            update: jest
              .fn()
              .mockImplementation((id: number, achievement: UpdateAchievementDto, currentUserId: number) =>
                Promise.resolve({ ...fakeAchievement, id }),
              ),
            remove: jest.fn().mockResolvedValue(fakeAchievement),
          },
        },
      ],
    }).compile();

    controller = module.get<AchievementsController>(AchievementsController);
    service = module.get<AchievementsService>(AchievementsService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should create achievement', async () => {
    const createAchievementDto: CreateAchievementDto = {
      title: fakeAchievement.title,
      description: fakeAchievement.description,
    };
    const achievement = await controller.create(createAchievementDto, fakeUser);

    expect(achievement).toEqual({ ...fakeAchievement });
    expect(service.create).toBeCalledTimes(1);
    expect(service.create).toBeCalledWith(createAchievementDto, fakeUser.id);
  });

  it('should find all achievements', async () => {
    const achievements = await controller.findAll();

    expect(achievements).toEqual([fakeAchievement]);
    expect(service.findAll).toBeCalledTimes(1);
  });

  it('should find one achievement by id', async () => {
    const achievement = await controller.findOne(`${fakeAchievement.id}`);

    expect(achievement).toEqual(fakeAchievement);
    expect(service.findOne).toBeCalledTimes(1);
    expect(service.findOne).toBeCalledWith(fakeAchievement.id);
  });

  it('should update achievement', async () => {
    const updateAchievementDto = {
      title: faker.datatype.string(),
    };
    const achievement = await controller.update(`${fakeAchievement.id}`, updateAchievementDto, fakeUser);

    expect(achievement).toEqual(fakeAchievement);
    expect(service.update).toBeCalledTimes(1);
    expect(service.update).toBeCalledWith(fakeAchievement.id, updateAchievementDto, fakeUser.id);
  });

  it('should remove achievement', async () => {
    const achievement = await controller.remove(`${fakeAchievement.id}`);

    expect(achievement).toEqual(fakeAchievement);
    expect(service.remove).toBeCalledTimes(1);
    expect(service.remove).toBeCalledWith(fakeAchievement.id);
  });
});
