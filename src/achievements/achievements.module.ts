import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { KeycloakModule } from '../keycloak/keycloak.module';
import { AchievementsController } from './achievements.controller';
import { AchievementsService } from './achievements.service';
import { Achievement } from './entities/achievement.entity';
import { AchievementExistsRule } from './validators/achievement-exists-rule.validator';

@Module({
  imports: [TypeOrmModule.forFeature([Achievement]), KeycloakModule],
  controllers: [AchievementsController],
  providers: [AchievementsService, AchievementExistsRule],
})
export class AchievementsModule {}
