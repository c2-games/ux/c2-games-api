import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateAchievementDto } from './dto/create-achievement.dto';
import { GetAchievementRequestParamsDto } from './dto/get-achievement-request-params.dto';
import { UpdateAchievementDto } from './dto/update-achievement.dto';
import { Achievement } from './entities/achievement.entity';

@Injectable()
export class AchievementsService {
  constructor(@InjectRepository(Achievement) private repo: Repository<Achievement>) {}

  async create(createAchievementDto: CreateAchievementDto, currentUserId = 0) {
    const achievement = this.repo.create({
      ...createAchievementDto,
      createdByUserId: currentUserId,
      updatedByUserId: currentUserId,
    });
    await this.repo.save(achievement);
    return achievement;
  }

  findAll(params?: GetAchievementRequestParamsDto) {
    let query = this.repo.createQueryBuilder('achievements').where('1=1');
    query = params.title
      ? query.andWhere('achievements.title = :title', {
          title: params.title,
        })
      : query;
    query = params.description
      ? query.andWhere('achievements.description = :description', {
          description: params.description,
        })
      : query;
    return query.getMany();
  }

  async findOne(id: number) {
    if (!id) {
      throw new BadRequestException();
    }
    const achievement = await this.repo.findOne(id);
    if (!achievement) {
      throw new NotFoundException('Achievement not found');
    }
    return achievement;
  }

  async update(id: number, updateAchievementDto: UpdateAchievementDto, currentUserId = 0) {
    const achievement = await this.findOne(id);
    Object.assign(updateAchievementDto, { updatedByUserId: currentUserId });
    Object.assign(achievement, updateAchievementDto);
    await this.repo.save(achievement);
    return achievement;
  }

  async remove(id: number) {
    const achievement = await this.findOne(id);
    await this.repo.remove(achievement);
    return achievement;
  }
}
