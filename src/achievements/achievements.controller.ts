import { Body, Controller, Delete, Get, Param, Patch, Post, Query, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiCreatedResponse, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { UserRoles } from 'src/auth/decorators/roles.decorator';
import { UserRole } from 'src/auth/decorators/user-role.enum';
import { UserRolesGuard } from 'src/auth/guards/user-roles.guard';
import { CurrentUser } from '../users/decorators/current-user.decorator';
import { User } from '../users/entities/user.entity';
import { AchievementsService } from './achievements.service';
import { CreateAchievementDto } from './dto/create-achievement.dto';
import { GetAchievementRequestParamsDto } from './dto/get-achievement-request-params.dto';
import { UpdateAchievementDto } from './dto/update-achievement.dto';

@ApiTags('Achievements')
@ApiBearerAuth()
@Controller('achievements')
@UseGuards(UserRolesGuard)
export class AchievementsController {
  constructor(private readonly achievementsService: AchievementsService) {}

  @Post()
  @ApiOperation({ summary: 'Create achievement' })
  @ApiCreatedResponse({ description: 'Achievement created' })
  @UserRoles(UserRole.AwardAdmin)
  create(@Body() createAchievementDto: CreateAchievementDto, @CurrentUser() currentUser: User) {
    return this.achievementsService.create(createAchievementDto, currentUser.id);
  }

  @Get()
  @ApiOperation({ summary: 'Find all achievements' })
  @ApiOkResponse({ description: 'Achievements found' })
  findAll(@Query() params?: GetAchievementRequestParamsDto) {
    return this.achievementsService.findAll(params);
  }

  @Get(':id')
  @ApiOperation({ summary: 'Find achievement by ID' })
  @ApiOkResponse({ description: 'Achievement found' })
  findOne(@Param('id') id: string) {
    return this.achievementsService.findOne(+id);
  }

  @Patch(':id')
  @ApiOperation({ summary: 'Update achievement' })
  @ApiOkResponse({ description: 'Achievement updated' })
  @UserRoles(UserRole.AwardAdmin)
  update(
    @Param('id') id: string,
    @Body() updateAchievementDto: UpdateAchievementDto,
    @CurrentUser() currentUser: User,
  ) {
    return this.achievementsService.update(+id, updateAchievementDto, currentUser.id);
  }

  @Delete(':id')
  @ApiOperation({ summary: 'Remove achievement' })
  @ApiOkResponse({ description: 'Achievement removed' })
  @UserRoles(UserRole.AwardAdmin)
  remove(@Param('id') id: string) {
    return this.achievementsService.remove(+id);
  }
}
