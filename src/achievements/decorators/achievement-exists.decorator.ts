import { registerDecorator, ValidationOptions } from 'class-validator';
import { AchievementExistsRule } from '../validators/achievement-exists-rule.validator';

export function AchievementExists(validationOptions?: ValidationOptions) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      name: 'AchievementExists',
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: AchievementExistsRule,
    });
  };
}
