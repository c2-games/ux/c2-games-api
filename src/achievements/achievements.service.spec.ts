import { faker } from '@faker-js/faker';
import { BadRequestException, NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { createFakeAchievement } from '../utils/test-helpers/fakes/achievement-faker';
import { AchievementsService } from './achievements.service';
import { GetAchievementRequestParamsDto } from './dto/get-achievement-request-params.dto';
import { Achievement } from './entities/achievement.entity';

const currentUserId = faker.datatype.number();

const testAchievement = createFakeAchievement(currentUserId);

const getAchievementRequestParamsDto = new GetAchievementRequestParamsDto();

describe('AchievementsService', () => {
  let service: AchievementsService;
  let repo: Repository<Achievement>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AchievementsService,
        {
          provide: getRepositoryToken(Achievement),
          useValue: {
            create: jest.fn().mockReturnValue(testAchievement),
            save: jest.fn(),
            findAll: jest.fn().mockResolvedValue([testAchievement]),
            createQueryBuilder: jest.fn(() => ({
              where: jest.fn().mockReturnThis(),
              andWhere: jest.fn().mockReturnThis(),
              getMany: jest.fn().mockResolvedValue([testAchievement]),
            })),
            findOne: jest.fn().mockResolvedValue(testAchievement),
            update: jest.fn().mockResolvedValue(true),
            remove: jest.fn().mockResolvedValue(true),
          },
        },
      ],
    }).compile();

    service = module.get<AchievementsService>(AchievementsService);
    repo = module.get<Repository<Achievement>>(getRepositoryToken(Achievement));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should create a new achievement when provided with valid createAchievementDto', async () => {
    const createAchievementDto = {
      title: testAchievement.title,
      description: testAchievement.description,
    };

    const achievement = await service.create(createAchievementDto, currentUserId);

    expect(achievement).toEqual(testAchievement);
    expect(repo.create).toBeCalledTimes(1);
    expect(repo.create).toBeCalledWith({
      ...createAchievementDto,
      createdByUserId: currentUserId,
      updatedByUserId: currentUserId,
    });
    expect(repo.save).toBeCalledTimes(1);
  });

  it('should find all achievements', async () => {
    const achievements = await service.findAll(getAchievementRequestParamsDto);

    expect(achievements).toEqual([testAchievement]);
  });

  it('should find one achievement', async () => {
    const achievement = await service.findOne(faker.datatype.number());

    expect(achievement).toEqual(testAchievement);
  });

  it('should throw bad request exception on findOne if id not provided', async () => {
    await expect(() => service.findOne(null)).rejects.toThrow(BadRequestException);
  });

  it('should throw not found exception on findOne if achievement not found', async () => {
    const achievementId = faker.datatype.number();
    const repoSpy = jest.spyOn(repo, 'findOne').mockReturnValue(undefined);

    await expect(() => service.findOne(achievementId)).rejects.toThrow(new NotFoundException('Achievement not found'));
    expect(repoSpy).toBeCalledTimes(1);
    expect(repoSpy).toBeCalledWith(achievementId);
  });

  it('should update achievement', async () => {
    const achievementId = faker.datatype.number();
    const updateAchievementDto = { title: 'This title has been updated' };

    const achievement = await service.update(achievementId, updateAchievementDto, currentUserId);

    expect(achievement).toEqual({
      ...testAchievement,
      ...updateAchievementDto,
    });
    expect(repo.findOne).toBeCalledTimes(1);
    expect(repo.findOne).toBeCalledWith(achievementId);
    expect(repo.save).toBeCalledTimes(1);
  });

  it('should remove achievement', async () => {
    const achievementId = faker.datatype.number();

    const achievement = await service.remove(achievementId);

    expect(achievement).toEqual(testAchievement);
  });
});
