import { Injectable } from '@nestjs/common';
import { ValidatorConstraint, ValidatorConstraintInterface } from 'class-validator';
import { AchievementsService } from '../achievements.service';

@ValidatorConstraint({ name: 'AchievementExists', async: true })
@Injectable()
export class AchievementExistsRule implements ValidatorConstraintInterface {
  constructor(private readonly achievementsService: AchievementsService) {}

  async validate(id: number) {
    try {
      await this.achievementsService.findOne(id);
    } catch (error) {
      return false;
    }
    return true;
  }

  defaultMessage() {
    return `Achievement does not exist`;
  }
}
