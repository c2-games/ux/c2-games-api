import { NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { createFakeAchievement } from '../../utils/test-helpers/fakes/achievement-faker';
import { AchievementsService } from '../achievements.service';
import { AchievementExistsRule } from './achievement-exists-rule.validator';

const fakeAchievement = createFakeAchievement();

describe('AchievementExistsRule', () => {
  let achievementExistsRule: AchievementExistsRule;
  let service: AchievementsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AchievementExistsRule,
        {
          provide: AchievementsService,
          useValue: {
            findOne: jest.fn().mockImplementation((id: number) => Promise.resolve(fakeAchievement)),
          },
        },
      ],
    }).compile();

    achievementExistsRule = module.get<AchievementExistsRule>(AchievementExistsRule);
    service = module.get<AchievementsService>(AchievementsService);
  });

  it('should return true when achievement exists', async () => {
    expect(achievementExistsRule.validate(fakeAchievement.id)).resolves.toEqual(true);
    expect(service.findOne).toBeCalledTimes(1);
    expect(service.findOne).toBeCalledWith(fakeAchievement.id);
  });

  it('should return false when achievement does not exist', async () => {
    const findOneSpy = jest.spyOn(service, 'findOne').mockRejectedValueOnce(NotFoundException);
    expect(achievementExistsRule.validate(fakeAchievement.id)).resolves.toEqual(false);
    expect(findOneSpy).toBeCalledTimes(1);
    expect(findOneSpy).toBeCalledWith(fakeAchievement.id);
  });

  it('should return correct default message', () => {
    expect(achievementExistsRule.defaultMessage()).toEqual('Achievement does not exist');
  });
});
