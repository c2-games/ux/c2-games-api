import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

export class GetAchievementRequestParamsDto {
  @ApiProperty({
    type: 'string',
    required: false,
    example: 'Achievement Title',
  })
  @IsString()
  @IsOptional()
  title?: string;

  @ApiProperty({
    type: 'string',
    required: false,
    example: 'This is an achievement description',
  })
  @IsString()
  @IsOptional()
  description?: string;
}
