import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class CreateAchievementDto {
  @ApiProperty({
    type: 'string',
    required: true,
    example: 'Achievement Title',
  })
  @IsString()
  title: string;

  @ApiProperty({
    type: 'string',
    required: true,
    example: 'This is an achievement description',
  })
  @IsString()
  description: string;
}
