import { faker } from '@faker-js/faker';
import { Test, TestingModule } from '@nestjs/testing';
import { KeycloakModule } from '../keycloak/keycloak.module';
import { createFakeEvent } from '../utils/test-helpers/fakes/event-faker';
import { createFakeUser } from '../utils/test-helpers/fakes/user-faker';
import { CreateEventDto } from './dto/create-event.dto';
import { UpdateEventDto } from './dto/update-event.dto';
import { EventsController } from './events.controller';
import { EventsService } from './events.service';

const currentUserId = faker.datatype.number();

const fakeEvent = createFakeEvent(currentUserId);
const fakeUser = createFakeUser(currentUserId);

describe('EventsController', () => {
  let controller: EventsController;
  let service: EventsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [KeycloakModule],
      controllers: [EventsController],
      providers: [
        {
          provide: EventsService,
          useValue: {
            create: jest
              .fn()
              .mockImplementation((event: CreateEventDto, currentUserId: number) =>
                Promise.resolve({ ...fakeEvent, ...event }),
              ),
            findAll: jest.fn().mockResolvedValue([fakeEvent]),
            findOne: jest.fn().mockImplementation((id: number) => Promise.resolve({ ...fakeEvent, id })),
            update: jest
              .fn()
              .mockImplementation((id: number, event: UpdateEventDto, currentUserId: number) =>
                Promise.resolve({ ...fakeEvent, id }),
              ),
            remove: jest.fn().mockResolvedValue(fakeEvent),
          },
        },
      ],
    }).compile();

    controller = module.get<EventsController>(EventsController);
    service = module.get<EventsService>(EventsService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should create event', async () => {
    const createEventDto: CreateEventDto = {
      name: fakeEvent.name,
      startsOn: fakeEvent.startsOn,
    };
    const event = await controller.create(createEventDto, fakeUser);

    expect(event).toEqual({ ...fakeEvent });
    expect(service.create).toBeCalledTimes(1);
    expect(service.create).toBeCalledWith(createEventDto, fakeUser.id);
  });

  it('should find all events', async () => {
    const events = await controller.findAll();

    expect(events).toEqual([fakeEvent]);
    expect(service.findAll).toBeCalledTimes(1);
  });

  it('should find one event by id', async () => {
    const event = await controller.findOne(`${fakeEvent.id}`);

    expect(event).toEqual(fakeEvent);
    expect(service.findOne).toBeCalledTimes(1);
    expect(service.findOne).toBeCalledWith(fakeEvent.id);
  });

  it('should update event', async () => {
    const updateEventDto = {
      name: faker.datatype.string(),
    };
    const event = await controller.update(`${fakeEvent.id}`, updateEventDto, fakeUser);

    expect(event).toEqual(fakeEvent);
    expect(service.update).toBeCalledTimes(1);
    expect(service.update).toBeCalledWith(fakeEvent.id, updateEventDto, fakeUser.id);
  });

  it('should remove event', async () => {
    const event = await controller.remove(`${fakeEvent.id}`);

    expect(event).toEqual(fakeEvent);
    expect(service.remove).toBeCalledTimes(1);
    expect(service.remove).toBeCalledWith(fakeEvent.id);
  });
});
