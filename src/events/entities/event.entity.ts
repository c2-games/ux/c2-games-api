import { Logger } from '@nestjs/common';
import {
  AfterInsert,
  AfterRemove,
  AfterUpdate,
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { EventTeam } from '../../event-teams/entities/event-team.entity';
import { EventTeamEventChecklistItem } from '../../event-team-event-checklist-items/entities/event-team-event-checklist-item.entity';
import { Region, Regions } from '../../institutions/entities/regions';

@Entity({ name: 'events' })
export class Event {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'name' })
  name: string;

  // @Column('array', { name: 'regions', enum: Object.keys(Regions), array: true })
  @Column({ type: 'text', name: 'regions', array: true, enum: Object.keys(Regions), nullable: true })
  regions: Region[];

  @Column({ name: 'starts_on' })
  startsOn: Date;

  @Column({ name: 'enabled', default: true })
  enabled: boolean;

  @Column({ name: 'created_by_user_id', default: 0 })
  createdByUserId: number;

  @CreateDateColumn({
    name: 'created_date',
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
  })
  createdDate: Date;

  @Column({ name: 'updated_by_user_id', default: 0 })
  updatedByUserId: number;

  @Column({ nullable: true })
  rules: string;

  @UpdateDateColumn({
    name: 'updated_date',
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
  })
  updatedDate: Date;

  @OneToMany(() => EventTeamEventChecklistItem, (eventTeamEventChecklistItem) => eventTeamEventChecklistItem.event)
  eventTeamEventChecklistItems: EventTeamEventChecklistItem[];

  @OneToMany(() => EventTeam, (eventTeam) => eventTeam.event)
  eventTeams: EventTeam[];

  @AfterInsert()
  logInsert() {
    Logger.log(`Inserted Event with id ${this.id}`);
  }

  @AfterUpdate()
  logUpdate() {
    Logger.log(`Updated Event with id ${this.id}`);
  }

  @AfterRemove()
  logRemove() {
    Logger.log('Removed Event');
  }
}
