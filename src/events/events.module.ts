import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { KeycloakModule } from '../keycloak/keycloak.module';
import { EventTeamEventChecklistItemsModule } from '../event-team-event-checklist-items/event-team-event-checklist-items.module';
import { Event } from './entities/event.entity';
import { EventsController } from './events.controller';
import { EventsService } from './events.service';
import { EventExistsRule } from './validators/event-exists-rule.validator';

@Module({
  imports: [TypeOrmModule.forFeature([Event]), EventTeamEventChecklistItemsModule, KeycloakModule],
  controllers: [EventsController],
  providers: [EventsService, EventExistsRule],
})
export class EventsModule {}
