import { Body, Controller, Delete, Get, Param, Patch, Post, Query, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiCreatedResponse, ApiOkResponse, ApiOperation, ApiParam, ApiTags } from '@nestjs/swagger';
import { Unprotected } from 'nest-keycloak-connect';
import { UserRoles } from 'src/auth/decorators/roles.decorator';
import { UserRole } from 'src/auth/decorators/user-role.enum';
import { UserRolesGuard } from 'src/auth/guards/user-roles.guard';
import { CurrentUser } from '../users/decorators/current-user.decorator';
import { User } from '../users/entities/user.entity';
import { CreateEventDto } from './dto/create-event.dto';
import { GetEventRequestParamsDto } from './dto/get-event-request-params.dto';
import { UpdateEventDto } from './dto/update-event.dto';
import { EventsService } from './events.service';
import { Region, Regions } from '../institutions/entities/regions';

@ApiTags('Events')
@ApiBearerAuth()
@Controller('events')
@UseGuards(UserRolesGuard)
export class EventsController {
  constructor(private readonly eventsService: EventsService) {}

  @Post()
  @ApiOperation({ summary: 'Create event' })
  @ApiCreatedResponse({ description: 'Event created' })
  @UserRoles(UserRole.EventAdmin)
  create(@Body() createEventDto: CreateEventDto, @CurrentUser() currentUser: User) {
    return this.eventsService.create(createEventDto, currentUser.id);
  }

  @Get()
  @ApiOperation({ summary: 'Find all events' })
  @ApiOkResponse({ description: 'Events found' })
  @Unprotected()
  findAll(@Query() params?: GetEventRequestParamsDto) {
    return this.eventsService.findAll(params);
  }

  @Get('by-region/:region')
  @ApiOperation({ summary: 'Find all event teams by event' })
  @ApiOkResponse({ description: 'Event teams found' })
  @ApiParam({ name: 'region', enum: Object.keys(Regions) })
  @Unprotected()
  findByRegion(@Param('region') region: Region) {
    return this.eventsService.findAll({ region });
  }

  @Get(':id')
  @ApiOperation({ summary: 'Find event by ID' })
  @ApiOkResponse({ description: 'Event found' })
  @Unprotected()
  findOne(@Param('id') id: string) {
    return this.eventsService.findOne(+id);
  }

  @Get(':id/export')
  @ApiOperation({ summary: 'Dump the full information about the event' })
  @ApiOkResponse({ description: 'Event found' })
  @UserRoles(UserRole.EventAdmin)
  exportEvent(@Param('id') id: string) {
    return this.eventsService.exportEvent(+id);
  }

  @Patch(':id')
  @ApiOperation({ summary: 'Update event' })
  @ApiOkResponse({ description: 'Event updated' })
  @UserRoles(UserRole.EventAdmin)
  update(@Param('id') id: string, @Body() updateEventDto: UpdateEventDto, @CurrentUser() currentUser: User) {
    return this.eventsService.update(+id, updateEventDto, currentUser.id);
  }

  @Delete(':id')
  @ApiOperation({ summary: 'Remove event' })
  @ApiOkResponse({ description: 'Event removed' })
  @UserRoles(UserRole.EventAdmin)
  remove(@Param('id') id: string) {
    return this.eventsService.remove(+id);
  }
}
