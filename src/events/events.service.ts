import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { EventTeamEventChecklistItemsService } from '../event-team-event-checklist-items/event-team-event-checklist-items.service';
import { CreateEventDto } from './dto/create-event.dto';
import { GetEventRequestParamsDto } from './dto/get-event-request-params.dto';
import { UpdateEventDto } from './dto/update-event.dto';
import { Event } from './entities/event.entity';

@Injectable()
export class EventsService {
  constructor(
    @InjectRepository(Event) private repo: Repository<Event>,
    private eventTeamEventChecklistItemsService: EventTeamEventChecklistItemsService,
  ) {}

  async create(createEventDto: CreateEventDto, currentUserId = 0) {
    const event = this.repo.create({
      ...createEventDto,
      createdByUserId: currentUserId,
      updatedByUserId: currentUserId,
    });
    await this.repo.save(event);
    return event;
  }

  findAll(params?: GetEventRequestParamsDto) {
    let query = this.repo.createQueryBuilder('events');
    if (params.region) {
      query = query.andWhere('events.regions && :regions', { regions: [params.region] });
    }
    query = params.title
      ? query.andWhere('events.name = :title', {
          title: params.title,
        })
      : query;
    return query.getMany();
  }

  async findOne(id: number) {
    if (!id) {
      throw new BadRequestException();
    }
    const event = await this.repo.findOne(id);
    if (!event) {
      throw new NotFoundException('Event not found');
    }
    return event;
  }

  async exportEvent(id: number) {
    if (!id) {
      throw new BadRequestException();
    }

    const query = this.repo
      .createQueryBuilder('events')
      .leftJoinAndSelect('events.eventTeams', 'eventTeams')
      .orderBy('eventTeams.createdDate')
      .leftJoinAndSelect('eventTeams.eventTeamUsers', 'users')
      .leftJoinAndSelect('users.user', 'user')
      .whereInIds(id);
    const result = await query.getOne();

    if (!result) {
      throw new NotFoundException('Event not found');
    }
    return result;
  }

  async update(id: number, updateEventDto: UpdateEventDto, currentUserId = 0) {
    const event = await this.findOne(id);
    Object.assign(updateEventDto, { updatedByUserId: currentUserId });
    Object.assign(event, updateEventDto);
    await this.repo.save(event);
    return event;
  }

  async remove(id: number) {
    const event = await this.findOne(id);
    const eventTeamEventChecklistItems = await this.eventTeamEventChecklistItemsService.findAll({ eventId: id });
    if (eventTeamEventChecklistItems.length > 0) {
      throw new BadRequestException(
        'Cannot delete Event because it is associated with existing Event Team Event Checklist Items',
      );
    }
    await this.repo.remove(event);
    return event;
  }
}
