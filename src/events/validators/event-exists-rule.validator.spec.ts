import { NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { createFakeEvent } from '../../utils/test-helpers/fakes/event-faker';
import { EventsService } from '../events.service';
import { EventExistsRule } from './event-exists-rule.validator';

const fakeEvent = createFakeEvent();

describe('EventExistsRule', () => {
  let eventExistsRule: EventExistsRule;
  let service: EventsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        EventExistsRule,
        {
          provide: EventsService,
          useValue: {
            findOne: jest.fn().mockImplementation((id: number) => Promise.resolve(fakeEvent)),
          },
        },
      ],
    }).compile();

    eventExistsRule = module.get<EventExistsRule>(EventExistsRule);
    service = module.get<EventsService>(EventsService);
  });

  it('should return true when event exists', async () => {
    expect(eventExistsRule.validate(fakeEvent.id)).resolves.toEqual(true);
    expect(service.findOne).toBeCalledTimes(1);
    expect(service.findOne).toBeCalledWith(fakeEvent.id);
  });

  it('should return false when event does not exist', async () => {
    const findOneSpy = jest.spyOn(service, 'findOne').mockRejectedValueOnce(NotFoundException);
    expect(eventExistsRule.validate(fakeEvent.id)).resolves.toEqual(false);
    expect(findOneSpy).toBeCalledTimes(1);
    expect(findOneSpy).toBeCalledWith(fakeEvent.id);
  });

  it('should return correct default message', () => {
    expect(eventExistsRule.defaultMessage()).toEqual('Event does not exist');
  });
});
