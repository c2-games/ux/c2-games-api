import { Injectable } from '@nestjs/common';
import { ValidatorConstraint, ValidatorConstraintInterface } from 'class-validator';
import { EventsService } from '../events.service';

@ValidatorConstraint({ name: 'EventExists', async: true })
@Injectable()
export class EventExistsRule implements ValidatorConstraintInterface {
  constructor(private readonly eventsService: EventsService) {}

  async validate(id: number) {
    try {
      await this.eventsService.findOne(id);
    } catch (error) {
      return false;
    }
    return true;
  }

  defaultMessage() {
    return `Event does not exist`;
  }
}
