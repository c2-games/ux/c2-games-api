import { faker } from '@faker-js/faker';
import { BadRequestException, NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { EventTeamEventChecklistItemsService } from '../event-team-event-checklist-items/event-team-event-checklist-items.service';
import { createFakeEvent } from '../utils/test-helpers/fakes/event-faker';
import { createFakeEventTeamEventChecklistItem } from '../utils/test-helpers/fakes/event-team-event-checklist-item-faker';
import { GetEventRequestParamsDto } from './dto/get-event-request-params.dto';
import { Event } from './entities/event.entity';
import { EventsService } from './events.service';

const currentUserId = faker.datatype.number();

const testEvent = createFakeEvent(currentUserId);
const testEventTeamEventChecklistItem = createFakeEventTeamEventChecklistItem({
  userId: currentUserId,
});

const getEventRequestParamsDto = new GetEventRequestParamsDto();

describe('EventsService', () => {
  let service: EventsService;
  let eventTeamEventChecklistItemsService: EventTeamEventChecklistItemsService;
  let repo: Repository<Event>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        EventsService,
        {
          provide: getRepositoryToken(Event),
          useValue: {
            create: jest.fn().mockReturnValue(testEvent),
            save: jest.fn(),
            findAll: jest.fn().mockResolvedValue([testEvent]),
            createQueryBuilder: jest.fn(() => ({
              where: jest.fn().mockReturnThis(),
              andWhere: jest.fn().mockReturnThis(),
              getMany: jest.fn().mockResolvedValue([testEvent]),
            })),
            findOne: jest.fn().mockResolvedValue(testEvent),
            update: jest.fn().mockResolvedValue(true),
            remove: jest.fn().mockResolvedValue(true),
          },
        },
        {
          provide: EventTeamEventChecklistItemsService,
          useValue: {
            findAll: jest.fn().mockResolvedValue([]),
          },
        },
      ],
    }).compile();

    service = module.get<EventsService>(EventsService);
    eventTeamEventChecklistItemsService = module.get<EventTeamEventChecklistItemsService>(
      EventTeamEventChecklistItemsService,
    );
    repo = module.get<Repository<Event>>(getRepositoryToken(Event));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should create a new event when provided with valid createEventDto', async () => {
    const createEventDto = {
      name: testEvent.name,
      startsOn: testEvent.startsOn,
    };

    const event = await service.create(createEventDto, currentUserId);

    expect(event).toEqual(testEvent);
    expect(repo.create).toBeCalledTimes(1);
    expect(repo.create).toBeCalledWith({
      ...createEventDto,
      createdByUserId: currentUserId,
      updatedByUserId: currentUserId,
    });
    expect(repo.save).toBeCalledTimes(1);
  });

  it('should find all events', async () => {
    const events = await service.findAll(getEventRequestParamsDto);

    expect(events).toEqual([testEvent]);
  });

  it('should find one event', async () => {
    const event = await service.findOne(faker.datatype.number());

    expect(event).toEqual(testEvent);
  });

  it('should throw bad request exception on findOne if id not provided', async () => {
    await expect(() => service.findOne(null)).rejects.toThrow(BadRequestException);
  });

  it('should throw not found exception on findOne if event not found', async () => {
    const eventId = faker.datatype.number();
    const repoSpy = jest.spyOn(repo, 'findOne').mockReturnValue(undefined);

    await expect(() => service.findOne(eventId)).rejects.toThrow(new NotFoundException('Event not found'));
    expect(repoSpy).toBeCalledTimes(1);
    expect(repoSpy).toBeCalledWith(eventId);
  });

  it('should update event', async () => {
    const eventId = faker.datatype.number();
    const updateEventDto = { name: 'This name has been updated' };

    const event = await service.update(eventId, updateEventDto, currentUserId);

    expect(event).toEqual({
      ...testEvent,
      ...updateEventDto,
    });
    expect(repo.findOne).toBeCalledTimes(1);
    expect(repo.findOne).toBeCalledWith(eventId);
    expect(repo.save).toBeCalledTimes(1);
  });

  it('should remove event', async () => {
    const eventId = faker.datatype.number();

    const event = await service.remove(eventId);

    expect(event).toEqual(testEvent);
  });

  it('should throw bad request exception on remove if event has user event checklist items', async () => {
    jest.spyOn(eventTeamEventChecklistItemsService, 'findAll').mockResolvedValue([testEventTeamEventChecklistItem]);
    await expect(() => service.remove(testEvent.id)).rejects.toThrow(
      new BadRequestException(
        'Cannot delete Event because it is associated with existing Event Team Event Checklist Items',
      ),
    );
  });
});
