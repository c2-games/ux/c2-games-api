import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsBoolean, IsDate, IsOptional, IsString } from 'class-validator';
import { Region, Regions } from '../../institutions/entities/regions';

export class UpdateEventDto {
  @ApiProperty({
    type: 'string',
    required: true,
    example: 'Event Name',
  })
  @IsString()
  @IsOptional()
  name?: string;

  @ApiProperty({
    type: 'string',
    required: false,
    description: 'Link to rules for the event',
    example: 'https://www.ncaecybergames.org/rules',
  })
  @IsString()
  @IsOptional()
  rules: string;

  @ApiProperty({
    type: 'array',
    items: {
      type: 'string',
    },
    required: true,
    enum: Object.keys(Regions),
  })
  @IsArray()
  @IsOptional()
  regions: Region[];

  @ApiProperty({
    type: 'date',
    required: true,
    example: '2022-08-01T20:29:56.561Z',
  })
  @IsDate()
  @IsOptional()
  startsOn?: Date;

  @ApiProperty({
    type: 'boolean',
    required: false,
    example: 'true',
  })
  @IsBoolean()
  @IsOptional()
  enabled?: boolean;
}
