import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';
import { Region, Regions } from '../../institutions/entities/regions';

export class GetEventRequestParamsDto {
  @ApiProperty({
    type: 'string',
    required: false,
    example: 'Event Name',
  })
  @IsString()
  @IsOptional()
  title?: string;

  @ApiProperty({
    type: 'string',
    required: false,
    example: 'northeast',
    enum: Object.keys(Regions),
  })
  @IsString()
  @IsOptional()
  region?: Region;
}
