import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsOptional } from 'class-validator';
import { EventExists } from '../decorators/event-exists.decorator';

export class EventIdParam {
  @ApiProperty({
    type: 'number',
    required: false,
    example: '123',
  })
  @IsInt()
  @EventExists()
  @IsOptional()
  eventId?: number;
}
