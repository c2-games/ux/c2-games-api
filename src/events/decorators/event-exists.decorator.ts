import { registerDecorator, ValidationOptions } from 'class-validator';
import { EventExistsRule } from '../validators/event-exists-rule.validator';

export function EventExists(validationOptions?: ValidationOptions) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      name: 'EventExists',
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: EventExistsRule,
    });
  };
}
