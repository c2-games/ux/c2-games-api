import { BadRequestException, ForbiddenException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindOneOptions, Repository } from 'typeorm';
import { CreateEventTeamDto } from './dto/create-event-team.dto';
import { GetEventTeamsRequestParamsDto } from './dto/get-event-team-request-params.dto';
import { UpdateEventTeamDto } from './dto/update-event-team.dto';
import { EventTeam } from './entities/event-team.entity';
import { User } from '../users/entities/user.entity';
import { GetDomainTeamsDto } from './dto/get-domain-teams.dto';
import { UserRole } from '../auth/decorators/user-role.enum';
import { UserRolesGuard } from '../auth/guards/user-roles.guard';

@Injectable()
export class EventTeamsService {
  constructor(
    @InjectRepository(EventTeam) private repo: Repository<EventTeam>,
    private readonly roleGuard: UserRolesGuard,
  ) {}

  /**
   * Add relevant relations to a QueryBuilder object
   * @param query
   */
  _includeRelations(query) {
    // include team members
    query.leftJoinAndSelect('eventTeams.eventTeamUsers', 'users');
    // include user details for members
    query.leftJoinAndSelect('users.user', 'user');
    // include event information
    query.leftJoinAndSelect('eventTeams.event', 'event');
    // include a count of the number of members on a team (possibly produces a second query)
    query.loadRelationCountAndMap('eventTeams.memberCount', 'eventTeams.eventTeamUsers', 'memberCount');
  }

  async userCanModifyTeam(user: User, teamId: number, autoError = true) {
    // Event Admins can modify any team
    if (this.roleGuard.getRoles(user).includes(UserRole.EventAdmin)) return true;

    // check if this user owns this team
    const team = await this.findOne({ where: { id: teamId }, relations: ['event'] }, autoError);

    const currentDate = new Date();
    const diffTime = team.event.startsOn.getTime() - currentDate.getTime();
    const diffDays = diffTime / (1000 * 3600 * 24);
    // Check if the event is closer than 2 days
    if (diffDays < 2) {
      throw new ForbiddenException('Roster modifications are forbidden within 2 days of an event');
      return false;
    }

    if (team.createdByUserId !== user.id) {
      if (autoError) throw new ForbiddenException('user cannot modify this team');
      else return false;
    }
    return true;
  }

  async create(createEventTeamDto: CreateEventTeamDto, currentUserId = 0) {
    const eventTeam = this.repo.create({
      ...createEventTeamDto,
      createdByUserId: currentUserId,
      updatedByUserId: currentUserId,
    });
    await this.repo.save(eventTeam);
    return eventTeam;
  }

  async findAll(params?: GetEventTeamsRequestParamsDto) {
    let query = this.repo.createQueryBuilder('eventTeams');
    if (params?.eventId) {
      query = query.andWhere('eventTeams.eventId = :eventId', {
        eventId: params.eventId,
      });
    }
    if (params?.region) {
      query = query.andWhere(':region = ANY(event.regions)', {
        region: params.region,
      });
    }

    this._includeRelations(query);
    let results = await query.getMany();

    // Filtering by number of team, members is a real pain
    // with TypeORM and SQL, so, instead we filter here in JS.
    if (params?.minMembers !== undefined) {
      results = results.filter((t) => t.memberCount >= params.minMembers);
    }

    if (params?.maxMembers !== undefined) {
      results = results.filter((t) => t.memberCount <= params.maxMembers);
    }

    return results;
  }

  async count(options: FindOneOptions<EventTeam>) {
    return this.repo.count(options);
  }

  async findOne(options: FindOneOptions, autoError = true): Promise<EventTeam | undefined> {
    const eventTeam = await this.repo.findOne(options);
    if (!eventTeam && autoError) {
      throw new NotFoundException('Event Team not found');
    }
    return eventTeam;
  }

  async findOneById(id: number) {
    if (!id) {
      throw new BadRequestException('Event Team ID not provided');
    }
    const query = this.repo.createQueryBuilder('eventTeams');
    this._includeRelations(query);
    query.andWhereInIds([id]);
    const eventTeam = await query.getOneOrFail();
    if (!eventTeam) {
      throw new NotFoundException('Event Team not found');
    }
    return eventTeam;
  }

  async findTeamByDomain(params?: GetDomainTeamsDto) {
    let query = this.repo.createQueryBuilder('et');
    const teams = await query
      .innerJoinAndSelect(
        'event_teams_x_users',
        'etxu',
        'et.id = etxu.event_team_id AND et.created_by_user_id = etxu.user_id',
      )
      .innerJoinAndSelect('users', 'u', 'etxu.user_id = u.id')
      .select(['u.email AS email', 'et.name AS name', 'et.id as id'])
      .where('u.email LIKE :domain', { domain: `%${params.domain}` })
      .getRawMany();
    query = this.repo.createQueryBuilder('eventTeams').andWhereInIds(teams.map((t) => t.id));
    this._includeRelations(query);
    return query.getMany();
  }

  async update(id: number, updateEventTeamDto: UpdateEventTeamDto, currentUserId = 0) {
    const eventTeam = await this.findOneById(id);
    Object.assign(updateEventTeamDto, { updatedByUserId: currentUserId });
    Object.assign(eventTeam, updateEventTeamDto);
    await this.repo.save(eventTeam);
    return eventTeam;
  }

  async remove(id: number) {
    const eventTeam = await this.findOneById(id);
    await this.repo.remove(eventTeam);
    return eventTeam;
  }
}
