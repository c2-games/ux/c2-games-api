import { Logger } from '@nestjs/common';
import {
  AfterInsert,
  AfterRemove,
  AfterUpdate,
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { EventTeamEventChecklistItem } from '../../event-team-event-checklist-items/entities/event-team-event-checklist-item.entity';
import { EventTeamUser } from '../../event-team-users/entities/event-team-user.entity';
import { Event } from '../../events/entities/event.entity';

@Entity({ name: 'event_teams' })
export class EventTeam {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'event_id' })
  eventId: number;

  @Column({ name: 'name' })
  name: string;

  @Column({ name: 'enabled', default: true })
  enabled: boolean;

  @Column({ name: 'created_by_user_id', default: 0 })
  createdByUserId: number;

  @CreateDateColumn({
    name: 'created_date',
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
  })
  createdDate: Date;

  @Column({ name: 'updated_by_user_id', default: 0 })
  updatedByUserId: number;

  @UpdateDateColumn({
    name: 'updated_date',
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
  })
  updatedDate: Date;

  @ManyToOne(() => Event)
  @JoinColumn({ name: 'event_id', referencedColumnName: 'id' })
  event: Event;

  @OneToMany(
    () => EventTeamEventChecklistItem,
    (eventTeamEventChecklistItems) => eventTeamEventChecklistItems.eventTeam,
  )
  eventTeamEventChecklistItems: EventTeamEventChecklistItem[];

  @OneToMany(() => EventTeamUser, (eventTeamUser) => eventTeamUser.eventTeam)
  eventTeamUsers: EventTeamUser[];

  // memberCount is sometimes populated using loadRelationCountAndMap, but is not guaranteed
  memberCount?: number;

  @AfterInsert()
  logInsert() {
    Logger.log(`Inserted Event Team with id ${this.id}`);
  }

  @AfterUpdate()
  logUpdate() {
    Logger.log(`Updated Event Team with id ${this.id}`);
  }

  @AfterRemove()
  logRemove() {
    Logger.log('Removed Event Team');
  }
}
