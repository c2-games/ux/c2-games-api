import { Injectable } from '@nestjs/common';
import { ValidatorConstraint, ValidatorConstraintInterface } from 'class-validator';
import { EventTeamsService } from '../event-teams.service';

@ValidatorConstraint({ name: 'EventTeamExists', async: true })
@Injectable()
export class EventTeamExistsRule implements ValidatorConstraintInterface {
  constructor(private readonly eventTeamsService: EventTeamsService) {}

  async validate(id: number) {
    try {
      await this.eventTeamsService.findOneById(id);
    } catch (error) {
      return false;
    }
    return true;
  }

  defaultMessage() {
    return `Event Team does not exist`;
  }
}
