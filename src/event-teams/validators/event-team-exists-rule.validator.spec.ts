import { NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { createFakeEventTeam } from '../../utils/test-helpers/fakes/event-team-faker';
import { EventTeamsService } from '../event-teams.service';
import { EventTeamExistsRule } from './event-team-exists-rule.validator';

const fakeEventTeam = createFakeEventTeam();

describe('EventTeamExistsRule', () => {
  let eventTeamExistsRule: EventTeamExistsRule;
  let service: EventTeamsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        EventTeamExistsRule,
        {
          provide: EventTeamsService,
          useValue: {
            findOne: jest.fn().mockImplementation((id: number) => Promise.resolve(fakeEventTeam)),
          },
        },
      ],
    }).compile();

    eventTeamExistsRule = module.get<EventTeamExistsRule>(EventTeamExistsRule);
    service = module.get<EventTeamsService>(EventTeamsService);
  });

  it('should return true when event team exists', async () => {
    expect(eventTeamExistsRule.validate(fakeEventTeam.id)).resolves.toEqual(true);
    expect(service.findOneById).toBeCalledTimes(1);
    expect(service.findOneById).toBeCalledWith(fakeEventTeam.id);
  });

  it('should return false when event team does not exist', async () => {
    const findOneSpy = jest.spyOn(service, 'findOneById').mockRejectedValueOnce(NotFoundException);
    expect(eventTeamExistsRule.validate(fakeEventTeam.id)).resolves.toEqual(false);
    expect(findOneSpy).toBeCalledTimes(1);
    expect(findOneSpy).toBeCalledWith(fakeEventTeam.id);
  });

  it('should return correct default message', () => {
    expect(eventTeamExistsRule.defaultMessage()).toEqual('Event Team does not exist');
  });
});
