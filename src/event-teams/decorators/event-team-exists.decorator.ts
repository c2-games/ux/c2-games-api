import { registerDecorator, ValidationOptions } from 'class-validator';
import { EventTeamExistsRule } from '../validators/event-team-exists-rule.validator';

export function EventTeamExists(validationOptions?: ValidationOptions) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      name: 'EventTeamExists',
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: EventTeamExistsRule,
    });
  };
}
