import { faker } from '@faker-js/faker';
import { BadRequestException, NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { createFakeEventTeam } from '../utils/test-helpers/fakes/event-team-faker';
import { GetEventTeamsRequestParamsDto } from './dto/get-event-team-request-params.dto';
import { EventTeam } from './entities/event-team.entity';
import { EventTeamsService } from './event-teams.service';

const currentUserId = faker.datatype.number();

const testEventTeam = createFakeEventTeam(currentUserId);

const getEventTeamsRequestParamsDto = new GetEventTeamsRequestParamsDto();

describe('EventTeamsService', () => {
  let service: EventTeamsService;
  let repo: Repository<EventTeam>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        EventTeamsService,
        {
          provide: getRepositoryToken(EventTeam),
          useValue: {
            create: jest.fn().mockReturnValue(testEventTeam),
            save: jest.fn(),
            findAll: jest.fn().mockResolvedValue([testEventTeam]),
            createQueryBuilder: jest.fn(() => ({
              where: jest.fn().mockReturnThis(),
              andWhere: jest.fn().mockReturnThis(),
              innerJoinAndSelect: jest.fn().mockReturnThis(),
              getMany: jest.fn().mockResolvedValue([testEventTeam]),
            })),
            findOne: jest.fn().mockResolvedValue(testEventTeam),
            update: jest.fn().mockResolvedValue(true),
            remove: jest.fn().mockResolvedValue(true),
          },
        },
      ],
    }).compile();

    service = module.get<EventTeamsService>(EventTeamsService);
    repo = module.get<Repository<EventTeam>>(getRepositoryToken(EventTeam));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should create a new event team when provided with valid createEventTeamDto', async () => {
    const createEventTeamDto = {
      name: testEventTeam.name,
      description: testEventTeam,
    };

    const eventTeam = await service.create(createEventTeamDto, currentUserId);

    expect(eventTeam).toEqual(testEventTeam);
    expect(repo.create).toBeCalledTimes(1);
    expect(repo.create).toBeCalledWith({
      ...createEventTeamDto,
      createdByUserId: currentUserId,
      updatedByUserId: currentUserId,
    });
    expect(repo.save).toBeCalledTimes(1);
  });

  it('should find all event teams', async () => {
    const eventTeams = await service.findAll(getEventTeamsRequestParamsDto);

    expect(eventTeams).toEqual([testEventTeam]);
  });

  it('should find one event team', async () => {
    const eventTeam = await service.findOneById(faker.datatype.number());

    expect(eventTeam).toEqual(testEventTeam);
  });

  it('should find event for team for user with same domain', async () => {
    const eventTeam = await service.findTeamByDomain({ domain: 'gmail.com' });

    expect(eventTeam).toEqual([testEventTeam]);
  });

  it('should throw bad request exception on findOne if id not provided', async () => {
    await expect(() => service.findOneById(null)).rejects.toThrow(BadRequestException);
  });

  it('should throw not found exception on findOne if event team not found', async () => {
    const eventTeamId = faker.datatype.number();
    const repoSpy = jest.spyOn(repo, 'findOne').mockReturnValue(undefined);

    await expect(() => service.findOneById(eventTeamId)).rejects.toThrow(new NotFoundException('Event Team not found'));
    expect(repoSpy).toBeCalledTimes(1);
    expect(repoSpy).toBeCalledWith(eventTeamId);
  });

  it('should update event team', async () => {
    const eventTeamId = faker.datatype.number();
    const updateEventTeamDto = { name: 'This name has been updated' };

    const eventTeam = await service.update(eventTeamId, updateEventTeamDto, currentUserId);

    expect(eventTeam).toEqual({
      ...testEventTeam,
      ...updateEventTeamDto,
    });
    expect(repo.findOne).toBeCalledTimes(1);
    expect(repo.findOne).toBeCalledWith(eventTeamId);
    expect(repo.save).toBeCalledTimes(1);
  });

  it('should remove event team', async () => {
    const eventTeamId = faker.datatype.number();

    const eventTeam = await service.remove(eventTeamId);

    expect(eventTeam).toEqual(testEventTeam);
  });
});
