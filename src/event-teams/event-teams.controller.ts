import {
  BadRequestException,
  Body,
  ConflictException,
  Controller,
  Delete,
  Get,
  Header,
  NotFoundException,
  Param,
  Patch,
  Post,
  Query,
  StreamableFile,
  UseGuards,
} from '@nestjs/common';
import { ApiBearerAuth, ApiCreatedResponse, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { EventIdParam } from '../events/dto/event-id-param';
import { CurrentUser } from '../users/decorators/current-user.decorator';
import { User } from '../users/entities/user.entity';
import { CopyTeamParam, EventTeamIdParam } from './dto/event-team-id-param';
import { UpdateEventTeamDto } from './dto/update-event-team.dto';
import { EventTeamsService } from './event-teams.service';
import { InstitutionsService } from '../institutions/institutions.service';
import { EventsService } from '../events/events.service';
import { UserRoles } from '../auth/decorators/roles.decorator';
import { UserRole } from '../auth/decorators/user-role.enum';
import { UserRolesGuard } from '../auth/guards/user-roles.guard';
import { EventTeamUsersService } from '../event-team-users/event-team-users.service';
import { GetEventTeamsRequestParamsDto } from './dto/get-event-team-request-params.dto';
import { Parser } from 'json2csv';
import { parseEmailDomains } from '../utils';

@ApiTags('Event Teams')
@ApiBearerAuth()
@Controller()
@UseGuards(UserRolesGuard)
export class EventTeamsController {
  constructor(
    private readonly eventTeamsService: EventTeamsService,
    private readonly eventTeamUsersService: EventTeamUsersService,
    private readonly eventsService: EventsService,
    private readonly institutionsService: InstitutionsService,
  ) {}
  @Get('events/teams/domain')
  @ApiOperation({ summary: 'Find teams with same domain' })
  @ApiOkResponse({ description: 'Existing teams found with same domain' })
  async findAllTeamsByDomain(@CurrentUser() user: User) {
    const { institutionDomain } = parseEmailDomains(user.email);
    return await this.eventTeamsService.findTeamByDomain({ domain: institutionDomain });
  }

  @Get('teams')
  @ApiOperation({ summary: 'Get all teams with events' })
  @ApiOkResponse({ description: 'All Event Teams' })
  @UserRoles(UserRole.Staff)
  async findAll(@Query() params?: GetEventTeamsRequestParamsDto) {
    return this.eventTeamsService.findAll(params);
  }

  async sendCsv(data: unknown[], fields: string[]) {
    const parser = new Parser({ fields });
    const csv = parser.parse(data);
    return new StreamableFile(Buffer.from(csv, 'utf8'));
  }

  @Get('teams/export/summary')
  @ApiOperation({ summary: 'Export Team Summary' })
  @ApiOkResponse({ description: 'Export a CSV Summary of Team Registrations' })
  @Header('Content-Type', 'application/csv')
  @Header('Content-Disposition', 'attachment; filename="team-summary.csv"')
  @UserRoles(UserRole.Staff)
  async export(@Query() params?: GetEventTeamsRequestParamsDto): Promise<StreamableFile> {
    const rawData = await this.eventTeamsService.findAll(params);
    const fields = [
      'Name',
      'Last Update',
      'Member Count',
      'Event Name',
      'Event Date',
      'Regions',
      'Team Captain',
      'Members',
    ];
    const formattedData = rawData.map((team) => {
      return {
        Name: team.name,
        'Last Update': team.updatedDate,
        'Member Count': team.memberCount,
        'Event Name': team.event.name,
        'Event Date': team.event.startsOn,
        Regions: team.event.regions.join(', '),
        'Team Captain': team.eventTeamUsers.find((u) => u.userId === team.createdByUserId)?.user?.email,
        Members: team.eventTeamUsers.map((u) => u.user.email).join(';'),
      };
    });
    return this.sendCsv(formattedData, fields);
  }

  @Post('events/:eventId/teams')
  @ApiOperation({ summary: 'Create event team' })
  @ApiCreatedResponse({ description: 'Event team created' })
  async create(@Param() param: EventIdParam, @Query('institutionId') institutionId: number, @CurrentUser() user: User) {
    const { eventId } = param;
    const { institutionDomain, emailDomain } = parseEmailDomains(user.email);
    // first check for the email domain itself
    let institutionList = await this.institutionsService.findAll({ domain: emailDomain });
    let institution;

    if (institutionList.length === 0) {
      // if no results are found, try just the primary domain - maybe their email is `my.<school>.edu` or similar
      institutionList = await this.institutionsService.findAll({ domain: institutionDomain });
      // still nothing? okay, error
      if (institutionList.length === 0)
        throw new NotFoundException('Could not find institution associated with this email, please contact support!');
    }

    // if more than one institution was found, we're not sure what institution this user belongs to
    if (institutionList.length > 1) {
      // if the user elected an institution ID, check if it's found in the list of matching institutions
      if (institutionId) institution = institutionList.find((i) => i.id === institutionId);
      // If it's not found, have the user contact support
      if (!institution)
        throw new NotFoundException('Multiple institutions associated with this email, please contact support!');
    } else {
      // Otherwise, we found the only matching institution
      institution = institutionList[0];
    }

    const event = await this.eventsService.findOne(eventId);

    // verify user's region is appropriate for the event
    if (!event.regions.includes(institution.region))
      throw new BadRequestException("User's Institution does not belong to this region");

    // check if event it at capacity
    const teamCount = await this.eventTeamsService.count({ where: { eventId } });
    if (teamCount >= 12) throw new ConflictException('Event is at capacity - check back later for new openings');

    // Check if the event is closer than 9 days
    const currentDate = new Date();
    const diffTime = event.startsOn.getTime() - currentDate.getTime();
    const diffDays = diffTime / (1000 * 3600 * 24);

    if (diffDays < 9) {
      throw new BadRequestException('Event is closed for new team registration');
    }

    // check if user is on a team for the given event
    const exists = await this.eventTeamUsersService.findOne(
      {
        where: {
          userId: user.id,
          eventTeam: {
            eventId,
          },
        },
        relations: ['eventTeam'],
      },
      false,
    );

    if (exists) {
      throw new ConflictException('user is already on a team for this event');
    }

    return this.eventTeamsService.create({ eventId, name: institution.name }, user.id);
  }

  @Get('events/:eventId/teams/count')
  @ApiOperation({ summary: 'Get number of teams registered for an event' })
  @ApiOkResponse({ description: 'Number of teams registered for an event' })
  async eventTeamCount(@Param() param: EventIdParam) {
    return {
      count: await this.eventTeamsService.count({ where: { eventId: param.eventId } }),
    };
  }

  @Get('events/:eventId/teams')
  @ApiOperation({ summary: 'Find all event teams by event' })
  @ApiOkResponse({ description: 'Event teams found' })
  findAllByEvent(@Param() param: EventIdParam) {
    const { eventId } = param;
    return this.eventTeamsService.findAll({ eventId });
  }

  @Get('events/teams/:eventTeamId')
  @ApiOperation({ summary: 'Find event team by ID' })
  @ApiOkResponse({ description: 'Event team found' })
  findOne(@Param() param: EventTeamIdParam) {
    const { eventTeamId } = param;
    return this.eventTeamsService.findOneById(eventTeamId);
  }

  @Post('events/teams/:eventTeamId/copy/:eventId')
  @ApiOperation({ summary: 'Copy a team' })
  @ApiOkResponse({ description: 'Copy a team into a new event' })
  @UserRoles(UserRole.EventAdmin)
  async copyTeam(@Param() param: CopyTeamParam, @CurrentUser() currentUser: User) {
    const { eventTeamId, eventId } = param;
    // get old team for name and members
    const oldTeam = await this.eventTeamsService.findOneById(eventTeamId);
    // create a new team
    const newTeam = await this.eventTeamsService.create(
      {
        eventId,
        name: oldTeam.name,
      },
      // pass previous created by to track the team captain
      oldTeam.createdByUserId,
    );
    for (const user of oldTeam.eventTeamUsers) {
      await this.eventTeamUsersService.create(
        {
          eventTeamId: newTeam.id,
          userId: user.userId,
        },
        // use the actual id of the person copying the team here
        currentUser.id,
      );
    }
    return this.findOne({ eventTeamId: newTeam.id });
  }

  @Patch('events/teams/:eventTeamId')
  @ApiOperation({ summary: 'Update event team' })
  @ApiOkResponse({ description: 'Event team updated' })
  async update(
    @Param() param: EventTeamIdParam,
    @Body() updateEventTeamDto: UpdateEventTeamDto,
    @CurrentUser() user: User,
  ) {
    const { eventTeamId } = param;
    await this.eventTeamsService.userCanModifyTeam(user, eventTeamId);
    return this.eventTeamsService.update(eventTeamId, updateEventTeamDto, user.id);
  }

  @Delete('events/teams/:eventTeamId')
  @ApiOperation({ summary: 'Remove event team' })
  @ApiOkResponse({ description: 'Event team removed' })
  async remove(@Param() param: EventTeamIdParam, @CurrentUser() user: User) {
    const { eventTeamId } = param;
    await this.eventTeamsService.userCanModifyTeam(user, eventTeamId);
    // remove users of a team first
    await this.eventTeamUsersService.removeByTeamId(eventTeamId);
    // then remove the team itself
    return this.eventTeamsService.remove(eventTeamId);
  }
}
