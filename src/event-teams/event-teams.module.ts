import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { KeycloakModule } from '../keycloak/keycloak.module';
import { EventTeam } from './entities/event-team.entity';
import { EventTeamsController } from './event-teams.controller';
import { EventTeamsService } from './event-teams.service';
import { EventTeamExistsRule } from './validators/event-team-exists-rule.validator';
import { InstitutionsService } from '../institutions/institutions.service';
import { Event } from '../events/entities/event.entity';
import { EventsService } from '../events/events.service';
import { Institution } from '../institutions/entities/institution.entity';
import { EventTeamEventChecklistItemsService } from '../event-team-event-checklist-items/event-team-event-checklist-items.service';
import { EventTeamEventChecklistItem } from '../event-team-event-checklist-items/entities/event-team-event-checklist-item.entity';
import { EventTeamUsersService } from '../event-team-users/event-team-users.service';
import { EventTeamUser } from '../event-team-users/entities/event-team-user.entity';
import { UserRolesGuard } from '../auth/guards/user-roles.guard';
import { User } from '../users/entities/user.entity';
@Module({
  imports: [
    TypeOrmModule.forFeature([EventTeam, EventTeamUser, Event, Institution, EventTeamEventChecklistItem, User]),
    KeycloakModule,
  ],
  controllers: [EventTeamsController],
  providers: [
    EventTeamsService,
    EventTeamUsersService,
    EventTeamExistsRule,
    EventsService,
    InstitutionsService,
    EventTeamEventChecklistItemsService,
    UserRolesGuard,
  ],
  exports: [EventTeamsService],
})
export class EventTeamsModule {}
