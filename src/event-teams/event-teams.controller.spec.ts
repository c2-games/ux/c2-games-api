import { faker } from '@faker-js/faker';
import { Test, TestingModule } from '@nestjs/testing';
import { EventIdParam } from '../events/dto/event-id-param';
import { KeycloakModule } from '../keycloak/keycloak.module';
import { createFakeEventTeam } from '../utils/test-helpers/fakes/event-team-faker';
import { createFakeUser } from '../utils/test-helpers/fakes/user-faker';
import { PostEventTeamRequestBodyDto } from './dto/post-event-team-request-body.dto';
import { UpdateEventTeamDto } from './dto/update-event-team.dto';
import { EventTeamsController } from './event-teams.controller';
import { EventTeamsService } from './event-teams.service';
import { InstitutionsService } from '../institutions/institutions.service';
import { createFakeInstitution } from '../utils/test-helpers/fakes/institution-faker';
import { EventsService } from '../events/events.service';
import { createFakeEvent } from '../utils/test-helpers/fakes/event-faker';

const currentUserId = faker.datatype.number();

const fakeEventTeam = createFakeEventTeam(currentUserId);
const fakeEvent = createFakeEvent();
const fakeUser = createFakeUser(currentUserId, 'user@mvcc.edu');
const fakeInstitutions = [createFakeInstitution('mvcc.edu'), createFakeInstitution('my.gatech.edu')];

describe('EventTeamsController', () => {
  let controller: EventTeamsController;
  let service: EventTeamsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [KeycloakModule],
      controllers: [EventTeamsController],
      providers: [
        {
          provide: EventTeamsService,
          useValue: {
            create: jest
              .fn()
              .mockImplementation(
                (param: EventIdParam, eventTeam: PostEventTeamRequestBodyDto, currentUserId: number) =>
                  Promise.resolve({
                    ...fakeEventTeam,
                    ...eventTeam,
                    eventId: param.eventId,
                  }),
              ),
            findAll: jest.fn().mockResolvedValue([fakeEventTeam]),
            findOne: jest.fn().mockReturnValue(null),
            userCanModifyTeam: jest.fn().mockReturnValue(true),
            findByUserDomain: jest.fn().mockResolvedValue([fakeEventTeam]),
            findOneById: jest.fn().mockImplementation((id: number) => {
              return {
                ...fakeEventTeam,
                id,
              };
            }),
            update: jest
              .fn()
              .mockImplementation((id: number, eventTeam: UpdateEventTeamDto, currentUserId: number) =>
                Promise.resolve({ ...fakeEventTeam, id }),
              ),
            remove: jest.fn().mockResolvedValue(fakeEventTeam),
          },
        },
        {
          provide: InstitutionsService,
          useValue: {
            findAll: ({ domain }) => fakeInstitutions.filter((i) => i.domain === domain),
          },
        },
        {
          provide: EventsService,
          useValue: {
            findOne: jest.fn().mockImplementation((id: number) => Promise.resolve({ ...fakeEvent, id })),
          },
        },
      ],
    }).compile();

    controller = module.get<EventTeamsController>(EventTeamsController);
    service = module.get<EventTeamsService>(EventTeamsService);
  });

  describe('should allow users to sign up when', () => {
    it('user email matches primary domain', async () => {
      const param: EventIdParam = { eventId: fakeEventTeam.eventId };
      const user = createFakeUser(faker.datatype.number(), 'user@mvcc.edu');

      await controller.create(param, user);

      expect(service.create).toBeCalledTimes(1);
    });

    it('user email shares primary domain', async () => {
      const param: EventIdParam = { eventId: fakeEventTeam.eventId };
      const user = createFakeUser(faker.datatype.number(), 'user@my.mvcc.edu');

      await controller.create(param, user);

      expect(service.create).toBeCalledTimes(1);
    });

    it('user email shares full domain', async () => {
      const param: EventIdParam = { eventId: fakeEventTeam.eventId };
      const user = createFakeUser(faker.datatype.number(), 'user@my.gatech.edu');

      await controller.create(param, user);

      expect(service.create).toBeCalledTimes(1);
    });
  });

  describe('should NOT allow users to sign up when', () => {
    it('user email does not share primary domain', async () => {
      const param: EventIdParam = { eventId: fakeEventTeam.eventId };
      const user = createFakeUser(faker.datatype.number(), 'user@c2games.edu');

      await expect(async () => await controller.create(param, user)).rejects.toThrowError();
      // await controller.create(param, user);

      expect(service.create).toBeCalledTimes(0);
    });
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should create event team', async () => {
    const param: EventIdParam = { eventId: fakeEventTeam.eventId };
    const eventTeam = await controller.create(param, fakeUser);

    expect(eventTeam).toEqual({ ...fakeEventTeam });
    expect(service.create).toBeCalledTimes(1);
    expect(service.create).toBeCalledWith(expect.objectContaining(param), fakeUser.id);
  });

  it('should find all event teams by event', async () => {
    const param: EventIdParam = { eventId: fakeEventTeam.eventId };
    const eventTeams = await controller.findAllByEvent(param);

    expect(eventTeams).toEqual([fakeEventTeam]);
    expect(service.findAll).toBeCalledTimes(1);
    expect(service.findAll).toBeCalledWith({ eventId: param.eventId });
  });

  it('should find all teams by user domain', async () => {
    const user = createFakeUser(faker.datatype.number(), 'user@mvcc.edu');
    const eventTeams = await controller.findAllTeamsByDomain(user);
    expect(service.findTeamByDomain).toBeCalledTimes(1);
    expect(service.findTeamByDomain).toBeCalledWith('mvcc.edu');
  });

  it('should find one event team by id', async () => {
    const eventTeam = await controller.findOne({
      eventTeamId: fakeEventTeam.id,
    });

    expect(eventTeam).toEqual(fakeEventTeam);
    expect(service.findOneById).toBeCalledTimes(1);
    expect(service.findOneById).toBeCalledWith(fakeEventTeam.id);
  });

  it('should update event team', async () => {
    const updateEventTeamDto = {
      name: faker.datatype.string(),
    };
    const eventTeam = await controller.update({ eventTeamId: fakeEventTeam.id }, updateEventTeamDto, fakeUser);

    expect(eventTeam).toEqual(fakeEventTeam);
    expect(service.update).toBeCalledTimes(1);
    expect(service.update).toBeCalledWith(fakeEventTeam.id, updateEventTeamDto, fakeUser.id);
  });

  it('should remove event team', async () => {
    const eventTeam = await controller.remove(
      {
        eventTeamId: fakeEventTeam.id,
      },
      fakeUser,
    );

    expect(eventTeam).toEqual(fakeEventTeam);
    expect(service.remove).toBeCalledTimes(1);
    expect(service.remove).toBeCalledWith(fakeEventTeam.id);
  });
});
