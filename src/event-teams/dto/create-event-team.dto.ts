import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsOptional, IsString } from 'class-validator';
import { EventExists } from '../../events/decorators/event-exists.decorator';

export class CreateEventTeamDto {
  @ApiProperty({
    type: 'number',
    required: true,
    example: '123',
  })
  @IsInt()
  @EventExists()
  @IsOptional()
  eventId?: number;

  @ApiProperty({
    type: 'string',
    required: true,
    example: 'Event Team Name',
  })
  @IsString()
  name: string;
}
