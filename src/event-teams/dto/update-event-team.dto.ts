import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsOptional, IsString } from 'class-validator';

export class UpdateEventTeamDto {
  @ApiProperty({
    type: 'string',
    required: false,
    example: 'Event Team Name edited',
  })
  @IsString()
  @IsOptional()
  name?: string;

  @ApiProperty({
    type: 'boolean',
    required: false,
    example: 'true',
  })
  @IsBoolean()
  @IsOptional()
  enabled?: boolean;
}
