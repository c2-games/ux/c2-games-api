import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class PatchEventTeamRequestBodyDto {
  @ApiProperty({
    type: 'string',
    required: true,
    example: 'Event Team Name',
  })
  @IsString()
  name: string;
}
