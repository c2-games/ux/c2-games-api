import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsOptional } from 'class-validator';
import { EventTeamExists } from '../decorators/event-team-exists.decorator';
import { EventExists } from '../../events/decorators/event-exists.decorator';

export class EventTeamIdParam {
  @ApiProperty({
    type: 'number',
    required: false,
    example: '123',
  })
  @IsInt()
  @EventTeamExists()
  @IsOptional()
  eventTeamId?: number;
}

export class CopyTeamParam {
  @ApiProperty({
    type: 'number',
    required: false,
  })
  @IsInt()
  @EventTeamExists()
  eventTeamId?: number;

  @ApiProperty({
    type: 'number',
    required: false,
  })
  @IsInt()
  @EventExists()
  eventId?: number;
}
