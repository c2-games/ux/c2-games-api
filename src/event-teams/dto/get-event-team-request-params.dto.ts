import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsOptional, IsString, IsEnum, Max, Min } from 'class-validator';
import { EventExists } from '../../events/decorators/event-exists.decorator';
import { Region, Regions } from '../../institutions/entities/regions';

export class GetEventTeamsRequestParamsDto {
  @ApiProperty({
    type: 'number',
    required: false,
    minimum: 1,
    maximum: 10,
  })
  @IsInt()
  @Min(1)
  @Max(10)
  @IsOptional()
  minMembers?: number;

  @ApiProperty({
    type: 'number',
    required: false,
    minimum: 1,
    maximum: 10,
  })
  @IsInt()
  @Min(1)
  @Max(10)
  @IsOptional()
  maxMembers?: number;

  @ApiProperty({
    type: 'number',
    required: false,
  })
  @IsInt()
  @EventExists()
  @IsOptional()
  eventId?: number;

  @ApiProperty({
    type: 'string',
    enum: Object.keys(Regions),
    required: false,
  })
  @IsString()
  @IsOptional()
  region?: Region;
}
