import { ApiProperty } from '@nestjs/swagger';

export class GetDomainTeamsDto {
  @ApiProperty({
    type: 'string',
    required: true,
    example: 'mvcc.edu',
  })
  domain: string;
}
