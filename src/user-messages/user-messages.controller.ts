import { Body, Controller, Delete, Get, Param, Patch, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiCreatedResponse, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { UserRoles } from 'src/auth/decorators/roles.decorator';
import { UserRole } from 'src/auth/decorators/user-role.enum';
import { UserRolesGuard } from 'src/auth/guards/user-roles.guard';
import { CurrentUser } from '../users/decorators/current-user.decorator';
import { User } from '../users/entities/user.entity';
import { PostUserMessageParamsDto } from './dto/post-user-message-params.dto';
import { UpdateUserMessageDto } from './dto/update-user-message.dto';
import { UserMessagesService } from './user-messages.service';

@ApiTags('User Messages')
@ApiBearerAuth()
@Controller()
@UseGuards(UserRolesGuard)
export class UserMessagesController {
  constructor(private readonly userMessagesService: UserMessagesService) {}

  @Post('users/:userId/message/:messageId')
  @ApiOperation({ summary: 'Create user message' })
  @ApiCreatedResponse({
    description: 'User message created',
  })
  @UserRoles(UserRole.Announcer)
  create(@Param() params: PostUserMessageParamsDto, @CurrentUser() currentUser: User) {
    const createUserMessageDto = { ...params };
    return this.userMessagesService.create(createUserMessageDto, currentUser.id);
  }

  @Get('user-messages')
  @ApiOperation({ summary: 'Find all user messages' })
  @ApiOkResponse({ description: 'User messages found' })
  findAll() {
    return this.userMessagesService.findAll();
  }

  @Get('users/messages/:messageId')
  @ApiOperation({ summary: 'Find all user messages by message ID' })
  @ApiOkResponse({ description: 'User messages found' })
  findAllByMessage(@Param('messageId') messageId: number) {
    return this.userMessagesService.findAll({ messageId });
  }

  @Get('users/:userId/messages')
  @ApiOperation({ summary: 'Find all user messages by user ID' })
  @ApiOkResponse({ description: 'User messages found' })
  findAllByUser(@Param('userId') userId: number) {
    return this.userMessagesService.findAll({ userId });
  }

  @Get('user-messages/:id')
  @ApiOperation({ summary: 'Find user message by ID' })
  @ApiOkResponse({ description: 'User message found' })
  findOne(@Param('id') id: string) {
    return this.userMessagesService.findOne(+id);
  }

  @Patch('user-messages/:id')
  @ApiOperation({ summary: 'Update user message' })
  @ApiOkResponse({ description: 'User message updated' })
  @UserRoles(UserRole.Announcer)
  update(
    @Param('id') id: string,
    @Body() updateUserMessageDto: UpdateUserMessageDto,
    @CurrentUser() currentUser: User,
  ) {
    return this.userMessagesService.update(+id, updateUserMessageDto, currentUser.id);
  }

  @Delete('user-messages/:id')
  @ApiOperation({ summary: 'Remove user message' })
  @ApiOkResponse({ description: 'User message removed' })
  @UserRoles(UserRole.Announcer)
  remove(@Param('id') id: string) {
    return this.userMessagesService.remove(+id);
  }
}
