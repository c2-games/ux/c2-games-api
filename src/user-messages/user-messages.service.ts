import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { abort } from 'process';
import { Repository } from 'typeorm';
import { Message } from '../messages/entities/message.entity';
import { CreateUserMessageDto } from './dto/create-user-message.dto';
import { UpdateUserMessageDto } from './dto/update-user-message.dto';
import { UserMessageDto } from './dto/user-message.dto';
import { UserMessageQueryDto } from './dto/user-message-query.dto';
import { UserMessage } from './entities/user-message.entity';

@Injectable()
export class UserMessagesService {
  constructor(
    @InjectRepository(UserMessage)
    private repo: Repository<UserMessage>,
    @InjectRepository(Message)
    private messageRepo: Repository<Message>,
  ) {}

  async create(createUserMessageDto: CreateUserMessageDto, currentUserId = 0) {
    const userMessage = this.repo.create({
      ...createUserMessageDto,
      createdByUserId: currentUserId,
      updatedByUserId: currentUserId,
    });
    await this.repo.save(userMessage);
    return userMessage;
  }

  async findAll(params: UserMessageQueryDto = {}) {
    let subquery = this.repo.createQueryBuilder().select('*').where('1=1');
    subquery = params.userId ? subquery.andWhere(`user_id = ${params.userId}`) : subquery;
    subquery = params.seenOn ? subquery.andWhere(`seen_on = ${params.seenOn}`) : subquery;
    let query = this.messageRepo
      .createQueryBuilder('messages')
      .select(
        'um.id as id, um.user_id, messages.id as message_id, messages.content, messages.announced_on, um.seen_on, messages.enabled',
      )
      .leftJoin('(' + subquery.getQuery() + ')', 'um', 'um.message_id = messages.id')
      .where('1=1');
    query = params.messageId
      ? query.andWhere('message_id = :messageId', {
          messageId: params.messageId,
        })
      : query;
    const results = await query.getRawMany();
    return results.map((record) => new UserMessageDto().fromRawSql(record));
  }

  async findOne(id: number) {
    if (!id) {
      throw new BadRequestException('User Message ID not provided');
    }
    const userMessage = await this.repo.findOne(id);
    if (!userMessage) {
      throw new NotFoundException('User Message not found');
    }
    return userMessage;
  }

  async update(id: number, updateUserMessageDto: UpdateUserMessageDto, currentUserId = 0) {
    const userMessage = await this.findOne(id);
    Object.assign(updateUserMessageDto, { updatedByUserId: currentUserId });
    Object.assign(userMessage, updateUserMessageDto);
    await this.repo.save(userMessage);
    return userMessage;
  }

  async remove(id: number) {
    const userMessage = await this.findOne(id);
    await this.repo.remove(userMessage);
    return userMessage;
  }
}
