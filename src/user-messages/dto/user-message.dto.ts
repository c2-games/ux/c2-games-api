import { snakeCaseToCamelCase } from '../../utils/test-helpers/string-utils';

export class UserMessageDto {
  id: number;
  userId: number;
  messageId: number;
  content: string;
  announcedOn: Date;
  seenOn: Date;
  enabled: boolean;

  fromRawSql(raw) {
    const userMessageDto = new UserMessageDto();
    Object.keys(raw).forEach((key) => {
      userMessageDto[snakeCaseToCamelCase(key)] = raw[key];
    });
    return userMessageDto;
  }
}
