import { ApiProperty } from '@nestjs/swagger';
import { IsDate, IsInt, IsOptional } from 'class-validator';
import { MessageExists } from '../../messages/decorators/message-exists.decorator';
import { UserExists } from '../../users/decorators/user-exists.decorator';

export class CreateUserMessageDto {
  @ApiProperty({
    type: 'number',
    required: true,
    example: '123',
  })
  @IsInt()
  @UserExists()
  userId: number;

  @ApiProperty({
    type: 'number',
    required: true,
    example: '123',
  })
  @IsInt()
  @MessageExists()
  messageId: number;
}
