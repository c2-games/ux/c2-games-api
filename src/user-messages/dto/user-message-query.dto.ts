import { ApiProperty } from '@nestjs/swagger';
import { IsDate, IsInt, IsOptional } from 'class-validator';
import { MessageExists } from '../../messages/decorators/message-exists.decorator';
import { UserExists } from '../../users/decorators/user-exists.decorator';

export class UserMessageQueryDto {
  @ApiProperty({
    type: 'number',
    required: false,
    example: '123',
  })
  @IsInt()
  @UserExists()
  @IsOptional()
  userId?: number;

  @ApiProperty({
    type: 'number',
    required: false,
    example: '123',
  })
  @IsInt()
  @MessageExists()
  @IsOptional()
  messageId?: number;

  @ApiProperty({
    type: 'date',
    required: false,
    example: '2022-08-01T20:29:56.561Z',
  })
  @IsDate()
  @IsOptional()
  seenOn?: Date;
}
