import { faker } from '@faker-js/faker';
import { Test, TestingModule } from '@nestjs/testing';
import { KeycloakModule } from '../keycloak/keycloak.module';
import { createFakeUser } from '../utils/test-helpers/fakes/user-faker';
import { createFakeUserMessage } from '../utils/test-helpers/fakes/user-message-faker';
import { CreateUserMessageDto } from './dto/create-user-message.dto';
import { PostUserMessageParamsDto } from './dto/post-user-message-params.dto';
import { UpdateUserMessageDto } from './dto/update-user-message.dto';
import { UserMessagesController } from './user-messages.controller';
import { UserMessagesService } from './user-messages.service';

const currentUserId = faker.datatype.number();

const fakeUserMessage = createFakeUserMessage({
  userId: currentUserId,
});
const fakeUser = createFakeUser(currentUserId);

const postUserMessageParamsDto: PostUserMessageParamsDto = {
  userId: fakeUserMessage.userId,
  messageId: fakeUserMessage.messageId,
};

describe('UserMessagesController', () => {
  let controller: UserMessagesController;
  let service: UserMessagesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [KeycloakModule],
      controllers: [UserMessagesController],
      providers: [
        {
          provide: UserMessagesService,
          useValue: {
            create: jest.fn().mockImplementation((userMessage: CreateUserMessageDto, currentUserId: number) =>
              Promise.resolve({
                ...fakeUserMessage,
                ...userMessage,
              }),
            ),
            findAll: jest.fn().mockResolvedValue([fakeUserMessage]),
            findOne: jest.fn().mockImplementation((id: number) => Promise.resolve({ ...fakeUserMessage, id })),
            update: jest
              .fn()
              .mockImplementation((id: number, userMessage: UpdateUserMessageDto, currentUserId: number) =>
                Promise.resolve({ ...fakeUserMessage, id }),
              ),
            remove: jest.fn().mockResolvedValue(fakeUserMessage),
          },
        },
      ],
    }).compile();

    controller = module.get<UserMessagesController>(UserMessagesController);
    service = module.get<UserMessagesService>(UserMessagesService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should create user message', async () => {
    const createUserMessageDto: CreateUserMessageDto = {
      userId: fakeUserMessage.userId,
      messageId: fakeUserMessage.messageId,
    };
    const userMessage = await controller.create(postUserMessageParamsDto, fakeUser);

    expect(userMessage).toEqual({
      ...fakeUserMessage,
    });
    expect(service.create).toBeCalledTimes(1);
    expect(service.create).toBeCalledWith(createUserMessageDto, fakeUser.id);
  });

  it('should find all user messages', async () => {
    const userMessages = await controller.findAll();

    expect(userMessages).toEqual([fakeUserMessage]);
    expect(service.findAll).toBeCalledTimes(1);
  });

  it('should find all user messages by message', async () => {
    const userMessages = await controller.findAllByMessage(fakeUserMessage.messageId);

    expect(userMessages).toEqual([fakeUserMessage]);
    expect(service.findAll).toBeCalledTimes(1);
  });

  it('should find all user messages by user', async () => {
    const userMessages = await controller.findAllByUser(fakeUserMessage.userId);

    expect(userMessages).toEqual([fakeUserMessage]);
    expect(service.findAll).toBeCalledTimes(1);
  });

  it('should find one user message by id', async () => {
    const userMessage = await controller.findOne(`${fakeUserMessage.id}`);

    expect(userMessage).toEqual(fakeUserMessage);
    expect(service.findOne).toBeCalledTimes(1);
    expect(service.findOne).toBeCalledWith(fakeUserMessage.id);
  });

  it('should update user message', async () => {
    const updateUserMessageDto = {
      seenOn: faker.datatype.datetime(),
    };
    const userMessage = await controller.update(`${fakeUserMessage.id}`, updateUserMessageDto, fakeUser);

    expect(userMessage).toEqual(fakeUserMessage);
    expect(service.update).toBeCalledTimes(1);
    expect(service.update).toBeCalledWith(fakeUserMessage.id, updateUserMessageDto, fakeUser.id);
  });

  it('should remove user message', async () => {
    const userMessage = await controller.remove(`${fakeUserMessage.id}`);

    expect(userMessage).toEqual(fakeUserMessage);
    expect(service.remove).toBeCalledTimes(1);
    expect(service.remove).toBeCalledWith(fakeUserMessage.id);
  });
});
