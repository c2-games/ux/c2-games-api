import { faker } from '@faker-js/faker';
import { BadRequestException, NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Message } from '../messages/entities/message.entity';
import { createFakeUserMessageDto } from '../utils/test-helpers/fakes/user-message-dto-faker';
import { UserMessageQueryDto } from './dto/user-message-query.dto';
import { UserMessage } from './entities/user-message.entity';
import { UserMessagesService } from './user-messages.service';

const currentUserId = faker.datatype.number();

const testUserMessage = createFakeUserMessageDto({ currentUserId });

const userMessageQueryDto = new UserMessageQueryDto();

describe('UserMessagesService', () => {
  let service: UserMessagesService;
  let repo: Repository<UserMessage>;
  let messageRepo: Repository<Message>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UserMessagesService,
        {
          provide: getRepositoryToken(UserMessage),
          useValue: {
            create: jest.fn().mockReturnValue(testUserMessage),
            save: jest.fn(),
            findAll: jest.fn().mockResolvedValue([testUserMessage]),
            createQueryBuilder: jest.fn(() => ({
              select: jest.fn().mockReturnThis(),
              where: jest.fn().mockReturnThis(),
              andWhere: jest.fn().mockReturnThis(),
              getQuery: jest.fn().mockReturnThis(),
            })),
            findOne: jest.fn().mockResolvedValue(testUserMessage),
            update: jest.fn().mockResolvedValue(true),
            remove: jest.fn().mockResolvedValue(true),
          },
        },
        {
          provide: getRepositoryToken(Message),
          useValue: {
            createQueryBuilder: jest.fn(() => ({
              select: jest.fn().mockReturnThis(),
              leftJoin: jest.fn().mockReturnThis(),
              where: jest.fn().mockReturnThis(),
              andWhere: jest.fn().mockReturnThis(),
              getRawMany: jest.fn().mockResolvedValue([testUserMessage]),
            })),
            findOne: jest.fn().mockResolvedValue(testUserMessage),
            update: jest.fn().mockResolvedValue(true),
            remove: jest.fn().mockResolvedValue(true),
          },
        },
      ],
    }).compile();

    service = module.get<UserMessagesService>(UserMessagesService);
    repo = module.get<Repository<UserMessage>>(getRepositoryToken(UserMessage));
    messageRepo = module.get<Repository<Message>>(getRepositoryToken(Message));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should create a new user message when provided with valid createUserMessageDto', async () => {
    const createUserMessageDto = {
      userId: testUserMessage.userId,
      messageId: testUserMessage.messageId,
    };

    const userMessage = await service.create(createUserMessageDto, currentUserId);

    expect(userMessage).toEqual(testUserMessage);
    expect(repo.create).toBeCalledTimes(1);
    expect(repo.create).toBeCalledWith({
      ...createUserMessageDto,
      createdByUserId: currentUserId,
      updatedByUserId: currentUserId,
    });
    expect(repo.save).toBeCalledTimes(1);
  });

  it('should find all user messages', async () => {
    const userMessages = await service.findAll(userMessageQueryDto);

    expect(userMessages).toEqual([testUserMessage]);
  });

  it('should find one user message', async () => {
    const userMessage = await service.findOne(faker.datatype.number());

    expect(userMessage).toEqual(testUserMessage);
  });

  it('should throw bad request exception on findOne if id not provided', async () => {
    await expect(() => service.findOne(null)).rejects.toThrow(new BadRequestException('User Message ID not provided'));
  });

  it('should throw not found exception on findOne if user message not found', async () => {
    const userMessageId = faker.datatype.number();
    const repoSpy = jest.spyOn(repo, 'findOne').mockReturnValue(undefined);

    await expect(() => service.findOne(userMessageId)).rejects.toThrow(new NotFoundException('User Message not found'));
    expect(repoSpy).toBeCalledTimes(1);
    expect(repoSpy).toBeCalledWith(userMessageId);
  });

  it('should update user message', async () => {
    const userMessageId = faker.datatype.number();
    const updateUserMessageDto = { seenOn: faker.datatype.datetime() };

    const userMessage = await service.update(userMessageId, updateUserMessageDto, currentUserId);

    expect(userMessage).toEqual({
      ...testUserMessage,
      ...updateUserMessageDto,
    });
    expect(repo.findOne).toBeCalledTimes(1);
    expect(repo.findOne).toBeCalledWith(userMessageId);
    expect(repo.save).toBeCalledTimes(1);
  });

  it('should remove user message', async () => {
    const userMessageId = faker.datatype.number();

    const userMessage = await service.remove(userMessageId);

    expect(userMessage).toEqual(testUserMessage);
  });
});
