import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { KeycloakModule } from '../keycloak/keycloak.module';
import { Message } from '../messages/entities/message.entity';
import { MessagesModule } from '../messages/messages.module';
import { UserMessage } from './entities/user-message.entity';
import { UserMessagesController } from './user-messages.controller';
import { UserMessagesService } from './user-messages.service';

@Module({
  imports: [TypeOrmModule.forFeature([Message, UserMessage]), KeycloakModule, MessagesModule],
  controllers: [UserMessagesController],
  providers: [UserMessagesService],
})
export class UserMessagesModule {}
