import { Logger } from '@nestjs/common';
import {
  AfterInsert,
  AfterRemove,
  AfterUpdate,
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  Unique,
  UpdateDateColumn,
} from 'typeorm';
import { Message } from '../../messages/entities/message.entity';
import { User } from '../../users/entities/user.entity';

@Entity({ name: 'users_x_messages' })
@Unique(['userId', 'messageId'])
export class UserMessage {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'user_id' })
  userId: number;

  @Column({ name: 'message_id' })
  messageId: number;

  @Column({
    name: 'seen_on',
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
  })
  seenOn: Date;

  @Column({ name: 'enabled', default: true })
  enabled: boolean;

  @Column({ name: 'created_by_user_id', default: 0 })
  createdByUserId: number;

  @CreateDateColumn({
    name: 'created_date',
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
  })
  createdDate: Date;

  @Column({ name: 'updated_by_user_id', default: 0 })
  updatedByUserId: number;

  @UpdateDateColumn({
    name: 'updated_date',
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
  })
  updatedDate: Date;

  @ManyToOne(() => User, (user) => user.userMessages)
  @JoinColumn({ name: 'user_id', referencedColumnName: 'id' })
  user: User;

  @ManyToOne(() => Message, (message) => message.userMessages)
  @JoinColumn({ name: 'message_id', referencedColumnName: 'id' })
  message: Message;

  @AfterInsert()
  logInsert() {
    Logger.log(`Inserted User Message with id ${this.id}`);
  }

  @AfterUpdate()
  logUpdate() {
    Logger.log(`Updated User Message with id ${this.id}`);
  }

  @AfterRemove()
  logRemove() {
    Logger.log('Removed User Message');
  }
}
