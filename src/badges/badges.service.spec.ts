import { faker } from '@faker-js/faker';
import { BadRequestException, NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { BadgesService } from './badges.service';
import { GetBadgeRequestParamsDto } from './dto/get-badge-request-params.dto';
import { Badge } from './entities/badge.entity';
import { createFakeBadge } from '../utils/test-helpers/fakes/badge-faker';

const currentUserId = faker.datatype.number();

const testBadge = createFakeBadge(currentUserId);

const getBadgeRequestParamsDto = new GetBadgeRequestParamsDto();

describe('BadgesService', () => {
  let service: BadgesService;
  let repo: Repository<Badge>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        BadgesService,
        {
          provide: getRepositoryToken(Badge),
          useValue: {
            create: jest.fn().mockReturnValue(testBadge),
            save: jest.fn(),
            findAll: jest.fn().mockResolvedValue([testBadge]),
            createQueryBuilder: jest.fn(() => ({
              where: jest.fn().mockReturnThis(),
              andWhere: jest.fn().mockReturnThis(),
              getMany: jest.fn().mockResolvedValue([testBadge]),
            })),
            findOne: jest.fn().mockResolvedValue(testBadge),
            update: jest.fn().mockResolvedValue(true),
            remove: jest.fn().mockResolvedValue(true),
          },
        },
      ],
    }).compile();

    service = module.get<BadgesService>(BadgesService);
    repo = module.get<Repository<Badge>>(getRepositoryToken(Badge));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should create a new badge when provided with valid createBadgeDto', async () => {
    const createBadgeDto = {
      title: testBadge.title,
      description: testBadge.description,
    };

    const badge = await service.create(createBadgeDto, currentUserId);

    expect(badge).toEqual(testBadge);
    expect(repo.create).toBeCalledTimes(1);
    expect(repo.create).toBeCalledWith({
      ...createBadgeDto,
      createdByUserId: currentUserId,
      updatedByUserId: currentUserId,
    });
    expect(repo.save).toBeCalledTimes(1);
  });

  it('should find all badges', async () => {
    const badges = await service.findAll(getBadgeRequestParamsDto);

    expect(badges).toEqual([testBadge]);
  });

  it('should find one badge', async () => {
    const badge = await service.findOne(faker.datatype.number());

    expect(badge).toEqual(testBadge);
  });

  it('should throw bad request exception on findOne if id not provided', async () => {
    await expect(() => service.findOne(null)).rejects.toThrow(BadRequestException);
  });

  it('should throw not found exception on findOne if badge not found', async () => {
    const badgeId = faker.datatype.number();
    const repoSpy = jest.spyOn(repo, 'findOne').mockReturnValue(undefined);

    await expect(() => service.findOne(badgeId)).rejects.toThrow(new NotFoundException('Badge not found'));
    expect(repoSpy).toBeCalledTimes(1);
    expect(repoSpy).toBeCalledWith(badgeId);
  });

  it('should update badge', async () => {
    const badgeId = faker.datatype.number();
    const updateBadgeDto = { title: 'This title has been updated' };

    const badge = await service.update(badgeId, updateBadgeDto, currentUserId);

    expect(badge).toEqual({ ...testBadge, ...updateBadgeDto });
    expect(repo.findOne).toBeCalledTimes(1);
    expect(repo.findOne).toBeCalledWith(badgeId);
    expect(repo.save).toBeCalledTimes(1);
  });

  it('should remove badge', async () => {
    const badgeId = faker.datatype.number();

    const badge = await service.remove(badgeId);

    expect(badge).toEqual(testBadge);
  });
});
