import { Injectable } from '@nestjs/common';
import { ValidatorConstraint, ValidatorConstraintInterface } from 'class-validator';
import { BadgesService } from '../badges.service';

@ValidatorConstraint({ name: 'BadgeExists', async: true })
@Injectable()
export class BadgeExistsRule implements ValidatorConstraintInterface {
  constructor(private readonly badgesService: BadgesService) {}

  async validate(id: number) {
    try {
      await this.badgesService.findOne(id);
    } catch (error) {
      return false;
    }
    return true;
  }

  defaultMessage() {
    return `Badge does not exist`;
  }
}
