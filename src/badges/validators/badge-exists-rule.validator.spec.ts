import { NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { createFakeBadge } from '../../utils/test-helpers/fakes/badge-faker';
import { BadgesService } from '../badges.service';
import { BadgeExistsRule } from './badge-exists-rule.validator';

const fakeBadge = createFakeBadge();

describe('BadgeExistsRule', () => {
  let badgeExistsRule: BadgeExistsRule;
  let service: BadgesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        BadgeExistsRule,
        {
          provide: BadgesService,
          useValue: {
            findOne: jest.fn().mockImplementation((id: number) => Promise.resolve(fakeBadge)),
          },
        },
      ],
    }).compile();

    badgeExistsRule = module.get<BadgeExistsRule>(BadgeExistsRule);
    service = module.get<BadgesService>(BadgesService);
  });

  it('should return true when badge exists', async () => {
    expect(badgeExistsRule.validate(fakeBadge.id)).resolves.toEqual(true);
    expect(service.findOne).toBeCalledTimes(1);
    expect(service.findOne).toBeCalledWith(fakeBadge.id);
  });

  it('should return false when badge does not exist', async () => {
    const findOneSpy = jest.spyOn(service, 'findOne').mockRejectedValueOnce(NotFoundException);
    expect(badgeExistsRule.validate(fakeBadge.id)).resolves.toEqual(false);
    expect(findOneSpy).toBeCalledTimes(1);
    expect(findOneSpy).toBeCalledWith(fakeBadge.id);
  });

  it('should return correct default message', () => {
    expect(badgeExistsRule.defaultMessage()).toEqual('Badge does not exist');
  });
});
