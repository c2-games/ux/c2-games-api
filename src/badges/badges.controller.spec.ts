import { faker } from '@faker-js/faker';
import { Test, TestingModule } from '@nestjs/testing';
import { KeycloakModule } from '../keycloak/keycloak.module';
import { createFakeBadge } from '../utils/test-helpers/fakes/badge-faker';
import { createFakeUser } from '../utils/test-helpers/fakes/user-faker';
import { BadgesController } from './badges.controller';
import { BadgesService } from './badges.service';
import { CreateBadgeDto } from './dto/create-badge.dto';
import { UpdateBadgeDto } from './dto/update-badge.dto';

const currentUserId = faker.datatype.number();

const fakeBadge = createFakeBadge(currentUserId);
const fakeUser = createFakeUser(currentUserId);

describe('BadgesController', () => {
  let controller: BadgesController;
  let service: BadgesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [KeycloakModule],
      controllers: [BadgesController],
      providers: [
        {
          provide: BadgesService,
          useValue: {
            create: jest
              .fn()
              .mockImplementation((badge: CreateBadgeDto, currentUserId: number) =>
                Promise.resolve({ ...fakeBadge, ...badge }),
              ),
            findAll: jest.fn().mockResolvedValue([fakeBadge]),
            findOne: jest.fn().mockImplementation((id: number) => Promise.resolve({ ...fakeBadge, id })),
            update: jest
              .fn()
              .mockImplementation((id: number, badge: UpdateBadgeDto, currentUserId: number) =>
                Promise.resolve({ ...fakeBadge, id }),
              ),
            remove: jest.fn().mockResolvedValue(fakeBadge),
          },
        },
      ],
    }).compile();

    controller = module.get<BadgesController>(BadgesController);
    service = module.get<BadgesService>(BadgesService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should create badge', async () => {
    const createBadgeDto: CreateBadgeDto = {
      title: fakeBadge.title,
      description: fakeBadge.description,
    };
    const badge = await controller.create(createBadgeDto, fakeUser);

    expect(badge).toEqual({ ...fakeBadge });
    expect(service.create).toBeCalledTimes(1);
    expect(service.create).toBeCalledWith(createBadgeDto, fakeUser.id);
  });

  it('should find all badges', async () => {
    const badges = await controller.findAll();

    expect(badges).toEqual([fakeBadge]);
    expect(service.findAll).toBeCalledTimes(1);
  });

  it('should find one badge by id', async () => {
    const badge = await controller.findOne(`${fakeBadge.id}`);

    expect(badge).toEqual(fakeBadge);
    expect(service.findOne).toBeCalledTimes(1);
    expect(service.findOne).toBeCalledWith(fakeBadge.id);
  });

  it('should update badge', async () => {
    const updateBadgeDto = {
      title: faker.datatype.string(),
    };
    const badge = await controller.update(`${fakeBadge.id}`, updateBadgeDto, fakeUser);

    expect(badge).toEqual(fakeBadge);
    expect(service.update).toBeCalledTimes(1);
    expect(service.update).toBeCalledWith(fakeBadge.id, updateBadgeDto, fakeUser.id);
  });

  it('should remove badge', async () => {
    const badge = await controller.remove(`${fakeBadge.id}`);

    expect(badge).toEqual(fakeBadge);
    expect(service.remove).toBeCalledTimes(1);
    expect(service.remove).toBeCalledWith(fakeBadge.id);
  });
});
