import { Body, Controller, Delete, Get, Param, Patch, Post, Query, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiCreatedResponse, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { UserRoles } from 'src/auth/decorators/roles.decorator';
import { UserRole } from 'src/auth/decorators/user-role.enum';
import { UserRolesGuard } from 'src/auth/guards/user-roles.guard';
import { CurrentUser } from '../users/decorators/current-user.decorator';
import { User } from '../users/entities/user.entity';
import { BadgesService } from './badges.service';
import { CreateBadgeDto } from './dto/create-badge.dto';
import { GetBadgeRequestParamsDto } from './dto/get-badge-request-params.dto';
import { UpdateBadgeDto } from './dto/update-badge.dto';

@ApiTags('Badges')
@ApiBearerAuth()
@Controller('badges')
@UseGuards(UserRolesGuard)
export class BadgesController {
  constructor(private readonly badgesService: BadgesService) {}

  @Post()
  @ApiOperation({ summary: 'Create badge' })
  @ApiCreatedResponse({ description: 'Badge created' })
  @UserRoles(UserRole.AwardAdmin)
  create(@Body() createBadgeDto: CreateBadgeDto, @CurrentUser() currentUser: User) {
    return this.badgesService.create(createBadgeDto, currentUser.id);
  }

  @Get()
  @ApiOperation({ summary: 'Find all badges' })
  @ApiOkResponse({ description: 'Badges found' })
  findAll(@Query() params?: GetBadgeRequestParamsDto) {
    return this.badgesService.findAll(params);
  }

  @Get(':id')
  @ApiOperation({ summary: 'Find badge by ID' })
  @ApiOkResponse({ description: 'Badge found' })
  findOne(@Param('id') id: string) {
    return this.badgesService.findOne(+id);
  }

  @Patch(':id')
  @ApiOperation({ summary: 'Update badge' })
  @ApiOkResponse({ description: 'Badge updated' })
  @UserRoles(UserRole.AwardAdmin)
  update(@Param('id') id: string, @Body() updateBadgeDto: UpdateBadgeDto, @CurrentUser() currentUser: User) {
    return this.badgesService.update(+id, updateBadgeDto, currentUser.id);
  }

  @Delete(':id')
  @ApiOperation({ summary: 'Remove badge' })
  @ApiOkResponse({ description: 'Badge removed' })
  @UserRoles(UserRole.AwardAdmin)
  remove(@Param('id') id: string) {
    return this.badgesService.remove(+id);
  }
}
