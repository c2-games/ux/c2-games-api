import { registerDecorator, ValidationOptions } from 'class-validator';
import { BadgeExistsRule } from '../validators/badge-exists-rule.validator';

export function BadgeExists(validationOptions?: ValidationOptions) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      name: 'BadgeExists',
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: BadgeExistsRule,
    });
  };
}
