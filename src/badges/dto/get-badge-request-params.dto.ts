import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

export class GetBadgeRequestParamsDto {
  @ApiProperty({
    type: 'string',
    required: false,
    example: 'Badge Title',
  })
  @IsString()
  @IsOptional()
  title?: string;

  @ApiProperty({
    type: 'string',
    required: false,
    example: 'This is a badge description',
  })
  @IsString()
  @IsOptional()
  description?: string;
}
