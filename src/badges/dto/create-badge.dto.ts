import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

export class CreateBadgeDto {
  @ApiProperty({
    type: 'string',
    required: true,
    example: 'Badge Title',
  })
  @IsString()
  title: string;

  @ApiProperty({
    type: 'string',
    required: true,
    example: 'This is a badge description',
  })
  @IsString()
  description: string;

  @ApiProperty({
    type: 'string',
    required: true,
    example: 'http://image-url.com',
  })
  @IsString()
  @IsOptional()
  imageUrl?: string;
}
