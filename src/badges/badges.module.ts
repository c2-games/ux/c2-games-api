import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { KeycloakModule } from '../keycloak/keycloak.module';
import { BadgesController } from './badges.controller';
import { BadgesService } from './badges.service';
import { Badge } from './entities/badge.entity';
import { BadgeExistsRule } from './validators/badge-exists-rule.validator';

@Module({
  imports: [TypeOrmModule.forFeature([Badge]), KeycloakModule],
  controllers: [BadgesController],
  providers: [BadgesService, BadgeExistsRule],
})
export class BadgesModule {}
