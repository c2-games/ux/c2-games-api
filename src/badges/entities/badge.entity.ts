import { Logger } from '@nestjs/common';
import {
  AfterInsert,
  AfterRemove,
  AfterUpdate,
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { UserBadge } from '../../user-badges/entities/user-badge.entity';

@Entity({ name: 'badges' })
export class Badge {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'title', unique: true })
  title: string;

  @Column({ name: 'description' })
  description: string;

  @Column({ name: 'image_url' })
  imageUrl: string;

  @Column({ name: 'enabled', default: true })
  enabled: boolean;

  @Column({ name: 'created_by_user_id', default: 0 })
  createdByUserId: number;

  @CreateDateColumn({
    name: 'created_date',
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
  })
  createdDate: Date;

  @Column({ name: 'updated_by_user_id', default: 0 })
  updatedByUserId: number;

  @UpdateDateColumn({
    name: 'updated_date',
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
  })
  updatedDate: Date;

  @OneToMany(() => UserBadge, (userBadge) => userBadge.badge)
  userBadges: UserBadge[];

  @AfterInsert()
  logInsert() {
    Logger.log(`Inserted Badge with id ${this.id}`);
  }

  @AfterUpdate()
  logUpdate() {
    Logger.log(`Updated Badge with id ${this.id}`);
  }

  @AfterRemove()
  logRemove() {
    Logger.log('Removed Badge');
  }
}
