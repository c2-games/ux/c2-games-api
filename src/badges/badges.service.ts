import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateBadgeDto } from './dto/create-badge.dto';
import { GetBadgeRequestParamsDto } from './dto/get-badge-request-params.dto';
import { UpdateBadgeDto } from './dto/update-badge.dto';
import { Badge } from './entities/badge.entity';

@Injectable()
export class BadgesService {
  constructor(@InjectRepository(Badge) private repo: Repository<Badge>) {}

  async create(createBadgeDto: CreateBadgeDto, currentUserId = 0) {
    const badge = this.repo.create({
      ...createBadgeDto,
      createdByUserId: currentUserId,
      updatedByUserId: currentUserId,
    });
    await this.repo.save(badge);
    return badge;
  }

  findAll(params?: GetBadgeRequestParamsDto) {
    let query = this.repo.createQueryBuilder('badges').where('1=1');
    query = params.title
      ? query.andWhere('badges.title = :title', {
          title: params.title,
        })
      : query;
    query = params.description
      ? query.andWhere('badges.description = :description', {
          description: params.description,
        })
      : query;
    return query.getMany();
  }

  async findOne(id: number) {
    if (!id) {
      throw new BadRequestException();
    }
    const badge = await this.repo.findOne(id);
    if (!badge) {
      throw new NotFoundException('Badge not found');
    }
    return badge;
  }

  async update(id: number, updateBadgeDto: UpdateBadgeDto, currentUserId = 0) {
    const badge = await this.findOne(id);
    Object.assign(updateBadgeDto, { updatedByUserId: currentUserId });
    Object.assign(badge, updateBadgeDto);
    await this.repo.save(badge);
    return badge;
  }

  async remove(id: number) {
    const badge = await this.findOne(id);
    await this.repo.remove(badge);
    return badge;
  }
}
