import { Body, Controller, Get, HttpException, HttpStatus, Post } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { Unprotected } from 'nest-keycloak-connect';
import { CurrentUser } from '../users/decorators/current-user.decorator';
import { User } from '../users/entities/user.entity';
import { AuthService } from './auth.service';
import { LoginDto } from './dto/login.dto';
import { Grant } from 'keycloak-connect';
import { Logger } from '@nestjs/common';

@ApiTags('Auth')
@ApiBearerAuth()
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}
  @Post('login')
  @Unprotected()
  @ApiOperation({ summary: 'Log in' })
  @ApiOkResponse({ description: 'Login successful!' })
  async login(@Body() loginDto: LoginDto): Promise<Grant> {
    const { email, password } = loginDto;
    try {
      return await this.authService.login(email, password);
    } catch (err) {
      Logger.log(`authentication failed for user ${email}: ${err}`);
      throw new HttpException('Authentication Failed', HttpStatus.UNAUTHORIZED);
    }
  }

  @Get('whoami')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Find current user' })
  @ApiOkResponse({ description: 'Operation successful!' })
  whoAmI(@CurrentUser() user: User) {
    return user ? user : 'unknown';
  }
}
