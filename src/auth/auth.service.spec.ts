import { faker } from '@faker-js/faker';
import { Test, TestingModule } from '@nestjs/testing';
import KeycloakConnect, { Token } from 'keycloak-connect';
import { KEYCLOAK_INSTANCE } from 'nest-keycloak-connect';
import { AuthService } from './auth.service';

const fakeGrant = {
  access_token: 'fake-access-token' as unknown as Token,
  update: () => {
    return null;
  },
  isExpired: () => false,
} as KeycloakConnect.Grant;

describe('AuthService', () => {
  let service: AuthService;

  beforeEach(async () => {
    const obtainDirectly = jest.fn().mockResolvedValue(fakeGrant);

    const module: TestingModule = await Test.createTestingModule({
      providers: [AuthService],
    })
      .useMocker((token) => {
        if (token === KEYCLOAK_INSTANCE) {
          return { grantManager: { obtainDirectly } };
        }
      })
      .compile();

    service = module.get<AuthService>(AuthService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should login', () => {
    expect(service.login(faker.datatype.string(), faker.datatype.string())).resolves.toEqual(fakeGrant);
  });
});
