import { SetMetadata } from '@nestjs/common';
import { UserRole } from './user-role.enum';

export const UserRolesMetaDataKey = 'user-roles';

export function UserRoles(...roles: UserRole[]) {
  return SetMetadata(UserRolesMetaDataKey, roles);
}
