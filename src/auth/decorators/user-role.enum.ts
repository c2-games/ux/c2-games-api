export enum UserRole {
  Announcer = 'announcer',
  AwardAdmin = 'award_admin',
  EventAdmin = 'event_admin',
  Staff = 'staff',
}
