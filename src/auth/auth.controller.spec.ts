import { faker } from '@faker-js/faker';
import { Test, TestingModule } from '@nestjs/testing';
import KeycloakConnect, { Token } from 'keycloak-connect';
import { createFakeUser } from '../utils/test-helpers/fakes/user-faker';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';

const fakeGrant = {
  access_token: 'fake-access-token' as unknown as Token,
  update: () => {
    return null;
  },
  isExpired: () => false,
} as KeycloakConnect.Grant;

describe('AuthController', () => {
  let controller: AuthController;
  let service: AuthService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AuthController],
      providers: [
        {
          provide: AuthService,
          useValue: {
            login: jest.fn().mockImplementation(() => Promise.resolve(fakeGrant)),
            whoAmI: jest.fn(),
          },
        },
      ],
    }).compile();

    controller = module.get<AuthController>(AuthController);
    service = module.get<AuthService>(AuthService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should log in with valid request body', async () => {
    const loginDto = {
      email: faker.datatype.string(),
      password: faker.datatype.string(),
    };

    expect(controller.login(loginDto)).resolves.toEqual(fakeGrant);
    expect(service.login).toBeCalledTimes(1);
    expect(service.login).toBeCalledWith(loginDto.email, loginDto.password);
  });

  it('should return current user', () => {
    const fakeUser = createFakeUser();

    expect(controller.whoAmI(fakeUser)).toEqual(fakeUser);
  });
});
