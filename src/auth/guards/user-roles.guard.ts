import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { UserRolesMetaDataKey } from '../decorators/roles.decorator';
import { UserRole } from '../decorators/user-role.enum';

@Injectable()
export class UserRolesGuard implements CanActivate {
  constructor(private reflector: Reflector) {}

  getRoles(user: any): string[] {
    const claims = user['https://c2games.org/jwt/claims'];
    if (!claims) return [];
    return claims['x-tracking-api-roles'] ?? [];
  }

  canActivate(context: ExecutionContext): boolean {
    const requiredRoles = this.reflector.getAllAndOverride<UserRole[]>(UserRolesMetaDataKey, [
      context.getHandler(),
      context.getClass(),
    ]);
    if (!requiredRoles) {
      return true;
    }
    const { user } = context.switchToHttp().getRequest();
    const userRoles = this.getRoles(user);
    return requiredRoles.some((role) => userRoles?.includes(role));
  }
}
