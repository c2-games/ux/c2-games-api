import { Inject, Injectable } from '@nestjs/common';
import KeycloakConnect, { Grant } from 'keycloak-connect';
import { KEYCLOAK_INSTANCE } from 'nest-keycloak-connect';

@Injectable()
export class AuthService {
  constructor(@Inject(KEYCLOAK_INSTANCE) private keycloak: KeycloakConnect.Keycloak) {}

  login(email: string, password: string): Promise<Grant> {
    return this.keycloak.grantManager.obtainDirectly(email, password);
  }
}
