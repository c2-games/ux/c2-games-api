--
-- PostgreSQL database dump
--

-- Dumped from database version 14.2 (Debian 14.2-1.pgdg110+1)
-- Dumped by pg_dump version 14.2 (Debian 14.2-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: achievements; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.achievements (
    id integer NOT NULL,
    title character varying NOT NULL,
    description character varying NOT NULL,
    enabled boolean DEFAULT true NOT NULL,
    created_by_user_id integer DEFAULT 0 NOT NULL,
    created_date timestamp without time zone DEFAULT ('now'::text)::timestamp(6) with time zone NOT NULL,
    updated_by_user_id integer DEFAULT 0 NOT NULL,
    updated_date timestamp without time zone DEFAULT ('now'::text)::timestamp(6) with time zone NOT NULL
);


ALTER TABLE public.achievements OWNER TO root;

--
-- Name: achievements_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.achievements_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.achievements_id_seq OWNER TO root;

--
-- Name: achievements_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.achievements_id_seq OWNED BY public.achievements.id;


--
-- Name: badges; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.badges (
    id integer NOT NULL,
    title character varying NOT NULL,
    description character varying NOT NULL,
    image_url character varying NOT NULL,
    enabled boolean DEFAULT true NOT NULL,
    created_by_user_id integer DEFAULT 0 NOT NULL,
    created_date timestamp without time zone DEFAULT ('now'::text)::timestamp(6) with time zone NOT NULL,
    updated_by_user_id integer DEFAULT 0 NOT NULL,
    updated_date timestamp without time zone DEFAULT ('now'::text)::timestamp(6) with time zone NOT NULL
);


ALTER TABLE public.badges OWNER TO root;

--
-- Name: badges_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.badges_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.badges_id_seq OWNER TO root;

--
-- Name: badges_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.badges_id_seq OWNED BY public.badges.id;


--
-- Name: challenge_milestones; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.challenge_milestones (
    id integer NOT NULL,
    title character varying NOT NULL,
    description character varying NOT NULL,
    challenge_id integer NOT NULL,
    enabled boolean DEFAULT true NOT NULL,
    created_by_user_id integer DEFAULT 0 NOT NULL,
    created_date timestamp without time zone DEFAULT ('now'::text)::timestamp(6) with time zone NOT NULL,
    updated_by_user_id integer DEFAULT 0 NOT NULL,
    updated_date timestamp without time zone DEFAULT ('now'::text)::timestamp(6) with time zone NOT NULL
);


ALTER TABLE public.challenge_milestones OWNER TO root;

--
-- Name: challenge_milestones_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.challenge_milestones_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.challenge_milestones_id_seq OWNER TO root;

--
-- Name: challenge_milestones_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.challenge_milestones_id_seq OWNED BY public.challenge_milestones.id;


--
-- Name: challenges; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.challenges (
    id integer NOT NULL,
    title character varying NOT NULL,
    description character varying NOT NULL,
    enabled boolean DEFAULT true NOT NULL,
    created_by_user_id integer DEFAULT 0 NOT NULL,
    created_date timestamp without time zone DEFAULT ('now'::text)::timestamp(6) with time zone NOT NULL,
    updated_by_user_id integer DEFAULT 0 NOT NULL,
    updated_date timestamp without time zone DEFAULT ('now'::text)::timestamp(6) with time zone NOT NULL
);


ALTER TABLE public.challenges OWNER TO root;

--
-- Name: challenges_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.challenges_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.challenges_id_seq OWNER TO root;

--
-- Name: challenges_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.challenges_id_seq OWNED BY public.challenges.id;


--
-- Name: event_checklist_items; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.event_checklist_items (
    id integer NOT NULL,
    title character varying NOT NULL,
    description character varying NOT NULL,
    enabled boolean DEFAULT true NOT NULL,
    created_by_user_id integer DEFAULT 0 NOT NULL,
    created_date timestamp without time zone DEFAULT ('now'::text)::timestamp(6) with time zone NOT NULL,
    updated_by_user_id integer DEFAULT 0 NOT NULL,
    updated_date timestamp without time zone DEFAULT ('now'::text)::timestamp(6) with time zone NOT NULL
);


ALTER TABLE public.event_checklist_items OWNER TO root;

--
-- Name: event_checklist_items_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.event_checklist_items_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.event_checklist_items_id_seq OWNER TO root;

--
-- Name: event_checklist_items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.event_checklist_items_id_seq OWNED BY public.event_checklist_items.id;


--
-- Name: event_teams; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.event_teams (
    id integer NOT NULL,
    event_id integer NOT NULL,
    name character varying NOT NULL,
    enabled boolean DEFAULT true NOT NULL,
    created_by_user_id integer DEFAULT 0 NOT NULL,
    created_date timestamp without time zone DEFAULT ('now'::text)::timestamp(6) with time zone NOT NULL,
    updated_by_user_id integer DEFAULT 0 NOT NULL,
    updated_date timestamp without time zone DEFAULT ('now'::text)::timestamp(6) with time zone NOT NULL
);


ALTER TABLE public.event_teams OWNER TO root;

--
-- Name: event_teams_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.event_teams_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.event_teams_id_seq OWNER TO root;

--
-- Name: event_teams_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.event_teams_id_seq OWNED BY public.event_teams.id;


--
-- Name: event_teams_x_event_checklist_items; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.event_teams_x_event_checklist_items (
    id integer NOT NULL,
    event_id integer NOT NULL,
    event_team_id integer NOT NULL,
    event_checklist_item_id integer NOT NULL,
    completed_on timestamp without time zone,
    enabled boolean DEFAULT true NOT NULL,
    created_by_user_id integer DEFAULT 0 NOT NULL,
    created_date timestamp without time zone DEFAULT ('now'::text)::timestamp(6) with time zone NOT NULL,
    updated_by_user_id integer DEFAULT 0 NOT NULL,
    updated_date timestamp without time zone DEFAULT ('now'::text)::timestamp(6) with time zone NOT NULL
);


ALTER TABLE public.event_teams_x_event_checklist_items OWNER TO root;

--
-- Name: event_teams_x_event_checklist_items_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.event_teams_x_event_checklist_items_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.event_teams_x_event_checklist_items_id_seq OWNER TO root;

--
-- Name: event_teams_x_event_checklist_items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.event_teams_x_event_checklist_items_id_seq OWNED BY public.event_teams_x_event_checklist_items.id;


--
-- Name: event_teams_x_users; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.event_teams_x_users (
    id integer NOT NULL,
    event_team_id integer NOT NULL,
    user_id integer NOT NULL,
    added_on timestamp without time zone DEFAULT ('now'::text)::timestamp(6) with time zone,
    enabled boolean DEFAULT true NOT NULL,
    created_by_user_id integer DEFAULT 0 NOT NULL,
    created_date timestamp without time zone DEFAULT ('now'::text)::timestamp(6) with time zone NOT NULL,
    updated_by_user_id integer DEFAULT 0 NOT NULL,
    updated_date timestamp without time zone DEFAULT ('now'::text)::timestamp(6) with time zone NOT NULL
);


ALTER TABLE public.event_teams_x_users OWNER TO root;

--
-- Name: event_teams_x_users_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.event_teams_x_users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.event_teams_x_users_id_seq OWNER TO root;

--
-- Name: event_teams_x_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.event_teams_x_users_id_seq OWNED BY public.event_teams_x_users.id;


--
-- Name: events; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.events (
    id integer NOT NULL,
    name character varying NOT NULL,
    starts_on timestamp without time zone NOT NULL,
    enabled boolean DEFAULT true NOT NULL,
    created_by_user_id integer DEFAULT 0 NOT NULL,
    created_date timestamp without time zone DEFAULT ('now'::text)::timestamp(6) with time zone NOT NULL,
    updated_by_user_id integer DEFAULT 0 NOT NULL,
    updated_date timestamp without time zone DEFAULT ('now'::text)::timestamp(6) with time zone NOT NULL,
    regions text[],
    rules character varying
);


ALTER TABLE public.events OWNER TO root;

--
-- Name: events_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.events_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.events_id_seq OWNER TO root;

--
-- Name: events_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.events_id_seq OWNED BY public.events.id;


--
-- Name: institutions; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.institutions (
    id integer NOT NULL,
    name character varying NOT NULL,
    domain character varying NOT NULL,
    state character varying NOT NULL,
    region character varying NOT NULL,
    updated_date timestamp without time zone DEFAULT ('now'::text)::timestamp(6) with time zone NOT NULL,
    created_date timestamp without time zone DEFAULT ('now'::text)::timestamp(6) with time zone NOT NULL
);


ALTER TABLE public.institutions OWNER TO root;

--
-- Name: institutions_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.institutions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.institutions_id_seq OWNER TO root;

--
-- Name: institutions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.institutions_id_seq OWNED BY public.institutions.id;


--
-- Name: messages; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.messages (
    id integer NOT NULL,
    content character varying NOT NULL,
    announced_on timestamp without time zone DEFAULT ('now'::text)::timestamp(6) with time zone NOT NULL,
    enabled boolean DEFAULT true NOT NULL,
    created_by_user_id integer DEFAULT 0 NOT NULL,
    created_date timestamp without time zone DEFAULT ('now'::text)::timestamp(6) with time zone NOT NULL,
    updated_by_user_id integer DEFAULT 0 NOT NULL,
    updated_date timestamp without time zone DEFAULT ('now'::text)::timestamp(6) with time zone NOT NULL
);


ALTER TABLE public.messages OWNER TO root;

--
-- Name: messages_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.messages_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.messages_id_seq OWNER TO root;

--
-- Name: messages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.messages_id_seq OWNED BY public.messages.id;


--
-- Name: migrations; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.migrations (
    id integer NOT NULL,
    "timestamp" bigint NOT NULL,
    name character varying NOT NULL
);


ALTER TABLE public.migrations OWNER TO root;

--
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.migrations_id_seq OWNER TO root;

--
-- Name: migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;


--
-- Name: typeorm_metadata; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.typeorm_metadata (
    type character varying NOT NULL,
    database character varying,
    schema character varying,
    "table" character varying,
    name character varying,
    value text
);


ALTER TABLE public.typeorm_metadata OWNER TO root;

--
-- Name: user_checklist_items; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.user_checklist_items (
    id integer NOT NULL,
    title character varying NOT NULL,
    description character varying NOT NULL,
    enabled boolean DEFAULT true NOT NULL,
    created_by_user_id integer DEFAULT 0 NOT NULL,
    created_date timestamp without time zone DEFAULT ('now'::text)::timestamp(6) with time zone NOT NULL,
    updated_by_user_id integer DEFAULT 0 NOT NULL,
    updated_date timestamp without time zone DEFAULT ('now'::text)::timestamp(6) with time zone NOT NULL
);


ALTER TABLE public.user_checklist_items OWNER TO root;

--
-- Name: user_checklist_items_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.user_checklist_items_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_checklist_items_id_seq OWNER TO root;

--
-- Name: user_checklist_items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.user_checklist_items_id_seq OWNED BY public.user_checklist_items.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.users (
    id integer NOT NULL,
    email character varying NOT NULL,
    keycloak_sid character varying NOT NULL,
    enabled boolean DEFAULT true NOT NULL,
    created_by_user_id integer DEFAULT 0 NOT NULL,
    created_date timestamp without time zone DEFAULT ('now'::text)::timestamp(6) with time zone NOT NULL,
    updated_by_user_id integer DEFAULT 0 NOT NULL,
    updated_date timestamp without time zone DEFAULT ('now'::text)::timestamp(6) with time zone NOT NULL
);


ALTER TABLE public.users OWNER TO root;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO root;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: users_x_achievements; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.users_x_achievements (
    id integer NOT NULL,
    user_id integer NOT NULL,
    achievement_id integer NOT NULL,
    earned_on timestamp without time zone DEFAULT ('now'::text)::timestamp(6) with time zone NOT NULL,
    enabled boolean DEFAULT true NOT NULL,
    created_by_user_id integer DEFAULT 0 NOT NULL,
    created_date timestamp without time zone DEFAULT ('now'::text)::timestamp(6) with time zone NOT NULL,
    updated_by_user_id integer DEFAULT 0 NOT NULL,
    updated_date timestamp without time zone DEFAULT ('now'::text)::timestamp(6) with time zone NOT NULL
);


ALTER TABLE public.users_x_achievements OWNER TO root;

--
-- Name: users_x_achievements_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.users_x_achievements_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_x_achievements_id_seq OWNER TO root;

--
-- Name: users_x_achievements_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.users_x_achievements_id_seq OWNED BY public.users_x_achievements.id;


--
-- Name: users_x_badges; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.users_x_badges (
    id integer NOT NULL,
    user_id integer NOT NULL,
    badge_id integer NOT NULL,
    earned_on timestamp without time zone DEFAULT ('now'::text)::timestamp(6) with time zone NOT NULL,
    enabled boolean DEFAULT true NOT NULL,
    created_by_user_id integer DEFAULT 0 NOT NULL,
    created_date timestamp without time zone DEFAULT ('now'::text)::timestamp(6) with time zone NOT NULL,
    updated_by_user_id integer DEFAULT 0 NOT NULL,
    updated_date timestamp without time zone DEFAULT ('now'::text)::timestamp(6) with time zone NOT NULL
);


ALTER TABLE public.users_x_badges OWNER TO root;

--
-- Name: users_x_badges_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.users_x_badges_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_x_badges_id_seq OWNER TO root;

--
-- Name: users_x_badges_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.users_x_badges_id_seq OWNED BY public.users_x_badges.id;


--
-- Name: users_x_challenge_milestones; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.users_x_challenge_milestones (
    id integer NOT NULL,
    user_id integer NOT NULL,
    challenge_milestone_id integer NOT NULL,
    completed_on timestamp without time zone DEFAULT ('now'::text)::timestamp(6) with time zone NOT NULL,
    enabled boolean DEFAULT true NOT NULL,
    created_by_user_id integer DEFAULT 0 NOT NULL,
    created_date timestamp without time zone DEFAULT ('now'::text)::timestamp(6) with time zone NOT NULL,
    updated_by_user_id integer DEFAULT 0 NOT NULL,
    updated_date timestamp without time zone DEFAULT ('now'::text)::timestamp(6) with time zone NOT NULL
);


ALTER TABLE public.users_x_challenge_milestones OWNER TO root;

--
-- Name: users_x_challenge_milestones_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.users_x_challenge_milestones_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_x_challenge_milestones_id_seq OWNER TO root;

--
-- Name: users_x_challenge_milestones_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.users_x_challenge_milestones_id_seq OWNED BY public.users_x_challenge_milestones.id;


--
-- Name: users_x_messages; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.users_x_messages (
    id integer NOT NULL,
    user_id integer NOT NULL,
    message_id integer NOT NULL,
    seen_on timestamp without time zone DEFAULT ('now'::text)::timestamp(6) with time zone NOT NULL,
    enabled boolean DEFAULT true NOT NULL,
    created_by_user_id integer DEFAULT 0 NOT NULL,
    created_date timestamp without time zone DEFAULT ('now'::text)::timestamp(6) with time zone NOT NULL,
    updated_by_user_id integer DEFAULT 0 NOT NULL,
    updated_date timestamp without time zone DEFAULT ('now'::text)::timestamp(6) with time zone NOT NULL
);


ALTER TABLE public.users_x_messages OWNER TO root;

--
-- Name: users_x_messages_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.users_x_messages_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_x_messages_id_seq OWNER TO root;

--
-- Name: users_x_messages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.users_x_messages_id_seq OWNED BY public.users_x_messages.id;


--
-- Name: users_x_user_checklist_items; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.users_x_user_checklist_items (
    id integer NOT NULL,
    user_id integer NOT NULL,
    user_checklist_item_id integer NOT NULL,
    completed_on timestamp without time zone,
    enabled boolean DEFAULT true NOT NULL,
    created_by_user_id integer DEFAULT 0 NOT NULL,
    created_date timestamp without time zone DEFAULT ('now'::text)::timestamp(6) with time zone NOT NULL,
    updated_by_user_id integer DEFAULT 0 NOT NULL,
    updated_date timestamp without time zone DEFAULT ('now'::text)::timestamp(6) with time zone NOT NULL
);


ALTER TABLE public.users_x_user_checklist_items OWNER TO root;

--
-- Name: users_x_user_checklist_items_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.users_x_user_checklist_items_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_x_user_checklist_items_id_seq OWNER TO root;

--
-- Name: users_x_user_checklist_items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.users_x_user_checklist_items_id_seq OWNED BY public.users_x_user_checklist_items.id;


--
-- Name: achievements id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.achievements ALTER COLUMN id SET DEFAULT nextval('public.achievements_id_seq'::regclass);


--
-- Name: badges id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.badges ALTER COLUMN id SET DEFAULT nextval('public.badges_id_seq'::regclass);


--
-- Name: challenge_milestones id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.challenge_milestones ALTER COLUMN id SET DEFAULT nextval('public.challenge_milestones_id_seq'::regclass);


--
-- Name: challenges id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.challenges ALTER COLUMN id SET DEFAULT nextval('public.challenges_id_seq'::regclass);


--
-- Name: event_checklist_items id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.event_checklist_items ALTER COLUMN id SET DEFAULT nextval('public.event_checklist_items_id_seq'::regclass);


--
-- Name: event_teams id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.event_teams ALTER COLUMN id SET DEFAULT nextval('public.event_teams_id_seq'::regclass);


--
-- Name: event_teams_x_event_checklist_items id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.event_teams_x_event_checklist_items ALTER COLUMN id SET DEFAULT nextval('public.event_teams_x_event_checklist_items_id_seq'::regclass);


--
-- Name: event_teams_x_users id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.event_teams_x_users ALTER COLUMN id SET DEFAULT nextval('public.event_teams_x_users_id_seq'::regclass);


--
-- Name: events id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.events ALTER COLUMN id SET DEFAULT nextval('public.events_id_seq'::regclass);


--
-- Name: institutions id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.institutions ALTER COLUMN id SET DEFAULT nextval('public.institutions_id_seq'::regclass);


--
-- Name: messages id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.messages ALTER COLUMN id SET DEFAULT nextval('public.messages_id_seq'::regclass);


--
-- Name: migrations id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);


--
-- Name: user_checklist_items id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.user_checklist_items ALTER COLUMN id SET DEFAULT nextval('public.user_checklist_items_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Name: users_x_achievements id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.users_x_achievements ALTER COLUMN id SET DEFAULT nextval('public.users_x_achievements_id_seq'::regclass);


--
-- Name: users_x_badges id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.users_x_badges ALTER COLUMN id SET DEFAULT nextval('public.users_x_badges_id_seq'::regclass);


--
-- Name: users_x_challenge_milestones id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.users_x_challenge_milestones ALTER COLUMN id SET DEFAULT nextval('public.users_x_challenge_milestones_id_seq'::regclass);


--
-- Name: users_x_messages id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.users_x_messages ALTER COLUMN id SET DEFAULT nextval('public.users_x_messages_id_seq'::regclass);


--
-- Name: users_x_user_checklist_items id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.users_x_user_checklist_items ALTER COLUMN id SET DEFAULT nextval('public.users_x_user_checklist_items_id_seq'::regclass);


--
-- Data for Name: achievements; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.achievements (id, title, description, enabled, created_by_user_id, created_date, updated_by_user_id, updated_date) FROM stdin;
\.


--
-- Data for Name: badges; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.badges (id, title, description, image_url, enabled, created_by_user_id, created_date, updated_by_user_id, updated_date) FROM stdin;
\.


--
-- Data for Name: challenge_milestones; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.challenge_milestones (id, title, description, challenge_id, enabled, created_by_user_id, created_date, updated_by_user_id, updated_date) FROM stdin;
\.


--
-- Data for Name: challenges; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.challenges (id, title, description, enabled, created_by_user_id, created_date, updated_by_user_id, updated_date) FROM stdin;
\.


--
-- Data for Name: event_checklist_items; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.event_checklist_items (id, title, description, enabled, created_by_user_id, created_date, updated_by_user_id, updated_date) FROM stdin;
\.


--
-- Data for Name: event_teams; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.event_teams (id, event_id, name, enabled, created_by_user_id, created_date, updated_by_user_id, updated_date) FROM stdin;
20	15	Tracking API Community College	t	11	2023-01-06 01:25:37.46487	11	2023-01-06 01:25:37.46487
22	20	C2Games Academy	t	2	2023-01-06 19:14:25.024923	2	2023-01-06 19:14:25.024923
23	21	C2Games Academy	t	2	2023-01-06 21:31:17.539438	2	2023-01-06 21:31:17.539438
\.


--
-- Data for Name: event_teams_x_event_checklist_items; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.event_teams_x_event_checklist_items (id, event_id, event_team_id, event_checklist_item_id, completed_on, enabled, created_by_user_id, created_date, updated_by_user_id, updated_date) FROM stdin;
\.


--
-- Data for Name: event_teams_x_users; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.event_teams_x_users (id, event_team_id, user_id, added_on, enabled, created_by_user_id, created_date, updated_by_user_id, updated_date) FROM stdin;
33	20	11	2023-01-05 20:25:37.549	t	11	2023-01-06 01:25:37.693953	11	2023-01-06 01:25:37.693953
36	22	2	2023-01-06 14:14:25.109	t	2	2023-01-06 19:14:25.239194	2	2023-01-06 19:14:25.239194
37	22	12	2023-01-06 16:29:46.979	t	2	2023-01-06 21:29:47.159725	2	2023-01-06 21:29:47.159725
38	23	2	2023-01-06 16:31:17.626	t	2	2023-01-06 21:31:17.826258	2	2023-01-06 21:31:17.826258
\.


--
-- Data for Name: events; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.events (id, name, starts_on, enabled, created_by_user_id, created_date, updated_by_user_id, updated_date, regions, rules) FROM stdin;
15	NCAE CyberGames Northeast 2	2023-03-11 09:00:00	t	0	2022-12-14 02:15:02.949904	2	2022-12-21 18:08:49.322437	{northeast}	/rules
16	NCAE CyberGames Northwest 1	2023-03-11 11:00:00	t	0	2022-12-14 02:15:03.295483	2	2022-12-21 18:08:49.611942	{northwest}	/rules
17	NCAE CyberGames Northwest 2	2023-03-25 11:00:00	t	0	2022-12-14 02:15:03.639364	2	2022-12-21 18:08:49.883411	{northwest}	/rules
18	NCAE CyberGames Southeast 1	2023-03-04 09:00:00	t	0	2022-12-14 02:15:03.944962	2	2022-12-21 18:08:50.168888	{southeast}	/rules
19	NCAE CyberGames Southeast 2	2023-04-01 09:00:00	t	0	2022-12-14 02:15:04.278018	2	2022-12-21 18:08:50.466585	{southeast}	/rules
20	NCAE CyberGames Southwest 1	2023-03-04 11:00:00	t	0	2022-12-14 02:15:04.583942	2	2022-12-21 18:08:50.744039	{southwest}	/rules
21	NCAE CyberGames Southwest 2	2023-03-25 12:00:00	t	0	2022-12-14 02:15:04.862346	2	2022-12-21 18:08:51.051681	{southwest}	/rules
22	NCAE CyberGames Midwest 1	2023-02-18 10:00:00	t	0	2022-12-14 02:15:05.211226	2	2022-12-21 18:08:51.360728	{midwest}	/rules
23	NCAE CyberGames Midwest 2	2023-04-01 10:00:00	t	0	2022-12-14 02:15:05.485774	2	2022-12-21 18:08:51.661263	{midwest}	/rules
24	NCAE CyberGames East Wildcard	2023-04-08 09:00:00	t	0	2022-12-14 02:15:05.828301	2	2022-12-21 18:08:53.39939	{northeast,southeast}	/rules
25	NCAE CyberGames West Wildcard	2023-04-08 11:00:00	t	0	2022-12-14 02:15:06.160472	2	2022-12-21 18:08:54.093833	{northwest,southwest,midwest}	/rules
14	NCAE CyberGames Northeast 1	2023-02-18 09:00:00	t	0	2022-12-14 02:15:02.664805	2	2022-12-21 18:09:48.025395	{northeast}	/rules
\.


--
-- Data for Name: institutions; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.institutions (id, name, domain, state, region, updated_date, created_date) FROM stdin;
1	Air Force Institute of Technology	afit.edu	Ohio	midwest	2022-11-22 20:26:02.554037	2022-11-22 20:26:02.554037
2	Alamance Community College	alamancecc.edu	North Carolina	southeast	2022-11-22 20:26:02.90958	2022-11-22 20:26:02.90958
3	Alexandria Technical and Community College	alextech.edu	Minnesota	midwest	2022-11-22 20:26:03.262298	2022-11-22 20:26:03.262298
4	American Public University System	apus.edu	West Virginia	northeast	2022-11-22 20:26:03.684397	2022-11-22 20:26:03.684397
5	Anne Arundel Community College	aacc.edu	Maryland	northeast	2022-11-22 20:26:04.004355	2022-11-22 20:26:04.004355
6	Arapahoe Community College	arapahoe.edu	Colorado	northwest	2022-11-22 20:26:04.360896	2022-11-22 20:26:04.360896
7	Arizona State University	asu.edu	Arizona	southwest	2022-11-22 20:26:04.725304	2022-11-22 20:26:04.725304
8	Athens State University	athens.edu	Alabama	southeast	2022-11-22 20:26:05.266633	2022-11-22 20:26:05.266633
9	Auburn University	auburn.edu	Alabama	southeast	2022-11-22 20:26:05.635818	2022-11-22 20:26:05.635818
10	Augusta Technical College	augustatech.edu	Georgia	southeast	2022-11-22 20:26:06.052598	2022-11-22 20:26:06.052598
11	Augusta University	augusta.edu	Georgia	southeast	2022-11-22 20:26:06.611426	2022-11-22 20:26:06.611426
12	Baker College	baker.edu	Michigan	midwest	2022-11-22 20:26:07.001298	2022-11-22 20:26:07.001298
13	Bay Path University	baypath.edu	Massachusetts	northeast	2022-11-22 20:26:07.300386	2022-11-22 20:26:07.300386
14	Bellevue University	bellevue.edu	Nebraska	midwest	2022-11-22 20:26:07.617251	2022-11-22 20:26:07.617251
15	Binghamton University (SUNY at Binghamton)	binghamton.edu	New York	northeast	2022-11-22 20:26:07.993396	2022-11-22 20:26:07.993396
16	Bismarck State College	bismarckstate.edu	North Dakota	northwest	2022-11-22 20:26:08.288163	2022-11-22 20:26:08.288163
17	Bloomsburg University of Pennsylvania	bloomu.edu	Pennsylvania	northeast	2022-11-22 20:26:08.646165	2022-11-22 20:26:08.646165
18	Blue Ridge Community and Technical College	blueridgectc.edu	West Virginia	northeast	2022-11-22 20:26:08.939713	2022-11-22 20:26:08.939713
19	Blue Ridge Community College	blueridge.edu	North Carolina	southeast	2022-11-22 20:26:09.262765	2022-11-22 20:26:09.262765
20	Bluegrass Community and Technical College	kctcs.edu	Kentucky	southeast	2022-11-22 20:26:09.587739	2022-11-22 20:26:09.587739
21	Boise State University	boisestate.edu	Idaho	northwest	2022-11-22 20:26:09.939997	2022-11-22 20:26:09.939997
22	Bossier Parish Community College	bpcc.edu	Louisiana	southwest	2022-11-22 20:26:10.231518	2022-11-22 20:26:10.231518
23	Boston University	bu.edu	Massachusetts	northeast	2022-11-22 20:26:10.504668	2022-11-22 20:26:10.504668
24	Bowie State University	bowiestate.edu	Maryland	northeast	2022-11-22 20:26:10.785349	2022-11-22 20:26:10.785349
25	Brigham Young University	byu.edu	Utah	northwest	2022-11-22 20:26:11.080268	2022-11-22 20:26:11.080268
26	Brookdale Community College	brookdalecc.edu	New Jersey	northeast	2022-11-22 20:26:11.336752	2022-11-22 20:26:11.336752
27	Butler Community College	butlercc.edu	Kansas	midwest	2022-11-22 20:26:11.655188	2022-11-22 20:26:11.655188
28	Calhoun Community College	calhoun.edu	Alabama	southeast	2022-11-22 20:26:11.983187	2022-11-22 20:26:11.983187
29	California State Polytechnic University, Pomona	cpp.edu	California	southwest	2022-11-22 20:26:12.26648	2022-11-22 20:26:12.26648
30	California State University, Sacramento	csus.edu	California	southwest	2022-11-22 20:26:12.542981	2022-11-22 20:26:12.542981
31	California State University, San Bernardino	csusb.edu	California	southwest	2022-11-22 20:26:12.824631	2022-11-22 20:26:12.824631
32	California State University, San Marcos	csusm.edu	California	southwest	2022-11-22 20:26:13.110274	2022-11-22 20:26:13.110274
33	Capella University	capella.edu	Minnesota	midwest	2022-11-22 20:26:13.409328	2022-11-22 20:26:13.409328
34	Capitol Technology University	captechu.edu	Maryland	northeast	2022-11-22 20:26:13.798341	2022-11-22 20:26:13.798341
35	Carnegie Mellon University	cmu.edu	Pennsylvania	northeast	2022-11-22 20:26:14.098115	2022-11-22 20:26:14.098115
36	Cecil College	cecil.edu	Maryland	northeast	2022-11-22 20:26:14.471704	2022-11-22 20:26:14.471704
37	Cedarville University	cedarville.edu	Ohio	midwest	2022-11-22 20:26:14.812419	2022-11-22 20:26:14.812419
38	Central Michigan University	cmich.edu	Michigan	midwest	2022-11-22 20:26:15.090961	2022-11-22 20:26:15.090961
39	Central New Mexico Community College	cnm.edu	New Mexico	southwest	2022-11-22 20:26:15.355561	2022-11-22 20:26:15.355561
40	Central Piedmont Community College	cpcc.edu	North Carolina	southeast	2022-11-22 20:26:15.62563	2022-11-22 20:26:15.62563
41	Century College	century.edu	Minnesota	midwest	2022-11-22 20:26:15.929292	2022-11-22 20:26:15.929292
42	Champlain College	champlain.edu	Vermont	northeast	2022-11-22 20:26:16.19381	2022-11-22 20:26:16.19381
43	Chemeketa Community College	chemeketa.edu	Oregon	northwest	2022-11-22 20:26:16.479702	2022-11-22 20:26:16.479702
44	Chippewa Valley Technical College	cvtc.edu	Wisconsin	midwest	2022-11-22 20:26:16.833637	2022-11-22 20:26:16.833637
45	City College of San Francisco	ccsf.edu	California	southwest	2022-11-22 20:26:17.111478	2022-11-22 20:26:17.111478
46	City University of Seattle	cityu.edu	Washington	northwest	2022-11-22 20:26:17.491985	2022-11-22 20:26:17.491985
47	Clark State College	clarkstate.edu	Ohio	midwest	2022-11-22 20:26:17.881499	2022-11-22 20:26:17.881499
48	Clemson University	clemson.edu	South Carolina	southeast	2022-11-22 20:26:18.311132	2022-11-22 20:26:18.311132
49	Coastline Community College	coastline.edu	California	southwest	2022-11-22 20:26:18.602888	2022-11-22 20:26:18.602888
50	College of Coastal Georgia	ccga.edu	Georgia	southeast	2022-11-22 20:26:19.003164	2022-11-22 20:26:19.003164
51	College of DuPage	cod.edu	Illinois	midwest	2022-11-22 20:26:19.283699	2022-11-22 20:26:19.283699
52	College of Eastern Idaho	cei.edu	Idaho	northwest	2022-11-22 20:26:19.653396	2022-11-22 20:26:19.653396
53	College of Southern Maryland	csmd.edu	Maryland	northeast	2022-11-22 20:26:19.996743	2022-11-22 20:26:19.996743
54	College of Southern Nevada	csn.edu	Nevada	southwest	2022-11-22 20:26:20.280661	2022-11-22 20:26:20.280661
55	Collin College	collin.edu	Texas	southwest	2022-11-22 20:26:20.586825	2022-11-22 20:26:20.586825
56	Colorado Mesa University	coloradomesa.edu	Colorado	northwest	2022-11-22 20:26:20.869521	2022-11-22 20:26:20.869521
57	Colorado School of Mines	mines.edu	Colorado	northwest	2022-11-22 20:26:21.154065	2022-11-22 20:26:21.154065
58	Colorado State University	colostate.edu	Colorado	northwest	2022-11-22 20:26:21.470417	2022-11-22 20:26:21.470417
59	Colorado State University-Pueblo	csupueblo.edu	Colorado	northwest	2022-11-22 20:26:21.768392	2022-11-22 20:26:21.768392
60	Colorado Technical University	coloradotech.edu	Colorado	northwest	2022-11-22 20:26:22.053652	2022-11-22 20:26:22.053652
61	Columbia Basin College	columbiabasin.edu	Washington	northwest	2022-11-22 20:26:22.34572	2022-11-22 20:26:22.34572
62	Columbus State Community College	cscc.edu	Ohio	midwest	2022-11-22 20:26:22.633618	2022-11-22 20:26:22.633618
63	Columbus State University	columbusstate.edu	Georgia	southeast	2022-11-22 20:26:22.945222	2022-11-22 20:26:22.945222
64	Community College of Rhode Island	ccri.edu	Rhode Island	northeast	2022-11-22 20:26:23.23074	2022-11-22 20:26:23.23074
65	Cosumnes River College	losrios.edu	California	southwest	2022-11-22 20:26:23.518366	2022-11-22 20:26:23.518366
66	County College of Morris	ccm.edu	New Jersey	northeast	2022-11-22 20:26:23.780381	2022-11-22 20:26:23.780381
67	Cypress College	cypresscollege.edu	California	southwest	2022-11-22 20:26:24.054706	2022-11-22 20:26:24.054706
68	Dakota State University	dsu.edu	South Dakota	northwest	2022-11-22 20:26:24.341852	2022-11-22 20:26:24.341852
69	Danville Community College	vccs.edu	Virginia	northeast	2022-11-22 20:26:24.712848	2022-11-22 20:26:24.712848
70	Dartmouth College	dartmouth.edu	New Hampshire	northeast	2022-11-22 20:26:24.991631	2022-11-22 20:26:24.991631
71	Davenport University	davenport.edu	Michigan	midwest	2022-11-22 20:26:25.258814	2022-11-22 20:26:25.258814
72	Delta College	delta.edu	Michigan	midwest	2022-11-22 20:26:25.528106	2022-11-22 20:26:25.528106
73	DePaul University	depaul.edu	Illinois	midwest	2022-11-22 20:26:25.824703	2022-11-22 20:26:25.824703
74	Des Moines Area Community College	dmacc.edu	Iowa	midwest	2022-11-22 20:26:26.165339	2022-11-22 20:26:26.165339
75	Drexel University	drexel.edu	Pennsylvania	northeast	2022-11-22 20:26:26.455192	2022-11-22 20:26:26.455192
76	East Carolina University	ecu.edu	North Carolina	southeast	2022-11-22 20:26:26.74481	2022-11-22 20:26:26.74481
77	East Stroudsburg University	esu.edu	Pennsylvania	northeast	2022-11-22 20:26:27.029677	2022-11-22 20:26:27.029677
78	Eastern Florida State College	easternflorida.edu	Florida	southeast	2022-11-22 20:26:27.29545	2022-11-22 20:26:27.29545
79	Eastern Michigan University	emich.edu	Michigan	midwest	2022-11-22 20:26:27.6471	2022-11-22 20:26:27.6471
80	Eastern New Mexico University - Ruidoso	enmu.edu	New Mexico	southwest	2022-11-22 20:26:28.018503	2022-11-22 20:26:28.018503
81	Eastern Washington University	ewu.edu	Washington	northwest	2022-11-22 20:26:28.302929	2022-11-22 20:26:28.302929
82	ECPI University	ecpi.edu	Virginia	northeast	2022-11-22 20:26:29.627721	2022-11-22 20:26:29.627721
83	Edmonds College	edmonds.edu	Washington	northwest	2022-11-22 20:26:29.953839	2022-11-22 20:26:29.953839
84	El Paso Community College	epcc.edu	Texas	southwest	2022-11-22 20:26:30.22766	2022-11-22 20:26:30.22766
85	Embry-Riddle Aeronautical University	erau.edu	Florida	southeast	2022-11-22 20:26:30.506861	2022-11-22 20:26:30.506861
86	Estrella Mountain Community College	estrellamountain.edu	Arizona	southwest	2022-11-22 20:26:30.800576	2022-11-22 20:26:30.800576
87	Excelsior University	excelsior.edu	New York	northeast	2022-11-22 20:26:31.096242	2022-11-22 20:26:31.096242
88	Fairleigh Dickinson University	fdu.edu	New Jersey	northeast	2022-11-22 20:26:31.37662	2022-11-22 20:26:31.37662
89	Fayetteville Technical Community College	faytechcc.edu	North Carolina	southeast	2022-11-22 20:26:31.710962	2022-11-22 20:26:31.710962
90	Ferris State University	ferris.edu	Michigan	midwest	2022-11-22 20:26:32.000772	2022-11-22 20:26:32.000772
91	Florida Agricultural and Mechanical University	famu.edu	Florida	southeast	2022-11-22 20:26:32.336122	2022-11-22 20:26:32.336122
92	Florida Atlantic University	fau.edu	Florida	southeast	2022-11-22 20:26:32.614307	2022-11-22 20:26:32.614307
93	Florida Institute of Technology	fit.edu	Florida	southeast	2022-11-22 20:26:32.964106	2022-11-22 20:26:32.964106
94	Florida International University	fiu.edu	Florida	southeast	2022-11-22 20:26:33.250098	2022-11-22 20:26:33.250098
95	Florida Memorial University	fmuniv.edu	Florida	southeast	2022-11-22 20:26:33.538264	2022-11-22 20:26:33.538264
96	Florida State College at Jacksonville	fscj.edu	Florida	southeast	2022-11-22 20:26:33.811314	2022-11-22 20:26:33.811314
97	Florida State University	fsu.edu	Florida	southeast	2022-11-22 20:26:34.105797	2022-11-22 20:26:34.105797
98	Fordham University	fordham.edu	New York	northeast	2022-11-22 20:26:34.392197	2022-11-22 20:26:34.392197
99	Forsyth Technical Community College	forsythtech.edu	North Carolina	southeast	2022-11-22 20:26:34.685099	2022-11-22 20:26:34.685099
100	Fort Hays State University	fhsu.edu	Kansas	midwest	2022-11-22 20:26:34.968831	2022-11-22 20:26:34.968831
101	Franklin University	franklin.edu	Ohio	midwest	2022-11-22 20:26:35.296033	2022-11-22 20:26:35.296033
102	Fullerton College	fullcoll.edu	California	southwest	2022-11-22 20:26:35.585042	2022-11-22 20:26:35.585042
103	Gaston College	gaston.edu	North Carolina	southeast	2022-11-22 20:26:35.882945	2022-11-22 20:26:35.882945
104	George Mason University	gmu.edu	Virginia	northeast	2022-11-22 20:26:36.215666	2022-11-22 20:26:36.215666
105	Georgetown University	georgetown.edu	District of Columbia	northeast	2022-11-22 20:26:36.50017	2022-11-22 20:26:36.50017
106	Georgia Institute of Technology	gatech.edu	Georgia	southeast	2022-11-22 20:26:36.794984	2022-11-22 20:26:36.794984
107	Georgia State University	gsu.edu	Georgia	southeast	2022-11-22 20:26:37.202944	2022-11-22 20:26:37.202944
108	Germanna Community College	germanna.edu	Virginia	northeast	2022-11-22 20:26:37.498101	2022-11-22 20:26:37.498101
109	Glendale Community College	gccaz.edu	Arizona	southwest	2022-11-22 20:26:37.763236	2022-11-22 20:26:37.763236
110	Grand Canyon University	gcu.edu	Arizona	southwest	2022-11-22 20:26:38.087325	2022-11-22 20:26:38.087325
111	Grand Rapids Community College	grcc.edu	Michigan	midwest	2022-11-22 20:26:38.388872	2022-11-22 20:26:38.388872
112	Great Falls College Montana State University	gfcmsu.edu	Montana	northwest	2022-11-22 20:26:38.689937	2022-11-22 20:26:38.689937
113	Green River College	greenriver.edu	Washington	northwest	2022-11-22 20:26:38.995509	2022-11-22 20:26:38.995509
114	Guilford Technical Community College	gtcc.edu	North Carolina	southeast	2022-11-22 20:26:39.288107	2022-11-22 20:26:39.288107
115	Gwinnett Technical College	gwinnetttech.edu	Georgia	southeast	2022-11-22 20:26:39.578431	2022-11-22 20:26:39.578431
116	Hagerstown Community College	hagerstowncc.edu	Maryland	northeast	2022-11-22 20:26:40.079044	2022-11-22 20:26:40.079044
117	Hampton University	hamptonu.edu	Virginia	northeast	2022-11-22 20:26:40.37183	2022-11-22 20:26:40.37183
118	Harford Community College	harford.edu	Maryland	northeast	2022-11-22 20:26:40.673225	2022-11-22 20:26:40.673225
119	Henry Ford College	hfcc.edu	Michigan	midwest	2022-11-22 20:26:40.973855	2022-11-22 20:26:40.973855
120	Highline College	highline.edu	Washington	northwest	2022-11-22 20:26:41.38289	2022-11-22 20:26:41.38289
121	Hill College	hillcollege.edu	Texas	southwest	2022-11-22 20:26:41.718251	2022-11-22 20:26:41.718251
122	Honolulu Community College	hawaii.edu	Hawaii	southwest	2022-11-22 20:26:42.042944	2022-11-22 20:26:42.042944
123	Hood College	hood.edu	Maryland	northeast	2022-11-22 20:26:42.386464	2022-11-22 20:26:42.386464
124	Houston Community College	hccs.edu	Texas	southwest	2022-11-22 20:26:42.71449	2022-11-22 20:26:42.71449
125	Howard Community College	howardcc.edu	Maryland	northeast	2022-11-22 20:26:42.991614	2022-11-22 20:26:42.991614
126	Hudson County Community College	hccc.edu	New Jersey	northeast	2022-11-22 20:26:43.257056	2022-11-22 20:26:43.257056
127	Idaho State University	niatec.info	Idaho	northwest	2022-11-22 20:26:43.53459	2022-11-22 20:26:43.53459
128	Illinois Institute of Technology	iit.edu	Illinois	midwest	2022-11-22 20:26:43.887009	2022-11-22 20:26:43.887009
129	Illinois State University	illinoisstate.edu	Illinois	midwest	2022-11-22 20:26:44.158632	2022-11-22 20:26:44.158632
130	Indian River State College	irsc.edu	Florida	southeast	2022-11-22 20:26:44.415991	2022-11-22 20:26:44.415991
131	Indiana University	iu.edu	Indiana	midwest	2022-11-22 20:26:44.746854	2022-11-22 20:26:44.746854
132	Indiana University of Pennsylvania	iup.edu	Pennsylvania	northeast	2022-11-22 20:26:45.011004	2022-11-22 20:26:45.011004
133	Iowa State University	iastate.edu	Iowa	midwest	2022-11-22 20:26:45.277801	2022-11-22 20:26:45.277801
134	Ivy Tech Community College	ivytech.edu	Indiana	midwest	2022-11-22 20:26:45.5416	2022-11-22 20:26:45.5416
135	Jackson State Community College	jscc.edu	Tennessee	southeast	2022-11-22 20:26:45.824655	2022-11-22 20:26:45.824655
136	Jacksonville State University	jsu.edu	Alabama	southeast	2022-11-22 20:26:46.098657	2022-11-22 20:26:46.098657
137	James Madison University	jmu.edu	Virginia	northeast	2022-11-22 20:26:46.366704	2022-11-22 20:26:46.366704
138	John A Logan College	jalc.edu	Illinois	midwest	2022-11-22 20:26:46.621539	2022-11-22 20:26:46.621539
139	Johnson & Wales University	jwu.edu	Rhode Island	northeast	2022-11-22 20:26:46.91717	2022-11-22 20:26:46.91717
140	Johnson County Community College	jccc.edu	Kansas	midwest	2022-11-22 20:26:47.200914	2022-11-22 20:26:47.200914
141	Kansas State University	ksu.edu	Kansas	midwest	2022-11-22 20:26:47.508321	2022-11-22 20:26:47.508321
142	Kean University	kean.edu	New Jersey	northeast	2022-11-22 20:26:47.871639	2022-11-22 20:26:47.871639
143	Kennesaw State University	kennesaw.edu	Georgia	southeast	2022-11-22 20:26:48.14946	2022-11-22 20:26:48.14946
144	Kent State University	kent.edu	Ohio	midwest	2022-11-22 20:26:48.507791	2022-11-22 20:26:48.507791
145	Lake Superior College	lsc.edu	Minnesota	midwest	2022-11-22 20:26:48.771466	2022-11-22 20:26:48.771466
146	Lakeland Community College	lakelandcc.edu	Ohio	midwest	2022-11-22 20:26:49.069657	2022-11-22 20:26:49.069657
147	Lansing Community College	lcc.edu	Michigan	midwest	2022-11-22 20:26:49.359139	2022-11-22 20:26:49.359139
148	Laredo College	laredo.edu	Texas	southwest	2022-11-22 20:26:49.640482	2022-11-22 20:26:49.640482
149	Laurel Ridge Community College	lfcc.edu	Virginia	northeast	2022-11-22 20:26:49.926694	2022-11-22 20:26:49.926694
150	Leeward Community College	hawaii.edu	Hawaii	southwest	2022-11-22 20:26:50.24499	2022-11-22 20:26:50.24499
151	Lehigh Carbon Community College	lccc.edu	Pennsylvania	northeast	2022-11-22 20:26:50.52948	2022-11-22 20:26:50.52948
152	Lemoyne-Owen College	loc.edu	Tennessee	southeast	2022-11-22 20:26:50.821296	2022-11-22 20:26:50.821296
153	Lewis University	lewisu.edu	Illinois	midwest	2022-11-22 20:26:51.105235	2022-11-22 20:26:51.105235
154	Liberty University	liberty.edu	Virginia	northeast	2022-11-22 20:26:51.424801	2022-11-22 20:26:51.424801
155	Lincoln Land Community College	llcc.edu	Illinois	midwest	2022-11-22 20:26:51.72619	2022-11-22 20:26:51.72619
156	Long Beach City College	lbcc.edu	California	southwest	2022-11-22 20:26:52.094816	2022-11-22 20:26:52.094816
157	Louisiana State University	lsu.edu	Louisiana	southwest	2022-11-22 20:26:52.427327	2022-11-22 20:26:52.427327
158	Louisiana Tech University	latech.edu	Louisiana	southwest	2022-11-22 20:26:52.723332	2022-11-22 20:26:52.723332
159	Loyola University Chicago	luc.edu	Illinois	midwest	2022-11-22 20:26:53.058892	2022-11-22 20:26:53.058892
160	Macomb Community College	macomb.edu	Michigan	midwest	2022-11-22 20:26:53.357879	2022-11-22 20:26:53.357879
161	Madison College	madisoncollege.edu	Wisconsin	midwest	2022-11-22 20:26:53.650933	2022-11-22 20:26:53.650933
162	Marquette University	marquette.edu	Wisconsin	midwest	2022-11-22 20:26:53.932049	2022-11-22 20:26:53.932049
163	Marymount University	marymount.edu	Virginia	northeast	2022-11-22 20:26:54.209528	2022-11-22 20:26:54.209528
164	McLennan Community College	mclennan.edu	Texas	southwest	2022-11-22 20:26:54.508582	2022-11-22 20:26:54.508582
165	Mercy College	mercy.edu	New York	northeast	2022-11-22 20:26:54.878226	2022-11-22 20:26:54.878226
166	Metro State University	metrostate.edu	Minnesota	midwest	2022-11-22 20:26:55.193809	2022-11-22 20:26:55.193809
167	Metropolitan Community College	mccneb.edu	Nebraska	midwest	2022-11-22 20:26:55.473601	2022-11-22 20:26:55.473601
168	Metropolitan Community College - Kansas City	mcckc.edu	Missouri	midwest	2022-11-22 20:26:55.822771	2022-11-22 20:26:55.822771
169	Metropolitan State University of Denver	msudenver.edu	Colorado	northwest	2022-11-22 20:26:56.130072	2022-11-22 20:26:56.130072
170	Miami Dade College	mdc.edu	Florida	southeast	2022-11-22 20:26:56.428788	2022-11-22 20:26:56.428788
171	Middle Georgia State University	mga.edu	Georgia	southeast	2022-11-22 20:26:56.712216	2022-11-22 20:26:56.712216
172	Minot State University	minotstateu.edu	North Dakota	northwest	2022-11-22 20:26:57.008596	2022-11-22 20:26:57.008596
173	Mississippi State University	msstate.edu	Mississippi	southeast	2022-11-22 20:26:57.340842	2022-11-22 20:26:57.340842
174	Missoula College	umt.edu	Montana	northwest	2022-11-22 20:26:57.628184	2022-11-22 20:26:57.628184
175	Missouri University of Science and Technology	mst.edu	Missouri	midwest	2022-11-22 20:26:57.912627	2022-11-22 20:26:57.912627
176	Mohawk Valley Community College	mvcc.edu	New York	northeast	2022-11-22 20:26:58.211905	2022-11-22 20:26:58.211905
177	Montgomery College	montgomerycollege.edu	Maryland	northeast	2022-11-22 20:26:58.486236	2022-11-22 20:26:58.486236
178	Montreat College	montreat.edu	North Carolina	southeast	2022-11-22 20:26:58.770044	2022-11-22 20:26:58.770044
179	Moraine Valley Community College	morainevalley.edu	Illinois	midwest	2022-11-22 20:26:59.020382	2022-11-22 20:26:59.020382
180	Morgan State University	morgan.edu	Maryland	northeast	2022-11-22 20:26:59.322158	2022-11-22 20:26:59.322158
181	Mount Aloysius College	mtaloy.edu	Pennsylvania	northeast	2022-11-22 20:26:59.592651	2022-11-22 20:26:59.592651
182	Mountain Empire Community College	mecc.edu	Virginia	northeast	2022-11-22 20:26:59.908901	2022-11-22 20:26:59.908901
183	Mt. Hood Community College	mhcc.edu	Oregon	northwest	2022-11-22 20:27:00.216083	2022-11-22 20:27:00.216083
184	Murray State University	murraystate.edu	Kentucky	southeast	2022-11-22 20:27:00.530013	2022-11-22 20:27:00.530013
185	National University	nu.edu	California	southwest	2022-11-22 20:27:00.90731	2022-11-22 20:27:00.90731
186	Naval Postgraduate School	nps.edu	California	southwest	2022-11-22 20:27:01.197802	2022-11-22 20:27:01.197802
187	New England Institute of Technology	neit.edu	Rhode Island	northeast	2022-11-22 20:27:01.553469	2022-11-22 20:27:01.553469
188	New Jersey City University	njcu.edu	New Jersey	northeast	2022-11-22 20:27:01.872683	2022-11-22 20:27:01.872683
189	New Jersey Institute of Technology	njit.edu	New Jersey	northeast	2022-11-22 20:27:02.177386	2022-11-22 20:27:02.177386
190	New Mexico Tech	nmt.edu	New Mexico	southwest	2022-11-22 20:27:02.457266	2022-11-22 20:27:02.457266
191	New River Community College	nr.edu	Virginia	northeast	2022-11-22 20:27:02.747187	2022-11-22 20:27:02.747187
192	New York Institute of Technology	nyit.edu	New York	northeast	2022-11-22 20:27:03.015489	2022-11-22 20:27:03.015489
193	New York University	nyu.edu	New York	northeast	2022-11-22 20:27:03.292753	2022-11-22 20:27:03.292753
194	Norfolk State University	nsu.edu	Virginia	northeast	2022-11-22 20:27:03.602782	2022-11-22 20:27:03.602782
195	North Carolina A&T State University	ncat.edu	North Carolina	southeast	2022-11-22 20:27:03.907704	2022-11-22 20:27:03.907704
196	North Carolina State University	ncsu.edu	North Carolina	southeast	2022-11-22 20:27:04.178789	2022-11-22 20:27:04.178789
197	North Dakota State University	ndsu.edu	North Dakota	northwest	2022-11-22 20:27:04.536676	2022-11-22 20:27:04.536676
198	North Idaho College	nic.edu	Idaho	northwest	2022-11-22 20:27:04.815822	2022-11-22 20:27:04.815822
199	Northampton Community College	northampton.edu	Pennsylvania	northeast	2022-11-22 20:27:05.100292	2022-11-22 20:27:05.100292
200	Northeast Community College	northeast.edu	Nebraska	midwest	2022-11-22 20:27:05.438738	2022-11-22 20:27:05.438738
201	Northeastern University	neu.edu	Massachusetts	northeast	2022-11-22 20:27:05.7185	2022-11-22 20:27:05.7185
202	Northern Kentucky University	nku.edu	Kentucky	southeast	2022-11-22 20:27:06.109441	2022-11-22 20:27:06.109441
203	Northern Michigan University	nmu.edu	Michigan	midwest	2022-11-22 20:27:06.443127	2022-11-22 20:27:06.443127
204	Northern Virginia Community College	nvcc.edu	Virginia	northeast	2022-11-22 20:27:07.125337	2022-11-22 20:27:07.125337
205	Norwich University	norwich.edu	Vermont	northeast	2022-11-22 20:27:07.41701	2022-11-22 20:27:07.41701
206	Nova Southeastern University	nova.edu	Florida	southeast	2022-11-22 20:27:07.703262	2022-11-22 20:27:07.703262
207	Oakland University	oakland.edu	Michigan	midwest	2022-11-22 20:27:07.994126	2022-11-22 20:27:07.994126
208	Ohlone College	ohlone.edu	California	southwest	2022-11-22 20:27:08.311845	2022-11-22 20:27:08.311845
209	Oklahoma Christian University	oc.edu	Oklahoma	southwest	2022-11-22 20:27:08.617191	2022-11-22 20:27:08.617191
210	Oklahoma City Community College	occc.edu	Oklahoma	southwest	2022-11-22 20:27:08.904255	2022-11-22 20:27:08.904255
211	Oklahoma State University	okstate.edu	Oklahoma	southwest	2022-11-22 20:27:09.185169	2022-11-22 20:27:09.185169
212	Old Dominion University	odu.edu	Virginia	northeast	2022-11-22 20:27:09.443178	2022-11-22 20:27:09.443178
213	Our Lady of the Lake University	ollusa.edu	Texas	southwest	2022-11-22 20:27:09.724035	2022-11-22 20:27:09.724035
214	Owensboro Community and Technical College	kctcs.edu	Kentucky	southeast	2022-11-22 20:27:10.011735	2022-11-22 20:27:10.011735
215	Pace University	pace.edu	New York	northeast	2022-11-22 20:27:10.280253	2022-11-22 20:27:10.280253
216	Pennsylvania Highlands Community College	pennhighlands.edu	Pennsylvania	northeast	2022-11-22 20:27:10.554872	2022-11-22 20:27:10.554872
217	Pennsylvania State University	psu.edu	Pennsylvania	northeast	2022-11-22 20:27:10.836536	2022-11-22 20:27:10.836536
218	Pikes Peak Community College	ppcc.edu	Colorado	northwest	2022-11-22 20:27:11.180596	2022-11-22 20:27:11.180596
219	Pitt Community College	pittcc.edu	North Carolina	southeast	2022-11-22 20:27:11.468828	2022-11-22 20:27:11.468828
220	Pittsburgh Technical College	ptcollege.edu	Pennsylvania	northeast	2022-11-22 20:27:11.835199	2022-11-22 20:27:11.835199
221	Polytechnic University of Puerto Rico	pupr.edu	Puerto Rico	southeast	2022-11-22 20:27:12.130024	2022-11-22 20:27:12.130024
222	Portland Community College	pcc.edu	Oregon	northwest	2022-11-22 20:27:12.422158	2022-11-22 20:27:12.422158
223	Portland State University	pdx.edu	Oregon	northwest	2022-11-22 20:27:12.707246	2022-11-22 20:27:12.707246
224	Prince George's Community College	pgcc.edu	Maryland	northeast	2022-11-22 20:27:12.995265	2022-11-22 20:27:12.995265
225	Pueblo Community College	pueblocc.edu	Colorado	northwest	2022-11-22 20:27:13.278954	2022-11-22 20:27:13.278954
226	Purdue University	purdue.edu	Indiana	midwest	2022-11-22 20:27:13.565276	2022-11-22 20:27:13.565276
227	Purdue University Global	purdueglobal.edu	Indiana	midwest	2022-11-22 20:27:13.891476	2022-11-22 20:27:13.891476
228	Purdue University Northwest	pnw.edu	Indiana	midwest	2022-11-22 20:27:14.202533	2022-11-22 20:27:14.202533
229	Radford University	radford.edu	Virginia	northeast	2022-11-22 20:27:14.479039	2022-11-22 20:27:14.479039
230	Red Rocks Community College	rrcc.edu	Colorado	northwest	2022-11-22 20:27:14.754821	2022-11-22 20:27:14.754821
231	Regent University	regent.edu	Virginia	northeast	2022-11-22 20:27:15.009474	2022-11-22 20:27:15.009474
232	Regis University	regis.edu	Colorado	northwest	2022-11-22 20:27:15.266268	2022-11-22 20:27:15.266268
233	Roane State Community College	roanestate.edu	Tennessee	southeast	2022-11-22 20:27:15.583837	2022-11-22 20:27:15.583837
234	Robert Morris University	rmu.edu	Pennsylvania	northeast	2022-11-22 20:27:15.852866	2022-11-22 20:27:15.852866
235	Rochester Institute of Technology	rit.edu	New York	northeast	2022-11-22 20:27:17.156978	2022-11-22 20:27:17.156978
236	Rock Valley College	rockvalleycollege.edu	Illinois	midwest	2022-11-22 20:27:17.509586	2022-11-22 20:27:17.509586
237	Rockland Community College	sunyrockland.edu	New York	northeast	2022-11-22 20:27:17.894894	2022-11-22 20:27:17.894894
238	Roosevelt University	roosevelt.edu	Illinois	midwest	2022-11-22 20:27:18.268798	2022-11-22 20:27:18.268798
239	Rose State College	rose.edu	Oklahoma	southwest	2022-11-22 20:27:18.546748	2022-11-22 20:27:18.546748
240	Rowan College  at Burlington County	rcbc.edu	New Jersey	northeast	2022-11-22 20:27:18.819489	2022-11-22 20:27:18.819489
241	Rutgers, State University of New Jersey	rutgers.edu	New Jersey	northeast	2022-11-22 20:27:19.07923	2022-11-22 20:27:19.07923
242	Sacred Heart University	sacredheart.edu	Connecticut	northeast	2022-11-22 20:27:19.366829	2022-11-22 20:27:19.366829
243	Saint Leo University	saintleo.edu	Florida	southeast	2022-11-22 20:27:19.646559	2022-11-22 20:27:19.646559
244	Saint Vincent College	stvincent.edu	Pennsylvania	northeast	2022-11-22 20:27:20.031853	2022-11-22 20:27:20.031853
245	Sam Houston State University	shsu.edu	Texas	southwest	2022-11-22 20:27:20.316347	2022-11-22 20:27:20.316347
246	Sampson Community College	sampsoncc.edu	North Carolina	southeast	2022-11-22 20:27:20.607669	2022-11-22 20:27:20.607669
247	San Antonio College	alamo.edu	Texas	southwest	2022-11-22 20:27:20.893835	2022-11-22 20:27:20.893835
248	SANS Technology Institute	sans.edu	Maryland	northeast	2022-11-22 20:27:21.191981	2022-11-22 20:27:21.191981
249	Sierra College	sierracollege.edu	California	southwest	2022-11-22 20:27:21.481852	2022-11-22 20:27:21.481852
250	Sinclair Community College	sinclair.edu	Ohio	midwest	2022-11-22 20:27:21.773844	2022-11-22 20:27:21.773844
251	Snead State Community College	snead.edu	Alabama	southeast	2022-11-22 20:27:22.096158	2022-11-22 20:27:22.096158
252	South Carolina State University	scsu.edu	South Carolina	southeast	2022-11-22 20:27:22.375119	2022-11-22 20:27:22.375119
253	South Texas College	southtexascollege.edu	Texas	southwest	2022-11-22 20:27:22.665354	2022-11-22 20:27:22.665354
254	Southeast Missouri State University	semo.edu	Missouri	midwest	2022-11-22 20:27:23.14892	2022-11-22 20:27:23.14892
255	Southern Maine Community College	smccme.edu	Maine	northeast	2022-11-22 20:27:23.450114	2022-11-22 20:27:23.450114
256	Southern Methodist University	smu.edu	Texas	southwest	2022-11-22 20:27:23.73485	2022-11-22 20:27:23.73485
257	Southern Utah University	suu.edu	Utah	northwest	2022-11-22 20:27:24.020154	2022-11-22 20:27:24.020154
258	Southwest Virginia Community College	sw.edu	Virginia	northeast	2022-11-22 20:27:25.308964	2022-11-22 20:27:25.308964
259	Spokane Falls Community College	spokane.edu	Washington	northwest	2022-11-22 20:27:25.70488	2022-11-22 20:27:25.70488
260	St. Cloud State University	stcloudstate.edu	Minnesota	midwest	2022-11-22 20:27:25.970968	2022-11-22 20:27:25.970968
261	St. John's University	stjohns.edu	New York	northeast	2022-11-22 20:27:26.278905	2022-11-22 20:27:26.278905
262	St. Louis Community College	stlcc.edu	Missouri	midwest	2022-11-22 20:27:26.566686	2022-11-22 20:27:26.566686
263	St. Mary's University	stmarytx.edu	Texas	southwest	2022-11-22 20:27:26.842045	2022-11-22 20:27:26.842045
264	St. Petersburg College	spcollege.edu	Florida	southeast	2022-11-22 20:27:27.313283	2022-11-22 20:27:27.313283
265	St. Philip's College	alamo.edu	Texas	southwest	2022-11-22 20:27:27.586474	2022-11-22 20:27:27.586474
266	Stevens Institute of Technology	stevens.edu	New Jersey	northeast	2022-11-22 20:27:27.947928	2022-11-22 20:27:27.947928
267	Strayer University	strayer.edu	Virginia	northeast	2022-11-22 20:27:28.23609	2022-11-22 20:27:28.23609
268	Suffolk County Community College	sunysuffolk.edu	New York	northeast	2022-11-22 20:27:28.537368	2022-11-22 20:27:28.537368
269	Syracuse University	syr.edu	New York	northeast	2022-11-22 20:27:28.812965	2022-11-22 20:27:28.812965
270	Talladega College	talladega.edu	Alabama	southeast	2022-11-22 20:27:29.113858	2022-11-22 20:27:29.113858
271	Tennessee Tech University	tntech.edu	Tennessee	southeast	2022-11-22 20:27:29.3991	2022-11-22 20:27:29.3991
272	Terra State Community College	terra.edu	Ohio	midwest	2022-11-22 20:27:29.670187	2022-11-22 20:27:29.670187
273	Texas A&M - San Antonio	tamusa.edu	Texas	southwest	2022-11-22 20:27:29.951577	2022-11-22 20:27:29.951577
274	Texas A&M University	tamu.edu	Texas	southwest	2022-11-22 20:27:30.263283	2022-11-22 20:27:30.263283
275	Texas A&M University-Corpus Christi	tamucc.edu	Texas	southwest	2022-11-22 20:27:30.555411	2022-11-22 20:27:30.555411
276	Texas State Technical College Harlingen	tstc.edu	Texas	southwest	2022-11-22 20:27:30.852293	2022-11-22 20:27:30.852293
277	The Citadel	citadel.edu	South Carolina	southeast	2022-11-22 20:27:31.144665	2022-11-22 20:27:31.144665
278	The College of Westchester	cw.edu	New York	northeast	2022-11-22 20:27:31.40473	2022-11-22 20:27:31.40473
279	The Community College of Baltimore County	ccbcmd.edu	Maryland	northeast	2022-11-22 20:27:31.700929	2022-11-22 20:27:31.700929
280	The George Washington University	gwu.edu	District of Columbia	northeast	2022-11-22 20:27:32.055375	2022-11-22 20:27:32.055375
281	The Johns Hopkins University	jhu.edu	Maryland	northeast	2022-11-22 20:27:32.336812	2022-11-22 20:27:32.336812
282	The Ohio State University	osu.edu	Ohio	midwest	2022-11-22 20:27:32.610615	2022-11-22 20:27:32.610615
283	The University of Alabama	ua.edu	Alabama	southeast	2022-11-22 20:27:32.888082	2022-11-22 20:27:32.888082
284	The University of Alabama at Birmingham	uab.edu	Alabama	southeast	2022-11-22 20:27:33.440001	2022-11-22 20:27:33.440001
285	The University of Alabama Huntsville	uah.edu	Alabama	southeast	2022-11-22 20:27:33.72212	2022-11-22 20:27:33.72212
286	The University of Arizona	arizona.edu	Arizona	southwest	2022-11-22 20:27:34.009132	2022-11-22 20:27:34.009132
287	The University of Tennessee at Chattanooga	utc.edu	Tennessee	southeast	2022-11-22 20:27:34.284013	2022-11-22 20:27:34.284013
288	The University of Texas at San Antonio	utsa.edu	Texas	southwest	2022-11-22 20:27:34.573553	2022-11-22 20:27:34.573553
289	Thomas Nelson Community College	tncc.edu	Virginia	northeast	2022-11-22 20:27:34.845894	2022-11-22 20:27:34.845894
290	Tidewater Community College	tcc.edu	Virginia	northeast	2022-11-22 20:27:35.148057	2022-11-22 20:27:35.148057
291	Tiffin University	tiffin.edu	Ohio	midwest	2022-11-22 20:27:35.525257	2022-11-22 20:27:35.525257
292	Towson University	towson.edu	Maryland	northeast	2022-11-22 20:27:36.637693	2022-11-22 20:27:36.637693
293	Trident Technical College	tridenttech.edu	South Carolina	southeast	2022-11-22 20:27:37.412496	2022-11-22 20:27:37.412496
294	Tuskegee University	tuskegee.edu	Alabama	southeast	2022-11-22 20:27:37.713957	2022-11-22 20:27:37.713957
295	United States Air Force Academy	af.mil	Colorado	northwest	2022-11-22 20:27:38.112941	2022-11-22 20:27:38.112941
296	United States Naval Academy	usna.edu	Maryland	northeast	2022-11-22 20:27:38.695922	2022-11-22 20:27:38.695922
297	University at Albany - State University of New York	albany.edu	New York	northeast	2022-11-22 20:27:38.983498	2022-11-22 20:27:38.983498
298	University at Buffalo, the State University of New York	buffalo.edu	New York	northeast	2022-11-22 20:27:39.262884	2022-11-22 20:27:39.262884
299	University of Advancing Technology	uat.edu	Arizona	southwest	2022-11-22 20:27:39.568381	2022-11-22 20:27:39.568381
300	University of Arkansas	uark.edu	Arkansas	southwest	2022-11-22 20:27:39.949728	2022-11-22 20:27:39.949728
301	University Of Arkansas at Little Rock	ualr.edu	Arkansas	southwest	2022-11-22 20:27:40.250322	2022-11-22 20:27:40.250322
302	University of California, Davis	ucdavis.edu	California	southwest	2022-11-22 20:27:40.550367	2022-11-22 20:27:40.550367
303	University of California, Irvine	uci.edu	California	southwest	2022-11-22 20:27:40.827612	2022-11-22 20:27:40.827612
304	University of Central Florida	ucf.edu	Florida	southeast	2022-11-22 20:27:41.116613	2022-11-22 20:27:41.116613
305	University of Central Missouri	ucmo.edu	Missouri	midwest	2022-11-22 20:27:41.400495	2022-11-22 20:27:41.400495
306	University of Cincinnati	uc.edu	Ohio	midwest	2022-11-22 20:27:41.690886	2022-11-22 20:27:41.690886
307	University of Colorado, Colorado Springs	uccs.edu	Colorado	northwest	2022-11-22 20:27:42.043955	2022-11-22 20:27:42.043955
308	University of Connecticut	uconn.edu	Connecticut	northeast	2022-11-22 20:27:42.347561	2022-11-22 20:27:42.347561
309	University of Dallas	udallas.edu	Texas	southwest	2022-11-22 20:27:42.686838	2022-11-22 20:27:42.686838
310	University of Dayton	udayton.edu	Ohio	midwest	2022-11-22 20:27:42.944007	2022-11-22 20:27:42.944007
311	University of Delaware	udel.edu	Delaware	northeast	2022-11-22 20:27:43.227862	2022-11-22 20:27:43.227862
312	University of Denver	du.edu	Colorado	northwest	2022-11-22 20:27:43.510037	2022-11-22 20:27:43.510037
313	University of Detroit, Mercy	udmercy.edu	Michigan	midwest	2022-11-22 20:27:43.82215	2022-11-22 20:27:43.82215
314	University of Findlay	findlay.edu	Ohio	midwest	2022-11-22 20:27:44.106502	2022-11-22 20:27:44.106502
315	University of Florida	ufl.edu	Florida	southeast	2022-11-22 20:27:44.391953	2022-11-22 20:27:44.391953
316	University of Georgia	uga.edu	Georgia	southeast	2022-11-22 20:27:44.68545	2022-11-22 20:27:44.68545
317	University of Hawaii - West Oahu	hawaii.edu	Hawaii	southwest	2022-11-22 20:27:44.951454	2022-11-22 20:27:44.951454
318	University of Hawaii at Manoa	hawaii.edu	Hawaii	southwest	2022-11-22 20:27:45.222881	2022-11-22 20:27:45.222881
319	University of Hawaii Maui College	hawaii.edu	Hawaii	southwest	2022-11-22 20:27:45.522418	2022-11-22 20:27:45.522418
320	University of Houston	uh.edu	Texas	southwest	2022-11-22 20:27:45.81103	2022-11-22 20:27:45.81103
321	University of Idaho	uidaho.edu	Idaho	northwest	2022-11-22 20:27:46.171891	2022-11-22 20:27:46.171891
322	University of Illinois at Springfield	uis.edu	Illinois	midwest	2022-11-22 20:27:46.467526	2022-11-22 20:27:46.467526
323	University of Illinois at Urbana-Champaign	illinois.edu	Illinois	midwest	2022-11-22 20:27:47.148928	2022-11-22 20:27:47.148928
324	University of Kansas	ku.edu	Kansas	midwest	2022-11-22 20:27:47.436614	2022-11-22 20:27:47.436614
325	University of Louisville, Kentucky	louisville.edu	Kentucky	southeast	2022-11-22 20:27:47.732944	2022-11-22 20:27:47.732944
326	University of Maine at Augusta	uma.edu	Maine	northeast	2022-11-22 20:27:48.018682	2022-11-22 20:27:48.018682
327	University of Maryland Global Campus	umuc.edu	Maryland	northeast	2022-11-22 20:27:48.416783	2022-11-22 20:27:48.416783
328	University of Maryland, Baltimore County	umbc.edu	Maryland	northeast	2022-11-22 20:27:48.710888	2022-11-22 20:27:48.710888
329	University of Maryland, College Park	umd.edu	Maryland	northeast	2022-11-22 20:27:49.020717	2022-11-22 20:27:49.020717
330	University of Massachusetts Dartmouth	umassd.edu	Massachusetts	northeast	2022-11-22 20:27:49.330827	2022-11-22 20:27:49.330827
331	University of Massachusetts Lowell	uml.edu	Massachusetts	northeast	2022-11-22 20:27:49.604292	2022-11-22 20:27:49.604292
332	University of Memphis	memphis.edu	Tennessee	southeast	2022-11-22 20:27:49.902986	2022-11-22 20:27:49.902986
333	University of Missouri - Columbia	missouri.edu	Missouri	midwest	2022-11-22 20:27:50.206199	2022-11-22 20:27:50.206199
334	University of Missouri - St. Louis	umsl.edu	Missouri	midwest	2022-11-22 20:27:50.494092	2022-11-22 20:27:50.494092
335	University of Nebraska at Omaha	unomaha.edu	Nebraska	midwest	2022-11-22 20:27:50.766204	2022-11-22 20:27:50.766204
336	University of Nevada, Las Vegas	unlv.edu	Nevada	southwest	2022-11-22 20:27:51.025988	2022-11-22 20:27:51.025988
337	University of Nevada, Reno	unr.edu	Nevada	southwest	2022-11-22 20:27:51.316201	2022-11-22 20:27:51.316201
338	University of New Hampshire	unh.edu	New Hampshire	northeast	2022-11-22 20:27:51.612865	2022-11-22 20:27:51.612865
339	University of New Haven	newhaven.edu	Connecticut	northeast	2022-11-22 20:27:51.915097	2022-11-22 20:27:51.915097
340	University of New Mexico	unm.edu	New Mexico	southwest	2022-11-22 20:27:52.22618	2022-11-22 20:27:52.22618
341	University of New Orleans	uno.edu	Louisiana	southwest	2022-11-22 20:27:52.53057	2022-11-22 20:27:52.53057
342	University of North Carolina, Charlotte	uncc.edu	North Carolina	southeast	2022-11-22 20:27:52.822673	2022-11-22 20:27:52.822673
343	University of North Carolina, Wilmington	uncw.edu	North Carolina	southeast	2022-11-22 20:27:53.11037	2022-11-22 20:27:53.11037
344	University of North Florida	unf.edu	Florida	southeast	2022-11-22 20:27:53.406812	2022-11-22 20:27:53.406812
345	University of North Georgia	ung.edu	Georgia	southeast	2022-11-22 20:27:53.697743	2022-11-22 20:27:53.697743
346	University of North Texas	unt.edu	Texas	southwest	2022-11-22 20:27:53.985796	2022-11-22 20:27:53.985796
347	University of Pittsburgh	pitt.edu	Pennsylvania	northeast	2022-11-22 20:27:54.300735	2022-11-22 20:27:54.300735
348	University of San Diego	sandiego.edu	California	southwest	2022-11-22 20:27:54.608859	2022-11-22 20:27:54.608859
349	University of South Alabama	usacfits.org	Alabama	southeast	2022-11-22 20:27:54.918426	2022-11-22 20:27:54.918426
350	University of South Carolina	sc.edu	South Carolina	southeast	2022-11-22 20:27:55.197156	2022-11-22 20:27:55.197156
351	University of South Florida	usf.edu	Florida	southeast	2022-11-22 20:27:55.532756	2022-11-22 20:27:55.532756
352	University of Southern Maine	maine.edu	Maine	northeast	2022-11-22 20:27:55.814927	2022-11-22 20:27:55.814927
353	University of Texas at Dallas	utdallas.edu	Texas	southwest	2022-11-22 20:27:56.310399	2022-11-22 20:27:56.310399
354	University of Texas at El Paso	utep.edu	Texas	southwest	2022-11-22 20:27:56.588116	2022-11-22 20:27:56.588116
355	University of the Cumberlands	ucumberlands.edu	Kentucky	southeast	2022-11-22 20:27:56.869761	2022-11-22 20:27:56.869761
356	University of Tulsa	utulsa.edu	Oklahoma	southwest	2022-11-22 20:27:57.144098	2022-11-22 20:27:57.144098
357	University of Virginia	virginia.edu	Virginia	northeast	2022-11-22 20:27:57.429135	2022-11-22 20:27:57.429135
358	University of Washington	uw.edu	Washington	northwest	2022-11-22 20:27:57.728804	2022-11-22 20:27:57.728804
359	University of West Florida	uwf.edu	Florida	southeast	2022-11-22 20:27:58.015446	2022-11-22 20:27:58.015446
360	University of Wisconsin-Stout	uwstout.edu	Wisconsin	midwest	2022-11-22 20:27:58.298597	2022-11-22 20:27:58.298597
361	University of Wisconsin-Whitewater	uww.edu	Wisconsin	midwest	2022-11-22 20:27:58.637723	2022-11-22 20:27:58.637723
362	Utica University	utica.edu	New York	northeast	2022-11-22 20:27:58.991888	2022-11-22 20:27:58.991888
363	Valencia College	valenciacollege.edu	Florida	southeast	2022-11-22 20:27:59.338671	2022-11-22 20:27:59.338671
364	Valley Forge Military College	vfmac.edu	Pennsylvania	northeast	2022-11-22 20:27:59.650212	2022-11-22 20:27:59.650212
365	Vincennes University	vinu.edu	Indiana	midwest	2022-11-22 20:27:59.969328	2022-11-22 20:27:59.969328
366	Virginia Commonwealth University	vcu.edu	Virginia	northeast	2022-11-22 20:28:00.283094	2022-11-22 20:28:00.283094
367	Virginia Polytechnic Institute and State University	vt.edu	Virginia	northeast	2022-11-22 20:28:00.576584	2022-11-22 20:28:00.576584
368	Virginia Western Community College	virginiawestern.edu	Virginia	northeast	2022-11-22 20:28:00.852526	2022-11-22 20:28:00.852526
369	Volunteer State Community College	volstate.edu	Tennessee	southeast	2022-11-22 20:28:01.153979	2022-11-22 20:28:01.153979
370	Wake Technical Community College	waketech.edu	North Carolina	southeast	2022-11-22 20:28:01.448511	2022-11-22 20:28:01.448511
371	Walden University	waldenu.edu	Minnesota	midwest	2022-11-22 20:28:01.729171	2022-11-22 20:28:01.729171
372	Wallace State Community College	wallacestate.edu	Alabama	southeast	2022-11-22 20:28:02.012161	2022-11-22 20:28:02.012161
373	Walsh College	walshcollege.edu	Michigan	midwest	2022-11-22 20:28:02.293157	2022-11-22 20:28:02.293157
374	Washtenaw Community College	wccnet.edu	Michigan	midwest	2022-11-22 20:28:02.586764	2022-11-22 20:28:02.586764
375	Waukesha County Technical College	wctc.edu	Wisconsin	midwest	2022-11-22 20:28:02.948385	2022-11-22 20:28:02.948385
376	Weber State University	weber.edu	Utah	northwest	2022-11-22 20:28:03.28019	2022-11-22 20:28:03.28019
377	Webster University	webster.edu	Missouri	midwest	2022-11-22 20:28:03.560469	2022-11-22 20:28:03.560469
378	West Chester University of Pennsylvania	wcupa.edu	Pennsylvania	northeast	2022-11-22 20:28:03.840537	2022-11-22 20:28:03.840537
379	West Virginia University	wvu.edu	West Virginia	northeast	2022-11-22 20:28:04.128949	2022-11-22 20:28:04.128949
380	Westchester Community College	sunywcc.edu	New York	northeast	2022-11-22 20:28:04.42253	2022-11-22 20:28:04.42253
381	Western Governors University	wgu.edu	Washington	northwest	2022-11-22 20:28:04.719305	2022-11-22 20:28:04.719305
382	Western Washington University	wwu.edu	Washington	northwest	2022-11-22 20:28:05.02298	2022-11-22 20:28:05.02298
383	Whatcom Community College	ctc.edu	Washington	northwest	2022-11-22 20:28:05.324088	2022-11-22 20:28:05.324088
384	Wichita State University	wichita.edu	Kansas	midwest	2022-11-22 20:28:05.614576	2022-11-22 20:28:05.614576
385	Wilmington University	wilmu.edu	Delaware	northeast	2022-11-22 20:28:05.898219	2022-11-22 20:28:05.898219
386	Worcester Polytechnic Institute	wpi.edu	Massachusetts	northeast	2022-11-22 20:28:06.177675	2022-11-22 20:28:06.177675
387	Wright State University	wright.edu	Ohio	midwest	2022-11-22 20:28:06.471661	2022-11-22 20:28:06.471661
388	Xavier University	xavier.edu	Ohio	midwest	2022-11-22 20:28:06.769346	2022-11-22 20:28:06.769346
389	Tracking API Community College	tapi.edu	New York	northeast	2022-12-02 20:48:43.301187	2022-12-02 20:48:43.301187
390	Example University	example.com	Washington	northwest	2023-01-06 01:27:11.580454	2023-01-06 01:27:11.580454
391	C2Games Academy	c2games.org	Texas	southwest	2023-01-06 01:28:25.273734	2023-01-06 01:28:25.273734
392	C2Games Academy 2	c2games.org	Texas	southwest	2023-01-09 17:20:04.440059	2023-01-09 17:20:04.440059
\.


--
-- Data for Name: messages; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.messages (id, content, announced_on, enabled, created_by_user_id, created_date, updated_by_user_id, updated_date) FROM stdin;
\.


--
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.migrations (id, "timestamp", name) FROM stdin;
1	1660065716190	InitDB1660065716190
2	1660065819722	InitSeed1660065819722
3	1660149794212	ChallengeEntitiesRelations1660149794212
4	1661807631056	EventsAndChecklists1661807631056
5	1662151892448	MissingForeignKeys1662151892448
6	1662584574997	UserMessages1662584574997
7	1663689051745	UserMessagesDefaultSeenOn1663689051745
8	1669070483308	AddInstitutions1669070483308
9	1669073136222	AddRegionToEvents1669073136222
10	1671580481323	AddRulesToEvent1671580481323
\.


--
-- Data for Name: typeorm_metadata; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.typeorm_metadata (type, database, schema, "table", name, value) FROM stdin;
\.


--
-- Data for Name: user_checklist_items; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.user_checklist_items (id, title, description, enabled, created_by_user_id, created_date, updated_by_user_id, updated_date) FROM stdin;
1	User Checklist Item Title	This is a user checklist item description	t	0	2022-11-15 02:26:12.231745	0	2022-11-15 02:26:12.231745
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.users (id, email, keycloak_sid, enabled, created_by_user_id, created_date, updated_by_user_id, updated_date) FROM stdin;
1	test@example.com	34e16a74-7881-468f-aa32-35ca8bbce459	t	0	2022-11-15 02:26:12.231745	0	2022-11-15 02:26:12.231745
2	bdavis@c2games.org	d6936ce4-8f88-47d2-b041-adbb5eb21883	t	0	2022-11-22 20:09:42.527359	0	2022-11-22 20:09:42.527359
3	bdavis@mvcc.edu	32e658f2-d9d0-42ef-ab9c-1ea9e9feebbb	t	0	2022-11-22 20:31:13.594636	0	2022-11-22 20:31:13.594636
4	jhartman32@gatech.edu	59e55c57-516a-413f-84c7-39c4eb2f361d	t	0	2022-11-30 02:59:06.930169	0	2022-11-30 02:59:06.930169
5	jhartman32+jenny@gatech.edu	9e60a363-c835-4972-8580-b1a72fa67631	t	0	2022-11-30 03:01:53.656796	0	2022-11-30 03:01:53.656796
6	jhartman32+jessica@gatech.edu	18bb24b7-e00d-4fd2-b545-90f50c2605fb	t	0	2022-11-30 03:10:17.230652	0	2022-11-30 03:10:17.230652
7	jhartman32+johnny@gatech.edu	8e20587e-4471-4d85-902b-c1d8507a59de	t	0	2022-11-30 03:23:29.867083	0	2022-11-30 03:23:29.867083
8	billy@example.com	6c1cc5b4-c37c-4726-a6a3-1d87956c0f4f	t	0	2022-12-02 20:19:23.982161	0	2022-12-02 20:19:23.982161
9	tapi-eventadmin@example.com	693432a3-dbdd-46fb-adf8-f9232e3dee24	t	0	2022-12-02 20:48:43.295173	0	2022-12-02 20:48:43.295173
10	user2@my.tapi.edu	0464dd51-a632-4497-a658-09fb60e19307	t	0	2022-12-02 20:50:42.61625	0	2022-12-02 20:50:42.61625
11	user1@tapi.edu	6308121b-5c95-4a53-922d-8cf410f0c65b	t	0	2022-12-02 20:52:07.524826	0	2022-12-02 20:52:07.524826
12	sradigan@c2games.org	b8e9cc26-5c35-4826-86b1-0c7bbca8128b	t	0	2022-12-06 16:16:13.915749	0	2022-12-06 16:16:13.915749
13	user3@my.tapi.edu	16b659a4-1a4a-4207-8903-6b445adeaf05	t	0	2022-12-06 16:16:38.092716	0	2022-12-06 16:16:38.092716
14	gulinodan@gmail.com	fdd40dc5-0f65-44c0-9ccf-db8647e3f6bd	t	0	2022-12-13 23:26:01.223115	0	2022-12-13 23:26:01.223115
15	michaela.laden@gmail.com	63040761-c9a2-4b53-9eb5-2fb7055bf58c	t	0	2022-12-14 00:34:09.396226	0	2022-12-14 00:34:09.396226
16	dgulino@mvcc.edu	e92a4423-b992-4fb0-bffb-b51917457ee8	t	0	2022-12-14 01:52:38.421423	0	2022-12-14 01:52:38.421423
17	sradigan@mvcc.edu	d74c69a8-b2a4-494f-a599-bcab55a7e7bd	t	0	2022-12-15 17:16:37.842482	0	2022-12-15 17:16:37.842482
\.


--
-- Data for Name: users_x_achievements; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.users_x_achievements (id, user_id, achievement_id, earned_on, enabled, created_by_user_id, created_date, updated_by_user_id, updated_date) FROM stdin;
\.


--
-- Data for Name: users_x_badges; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.users_x_badges (id, user_id, badge_id, earned_on, enabled, created_by_user_id, created_date, updated_by_user_id, updated_date) FROM stdin;
\.


--
-- Data for Name: users_x_challenge_milestones; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.users_x_challenge_milestones (id, user_id, challenge_milestone_id, completed_on, enabled, created_by_user_id, created_date, updated_by_user_id, updated_date) FROM stdin;
\.


--
-- Data for Name: users_x_messages; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.users_x_messages (id, user_id, message_id, seen_on, enabled, created_by_user_id, created_date, updated_by_user_id, updated_date) FROM stdin;
\.


--
-- Data for Name: users_x_user_checklist_items; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.users_x_user_checklist_items (id, user_id, user_checklist_item_id, completed_on, enabled, created_by_user_id, created_date, updated_by_user_id, updated_date) FROM stdin;
1	1	1	2022-11-15 02:26:12.231745	t	0	2022-11-15 02:26:12.231745	0	2022-11-15 02:26:12.231745
\.


--
-- Name: achievements_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.achievements_id_seq', 2, true);


--
-- Name: badges_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.badges_id_seq', 2, true);


--
-- Name: challenge_milestones_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.challenge_milestones_id_seq', 2, true);


--
-- Name: challenges_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.challenges_id_seq', 1, true);


--
-- Name: event_checklist_items_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.event_checklist_items_id_seq', 1, false);


--
-- Name: event_teams_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.event_teams_id_seq', 23, true);


--
-- Name: event_teams_x_event_checklist_items_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.event_teams_x_event_checklist_items_id_seq', 1, false);


--
-- Name: event_teams_x_users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.event_teams_x_users_id_seq', 38, true);


--
-- Name: events_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.events_id_seq', 25, true);


--
-- Name: institutions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.institutions_id_seq', 392, true);


--
-- Name: messages_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.messages_id_seq', 1, false);


--
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.migrations_id_seq', 10, true);


--
-- Name: user_checklist_items_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.user_checklist_items_id_seq', 1, false);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.users_id_seq', 17, true);


--
-- Name: users_x_achievements_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.users_x_achievements_id_seq', 1, true);


--
-- Name: users_x_badges_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.users_x_badges_id_seq', 1, true);


--
-- Name: users_x_challenge_milestones_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.users_x_challenge_milestones_id_seq', 1, true);


--
-- Name: users_x_messages_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.users_x_messages_id_seq', 1, false);


--
-- Name: users_x_user_checklist_items_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.users_x_user_checklist_items_id_seq', 1, false);


--
-- Name: users_x_user_checklist_items PK_025edb93876a9ae0d1def89c7b7; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.users_x_user_checklist_items
    ADD CONSTRAINT "PK_025edb93876a9ae0d1def89c7b7" PRIMARY KEY (id);


--
-- Name: event_teams PK_02a00c33f9aedce303aabb6ba16; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.event_teams
    ADD CONSTRAINT "PK_02a00c33f9aedce303aabb6ba16" PRIMARY KEY (id);


--
-- Name: institutions PK_0be7539dcdba335470dc05e9690; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.institutions
    ADD CONSTRAINT "PK_0be7539dcdba335470dc05e9690" PRIMARY KEY (id);


--
-- Name: event_teams_x_users PK_106cadb04bdc1bdbd356ea751a0; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.event_teams_x_users
    ADD CONSTRAINT "PK_106cadb04bdc1bdbd356ea751a0" PRIMARY KEY (id);


--
-- Name: user_checklist_items PK_13297aa78a1712c12387679d72c; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.user_checklist_items
    ADD CONSTRAINT "PK_13297aa78a1712c12387679d72c" PRIMARY KEY (id);


--
-- Name: messages PK_18325f38ae6de43878487eff986; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.messages
    ADD CONSTRAINT "PK_18325f38ae6de43878487eff986" PRIMARY KEY (id);


--
-- Name: achievements PK_1bc19c37c6249f70186f318d71d; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.achievements
    ADD CONSTRAINT "PK_1bc19c37c6249f70186f318d71d" PRIMARY KEY (id);


--
-- Name: challenges PK_1e664e93171e20fe4d6125466af; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.challenges
    ADD CONSTRAINT "PK_1e664e93171e20fe4d6125466af" PRIMARY KEY (id);


--
-- Name: users_x_achievements PK_36994c83219f82be2e65de51449; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.users_x_achievements
    ADD CONSTRAINT "PK_36994c83219f82be2e65de51449" PRIMARY KEY (id);


--
-- Name: events PK_40731c7151fe4be3116e45ddf73; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.events
    ADD CONSTRAINT "PK_40731c7151fe4be3116e45ddf73" PRIMARY KEY (id);


--
-- Name: users_x_badges PK_4ffc42e5d68f18875c79ec7f01d; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.users_x_badges
    ADD CONSTRAINT "PK_4ffc42e5d68f18875c79ec7f01d" PRIMARY KEY (id);


--
-- Name: event_checklist_items PK_53049b762cc9a1f90ae8d08eb70; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.event_checklist_items
    ADD CONSTRAINT "PK_53049b762cc9a1f90ae8d08eb70" PRIMARY KEY (id);


--
-- Name: users_x_challenge_milestones PK_74fea21c854a7c7753124ef4be3; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.users_x_challenge_milestones
    ADD CONSTRAINT "PK_74fea21c854a7c7753124ef4be3" PRIMARY KEY (id);


--
-- Name: badges PK_8a651318b8de577e8e217676466; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.badges
    ADD CONSTRAINT "PK_8a651318b8de577e8e217676466" PRIMARY KEY (id);


--
-- Name: migrations PK_8c82d7f526340ab734260ea46be; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT "PK_8c82d7f526340ab734260ea46be" PRIMARY KEY (id);


--
-- Name: challenge_milestones PK_98e9e29971613b8b4df571fda62; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.challenge_milestones
    ADD CONSTRAINT "PK_98e9e29971613b8b4df571fda62" PRIMARY KEY (id);


--
-- Name: users PK_a3ffb1c0c8416b9fc6f907b7433; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT "PK_a3ffb1c0c8416b9fc6f907b7433" PRIMARY KEY (id);


--
-- Name: event_teams_x_event_checklist_items PK_e30673003c16bc2a3d97f9542a5; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.event_teams_x_event_checklist_items
    ADD CONSTRAINT "PK_e30673003c16bc2a3d97f9542a5" PRIMARY KEY (id);


--
-- Name: users_x_messages PK_fa9065556fa75b2e5f6d9e4ea49; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.users_x_messages
    ADD CONSTRAINT "PK_fa9065556fa75b2e5f6d9e4ea49" PRIMARY KEY (id);


--
-- Name: institutions UQ_15c98649276025998cd1acaf61c; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.institutions
    ADD CONSTRAINT "UQ_15c98649276025998cd1acaf61c" UNIQUE (name);


--
-- Name: users_x_user_checklist_items UQ_2d794451a674702f8e33e7e531c; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.users_x_user_checklist_items
    ADD CONSTRAINT "UQ_2d794451a674702f8e33e7e531c" UNIQUE (user_id, user_checklist_item_id);


--
-- Name: users_x_messages UQ_4c192e495a64863f27d675b3ee4; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.users_x_messages
    ADD CONSTRAINT "UQ_4c192e495a64863f27d675b3ee4" UNIQUE (user_id, message_id);


--
-- Name: event_teams_x_event_checklist_items UQ_64331f18cad1e944fc3cf278478; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.event_teams_x_event_checklist_items
    ADD CONSTRAINT "UQ_64331f18cad1e944fc3cf278478" UNIQUE (event_id, event_team_id, event_checklist_item_id);


--
-- Name: event_teams_x_users UQ_706269390c8baf39a1411563f40; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.event_teams_x_users
    ADD CONSTRAINT "UQ_706269390c8baf39a1411563f40" UNIQUE (event_team_id, user_id);


--
-- Name: users_x_challenge_milestones UQ_72c536445ace74c093e326be55a; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.users_x_challenge_milestones
    ADD CONSTRAINT "UQ_72c536445ace74c093e326be55a" UNIQUE (user_id, challenge_milestone_id);


--
-- Name: achievements UQ_8822867662de86cc1c8b2032239; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.achievements
    ADD CONSTRAINT "UQ_8822867662de86cc1c8b2032239" UNIQUE (title);


--
-- Name: users_x_badges UQ_8ba3ce1f91442fe5683853d5274; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.users_x_badges
    ADD CONSTRAINT "UQ_8ba3ce1f91442fe5683853d5274" UNIQUE (user_id, badge_id);


--
-- Name: users UQ_97672ac88f789774dd47f7c8be3; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT "UQ_97672ac88f789774dd47f7c8be3" UNIQUE (email);


--
-- Name: challenges UQ_9b25c2ccb1459524dd346e78dfb; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.challenges
    ADD CONSTRAINT "UQ_9b25c2ccb1459524dd346e78dfb" UNIQUE (title);


--
-- Name: users_x_achievements UQ_a13cc78f29003ff8fe505e13ef9; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.users_x_achievements
    ADD CONSTRAINT "UQ_a13cc78f29003ff8fe505e13ef9" UNIQUE (user_id, achievement_id);


--
-- Name: badges UQ_c8d0bf04b0999a223ff1a316bdb; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.badges
    ADD CONSTRAINT "UQ_c8d0bf04b0999a223ff1a316bdb" UNIQUE (title);


--
-- Name: users UQ_f198e7de30e4c305ef977632b9e; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT "UQ_f198e7de30e4c305ef977632b9e" UNIQUE (keycloak_sid);


--
-- Name: event_teams_x_event_checklist_items FK_21dbf3785b6f15bd12e1fdf91a6; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.event_teams_x_event_checklist_items
    ADD CONSTRAINT "FK_21dbf3785b6f15bd12e1fdf91a6" FOREIGN KEY (event_team_id) REFERENCES public.event_teams(id);


--
-- Name: users_x_badges FK_2d7a7b63e0f931348e963a1d6a7; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.users_x_badges
    ADD CONSTRAINT "FK_2d7a7b63e0f931348e963a1d6a7" FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: users_x_messages FK_3696fef62b1d27c011917000aad; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.users_x_messages
    ADD CONSTRAINT "FK_3696fef62b1d27c011917000aad" FOREIGN KEY (message_id) REFERENCES public.messages(id);


--
-- Name: challenge_milestones FK_39098f954faa66426a3101cdc47; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.challenge_milestones
    ADD CONSTRAINT "FK_39098f954faa66426a3101cdc47" FOREIGN KEY (challenge_id) REFERENCES public.challenges(id);


--
-- Name: event_teams_x_users FK_395e788f7cbf0d283e74b57c773; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.event_teams_x_users
    ADD CONSTRAINT "FK_395e788f7cbf0d283e74b57c773" FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: users_x_badges FK_69c99ba5877d2de368fafe69c2e; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.users_x_badges
    ADD CONSTRAINT "FK_69c99ba5877d2de368fafe69c2e" FOREIGN KEY (badge_id) REFERENCES public.badges(id);


--
-- Name: users_x_challenge_milestones FK_8082b3c5585f97808f632783d48; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.users_x_challenge_milestones
    ADD CONSTRAINT "FK_8082b3c5585f97808f632783d48" FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: event_teams_x_event_checklist_items FK_9a5b6849794d430576ef2365df5; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.event_teams_x_event_checklist_items
    ADD CONSTRAINT "FK_9a5b6849794d430576ef2365df5" FOREIGN KEY (event_checklist_item_id) REFERENCES public.event_checklist_items(id);


--
-- Name: users_x_messages FK_a2b3d80bd56d31a549d66dd7a8a; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.users_x_messages
    ADD CONSTRAINT "FK_a2b3d80bd56d31a549d66dd7a8a" FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: event_teams FK_b10f118ebff969c2f306569b160; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.event_teams
    ADD CONSTRAINT "FK_b10f118ebff969c2f306569b160" FOREIGN KEY (event_id) REFERENCES public.events(id);


--
-- Name: users_x_achievements FK_b731197b088f674f924faf23e5f; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.users_x_achievements
    ADD CONSTRAINT "FK_b731197b088f674f924faf23e5f" FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: users_x_challenge_milestones FK_baa5d2fadc61e3da0abbd2f55f8; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.users_x_challenge_milestones
    ADD CONSTRAINT "FK_baa5d2fadc61e3da0abbd2f55f8" FOREIGN KEY (challenge_milestone_id) REFERENCES public.challenge_milestones(id);


--
-- Name: users_x_user_checklist_items FK_d0e61a1b56188301ac483032221; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.users_x_user_checklist_items
    ADD CONSTRAINT "FK_d0e61a1b56188301ac483032221" FOREIGN KEY (user_checklist_item_id) REFERENCES public.user_checklist_items(id);


--
-- Name: users_x_achievements FK_f1e2c1e8e48551ed180d51aebd0; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.users_x_achievements
    ADD CONSTRAINT "FK_f1e2c1e8e48551ed180d51aebd0" FOREIGN KEY (achievement_id) REFERENCES public.achievements(id);


--
-- Name: event_teams_x_users FK_f6df99ad941d836471af2031bdb; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.event_teams_x_users
    ADD CONSTRAINT "FK_f6df99ad941d836471af2031bdb" FOREIGN KEY (event_team_id) REFERENCES public.event_teams(id);


--
-- Name: event_teams_x_event_checklist_items FK_f73a4907135f5d5be79d3203e33; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.event_teams_x_event_checklist_items
    ADD CONSTRAINT "FK_f73a4907135f5d5be79d3203e33" FOREIGN KEY (event_id) REFERENCES public.events(id);


--
-- Name: users_x_user_checklist_items FK_f86e93c310ce2bd0083b7a9a10a; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.users_x_user_checklist_items
    ADD CONSTRAINT "FK_f86e93c310ce2bd0083b7a9a10a" FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- PostgreSQL database dump complete
--

