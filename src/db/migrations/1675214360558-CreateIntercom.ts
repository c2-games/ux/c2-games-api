import {MigrationInterface, QueryRunner} from "typeorm";

export class CreateIntercom1675214360558 implements MigrationInterface {
    name = 'CreateIntercom1675214360558'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "intercom" ("id" SERIAL NOT NULL, "message" character varying NOT NULL, "enabled" boolean NOT NULL DEFAULT true, "created_by_user_id" integer NOT NULL DEFAULT '0', "intercom_color" character varying NOT NULL DEFAULT 'green', "created_date" TIMESTAMP NOT NULL DEFAULT ('now'::text)::timestamp(6) with time zone, "updated_by_user_id" integer NOT NULL DEFAULT '0', "updated_date" TIMESTAMP NOT NULL DEFAULT ('now'::text)::timestamp(6) with time zone, CONSTRAINT "PK_23776be4e1ce51b8fbb1ea0a2c6" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "intercom"`);
    }

}
