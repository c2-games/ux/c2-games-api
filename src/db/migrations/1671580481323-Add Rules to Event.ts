import {MigrationInterface, QueryRunner} from "typeorm";

export class AddRulesToEvent1671580481323 implements MigrationInterface {
    name = 'AddRulesToEvent1671580481323'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "events" ADD "rules" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "events" DROP COLUMN "rules"`);
    }

}
