import { MigrationInterface, QueryRunner } from 'typeorm';

export class MissingForeignKeys1662151892448 implements MigrationInterface {
  name = 'MissingForeignKeys1662151892448';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "users_x_badges" ADD CONSTRAINT "FK_2d7a7b63e0f931348e963a1d6a7" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "users_x_badges" ADD CONSTRAINT "FK_69c99ba5877d2de368fafe69c2e" FOREIGN KEY ("badge_id") REFERENCES "badges"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "users_x_challenge_milestones" ADD CONSTRAINT "FK_8082b3c5585f97808f632783d48" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "users_x_achievements" ADD CONSTRAINT "FK_b731197b088f674f924faf23e5f" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "users_x_achievements" ADD CONSTRAINT "FK_f1e2c1e8e48551ed180d51aebd0" FOREIGN KEY ("achievement_id") REFERENCES "achievements"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "users_x_achievements" DROP CONSTRAINT "FK_f1e2c1e8e48551ed180d51aebd0"`);
    await queryRunner.query(`ALTER TABLE "users_x_achievements" DROP CONSTRAINT "FK_b731197b088f674f924faf23e5f"`);
    await queryRunner.query(
      `ALTER TABLE "users_x_challenge_milestones" DROP CONSTRAINT "FK_8082b3c5585f97808f632783d48"`,
    );
    await queryRunner.query(`ALTER TABLE "users_x_badges" DROP CONSTRAINT "FK_69c99ba5877d2de368fafe69c2e"`);
    await queryRunner.query(`ALTER TABLE "users_x_badges" DROP CONSTRAINT "FK_2d7a7b63e0f931348e963a1d6a7"`);
  }
}
