import { MigrationInterface, QueryRunner } from 'typeorm';

export class UserMessagesDefaultSeenOn1663689051745 implements MigrationInterface {
  name = 'UserMessagesDefaultSeenOn1663689051745';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "users_x_messages" ALTER COLUMN "seen_on" SET DEFAULT ('now'::text)::timestamp(6) with time zone`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "users_x_messages" ALTER COLUMN "seen_on" DROP DEFAULT`);
  }
}
