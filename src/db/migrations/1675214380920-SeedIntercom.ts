import {MigrationInterface, QueryRunner} from "typeorm";

export class SeedIntercom1675214380920 implements MigrationInterface {

   public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`INSERT INTO intercom (ID, MESSAGE, INTERCOM_COLOR, ENABLED) VALUES (1, '', 'yellow', false)`);
    }

    public async down (): Promise<void> {
     // down needs to be here to make MigrationInterface not error, but it can't be empty
        console.log("down")
    }

}
