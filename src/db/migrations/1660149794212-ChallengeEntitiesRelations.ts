import { MigrationInterface, QueryRunner } from 'typeorm';

export class ChallengeEntitiesRelations1660149794212 implements MigrationInterface {
  name = 'ChallengeEntitiesRelations1660149794212';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `UPDATE "users_x_challenge_milestones" SET "completed_on" = "created_date" WHERE "completed_on" IS NULL`,
    );
    await queryRunner.query(`ALTER TABLE "users_x_challenge_milestones" ALTER COLUMN "completed_on" SET NOT NULL`);
    await queryRunner.query(
      `ALTER TABLE "users_x_challenge_milestones" ALTER COLUMN "completed_on" SET DEFAULT ('now'::text)::timestamp(6) with time zone`,
    );
    await queryRunner.query(
      `ALTER TABLE "users_x_challenge_milestones" ADD CONSTRAINT "FK_baa5d2fadc61e3da0abbd2f55f8" FOREIGN KEY ("challenge_milestone_id") REFERENCES "challenge_milestones"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "challenge_milestones" ADD CONSTRAINT "FK_39098f954faa66426a3101cdc47" FOREIGN KEY ("challenge_id") REFERENCES "challenges"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "challenge_milestones" DROP CONSTRAINT "FK_39098f954faa66426a3101cdc47"`);
    await queryRunner.query(
      `ALTER TABLE "users_x_challenge_milestones" DROP CONSTRAINT "FK_baa5d2fadc61e3da0abbd2f55f8"`,
    );
    await queryRunner.query(`ALTER TABLE "users_x_challenge_milestones" ALTER COLUMN "completed_on" DROP DEFAULT`);
    await queryRunner.query(`ALTER TABLE "users_x_challenge_milestones" ALTER COLUMN "completed_on" DROP NOT NULL`);
  }
}
