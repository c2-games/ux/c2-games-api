import { MigrationInterface, QueryRunner } from 'typeorm';

export class InitSeed1660065819722 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    // Seed Users
    await queryRunner.query(
      `INSERT INTO users (email, keycloak_sid) VALUES
        ('test@example.com', '34e16a74-7881-468f-aa32-35ca8bbce459')
      ;`,
    );

    // Seed Badges
    await queryRunner.query(
      `INSERT INTO badges (title, description, image_url) VALUES
        ('Tent Badge', 'You successfully pitched a tent!', 'https://freesvg.org/img/scout-badge.png'),
        ('Canada Badge', 'You went to Canada', 'https://freesvg.org/img/1295988641.png')
      ;`,
    );

    // Seed Challenges
    await queryRunner.query(
      `INSERT INTO challenges (title, description) VALUES
        ('First Challenge', 'This is the first challenge')
      ;`,
    );

    // Seed Achievements
    await queryRunner.query(
      `INSERT INTO achievements (title, description) VALUES
        ('Some Achievement', 'This is some achievement'),
        ('Some Other Achievement', 'This is some other achievement')
      ;`,
    );

    // Seed User Badges
    await queryRunner.query(
      `INSERT INTO users_x_badges (user_id, badge_id) VALUES
        (1, 1)
      ;`,
    );

    // Seed User Achievements
    await queryRunner.query(
      `INSERT INTO users_x_achievements (user_id, achievement_id) VALUES
        (1, 1)
      ;`,
    );

    // Seed Challenge Milestones
    await queryRunner.query(
      `INSERT INTO challenge_milestones (title, description, challenge_id) VALUES
        ('Challenge Milestone Title', 'This is a challenge milestone', 1),
        ('Another Challenge Milestone Title', 'This is another challenge milestone', 1)
      ;`,
    );

    // Seed User Challenge Milestones
    await queryRunner.query(
      `INSERT INTO users_x_challenge_milestones (user_id, challenge_milestone_id) VALUES
        (1, 1)
      ;`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DELETE FROM "users_x_challenge_milestones"`);
    await queryRunner.query(`DELETE FROM "users_x_badges"`);
    await queryRunner.query(`DELETE FROM "users_x_achievements"`);
    await queryRunner.query(`DELETE FROM "challenge_milestones"`);
    await queryRunner.query(`DELETE FROM "challenges"`);
    await queryRunner.query(`DELETE FROM "badges"`);
    await queryRunner.query(`DELETE FROM "achievements"`);
    await queryRunner.query(`DELETE FROM "users"`);
  }
}
