import { MigrationInterface, QueryRunner } from 'typeorm';

export class EventsAndChecklists1661807631056 implements MigrationInterface {
  name = 'EventsAndChecklists1661807631056';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "event_checklist_items" ("id" SERIAL NOT NULL, "title" character varying NOT NULL, "description" character varying NOT NULL, "enabled" boolean NOT NULL DEFAULT true, "created_by_user_id" integer NOT NULL DEFAULT '0', "created_date" TIMESTAMP NOT NULL DEFAULT ('now'::text)::timestamp(6) with time zone, "updated_by_user_id" integer NOT NULL DEFAULT '0', "updated_date" TIMESTAMP NOT NULL DEFAULT ('now'::text)::timestamp(6) with time zone, CONSTRAINT "PK_53049b762cc9a1f90ae8d08eb70" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "user_checklist_items" ("id" SERIAL NOT NULL, "title" character varying NOT NULL, "description" character varying NOT NULL, "enabled" boolean NOT NULL DEFAULT true, "created_by_user_id" integer NOT NULL DEFAULT '0', "created_date" TIMESTAMP NOT NULL DEFAULT ('now'::text)::timestamp(6) with time zone, "updated_by_user_id" integer NOT NULL DEFAULT '0', "updated_date" TIMESTAMP NOT NULL DEFAULT ('now'::text)::timestamp(6) with time zone, CONSTRAINT "PK_13297aa78a1712c12387679d72c" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "users_x_user_checklist_items" ("id" SERIAL NOT NULL, "user_id" integer NOT NULL, "user_checklist_item_id" integer NOT NULL, "completed_on" TIMESTAMP, "enabled" boolean NOT NULL DEFAULT true, "created_by_user_id" integer NOT NULL DEFAULT '0', "created_date" TIMESTAMP NOT NULL DEFAULT ('now'::text)::timestamp(6) with time zone, "updated_by_user_id" integer NOT NULL DEFAULT '0', "updated_date" TIMESTAMP NOT NULL DEFAULT ('now'::text)::timestamp(6) with time zone, CONSTRAINT "UQ_2d794451a674702f8e33e7e531c" UNIQUE ("user_id", "user_checklist_item_id"), CONSTRAINT "PK_025edb93876a9ae0d1def89c7b7" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "event_teams_x_users" ("id" SERIAL NOT NULL, "event_team_id" integer NOT NULL, "user_id" integer NOT NULL, "added_on" TIMESTAMP DEFAULT ('now'::text)::timestamp(6) with time zone, "enabled" boolean NOT NULL DEFAULT true, "created_by_user_id" integer NOT NULL DEFAULT '0', "created_date" TIMESTAMP NOT NULL DEFAULT ('now'::text)::timestamp(6) with time zone, "updated_by_user_id" integer NOT NULL DEFAULT '0', "updated_date" TIMESTAMP NOT NULL DEFAULT ('now'::text)::timestamp(6) with time zone, CONSTRAINT "UQ_706269390c8baf39a1411563f40" UNIQUE ("event_team_id", "user_id"), CONSTRAINT "PK_106cadb04bdc1bdbd356ea751a0" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "events" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "starts_on" TIMESTAMP NOT NULL, "enabled" boolean NOT NULL DEFAULT true, "created_by_user_id" integer NOT NULL DEFAULT '0', "created_date" TIMESTAMP NOT NULL DEFAULT ('now'::text)::timestamp(6) with time zone, "updated_by_user_id" integer NOT NULL DEFAULT '0', "updated_date" TIMESTAMP NOT NULL DEFAULT ('now'::text)::timestamp(6) with time zone, CONSTRAINT "PK_40731c7151fe4be3116e45ddf73" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "event_teams" ("id" SERIAL NOT NULL, "event_id" integer NOT NULL, "name" character varying NOT NULL, "enabled" boolean NOT NULL DEFAULT true, "created_by_user_id" integer NOT NULL DEFAULT '0', "created_date" TIMESTAMP NOT NULL DEFAULT ('now'::text)::timestamp(6) with time zone, "updated_by_user_id" integer NOT NULL DEFAULT '0', "updated_date" TIMESTAMP NOT NULL DEFAULT ('now'::text)::timestamp(6) with time zone, CONSTRAINT "PK_02a00c33f9aedce303aabb6ba16" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "event_teams_x_event_checklist_items" ("id" SERIAL NOT NULL, "event_id" integer NOT NULL, "event_team_id" integer NOT NULL, "event_checklist_item_id" integer NOT NULL, "completed_on" TIMESTAMP, "enabled" boolean NOT NULL DEFAULT true, "created_by_user_id" integer NOT NULL DEFAULT '0', "created_date" TIMESTAMP NOT NULL DEFAULT ('now'::text)::timestamp(6) with time zone, "updated_by_user_id" integer NOT NULL DEFAULT '0', "updated_date" TIMESTAMP NOT NULL DEFAULT ('now'::text)::timestamp(6) with time zone, CONSTRAINT "UQ_64331f18cad1e944fc3cf278478" UNIQUE ("event_id", "event_team_id", "event_checklist_item_id"), CONSTRAINT "PK_e30673003c16bc2a3d97f9542a5" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `ALTER TABLE "users_x_user_checklist_items" ADD CONSTRAINT "FK_f86e93c310ce2bd0083b7a9a10a" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "users_x_user_checklist_items" ADD CONSTRAINT "FK_d0e61a1b56188301ac483032221" FOREIGN KEY ("user_checklist_item_id") REFERENCES "user_checklist_items"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "event_teams_x_users" ADD CONSTRAINT "FK_f6df99ad941d836471af2031bdb" FOREIGN KEY ("event_team_id") REFERENCES "event_teams"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "event_teams_x_users" ADD CONSTRAINT "FK_395e788f7cbf0d283e74b57c773" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "event_teams" ADD CONSTRAINT "FK_b10f118ebff969c2f306569b160" FOREIGN KEY ("event_id") REFERENCES "events"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "event_teams_x_event_checklist_items" ADD CONSTRAINT "FK_f73a4907135f5d5be79d3203e33" FOREIGN KEY ("event_id") REFERENCES "events"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "event_teams_x_event_checklist_items" ADD CONSTRAINT "FK_21dbf3785b6f15bd12e1fdf91a6" FOREIGN KEY ("event_team_id") REFERENCES "event_teams"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "event_teams_x_event_checklist_items" ADD CONSTRAINT "FK_9a5b6849794d430576ef2365df5" FOREIGN KEY ("event_checklist_item_id") REFERENCES "event_checklist_items"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );

    // Seed Event Checklist Items
    await queryRunner.query(
      `INSERT INTO event_checklist_items (id, title, description) VALUES
            (1, 'Event Checklist Item Title', 'This is an event checklist item description')
          ;`,
    );

    // Seed User Checklist Items
    await queryRunner.query(
      `INSERT INTO user_checklist_items (id, title, description) VALUES
            (1, 'User Checklist Item Title', 'This is a user checklist item description')
          ;`,
    );

    // Seed User User Checklist Items
    await queryRunner.query(
      `INSERT INTO users_x_user_checklist_items (id, user_id, user_checklist_item_id, completed_on) VALUES
            (1, 1, 1, CURRENT_TIMESTAMP(6))
          ;`,
    );

    // Seed Events
    await queryRunner.query(
      `INSERT INTO events (id, name, starts_on) VALUES
            (1, 'Event Name', CURRENT_TIMESTAMP(6))
          ;`,
    );

    // Seed Event Teams
    await queryRunner.query(
      `INSERT INTO event_teams (id, event_id, name) VALUES
            (1, 1, 'Event Team Name')
          ;`,
    );

    // Seed Event Team Users
    await queryRunner.query(
      `INSERT INTO event_teams_x_users (id, event_team_id, user_id) VALUES
            (1, 1, 1)
          ;`,
    );

    // Seed Event Team Event Checklist Items
    await queryRunner.query(
      `INSERT INTO event_teams_x_event_checklist_items (id, event_id, event_team_id, event_checklist_item_id) VALUES
            (1, 1, 1, 1)
          ;`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "event_teams_x_event_checklist_items" DROP CONSTRAINT "FK_9a5b6849794d430576ef2365df5"`,
    );
    await queryRunner.query(
      `ALTER TABLE "event_teams_x_event_checklist_items" DROP CONSTRAINT "FK_21dbf3785b6f15bd12e1fdf91a6"`,
    );
    await queryRunner.query(
      `ALTER TABLE "event_teams_x_event_checklist_items" DROP CONSTRAINT "FK_f73a4907135f5d5be79d3203e33"`,
    );
    await queryRunner.query(`ALTER TABLE "event_teams" DROP CONSTRAINT "FK_b10f118ebff969c2f306569b160"`);
    await queryRunner.query(`ALTER TABLE "event_teams_x_users" DROP CONSTRAINT "FK_395e788f7cbf0d283e74b57c773"`);
    await queryRunner.query(`ALTER TABLE "event_teams_x_users" DROP CONSTRAINT "FK_f6df99ad941d836471af2031bdb"`);
    await queryRunner.query(
      `ALTER TABLE "users_x_user_checklist_items" DROP CONSTRAINT "FK_d0e61a1b56188301ac483032221"`,
    );
    await queryRunner.query(
      `ALTER TABLE "users_x_user_checklist_items" DROP CONSTRAINT "FK_f86e93c310ce2bd0083b7a9a10a"`,
    );
    await queryRunner.query(`DROP TABLE "event_teams_x_event_checklist_items"`);
    await queryRunner.query(`DROP TABLE "event_teams"`);
    await queryRunner.query(`DROP TABLE "events"`);
    await queryRunner.query(`DROP TABLE "event_teams_x_users"`);
    await queryRunner.query(`DROP TABLE "users_x_user_checklist_items"`);
    await queryRunner.query(`DROP TABLE "user_checklist_items"`);
    await queryRunner.query(`DROP TABLE "event_checklist_items"`);
  }
}
