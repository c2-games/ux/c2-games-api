import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddInstitutions1669070483308 implements MigrationInterface {
  name = 'AddInstitutions1669070483308';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "institutions" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "domain" character varying NOT NULL, "state" character varying NOT NULL, "region" character varying NOT NULL, "updated_date" TIMESTAMP NOT NULL DEFAULT ('now'::text)::timestamp(6) with time zone, "created_date" TIMESTAMP NOT NULL DEFAULT ('now'::text)::timestamp(6) with time zone, CONSTRAINT "UQ_15c98649276025998cd1acaf61c" UNIQUE ("name"), CONSTRAINT "PK_0be7539dcdba335470dc05e9690" PRIMARY KEY ("id"))`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE "institutions"`);
  }
}
