import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddRegionToEvents1669073136222 implements MigrationInterface {
  name = 'AddRegionToEvents1669073136222';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "events" ADD "regions" text array`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "events" DROP COLUMN "regions"`);
  }
}
