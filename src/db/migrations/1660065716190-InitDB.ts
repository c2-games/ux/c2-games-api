import { MigrationInterface, QueryRunner } from 'typeorm';

export class InitDB1660065716190 implements MigrationInterface {
  name = 'InitDB1660065716190';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "challenge_milestones" ("id" SERIAL NOT NULL, "title" character varying NOT NULL, "description" character varying NOT NULL, "challenge_id" integer NOT NULL, "enabled" boolean NOT NULL DEFAULT true, "created_by_user_id" integer NOT NULL DEFAULT '0', "created_date" TIMESTAMP NOT NULL DEFAULT ('now'::text)::timestamp(6) with time zone, "updated_by_user_id" integer NOT NULL DEFAULT '0', "updated_date" TIMESTAMP NOT NULL DEFAULT ('now'::text)::timestamp(6) with time zone, CONSTRAINT "PK_98e9e29971613b8b4df571fda62" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "users_x_achievements" ("id" SERIAL NOT NULL, "user_id" integer NOT NULL, "achievement_id" integer NOT NULL, "earned_on" TIMESTAMP NOT NULL DEFAULT ('now'::text)::timestamp(6) with time zone, "enabled" boolean NOT NULL DEFAULT true, "created_by_user_id" integer NOT NULL DEFAULT '0', "created_date" TIMESTAMP NOT NULL DEFAULT ('now'::text)::timestamp(6) with time zone, "updated_by_user_id" integer NOT NULL DEFAULT '0', "updated_date" TIMESTAMP NOT NULL DEFAULT ('now'::text)::timestamp(6) with time zone, CONSTRAINT "UQ_a13cc78f29003ff8fe505e13ef9" UNIQUE ("user_id", "achievement_id"), CONSTRAINT "PK_36994c83219f82be2e65de51449" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "challenges" ("id" SERIAL NOT NULL, "title" character varying NOT NULL, "description" character varying NOT NULL, "enabled" boolean NOT NULL DEFAULT true, "created_by_user_id" integer NOT NULL DEFAULT '0', "created_date" TIMESTAMP NOT NULL DEFAULT ('now'::text)::timestamp(6) with time zone, "updated_by_user_id" integer NOT NULL DEFAULT '0', "updated_date" TIMESTAMP NOT NULL DEFAULT ('now'::text)::timestamp(6) with time zone, CONSTRAINT "UQ_9b25c2ccb1459524dd346e78dfb" UNIQUE ("title"), CONSTRAINT "PK_1e664e93171e20fe4d6125466af" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "badges" ("id" SERIAL NOT NULL, "title" character varying NOT NULL, "description" character varying NOT NULL, "image_url" character varying NOT NULL, "enabled" boolean NOT NULL DEFAULT true, "created_by_user_id" integer NOT NULL DEFAULT '0', "created_date" TIMESTAMP NOT NULL DEFAULT ('now'::text)::timestamp(6) with time zone, "updated_by_user_id" integer NOT NULL DEFAULT '0', "updated_date" TIMESTAMP NOT NULL DEFAULT ('now'::text)::timestamp(6) with time zone, CONSTRAINT "UQ_c8d0bf04b0999a223ff1a316bdb" UNIQUE ("title"), CONSTRAINT "PK_8a651318b8de577e8e217676466" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "users_x_badges" ("id" SERIAL NOT NULL, "user_id" integer NOT NULL, "badge_id" integer NOT NULL, "earned_on" TIMESTAMP NOT NULL DEFAULT ('now'::text)::timestamp(6) with time zone, "enabled" boolean NOT NULL DEFAULT true, "created_by_user_id" integer NOT NULL DEFAULT '0', "created_date" TIMESTAMP NOT NULL DEFAULT ('now'::text)::timestamp(6) with time zone, "updated_by_user_id" integer NOT NULL DEFAULT '0', "updated_date" TIMESTAMP NOT NULL DEFAULT ('now'::text)::timestamp(6) with time zone, CONSTRAINT "UQ_8ba3ce1f91442fe5683853d5274" UNIQUE ("user_id", "badge_id"), CONSTRAINT "PK_4ffc42e5d68f18875c79ec7f01d" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "achievements" ("id" SERIAL NOT NULL, "title" character varying NOT NULL, "description" character varying NOT NULL, "enabled" boolean NOT NULL DEFAULT true, "created_by_user_id" integer NOT NULL DEFAULT '0', "created_date" TIMESTAMP NOT NULL DEFAULT ('now'::text)::timestamp(6) with time zone, "updated_by_user_id" integer NOT NULL DEFAULT '0', "updated_date" TIMESTAMP NOT NULL DEFAULT ('now'::text)::timestamp(6) with time zone, CONSTRAINT "UQ_8822867662de86cc1c8b2032239" UNIQUE ("title"), CONSTRAINT "PK_1bc19c37c6249f70186f318d71d" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "users" ("id" SERIAL NOT NULL, "email" character varying NOT NULL, "keycloak_sid" character varying NOT NULL, "enabled" boolean NOT NULL DEFAULT true, "created_by_user_id" integer NOT NULL DEFAULT '0', "created_date" TIMESTAMP NOT NULL DEFAULT ('now'::text)::timestamp(6) with time zone, "updated_by_user_id" integer NOT NULL DEFAULT '0', "updated_date" TIMESTAMP NOT NULL DEFAULT ('now'::text)::timestamp(6) with time zone, CONSTRAINT "UQ_97672ac88f789774dd47f7c8be3" UNIQUE ("email"), CONSTRAINT "UQ_f198e7de30e4c305ef977632b9e" UNIQUE ("keycloak_sid"), CONSTRAINT "PK_a3ffb1c0c8416b9fc6f907b7433" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "users_x_challenge_milestones" ("id" SERIAL NOT NULL, "user_id" integer NOT NULL, "challenge_milestone_id" integer NOT NULL, "completed_on" TIMESTAMP, "enabled" boolean NOT NULL DEFAULT true, "created_by_user_id" integer NOT NULL DEFAULT '0', "created_date" TIMESTAMP NOT NULL DEFAULT ('now'::text)::timestamp(6) with time zone, "updated_by_user_id" integer NOT NULL DEFAULT '0', "updated_date" TIMESTAMP NOT NULL DEFAULT ('now'::text)::timestamp(6) with time zone, CONSTRAINT "UQ_72c536445ace74c093e326be55a" UNIQUE ("user_id", "challenge_milestone_id"), CONSTRAINT "PK_74fea21c854a7c7753124ef4be3" PRIMARY KEY ("id"))`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE "users_x_challenge_milestones"`);
    await queryRunner.query(`DROP TABLE "users"`);
    await queryRunner.query(`DROP TABLE "achievements"`);
    await queryRunner.query(`DROP TABLE "users_x_badges"`);
    await queryRunner.query(`DROP TABLE "badges"`);
    await queryRunner.query(`DROP TABLE "challenges"`);
    await queryRunner.query(`DROP TABLE "users_x_achievements"`);
    await queryRunner.query(`DROP TABLE "challenge_milestones"`);
  }
}
