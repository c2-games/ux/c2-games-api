import { MigrationInterface, QueryRunner } from 'typeorm';

export class UserMessages1662584574997 implements MigrationInterface {
  name = 'UserMessages1662584574997';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "messages" ("id" SERIAL NOT NULL, "content" character varying NOT NULL, "announced_on" TIMESTAMP NOT NULL DEFAULT ('now'::text)::timestamp(6) with time zone, "enabled" boolean NOT NULL DEFAULT true, "created_by_user_id" integer NOT NULL DEFAULT '0', "created_date" TIMESTAMP NOT NULL DEFAULT ('now'::text)::timestamp(6) with time zone, "updated_by_user_id" integer NOT NULL DEFAULT '0', "updated_date" TIMESTAMP NOT NULL DEFAULT ('now'::text)::timestamp(6) with time zone, CONSTRAINT "PK_18325f38ae6de43878487eff986" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "users_x_messages" ("id" SERIAL NOT NULL, "user_id" integer NOT NULL, "message_id" integer NOT NULL, "seen_on" TIMESTAMP NOT NULL, "enabled" boolean NOT NULL DEFAULT true, "created_by_user_id" integer NOT NULL DEFAULT '0', "created_date" TIMESTAMP NOT NULL DEFAULT ('now'::text)::timestamp(6) with time zone, "updated_by_user_id" integer NOT NULL DEFAULT '0', "updated_date" TIMESTAMP NOT NULL DEFAULT ('now'::text)::timestamp(6) with time zone, CONSTRAINT "UQ_4c192e495a64863f27d675b3ee4" UNIQUE ("user_id", "message_id"), CONSTRAINT "PK_fa9065556fa75b2e5f6d9e4ea49" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `ALTER TABLE "users_x_messages" ADD CONSTRAINT "FK_a2b3d80bd56d31a549d66dd7a8a" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "users_x_messages" ADD CONSTRAINT "FK_3696fef62b1d27c011917000aad" FOREIGN KEY ("message_id") REFERENCES "messages"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "users_x_messages" DROP CONSTRAINT "FK_3696fef62b1d27c011917000aad"`);
    await queryRunner.query(`ALTER TABLE "users_x_messages" DROP CONSTRAINT "FK_a2b3d80bd56d31a549d66dd7a8a"`);
    await queryRunner.query(`DROP TABLE "users_x_messages"`);
    await queryRunner.query(`DROP TABLE "messages"`);
  }
}
