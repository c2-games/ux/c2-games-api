import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class CreateChallengeDto {
  @ApiProperty({
    type: 'string',
    required: true,
    example: 'Challenge Title',
  })
  @IsString()
  title: string;

  @ApiProperty({
    type: 'string',
    required: true,
    example: 'This is a challenge description',
  })
  @IsString()
  description: string;
}
