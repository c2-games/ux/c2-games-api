import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsOptional, IsString } from 'class-validator';

export class UpdateChallengeDto {
  @ApiProperty({
    type: 'string',
    required: false,
    example: 'Challenge Title',
  })
  @IsString()
  @IsOptional()
  title?: string;

  @ApiProperty({
    type: 'string',
    required: false,
    example: 'This is a challenge description',
  })
  @IsString()
  @IsOptional()
  description?: string;

  @ApiProperty({
    type: 'boolean',
    required: false,
    example: 'true',
  })
  @IsBoolean()
  @IsOptional()
  enabled?: boolean;
}
