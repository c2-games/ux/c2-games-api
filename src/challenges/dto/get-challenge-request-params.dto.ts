import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

export class GetChallengeRequestParamsDto {
  @ApiProperty({
    type: 'string',
    required: false,
    example: 'Challenge Title',
  })
  @IsString()
  @IsOptional()
  title?: string;

  @ApiProperty({
    type: 'string',
    required: false,
    example: 'This is a challenge description',
  })
  @IsString()
  @IsOptional()
  description?: string;
}
