import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateChallengeDto } from './dto/create-challenge.dto';
import { GetChallengeRequestParamsDto } from './dto/get-challenge-request-params.dto';
import { UpdateChallengeDto } from './dto/update-challenge.dto';
import { Challenge } from './entities/challenge.entity';

@Injectable()
export class ChallengesService {
  constructor(@InjectRepository(Challenge) private repo: Repository<Challenge>) {}

  async create(createChallengeDto: CreateChallengeDto, currentUserId = 0) {
    const challenge = this.repo.create({
      ...createChallengeDto,
      createdByUserId: currentUserId,
      updatedByUserId: currentUserId,
    });
    await this.repo.save(challenge);
    return challenge;
  }

  findAll(params?: GetChallengeRequestParamsDto) {
    let query = this.repo.createQueryBuilder('challenges').where('1=1');
    query = params.title
      ? query.andWhere('challenges.title = :title', {
          title: params.title,
        })
      : query;
    query = params.description
      ? query.andWhere('challenges.description = :description', {
          description: params.description,
        })
      : query;
    return query.getMany();
  }

  async findOne(id: number) {
    if (!id) {
      throw new BadRequestException();
    }
    const challenge = await this.repo.findOne(id);
    if (!challenge) {
      throw new NotFoundException('Challenge not found');
    }
    return challenge;
  }

  async update(id: number, updateChallengeDto: UpdateChallengeDto, currentUserId = 0) {
    const challenge = await this.findOne(id);
    Object.assign(updateChallengeDto, { updatedByUserId: currentUserId });
    Object.assign(challenge, updateChallengeDto);
    await this.repo.save(challenge);
    return challenge;
  }

  async remove(id: number) {
    const challenge = await this.findOne(id);
    await this.repo.remove(challenge);
    return challenge;
  }
}
