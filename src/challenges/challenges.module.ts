import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { KeycloakModule } from '../keycloak/keycloak.module';
import { ChallengesController } from './challenges.controller';
import { ChallengesService } from './challenges.service';
import { Challenge } from './entities/challenge.entity';
import { ChallengeExistsRule } from './validators/challenge-exists-rule.validator';

@Module({
  imports: [TypeOrmModule.forFeature([Challenge]), KeycloakModule],
  controllers: [ChallengesController],
  providers: [ChallengesService, ChallengeExistsRule],
})
export class ChallengesModule {}
