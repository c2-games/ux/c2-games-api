import { faker } from '@faker-js/faker';
import { Test, TestingModule } from '@nestjs/testing';
import { KeycloakModule } from '../keycloak/keycloak.module';
import { createFakeChallenge } from '../utils/test-helpers/fakes/challenge-faker';
import { createFakeUser } from '../utils/test-helpers/fakes/user-faker';
import { ChallengesController } from './challenges.controller';
import { ChallengesService } from './challenges.service';
import { CreateChallengeDto } from './dto/create-challenge.dto';
import { UpdateChallengeDto } from './dto/update-challenge.dto';

const currentUserId = faker.datatype.number();

const fakeChallenge = createFakeChallenge(currentUserId);
const fakeUser = createFakeUser(currentUserId);

describe('ChallengesController', () => {
  let controller: ChallengesController;
  let service: ChallengesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [KeycloakModule],
      controllers: [ChallengesController],
      providers: [
        {
          provide: ChallengesService,
          useValue: {
            create: jest
              .fn()
              .mockImplementation((challenge: CreateChallengeDto, currentUserId: number) =>
                Promise.resolve({ ...fakeChallenge, ...challenge }),
              ),
            findAll: jest.fn().mockResolvedValue([fakeChallenge]),
            findOne: jest.fn().mockImplementation((id: number) => Promise.resolve({ ...fakeChallenge, id })),
            update: jest
              .fn()
              .mockImplementation((id: number, challenge: UpdateChallengeDto, currentUserId: number) =>
                Promise.resolve({ ...fakeChallenge, id }),
              ),
            remove: jest.fn().mockResolvedValue(fakeChallenge),
          },
        },
      ],
    }).compile();

    controller = module.get<ChallengesController>(ChallengesController);
    service = module.get<ChallengesService>(ChallengesService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should create challenge', async () => {
    const createChallengeDto: CreateChallengeDto = {
      title: fakeChallenge.title,
      description: fakeChallenge.description,
    };
    const challenge = await controller.create(createChallengeDto, fakeUser);

    expect(challenge).toEqual({ ...fakeChallenge });
    expect(service.create).toBeCalledTimes(1);
    expect(service.create).toBeCalledWith(createChallengeDto, fakeUser.id);
  });

  it('should find all challenges', async () => {
    const challenges = await controller.findAll();

    expect(challenges).toEqual([fakeChallenge]);
    expect(service.findAll).toBeCalledTimes(1);
  });

  it('should find one challenge by id', async () => {
    const challenge = await controller.findOne(`${fakeChallenge.id}`);

    expect(challenge).toEqual(fakeChallenge);
    expect(service.findOne).toBeCalledTimes(1);
    expect(service.findOne).toBeCalledWith(fakeChallenge.id);
  });

  it('should update challenge', async () => {
    const updateChallengeDto = {
      title: faker.datatype.string(),
    };
    const challenge = await controller.update(`${fakeChallenge.id}`, updateChallengeDto, fakeUser);

    expect(challenge).toEqual(fakeChallenge);
    expect(service.update).toBeCalledTimes(1);
    expect(service.update).toBeCalledWith(fakeChallenge.id, updateChallengeDto, fakeUser.id);
  });

  it('should remove challenge', async () => {
    const challenge = await controller.remove(`${fakeChallenge.id}`);

    expect(challenge).toEqual(fakeChallenge);
    expect(service.remove).toBeCalledTimes(1);
    expect(service.remove).toBeCalledWith(fakeChallenge.id);
  });
});
