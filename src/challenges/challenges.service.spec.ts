import { faker } from '@faker-js/faker';
import { BadRequestException, NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ChallengesService } from './challenges.service';
import { GetChallengeRequestParamsDto } from './dto/get-challenge-request-params.dto';
import { Challenge } from './entities/challenge.entity';
import { createFakeChallenge } from '../utils/test-helpers/fakes/challenge-faker';

const currentUserId = faker.datatype.number();

const testChallenge = createFakeChallenge(currentUserId);

const getChallengeRequestParamsDto = new GetChallengeRequestParamsDto();

describe('ChallengesService', () => {
  let service: ChallengesService;
  let repo: Repository<Challenge>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ChallengesService,
        {
          provide: getRepositoryToken(Challenge),
          useValue: {
            create: jest.fn().mockReturnValue(testChallenge),
            save: jest.fn(),
            findAll: jest.fn().mockResolvedValue([testChallenge]),
            createQueryBuilder: jest.fn(() => ({
              where: jest.fn().mockReturnThis(),
              andWhere: jest.fn().mockReturnThis(),
              getMany: jest.fn().mockResolvedValue([testChallenge]),
            })),
            findOne: jest.fn().mockResolvedValue(testChallenge),
            update: jest.fn().mockResolvedValue(true),
            remove: jest.fn().mockResolvedValue(true),
          },
        },
      ],
    }).compile();

    service = module.get<ChallengesService>(ChallengesService);
    repo = module.get<Repository<Challenge>>(getRepositoryToken(Challenge));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should create a new challenge when provided with valid createChallengeDto', async () => {
    const createChallengeDto = {
      title: testChallenge.title,
      description: testChallenge.description,
    };

    const challenge = await service.create(createChallengeDto, currentUserId);

    expect(challenge).toEqual(testChallenge);
    expect(repo.create).toBeCalledTimes(1);
    expect(repo.create).toBeCalledWith({
      ...createChallengeDto,
      createdByUserId: currentUserId,
      updatedByUserId: currentUserId,
    });
    expect(repo.save).toBeCalledTimes(1);
  });

  it('should find all challenges', async () => {
    const challenges = await service.findAll(getChallengeRequestParamsDto);

    expect(challenges).toEqual([testChallenge]);
  });

  it('should find one challenge', async () => {
    const challenge = await service.findOne(faker.datatype.number());

    expect(challenge).toEqual(testChallenge);
  });

  it('should throw bad request exception on findOne if id not provided', async () => {
    await expect(() => service.findOne(null)).rejects.toThrow(BadRequestException);
  });

  it('should throw not found exception on findOne if challenge not found', async () => {
    const challengeId = faker.datatype.number();
    const repoSpy = jest.spyOn(repo, 'findOne').mockReturnValue(undefined);

    await expect(() => service.findOne(challengeId)).rejects.toThrow(new NotFoundException('Challenge not found'));
    expect(repoSpy).toBeCalledTimes(1);
    expect(repoSpy).toBeCalledWith(challengeId);
  });

  it('should update challenge', async () => {
    const challengeId = faker.datatype.number();
    const updateChallengeDto = { title: 'This title has been updated' };

    const challenge = await service.update(challengeId, updateChallengeDto, currentUserId);

    expect(challenge).toEqual({ ...testChallenge, ...updateChallengeDto });
    expect(repo.findOne).toBeCalledTimes(1);
    expect(repo.findOne).toBeCalledWith(challengeId);
    expect(repo.save).toBeCalledTimes(1);
  });

  it('should remove challenge', async () => {
    const challengeId = faker.datatype.number();

    const challenge = await service.remove(challengeId);

    expect(challenge).toEqual(testChallenge);
  });
});
