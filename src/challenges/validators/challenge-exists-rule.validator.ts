import { Injectable } from '@nestjs/common';
import { ValidatorConstraint, ValidatorConstraintInterface } from 'class-validator';
import { ChallengesService } from '../challenges.service';

@ValidatorConstraint({ name: 'BadgeExists', async: true })
@Injectable()
export class ChallengeExistsRule implements ValidatorConstraintInterface {
  constructor(private readonly challengeService: ChallengesService) {}

  async validate(id: number) {
    try {
      await this.challengeService.findOne(id);
    } catch (error) {
      return false;
    }
    return true;
  }

  defaultMessage() {
    return `Challenge does not exist`;
  }
}
