import { NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { createFakeChallenge } from '../../utils/test-helpers/fakes/challenge-faker';
import { ChallengesService } from '../challenges.service';
import { ChallengeExistsRule } from './challenge-exists-rule.validator';

const fakeChallenge = createFakeChallenge();

describe('ChallengeExistsRule', () => {
  let challengeExistsRule: ChallengeExistsRule;
  let service: ChallengesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ChallengeExistsRule,
        {
          provide: ChallengesService,
          useValue: {
            findOne: jest.fn().mockImplementation((id: number) => Promise.resolve(fakeChallenge)),
          },
        },
      ],
    }).compile();

    challengeExistsRule = module.get<ChallengeExistsRule>(ChallengeExistsRule);
    service = module.get<ChallengesService>(ChallengesService);
  });

  it('should return true when challenge exists', async () => {
    expect(challengeExistsRule.validate(fakeChallenge.id)).resolves.toEqual(true);
    expect(service.findOne).toBeCalledTimes(1);
    expect(service.findOne).toBeCalledWith(fakeChallenge.id);
  });

  it('should return false when challenge does not exist', async () => {
    const findOneSpy = jest.spyOn(service, 'findOne').mockRejectedValueOnce(NotFoundException);
    expect(challengeExistsRule.validate(fakeChallenge.id)).resolves.toEqual(false);
    expect(findOneSpy).toBeCalledTimes(1);
    expect(findOneSpy).toBeCalledWith(fakeChallenge.id);
  });

  it('should return correct default message', () => {
    expect(challengeExistsRule.defaultMessage()).toEqual('Challenge does not exist');
  });
});
