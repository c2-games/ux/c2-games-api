import { Logger } from '@nestjs/common';
import {
  AfterInsert,
  AfterRemove,
  AfterUpdate,
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { ChallengeMilestone } from '../../challenge-milestones/entities/challenge-milestone.entity';
import { UserChallengeMilestone } from '../../user-challenge-milestones/entities/user-challenge-milestone.entity';

@Entity({ name: 'challenges' })
export class Challenge {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'title', unique: true })
  title: string;

  @Column({ name: 'description' })
  description: string;

  @Column({ name: 'enabled', default: true })
  enabled: boolean;

  @Column({ name: 'created_by_user_id', default: 0 })
  createdByUserId: number;

  @CreateDateColumn({
    name: 'created_date',
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
  })
  createdDate: Date;

  @Column({ name: 'updated_by_user_id', default: 0 })
  updatedByUserId: number;

  @UpdateDateColumn({
    name: 'updated_date',
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
  })
  updatedDate: Date;

  @OneToMany(() => ChallengeMilestone, (challengeMilestone) => challengeMilestone.challenge)
  userChallengeMilestones: UserChallengeMilestone[];

  @AfterInsert()
  logInsert() {
    Logger.log(`Inserted Challenge with id ${this.id}`);
  }

  @AfterUpdate()
  logUpdate() {
    Logger.log(`Updated Challenge with id ${this.id}`);
  }

  @AfterRemove()
  logRemove() {
    Logger.log('Removed Challenge');
  }
}
