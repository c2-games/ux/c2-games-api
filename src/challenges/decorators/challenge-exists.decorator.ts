import { registerDecorator, ValidationOptions } from 'class-validator';
import { ChallengeExistsRule } from '../validators/challenge-exists-rule.validator';

export function ChallengeExists(validationOptions?: ValidationOptions) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      name: 'ChallengeExists',
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: ChallengeExistsRule,
    });
  };
}
