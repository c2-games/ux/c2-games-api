import { Body, Controller, Delete, Get, Param, Patch, Post, Query, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiCreatedResponse, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { UserRoles } from 'src/auth/decorators/roles.decorator';
import { UserRole } from 'src/auth/decorators/user-role.enum';
import { UserRolesGuard } from 'src/auth/guards/user-roles.guard';
import { CurrentUser } from '../users/decorators/current-user.decorator';
import { User } from '../users/entities/user.entity';
import { ChallengesService } from './challenges.service';
import { CreateChallengeDto } from './dto/create-challenge.dto';
import { GetChallengeRequestParamsDto } from './dto/get-challenge-request-params.dto';
import { UpdateChallengeDto } from './dto/update-challenge.dto';

@ApiTags('Challenges')
@ApiBearerAuth()
@Controller('challenges')
@UseGuards(UserRolesGuard)
export class ChallengesController {
  constructor(private readonly challengesService: ChallengesService) {}

  @Post()
  @ApiOperation({ summary: 'Create challenge' })
  @ApiCreatedResponse({ description: 'Challenge created' })
  @UserRoles(UserRole.AwardAdmin)
  create(@Body() createChallengeDto: CreateChallengeDto, @CurrentUser() currentUser: User) {
    return this.challengesService.create(createChallengeDto, currentUser.id);
  }

  @Get()
  @ApiOperation({ summary: 'Find all challenges' })
  @ApiOkResponse({ description: 'Challenges found' })
  findAll(@Query() params?: GetChallengeRequestParamsDto) {
    return this.challengesService.findAll(params);
  }

  @Get(':id')
  @ApiOperation({ summary: 'Find challenge by ID' })
  @ApiOkResponse({ description: 'Challenge found' })
  findOne(@Param('id') id: string) {
    return this.challengesService.findOne(+id);
  }

  @Patch(':id')
  @ApiOperation({ summary: 'Update challenge' })
  @ApiOkResponse({ description: 'Challenge updated' })
  @UserRoles(UserRole.AwardAdmin)
  update(@Param('id') id: string, @Body() updateChallengeDto: UpdateChallengeDto, @CurrentUser() currentUser: User) {
    return this.challengesService.update(+id, updateChallengeDto, currentUser.id);
  }

  @Delete(':id')
  @ApiOperation({ summary: 'Remove challenge' })
  @ApiOkResponse({ description: 'Challenge removed' })
  @UserRoles(UserRole.AwardAdmin)
  remove(@Param('id') id: string) {
    return this.challengesService.remove(+id);
  }
}
