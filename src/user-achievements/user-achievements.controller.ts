import { Controller, Get, Param, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiCreatedResponse, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { Roles } from 'nest-keycloak-connect';
import { UserRoles } from 'src/auth/decorators/roles.decorator';
import { UserRole } from 'src/auth/decorators/user-role.enum';
import { UserRolesGuard } from 'src/auth/guards/user-roles.guard';
import { CurrentUser } from '../users/decorators/current-user.decorator';
import { User } from '../users/entities/user.entity';
import { GetUserAchievementByUserRequestParamsDto } from './dto/get-user-achievement-by-user-request-params.dto';
import { PostUserAchievementRequestParamsDto } from './dto/post-user-achievement-request-params.dto';
import { UserAchievementsService } from './user-achievements.service';

@ApiTags('User Achievements')
@ApiBearerAuth()
@Controller()
@UseGuards(UserRolesGuard)
export class UserAchievementsController {
  constructor(private readonly userAchievementsService: UserAchievementsService) {}

  @Post('users/:userId/achievements/earned/:achievementId')
  @ApiOperation({ summary: 'Create user achievement' })
  @ApiCreatedResponse({ description: 'User achievement created' })
  @UserRoles(UserRole.AwardAdmin)
  create(@Param() params: PostUserAchievementRequestParamsDto, @CurrentUser() currentUser: User) {
    const { userId, achievementId } = params;
    const createUserAchievementDto = { userId, achievementId };
    return this.userAchievementsService.create(createUserAchievementDto, currentUser.id);
  }

  @Get('users/:userId/achievements')
  @ApiOperation({ summary: 'Find user achievements by user ID' })
  @ApiOkResponse({ description: 'User achievements found' })
  findAllByUserId(@Param() params: GetUserAchievementByUserRequestParamsDto) {
    const { userId } = params;
    return this.userAchievementsService.findAll({ userId });
  }

  // TODO: Remove endpoints if not needed

  // @Get('user-achievements')
  // @ApiOperation({ summary: 'Find all user achievements' })
  // @ApiOkResponse({ description: 'User achievements found' })
  // findAll(@Query() params?: GetUserAchievementRequestParamsDto) {
  //   return this.userAchievementsService.findAll(params);
  // }

  // @Get(':id')
  // @ApiOperation({ summary: 'Find user achievement by ID' })
  // @ApiOkResponse({ description: 'User achievement found' })
  // findOne(@Param('id') id: string) {
  //   return this.userAchievementsService.findOne(+id);
  // }

  // @Patch(':id')
  // @Roles({ roles: [UserRole.ACHIEVEMENT_ASSIGNER] })
  // @ApiOperation({ summary: 'Update user achievement' })
  // @ApiOkResponse({ description: 'User achievement updated' })
  // update(
  //   @Param('id') id: string,
  //   @Body() updateUserAchievementDto: UpdateUserAchievementDto,
  // ) {
  //   return this.userAchievementsService.update(+id, updateUserAchievementDto);
  // }

  // @Delete(':id')
  // @Roles({ roles: [UserRole.ACHIEVEMENT_ASSIGNER] })
  // @ApiOperation({ summary: 'Remove user achievement' })
  // @ApiOkResponse({ description: 'User achievement removed' })
  // remove(@Param('id') id: string) {
  //   return this.userAchievementsService.remove(+id);
  // }
}
