import { faker } from '@faker-js/faker';
import { Test, TestingModule } from '@nestjs/testing';
import { KeycloakModule } from '../keycloak/keycloak.module';
import { createFakeUserAchievement } from '../utils/test-helpers/fakes/user-achievement-faker';
import { createFakeUser } from '../utils/test-helpers/fakes/user-faker';
import { GetUserAchievementByUserRequestParamsDto } from './dto/get-user-achievement-by-user-request-params.dto';
import { PostUserAchievementRequestParamsDto } from './dto/post-user-achievement-request-params.dto';
import { UserAchievementsController } from './user-achievements.controller';
import { UserAchievementsService } from './user-achievements.service';

const currentUserId = faker.datatype.number();

const fakeUserAchievement = createFakeUserAchievement({ currentUserId });
const fakeUser = createFakeUser(currentUserId);

describe('UserAchievementsController', () => {
  let controller: UserAchievementsController;
  let service: UserAchievementsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [KeycloakModule],
      controllers: [UserAchievementsController],
      providers: [
        {
          provide: UserAchievementsService,
          useValue: {
            create: jest.fn().mockImplementation((params: PostUserAchievementRequestParamsDto, currentUserId: number) =>
              Promise.resolve({
                ...fakeUserAchievement,
                ...params,
              }),
            ),
            findAll: jest
              .fn()
              .mockImplementation((params: GetUserAchievementByUserRequestParamsDto) =>
                Promise.resolve([fakeUserAchievement]),
              ),
          },
        },
      ],
    }).compile();

    controller = module.get<UserAchievementsController>(UserAchievementsController);
    service = module.get<UserAchievementsService>(UserAchievementsService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should create user achievement', async () => {
    const params: PostUserAchievementRequestParamsDto = {
      userId: fakeUserAchievement.userId,
      achievementId: fakeUserAchievement.achievementId,
    };
    const userAchievement = await controller.create(params, fakeUser);

    expect(userAchievement).toEqual({ ...fakeUserAchievement });
    expect(service.create).toBeCalledTimes(1);
    expect(service.create).toBeCalledWith(params, fakeUser.id);
  });

  it('should find all user achievements', async () => {
    const params: GetUserAchievementByUserRequestParamsDto = {
      userId: fakeUserAchievement.userId,
    };
    const userAchievements = await controller.findAllByUserId(params);

    expect(userAchievements).toEqual([fakeUserAchievement]);
    expect(service.findAll).toBeCalledTimes(1);
    expect(service.findAll).toBeCalledWith(params);
  });
});
