import { ApiProperty } from '@nestjs/swagger';
import { IsInt } from 'class-validator';
import { UserExists } from '../../users/decorators/user-exists.decorator';

export class GetUserAchievementByUserRequestParamsDto {
  @ApiProperty({
    type: 'number',
    required: true,
    example: '123',
  })
  @IsInt()
  @UserExists()
  userId: number;
}
