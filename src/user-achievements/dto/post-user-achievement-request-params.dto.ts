import { ApiProperty } from '@nestjs/swagger';
import { IsInt } from 'class-validator';
import { AchievementExists } from '../../achievements/decorators/achievement-exists.decorator';
import { UserExists } from '../../users/decorators/user-exists.decorator';

export class PostUserAchievementRequestParamsDto {
  @ApiProperty({
    type: 'number',
    required: true,
    example: '123',
  })
  @IsInt()
  @UserExists()
  userId: number;

  @ApiProperty({
    type: 'number',
    required: true,
    example: '123',
  })
  @IsInt()
  @AchievementExists()
  achievementId: number;
}
