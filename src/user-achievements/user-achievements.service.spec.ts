import { faker } from '@faker-js/faker';
import { BadRequestException, NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { createFakeUserAchievement } from '../utils/test-helpers/fakes/user-achievement-faker';
import { GetUserAchievementRequestParamsDto } from './dto/get-user-achievement-request-params.dto';
import { UserAchievement } from './entities/user-achievement.entity';
import { UserAchievementsService } from './user-achievements.service';

const currentUserId = faker.datatype.number();

const testUserAchievement = createFakeUserAchievement({ currentUserId });

const getUserAchievementRequestParamsDto = new GetUserAchievementRequestParamsDto();

describe('UserAchievementsService', () => {
  let service: UserAchievementsService;
  let repo: Repository<UserAchievement>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UserAchievementsService,
        {
          provide: getRepositoryToken(UserAchievement),
          useValue: {
            create: jest.fn().mockReturnValue(testUserAchievement),
            save: jest.fn(),
            findAll: jest.fn().mockResolvedValue([testUserAchievement]),
            createQueryBuilder: jest.fn(() => ({
              where: jest.fn().mockReturnThis(),
              andWhere: jest.fn().mockReturnThis(),
              getMany: jest.fn().mockResolvedValue([testUserAchievement]),
            })),
            findOne: jest.fn().mockResolvedValue(testUserAchievement),
            update: jest.fn().mockResolvedValue(true),
            remove: jest.fn().mockResolvedValue(true),
          },
        },
      ],
    }).compile();

    service = module.get<UserAchievementsService>(UserAchievementsService);
    repo = module.get<Repository<UserAchievement>>(getRepositoryToken(UserAchievement));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should create a new user achievement when provided with valid createUserAchievementDto', async () => {
    const createUserAchievementDto = {
      userId: testUserAchievement.userId,
      achievementId: testUserAchievement.achievementId,
    };

    const userAchievement = await service.create(createUserAchievementDto, currentUserId);

    expect(userAchievement).toEqual(testUserAchievement);
    expect(repo.create).toBeCalledTimes(1);
    expect(repo.create).toBeCalledWith({
      ...createUserAchievementDto,
      createdByUserId: currentUserId,
      updatedByUserId: currentUserId,
    });
    expect(repo.save).toBeCalledTimes(1);
  });

  it('should find all user achievements', async () => {
    const userAchievements = await service.findAll(getUserAchievementRequestParamsDto);

    expect(userAchievements).toEqual([testUserAchievement]);
  });

  it('should find one user achievement', async () => {
    const userAchievement = await service.findOne(faker.datatype.number());

    expect(userAchievement).toEqual(testUserAchievement);
  });

  it('should throw bad request exception on findOne if id not provided', async () => {
    await expect(() => service.findOne(null)).rejects.toThrow(BadRequestException);
  });

  it('should throw not found exception on findOne if user achievement not found', async () => {
    const userAchievementId = faker.datatype.number();
    const repoSpy = jest.spyOn(repo, 'findOne').mockReturnValue(undefined);

    await expect(() => service.findOne(userAchievementId)).rejects.toThrow(
      new NotFoundException('User Achievement not found'),
    );
    expect(repoSpy).toBeCalledTimes(1);
    expect(repoSpy).toBeCalledWith(userAchievementId);
  });

  it('should update user achievement', async () => {
    const userAchievementId = faker.datatype.number();
    const updateUserAchievementDto = { earnedOn: faker.datatype.datetime() };

    const userAchievement = await service.update(userAchievementId, updateUserAchievementDto, currentUserId);

    expect(userAchievement).toEqual({
      ...testUserAchievement,
      ...updateUserAchievementDto,
    });
    expect(repo.findOne).toBeCalledTimes(1);
    expect(repo.findOne).toBeCalledWith(userAchievementId);
    expect(repo.save).toBeCalledTimes(1);
  });

  it('should remove user achievement', async () => {
    const userAchievementId = faker.datatype.number();

    const userAchievement = await service.remove(userAchievementId);

    expect(userAchievement).toEqual(testUserAchievement);
  });
});
