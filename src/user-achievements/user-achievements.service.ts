import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserAchievementDto } from './dto/create-user-achievement.dto';
import { GetUserAchievementRequestParamsDto } from './dto/get-user-achievement-request-params.dto';
import { UpdateUserAchievementDto } from './dto/update-user-achievement.dto';
import { UserAchievement } from './entities/user-achievement.entity';

@Injectable()
export class UserAchievementsService {
  constructor(
    @InjectRepository(UserAchievement)
    private repo: Repository<UserAchievement>,
  ) {}

  async create(createUserAchievementDto: CreateUserAchievementDto, currentUserId = 0) {
    const userAchievement = this.repo.create({
      ...createUserAchievementDto,
      createdByUserId: currentUserId,
      updatedByUserId: currentUserId,
    });
    await this.repo.save(userAchievement);
    return userAchievement;
  }

  findAll(params?: GetUserAchievementRequestParamsDto) {
    let query = this.repo.createQueryBuilder('users_x_achievements').where('1=1');
    query = params.userId
      ? query.andWhere('users_x_achievements.userId = :userId', {
          userId: params.userId,
        })
      : query;
    query = params.achievementId
      ? query.andWhere('users_x_achievements.achievementId = :achievementId', {
          achievementId: params.achievementId,
        })
      : query;
    query = params.earnedOn
      ? query.andWhere('users_x_achievements.earnedOn = :earnedOn', {
          earnedOn: params.earnedOn,
        })
      : query;
    return query.getMany();
  }

  async findOne(id: number) {
    if (!id) {
      throw new BadRequestException();
    }
    const userAchievement = await this.repo.findOne(id);
    if (!userAchievement) {
      throw new NotFoundException('User Achievement not found');
    }
    return userAchievement;
  }

  async update(id: number, updateUserAchievementDto: UpdateUserAchievementDto, currentUserId = 0) {
    const userAchievement = await this.findOne(id);
    Object.assign(updateUserAchievementDto, { updatedByUserId: currentUserId });
    Object.assign(userAchievement, updateUserAchievementDto);
    await this.repo.save(userAchievement);
    return userAchievement;
  }

  async remove(id: number) {
    const userAchievement = await this.findOne(id);
    await this.repo.remove(userAchievement);
    return userAchievement;
  }
}
