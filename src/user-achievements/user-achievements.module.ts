import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { KeycloakModule } from '../keycloak/keycloak.module';
import { UserAchievement } from './entities/user-achievement.entity';
import { UserAchievementsController } from './user-achievements.controller';
import { UserAchievementsService } from './user-achievements.service';

@Module({
  imports: [TypeOrmModule.forFeature([UserAchievement]), KeycloakModule],
  controllers: [UserAchievementsController],
  providers: [UserAchievementsService],
})
export class UserAchievementsModule {}
