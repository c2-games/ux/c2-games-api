import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { Intercom } from './entities/intercom.entity';
import { UpdateIntercomDto } from './dto/update-intercom.dto';

@Injectable()
export class IntercomService {
  constructor(@InjectRepository(Intercom) private repo: Repository<Intercom>) {}

  async getMessage(intercomId: number): Promise<Intercom | undefined> {
    const intercom = await this.repo.findOne(intercomId);
    if (!intercom) {
      throw new NotFoundException('No intercom entry found!');
    }
    return intercom;
  }

  async update(id: string, updateIntercomDto: UpdateIntercomDto, currentUserId = 0) {
    const intercom = await this.repo.findOne(parseInt(id));
    Object.assign(updateIntercomDto, { updatedByUserId: currentUserId });
    Object.assign(intercom, updateIntercomDto);
    await this.repo.save(intercom);
    return intercom;
  }
}
