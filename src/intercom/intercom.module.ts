import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { KeycloakModule } from '../keycloak/keycloak.module';
import { Intercom } from './entities/intercom.entity';
import { IntercomController } from './intercom.controller';
import { IntercomService } from './intercom.service';

@Module({
  imports: [TypeOrmModule.forFeature([Intercom]), KeycloakModule],
  controllers: [IntercomController],
  providers: [IntercomService],
  exports: [IntercomService],
})
export class IntercomModule {}
