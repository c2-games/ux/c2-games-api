import { faker } from '@faker-js/faker';
import { Test, TestingModule } from '@nestjs/testing';
import { EventIdParam } from '../events/dto/event-id-param';
import { KeycloakModule } from '../keycloak/keycloak.module';
import { createFakeUser } from '../utils/test-helpers/fakes/user-faker';
import { IntercomController } from './intercom.controller';
import { IntercomService } from './intercom.service';
import { UpdateIntercomDto } from './dto/update-intercom.dto';
import { createFakeIntercom } from '../utils/test-helpers/fakes/intercom-faker';

const fakeIntercom = createFakeIntercom(true, 'Test message');

describe('EventTeamsController', () => {
  let controller: IntercomController;
  let service: IntercomService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [KeycloakModule],
      controllers: [IntercomController],
      providers: [
        {
          provide: IntercomService,
          useValue: {
            getMessage: jest.fn().mockImplementation(() =>
              Promise.resolve({
                ...fakeIntercom,
              }),
            ),
            update: jest
              .fn()
              .mockImplementation((id: string, intercomDto: UpdateIntercomDto) =>
                Promise.resolve({ ...fakeIntercom, id }),
              ),
          },
        },
      ],
    }).compile();

    controller = module.get<IntercomController>(IntercomController);
    service = module.get<IntercomService>(IntercomService);
  });

  describe('should allow user to see messate', () => {
    it('user email matches primary domain', async () => {
      await controller.getMessage();

      expect(service.getMessage).toBeCalledTimes(1);
    });
  });
});
