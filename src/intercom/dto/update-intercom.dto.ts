import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsOptional, IsString } from 'class-validator';

export class UpdateIntercomDto {
  @ApiProperty({
    type: 'string',
    required: false,
    example: 'Don\t forget to sign up for the next competition!',
  })
  @IsString()
  @IsOptional()
  message?: string;

  @ApiProperty({
    type: 'boolean',
    required: false,
    example: 'true',
  })
  @IsBoolean()
  @IsOptional()
  enabled?: boolean;

  @ApiProperty({
    type: 'string',
    required: false,
    example: 'green',
  })
  @IsString()
  @IsOptional()
  intercom_color?: string;
}
