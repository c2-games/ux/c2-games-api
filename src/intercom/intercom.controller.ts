import { Body, Controller, Get, Param, Patch, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { CurrentUser } from '../users/decorators/current-user.decorator';
import { User } from '../users/entities/user.entity';
import { UserRolesGuard } from '../auth/guards/user-roles.guard';
import { IntercomService } from './intercom.service';
import { Intercom } from './entities/intercom.entity';
import { UpdateIntercomDto } from './dto/update-intercom.dto';
import { Public } from 'nest-keycloak-connect';
import { UserRoles } from '../auth/decorators/roles.decorator';
import { UserRole } from '../auth/decorators/user-role.enum';

@ApiTags('Intercom')
@Controller('intercom')
export class IntercomController {
  constructor(private readonly intercomService: IntercomService) {}

  @Get()
  @Public()
  @ApiOperation({ summary: 'Get intercom message' })
  @ApiOkResponse({ description: 'Get the current intercom message' })
  async getMessage() {
    const intercomId = 1;
    return this.intercomService.getMessage(intercomId);
  }

  @Patch(':id')
  @UseGuards(UserRolesGuard)
  @UserRoles(UserRole.EventAdmin)
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Update intercom message' })
  @ApiOkResponse({ description: 'Intercom message successfully updated' })
  async update(@Param('id') id: string, @Body() updateEventTeamDto: UpdateIntercomDto, @CurrentUser() user: User) {
    return this.intercomService.update(id, updateEventTeamDto, user.id);
  }
}
