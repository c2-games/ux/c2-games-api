import { Logger } from '@nestjs/common';
import {
  AfterInsert,
  AfterRemove,
  AfterUpdate,
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { EventTeamEventChecklistItem } from '../../event-team-event-checklist-items/entities/event-team-event-checklist-item.entity';

@Entity({ name: 'event_checklist_items' })
export class EventChecklistItem {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'title' })
  title: string;

  @Column({ name: 'description' })
  description: string;

  @Column({ name: 'enabled', default: true })
  enabled: boolean;

  @Column({ name: 'created_by_user_id', default: 0 })
  createdByUserId: number;

  @CreateDateColumn({
    name: 'created_date',
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
  })
  createdDate: Date;

  @Column({ name: 'updated_by_user_id', default: 0 })
  updatedByUserId: number;

  @UpdateDateColumn({
    name: 'updated_date',
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
  })
  updatedDate: Date;

  @OneToMany(
    () => EventTeamEventChecklistItem,
    (eventTeamEventChecklistItem) => eventTeamEventChecklistItem.eventChecklistItem,
  )
  eventTeamEventChecklistItems: EventTeamEventChecklistItem[];

  @AfterInsert()
  logInsert() {
    Logger.log(`Inserted Event Checklist Item with id ${this.id}`);
  }

  @AfterUpdate()
  logUpdate() {
    Logger.log(`Updated Event Checklist Item with id ${this.id}`);
  }

  @AfterRemove()
  logRemove() {
    Logger.log('Removed Event Checklist Item');
  }
}
