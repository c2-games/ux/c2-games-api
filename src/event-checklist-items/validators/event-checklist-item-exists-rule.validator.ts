import { Injectable } from '@nestjs/common';
import { ValidatorConstraint, ValidatorConstraintInterface } from 'class-validator';
import { EventChecklistItemsService } from '../event-checklist-items.service';

@ValidatorConstraint({ name: 'EventChecklistItemExists', async: true })
@Injectable()
export class EventChecklistItemExistsRule implements ValidatorConstraintInterface {
  constructor(private readonly eventChecklistItemsService: EventChecklistItemsService) {}

  async validate(id: number) {
    try {
      await this.eventChecklistItemsService.findOne(id);
    } catch (error) {
      return false;
    }
    return true;
  }

  defaultMessage() {
    return `Event Checklist Item does not exist`;
  }
}
