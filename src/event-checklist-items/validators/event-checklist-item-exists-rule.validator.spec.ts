import { NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { createFakeEventChecklistItem } from '../../utils/test-helpers/fakes/event-checklist-item-faker';
import { EventChecklistItemsService } from '../event-checklist-items.service';
import { EventChecklistItemExistsRule } from './event-checklist-item-exists-rule.validator';

const fakeEventChecklistItem = createFakeEventChecklistItem();

describe('EventChecklistItemExistsRule', () => {
  let eventChecklistItemExistsRule: EventChecklistItemExistsRule;
  let service: EventChecklistItemsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        EventChecklistItemExistsRule,
        {
          provide: EventChecklistItemsService,
          useValue: {
            findOne: jest.fn().mockImplementation((id: number) => Promise.resolve(fakeEventChecklistItem)),
          },
        },
      ],
    }).compile();

    eventChecklistItemExistsRule = module.get<EventChecklistItemExistsRule>(EventChecklistItemExistsRule);
    service = module.get<EventChecklistItemsService>(EventChecklistItemsService);
  });

  it('should return true when eventChecklistItem exists', async () => {
    expect(eventChecklistItemExistsRule.validate(fakeEventChecklistItem.id)).resolves.toEqual(true);
    expect(service.findOne).toBeCalledTimes(1);
    expect(service.findOne).toBeCalledWith(fakeEventChecklistItem.id);
  });

  it('should return false when event checklist item does not exist', async () => {
    const findOneSpy = jest.spyOn(service, 'findOne').mockRejectedValueOnce(NotFoundException);
    expect(eventChecklistItemExistsRule.validate(fakeEventChecklistItem.id)).resolves.toEqual(false);
    expect(findOneSpy).toBeCalledTimes(1);
    expect(findOneSpy).toBeCalledWith(fakeEventChecklistItem.id);
  });

  it('should return correct default message', () => {
    expect(eventChecklistItemExistsRule.defaultMessage()).toEqual('Event Checklist Item does not exist');
  });
});
