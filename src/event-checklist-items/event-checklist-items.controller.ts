import { Body, Controller, Delete, Get, Param, Patch, Post, Query, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiCreatedResponse, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { UserRoles } from 'src/auth/decorators/roles.decorator';
import { UserRole } from 'src/auth/decorators/user-role.enum';
import { UserRolesGuard } from 'src/auth/guards/user-roles.guard';
import { CurrentUser } from '../users/decorators/current-user.decorator';
import { User } from '../users/entities/user.entity';
import { CreateEventChecklistItemDto } from './dto/create-event-checklist-item.dto';
import { GetEventChecklistItemRequestParamsDto } from './dto/get-event-checklist-item-request-params.dto';
import { UpdateEventChecklistItemDto } from './dto/update-event-checklist-item.dto';
import { EventChecklistItemsService } from './event-checklist-items.service';

@ApiTags('Event Checklist Items')
@ApiBearerAuth()
@Controller('event-checklist-items')
@UseGuards(UserRolesGuard)
export class EventChecklistItemsController {
  constructor(private readonly eventChecklistItemsService: EventChecklistItemsService) {}

  @Post()
  @ApiOperation({ summary: 'Create event checklist item' })
  @ApiCreatedResponse({ description: 'Event checklist item created' })
  @UserRoles(UserRole.EventAdmin)
  create(@Body() createEventChecklistItemDto: CreateEventChecklistItemDto, @CurrentUser() currentUser: User) {
    return this.eventChecklistItemsService.create(createEventChecklistItemDto, currentUser.id);
  }

  @Get()
  @ApiOperation({ summary: 'Find all event checklist items' })
  @ApiOkResponse({ description: 'Event checklist items found' })
  findAll(@Query() params?: GetEventChecklistItemRequestParamsDto) {
    return this.eventChecklistItemsService.findAll(params);
  }

  @Get(':id')
  @ApiOperation({ summary: 'Find event checklist item by ID' })
  @ApiOkResponse({ description: 'Event checklist item found' })
  findOne(@Param('id') id: string) {
    return this.eventChecklistItemsService.findOne(+id);
  }

  @Patch(':id')
  @ApiOperation({ summary: 'Update event checklist item' })
  @ApiOkResponse({ description: 'Event checklist item updated' })
  @UserRoles(UserRole.EventAdmin)
  update(
    @Param('id') id: string,
    @Body() updateEventChecklistItemDto: UpdateEventChecklistItemDto,
    @CurrentUser() currentUser: User,
  ) {
    return this.eventChecklistItemsService.update(+id, updateEventChecklistItemDto, currentUser.id);
  }

  @Delete(':id')
  @ApiOperation({ summary: 'Remove event checklist item' })
  @ApiOkResponse({ description: 'Event checklist item removed' })
  @UserRoles(UserRole.EventAdmin)
  remove(@Param('id') id: string) {
    return this.eventChecklistItemsService.remove(+id);
  }
}
