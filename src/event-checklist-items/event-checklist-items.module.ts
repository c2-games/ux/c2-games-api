import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { KeycloakModule } from '../keycloak/keycloak.module';
import { EventChecklistItem } from './entities/event-checklist-item.entity';
import { EventChecklistItemsController } from './event-checklist-items.controller';
import { EventChecklistItemsService } from './event-checklist-items.service';
import { EventChecklistItemExistsRule } from './validators/event-checklist-item-exists-rule.validator';

@Module({
  imports: [TypeOrmModule.forFeature([EventChecklistItem]), KeycloakModule],
  controllers: [EventChecklistItemsController],
  providers: [EventChecklistItemsService, EventChecklistItemExistsRule],
})
export class EventChecklistItemsModule {}
