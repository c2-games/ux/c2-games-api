import { registerDecorator, ValidationOptions } from 'class-validator';
import { EventChecklistItemExistsRule } from '../validators/event-checklist-item-exists-rule.validator';

export function EventChecklistItemExists(validationOptions?: ValidationOptions) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      name: 'EventChecklistItemExists',
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: EventChecklistItemExistsRule,
    });
  };
}
