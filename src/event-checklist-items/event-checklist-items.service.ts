import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateEventChecklistItemDto } from './dto/create-event-checklist-item.dto';
import { GetEventChecklistItemRequestParamsDto } from './dto/get-event-checklist-item-request-params.dto';
import { UpdateEventChecklistItemDto } from './dto/update-event-checklist-item.dto';
import { EventChecklistItem } from './entities/event-checklist-item.entity';

@Injectable()
export class EventChecklistItemsService {
  constructor(
    @InjectRepository(EventChecklistItem)
    private repo: Repository<EventChecklistItem>,
  ) {}

  async create(createEventChecklistItemDto: CreateEventChecklistItemDto, currentUserId = 0) {
    const eventChecklistItem = this.repo.create({
      ...createEventChecklistItemDto,
      createdByUserId: currentUserId,
      updatedByUserId: currentUserId,
    });
    await this.repo.save(eventChecklistItem);
    return eventChecklistItem;
  }

  findAll(params?: GetEventChecklistItemRequestParamsDto) {
    let query = this.repo.createQueryBuilder('eventChecklistItems').where('1=1');
    query = params.title
      ? query.andWhere('eventChecklistItems.title = :title', {
          title: params.title,
        })
      : query;
    query = params.description
      ? query.andWhere('eventChecklistItems.description = :description', {
          description: params.description,
        })
      : query;
    return query.getMany();
  }

  async findOne(id: number) {
    if (!id) {
      throw new BadRequestException();
    }
    const eventChecklistItem = await this.repo.findOne(id);
    if (!eventChecklistItem) {
      throw new NotFoundException('Event Checklist Item not found');
    }
    return eventChecklistItem;
  }

  async update(id: number, updateEventChecklistItemDto: UpdateEventChecklistItemDto, currentUserId = 0) {
    const eventChecklistItem = await this.findOne(id);
    Object.assign(updateEventChecklistItemDto, {
      updatedByUserId: currentUserId,
    });
    Object.assign(eventChecklistItem, updateEventChecklistItemDto);
    await this.repo.save(eventChecklistItem);
    return eventChecklistItem;
  }

  async remove(id: number) {
    const eventChecklistItem = await this.findOne(id);
    await this.repo.remove(eventChecklistItem);
    return eventChecklistItem;
  }
}
