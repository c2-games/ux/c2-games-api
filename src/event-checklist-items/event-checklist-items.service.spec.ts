import { faker } from '@faker-js/faker';
import { BadRequestException, NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { createFakeEventChecklistItem } from '../utils/test-helpers/fakes/event-checklist-item-faker';
import { EventChecklistItemsService } from './event-checklist-items.service';
import { GetEventChecklistItemRequestParamsDto } from './dto/get-event-checklist-item-request-params.dto';
import { EventChecklistItem } from './entities/event-checklist-item.entity';

const currentUserId = faker.datatype.number();

const testEventChecklistItem = createFakeEventChecklistItem(currentUserId);

const getEventChecklistItemRequestParamsDto = new GetEventChecklistItemRequestParamsDto();

describe('EventChecklistItemsService', () => {
  let service: EventChecklistItemsService;
  let repo: Repository<EventChecklistItem>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        EventChecklistItemsService,
        {
          provide: getRepositoryToken(EventChecklistItem),
          useValue: {
            create: jest.fn().mockReturnValue(testEventChecklistItem),
            save: jest.fn(),
            findAll: jest.fn().mockResolvedValue([testEventChecklistItem]),
            createQueryBuilder: jest.fn(() => ({
              where: jest.fn().mockReturnThis(),
              andWhere: jest.fn().mockReturnThis(),
              getMany: jest.fn().mockResolvedValue([testEventChecklistItem]),
            })),
            findOne: jest.fn().mockResolvedValue(testEventChecklistItem),
            update: jest.fn().mockResolvedValue(true),
            remove: jest.fn().mockResolvedValue(true),
          },
        },
      ],
    }).compile();

    service = module.get<EventChecklistItemsService>(EventChecklistItemsService);
    repo = module.get<Repository<EventChecklistItem>>(getRepositoryToken(EventChecklistItem));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should create a new event checklist item when provided with valid createEventChecklistItemDto', async () => {
    const createEventChecklistItemDto = {
      title: testEventChecklistItem.title,
      description: testEventChecklistItem.description,
    };

    const eventChecklistItem = await service.create(createEventChecklistItemDto, currentUserId);

    expect(eventChecklistItem).toEqual(testEventChecklistItem);
    expect(repo.create).toBeCalledTimes(1);
    expect(repo.create).toBeCalledWith({
      ...createEventChecklistItemDto,
      createdByUserId: currentUserId,
      updatedByUserId: currentUserId,
    });
    expect(repo.save).toBeCalledTimes(1);
  });

  it('should find all event checklist items', async () => {
    const eventChecklistItems = await service.findAll(getEventChecklistItemRequestParamsDto);

    expect(eventChecklistItems).toEqual([testEventChecklistItem]);
  });

  it('should find one event checklist item', async () => {
    const eventChecklistItem = await service.findOne(faker.datatype.number());

    expect(eventChecklistItem).toEqual(testEventChecklistItem);
  });

  it('should throw bad request exception on findOne if id not provided', async () => {
    await expect(() => service.findOne(null)).rejects.toThrow(BadRequestException);
  });

  it('should throw not found exception on findOne if event checklist item not found', async () => {
    const eventChecklistItemId = faker.datatype.number();
    const repoSpy = jest.spyOn(repo, 'findOne').mockReturnValue(undefined);

    await expect(() => service.findOne(eventChecklistItemId)).rejects.toThrow(
      new NotFoundException('Event Checklist Item not found'),
    );
    expect(repoSpy).toBeCalledTimes(1);
    expect(repoSpy).toBeCalledWith(eventChecklistItemId);
  });

  it('should update event checklist item', async () => {
    const eventChecklistItemId = faker.datatype.number();
    const updateEventChecklistItemDto = {
      title: 'This title has been updated',
    };

    const eventChecklistItem = await service.update(eventChecklistItemId, updateEventChecklistItemDto, currentUserId);

    expect(eventChecklistItem).toEqual({
      ...testEventChecklistItem,
      ...updateEventChecklistItemDto,
    });
    expect(repo.findOne).toBeCalledTimes(1);
    expect(repo.findOne).toBeCalledWith(eventChecklistItemId);
    expect(repo.save).toBeCalledTimes(1);
  });

  it('should remove event checklist item', async () => {
    const eventChecklistItemId = faker.datatype.number();

    const eventChecklistItem = await service.remove(eventChecklistItemId);

    expect(eventChecklistItem).toEqual(testEventChecklistItem);
  });
});
