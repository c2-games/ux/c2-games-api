import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

export class GetEventChecklistItemRequestParamsDto {
  @ApiProperty({
    type: 'string',
    required: false,
    example: 'Event Checklist Item Title',
  })
  @IsString()
  @IsOptional()
  title?: string;

  @ApiProperty({
    type: 'string',
    required: false,
    example: 'This is an event checklist item description',
  })
  @IsString()
  @IsOptional()
  description?: string;
}
