import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class CreateEventChecklistItemDto {
  @ApiProperty({
    type: 'string',
    required: true,
    example: 'Event Checklist Item Title',
  })
  @IsString()
  title: string;

  @ApiProperty({
    type: 'string',
    required: true,
    example: 'This is an event checklist item description',
  })
  @IsString()
  description: string;
}
