import { faker } from '@faker-js/faker';
import { Test, TestingModule } from '@nestjs/testing';
import { KeycloakModule } from '../keycloak/keycloak.module';
import { createFakeEventChecklistItem } from '../utils/test-helpers/fakes/event-checklist-item-faker';
import { createFakeUser } from '../utils/test-helpers/fakes/user-faker';
import { CreateEventChecklistItemDto } from './dto/create-event-checklist-item.dto';
import { UpdateEventChecklistItemDto } from './dto/update-event-checklist-item.dto';
import { EventChecklistItemsController } from './event-checklist-items.controller';
import { EventChecklistItemsService } from './event-checklist-items.service';

const currentUserId = faker.datatype.number();

const fakeEventChecklistItem = createFakeEventChecklistItem(currentUserId);
const fakeUser = createFakeUser(currentUserId);

describe('EventChecklistItemsController', () => {
  let controller: EventChecklistItemsController;
  let service: EventChecklistItemsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [KeycloakModule],
      controllers: [EventChecklistItemsController],
      providers: [
        {
          provide: EventChecklistItemsService,
          useValue: {
            create: jest
              .fn()
              .mockImplementation((eventChecklistItem: CreateEventChecklistItemDto, currentUserId: number) =>
                Promise.resolve({
                  ...fakeEventChecklistItem,
                  ...eventChecklistItem,
                }),
              ),
            findAll: jest.fn().mockResolvedValue([fakeEventChecklistItem]),
            findOne: jest.fn().mockImplementation((id: number) => Promise.resolve({ ...fakeEventChecklistItem, id })),
            update: jest
              .fn()
              .mockImplementation(
                (id: number, eventChecklistItem: UpdateEventChecklistItemDto, currentUserId: number) =>
                  Promise.resolve({ ...fakeEventChecklistItem, id }),
              ),
            remove: jest.fn().mockResolvedValue(fakeEventChecklistItem),
          },
        },
      ],
    }).compile();

    controller = module.get<EventChecklistItemsController>(EventChecklistItemsController);
    service = module.get<EventChecklistItemsService>(EventChecklistItemsService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should create event checklist item', async () => {
    const createEventChecklistItemDto: CreateEventChecklistItemDto = {
      title: fakeEventChecklistItem.title,
      description: fakeEventChecklistItem.description,
    };
    const eventChecklistItem = await controller.create(createEventChecklistItemDto, fakeUser);

    expect(eventChecklistItem).toEqual({ ...fakeEventChecklistItem });
    expect(service.create).toBeCalledTimes(1);
    expect(service.create).toBeCalledWith(createEventChecklistItemDto, fakeUser.id);
  });

  it('should find all event checklist items', async () => {
    const eventChecklistItems = await controller.findAll();

    expect(eventChecklistItems).toEqual([fakeEventChecklistItem]);
    expect(service.findAll).toBeCalledTimes(1);
  });

  it('should find one event checklist item by id', async () => {
    const eventChecklistItem = await controller.findOne(`${fakeEventChecklistItem.id}`);

    expect(eventChecklistItem).toEqual(fakeEventChecklistItem);
    expect(service.findOne).toBeCalledTimes(1);
    expect(service.findOne).toBeCalledWith(fakeEventChecklistItem.id);
  });

  it('should update event checklist item', async () => {
    const updateEventChecklistItemDto = {
      title: faker.datatype.string(),
    };
    const eventChecklistItem = await controller.update(
      `${fakeEventChecklistItem.id}`,
      updateEventChecklistItemDto,
      fakeUser,
    );

    expect(eventChecklistItem).toEqual(fakeEventChecklistItem);
    expect(service.update).toBeCalledTimes(1);
    expect(service.update).toBeCalledWith(fakeEventChecklistItem.id, updateEventChecklistItemDto, fakeUser.id);
  });

  it('should remove event checklist item', async () => {
    const eventChecklistItem = await controller.remove(`${fakeEventChecklistItem.id}`);

    expect(eventChecklistItem).toEqual(fakeEventChecklistItem);
    expect(service.remove).toBeCalledTimes(1);
    expect(service.remove).toBeCalledWith(fakeEventChecklistItem.id);
  });
});
