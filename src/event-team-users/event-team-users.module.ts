import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { KeycloakModule } from '../keycloak/keycloak.module';
import { EventTeamUser } from './entities/event-team-user.entity';
import { EventTeamUsersController } from './event-team-users.controller';
import { EventTeamUsersService } from './event-team-users.service';
import { EventTeamsService } from '../event-teams/event-teams.service';
import { EventTeam } from '../event-teams/entities/event-team.entity';
import { UserRolesGuard } from '../auth/guards/user-roles.guard';

@Module({
  imports: [TypeOrmModule.forFeature([EventTeamUser, EventTeam]), KeycloakModule],
  controllers: [EventTeamUsersController],
  providers: [EventTeamUsersService, EventTeamsService, UserRolesGuard],
})
export class EventTeamUsersModule {}
