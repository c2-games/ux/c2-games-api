import { ApiProperty } from '@nestjs/swagger';
import { IsInt } from 'class-validator';
import { EventTeamExists } from '../../event-teams/decorators/event-team-exists.decorator';

export class GetEventTeamUsersByEventTeamRequestParamsDto {
  @ApiProperty({
    type: 'number',
    required: false,
    example: '123',
  })
  @IsInt()
  @EventTeamExists()
  eventTeamId: number;
}
