import { ApiProperty } from '@nestjs/swagger';
import { IsDate, IsInt, IsOptional } from 'class-validator';
import { EventTeamExists } from '../../event-teams/decorators/event-team-exists.decorator';
import { UserExists } from '../../users/decorators/user-exists.decorator';

export class CreateEventTeamUserDto {
  @ApiProperty({
    type: 'number',
    required: true,
    example: '123',
  })
  @IsInt()
  @EventTeamExists()
  eventTeamId: number;

  @ApiProperty({
    type: 'number',
    required: true,
    example: '123',
  })
  @IsInt()
  @UserExists()
  userId: number;

  @ApiProperty({
    type: 'date',
    required: false,
    example: '2022-08-01T20:29:56.561Z',
  })
  @IsDate()
  @IsOptional()
  addedOn?: Date;
}
