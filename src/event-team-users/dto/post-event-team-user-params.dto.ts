import { ApiProperty } from '@nestjs/swagger';
import { IsInt } from 'class-validator';
import { EventTeamExists } from '../../event-teams/decorators/event-team-exists.decorator';
import { UserExists } from '../../users/decorators/user-exists.decorator';

export class PostEventTeamUserParamsDto {
  @ApiProperty({
    type: 'number',
    required: true,
    example: '123',
  })
  @IsInt()
  @EventTeamExists()
  eventTeamId: number;

  @ApiProperty({
    type: 'number',
    required: true,
    example: '123',
  })
  @IsInt()
  @UserExists()
  userId: number;
}
