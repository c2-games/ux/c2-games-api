import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsDate, IsOptional } from 'class-validator';

export class UpdateEventTeamUserDto {
  @ApiProperty({
    type: 'date',
    required: false,
    example: '2022-08-01T20:29:56.561Z',
  })
  @IsDate()
  @IsOptional()
  addedOn?: Date;

  @ApiProperty({
    type: 'boolean',
    required: false,
    example: 'true',
  })
  @IsBoolean()
  @IsOptional()
  enabled?: boolean;
}
