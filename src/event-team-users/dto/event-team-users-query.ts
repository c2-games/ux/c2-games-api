import { IsDate, IsInt, IsOptional } from 'class-validator';
import { EventTeamExists } from '../../event-teams/decorators/event-team-exists.decorator';
import { UserExists } from '../../users/decorators/user-exists.decorator';

export class EventTeamUsersQuery {
  @IsInt()
  @EventTeamExists()
  @IsOptional()
  eventTeamId?: number;

  @IsInt()
  @UserExists()
  @IsOptional()
  userId?: number;

  @IsDate()
  @IsOptional()
  addedOn?: Date;
}
