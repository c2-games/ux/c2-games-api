import {
  BadRequestException,
  Body,
  ConflictException,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Query,
  ValidationPipe,
} from '@nestjs/common';
import { ApiBearerAuth, ApiCreatedResponse, ApiOkResponse, ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';
import { CurrentUser } from '../users/decorators/current-user.decorator';
import { User } from '../users/entities/user.entity';
import { EventTeamUsersQuery } from './dto/event-team-users-query';
import { GetEventTeamUsersByEventTeamRequestParamsDto } from './dto/get-event-team-users-by-event-team-request-params.dto';
import { PostEventTeamUserBodyDto } from './dto/post-event-team-user-body.dto';
import { PostEventTeamUserParamsDto } from './dto/post-event-team-user-params.dto';
import { EventTeamUsersService } from './event-team-users.service';
import { EventTeamsService } from '../event-teams/event-teams.service';
import type { CreateEventTeamUserDto } from './dto/create-event-team-user.dto';

@ApiTags('Event Team Users')
@ApiBearerAuth()
@Controller()
export class EventTeamUsersController {
  constructor(
    private readonly eventTeamUsersService: EventTeamUsersService,
    private readonly eventTeamsService: EventTeamsService,
  ) {}

  @Post('event-teams/:eventTeamId/users/:userId')
  @ApiOperation({ summary: 'Create event team user' })
  @ApiCreatedResponse({
    description: 'Event team user created',
  })
  async create(
    @Param() params: PostEventTeamUserParamsDto,
    @Body() body: PostEventTeamUserBodyDto,
    @CurrentUser() currentUser: User,
  ) {
    const teamCapacity = 10;
    const { eventTeamId, userId } = { ...params, ...body } as CreateEventTeamUserDto;
    await this.eventTeamsService.userCanModifyTeam(currentUser, eventTeamId);

    // check if user already exists
    const exists = await this.eventTeamUsersService.findOne(
      { where: { eventTeamId: eventTeamId, userId: userId } },
      false,
    );
    if (exists) throw new ConflictException('User already exists on team');

    // check team size
    const memberCount = await this.eventTeamUsersService.memberCount(eventTeamId);
    if (memberCount >= teamCapacity) throw new BadRequestException(`Team is at max capacity of ${teamCapacity}`);

    // add team member
    return this.eventTeamUsersService.create({ eventTeamId, userId }, currentUser.id);
  }

  @Get('event-teams/')
  @ApiQuery({ name: 'userId', type: 'number' })
  @ApiOperation({ summary: 'Find all event team users by query' })
  @ApiOkResponse({ description: 'Event team users found' })
  findAll(
    @Query(
      new ValidationPipe({
        transform: true,
        transformOptions: { enableImplicitConversion: true },
        whitelist: true,
        forbidNonWhitelisted: true,
      }),
    )
    query: EventTeamUsersQuery,
  ) {
    return this.eventTeamUsersService.findAll(query);
  }

  @Get('event-teams/:eventTeamId/users')
  @ApiOperation({ summary: 'Find all event team users by event team' })
  @ApiOkResponse({ description: 'Event team users found' })
  findAllByEventTeam(@Param() params?: GetEventTeamUsersByEventTeamRequestParamsDto) {
    return this.eventTeamUsersService.findAll(params);
  }

  @Get('event-team-users/:id')
  @ApiOperation({ summary: 'Find event team user by ID' })
  @ApiOkResponse({ description: 'Event team user found' })
  findOne(@Param('id') id: string) {
    return this.eventTeamUsersService.findOneById(+id);
  }

  // todo re-implement to allow enabling/disabling users within a team
  // @Patch('event-team-users/:id')
  // @ApiOperation({ summary: 'Update event team user' })
  // @ApiOkResponse({ description: 'Event team user updated' })
  // async update(
  //   @Param('id') id: string,
  //   @Body()
  //   updateEventTeamUserDto: UpdateEventTeamUserDto,
  //   @CurrentUser() currentUser: User,
  // ) {
  //   return this.eventTeamUsersService.update(+id, updateEventTeamUserDto, currentUser.id);
  // }

  @Delete('event-team-users/:id')
  @ApiOperation({ summary: 'Remove event team user' })
  @ApiOkResponse({ description: 'Event team user removed' })
  async remove(@Param('id') id: string, @CurrentUser() currentUser: User) {
    await this.eventTeamUsersService.canModifyTeamUser(currentUser, +id);
    return this.eventTeamUsersService.remove(+id);
  }
}
