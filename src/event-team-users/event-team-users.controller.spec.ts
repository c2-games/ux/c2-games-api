import { faker } from '@faker-js/faker';
import { Test, TestingModule } from '@nestjs/testing';
import { KeycloakModule } from '../keycloak/keycloak.module';
import { createFakeEventTeamUser } from '../utils/test-helpers/fakes/event-team-user-faker';
import { createFakeUser } from '../utils/test-helpers/fakes/user-faker';
import { CreateEventTeamUserDto } from './dto/create-event-team-user.dto';
import { GetEventTeamUsersByEventTeamRequestParamsDto } from './dto/get-event-team-users-by-event-team-request-params.dto';
import { PostEventTeamUserBodyDto } from './dto/post-event-team-user-body.dto';
import { PostEventTeamUserParamsDto } from './dto/post-event-team-user-params.dto';
import { UpdateEventTeamUserDto } from './dto/update-event-team-user.dto';
import { EventTeamUsersController } from './event-team-users.controller';
import { EventTeamUsersService } from './event-team-users.service';

const currentUserId = faker.datatype.number();

const fakeEventTeamUser = createFakeEventTeamUser({
  userId: currentUserId,
});
const fakeUser = createFakeUser(currentUserId);

const postEventTeamUserParamsDto: PostEventTeamUserParamsDto = {
  userId: fakeEventTeamUser.userId,
  eventTeamId: fakeEventTeamUser.eventTeamId,
};
const postEventTeamUserBodyDto: PostEventTeamUserBodyDto = {
  addedOn: fakeEventTeamUser.addedOn,
};
const getEventTeamUsersByEventTeamRequestParamsDto: GetEventTeamUsersByEventTeamRequestParamsDto = {
  eventTeamId: fakeEventTeamUser.eventTeamId,
};

describe('EventTeamUsersController', () => {
  let controller: EventTeamUsersController;
  let service: EventTeamUsersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [KeycloakModule],
      controllers: [EventTeamUsersController],
      providers: [
        {
          provide: EventTeamUsersService,
          useValue: {
            create: jest.fn().mockImplementation((eventTeamUser: CreateEventTeamUserDto, currentUserId: number) =>
              Promise.resolve({
                ...fakeEventTeamUser,
                ...eventTeamUser,
              }),
            ),
            findAll: jest.fn().mockResolvedValue([fakeEventTeamUser]),
            findOne: jest.fn().mockImplementation((id: number) => Promise.resolve({ ...fakeEventTeamUser, id })),
            update: jest
              .fn()
              .mockImplementation((id: number, eventTeamUser: UpdateEventTeamUserDto, currentUserId: number) =>
                Promise.resolve({ ...fakeEventTeamUser, id }),
              ),
            remove: jest.fn().mockResolvedValue(fakeEventTeamUser),
          },
        },
      ],
    }).compile();

    controller = module.get<EventTeamUsersController>(EventTeamUsersController);
    service = module.get<EventTeamUsersService>(EventTeamUsersService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should create event team user', async () => {
    const createEventTeamUserDto: CreateEventTeamUserDto = {
      userId: fakeEventTeamUser.userId,
      eventTeamId: fakeEventTeamUser.eventTeamId,
      addedOn: fakeEventTeamUser.addedOn,
    };
    const eventTeamUser = await controller.create(postEventTeamUserParamsDto, postEventTeamUserBodyDto, fakeUser);

    expect(eventTeamUser).toEqual({
      ...fakeEventTeamUser,
    });
    expect(service.create).toBeCalledTimes(1);
    expect(service.create).toBeCalledWith(createEventTeamUserDto, fakeUser.id);
  });

  it('should find all event team users', async () => {
    const eventTeamUsers = await controller.findAllByEventTeam(getEventTeamUsersByEventTeamRequestParamsDto);

    expect(eventTeamUsers).toEqual([fakeEventTeamUser]);
    expect(service.findAll).toBeCalledTimes(1);
  });

  it('should find one event team user by id', async () => {
    const eventTeamUser = await controller.findOne(`${fakeEventTeamUser.id}`);

    expect(eventTeamUser).toEqual(fakeEventTeamUser);
    expect(service.findOneById).toBeCalledTimes(1);
    expect(service.findOneById).toBeCalledWith(fakeEventTeamUser.id);
  });

  it('should update event team user', async () => {
    const updateEventTeamUserDto = {
      addedOn: faker.datatype.datetime(),
    };
    const eventTeamUser = await controller.update(`${fakeEventTeamUser.id}`, updateEventTeamUserDto, fakeUser);

    expect(eventTeamUser).toEqual(fakeEventTeamUser);
    expect(service.update).toBeCalledTimes(1);
    expect(service.update).toBeCalledWith(fakeEventTeamUser.id, updateEventTeamUserDto, fakeUser.id);
  });

  it('should remove event team user', async () => {
    const eventTeamUser = await controller.remove(`${fakeEventTeamUser.id}`);

    expect(eventTeamUser).toEqual(fakeEventTeamUser);
    expect(service.remove).toBeCalledTimes(1);
    expect(service.remove).toBeCalledWith(fakeEventTeamUser.id);
  });
});
