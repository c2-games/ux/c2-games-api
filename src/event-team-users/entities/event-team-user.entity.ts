import { Logger } from '@nestjs/common';
import {
  AfterInsert,
  AfterRemove,
  AfterUpdate,
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  Unique,
  UpdateDateColumn,
} from 'typeorm';
import { EventTeam } from '../../event-teams/entities/event-team.entity';
import { User } from '../../users/entities/user.entity';

@Entity({ name: 'event_teams_x_users' })
@Unique(['eventTeamId', 'userId'])
export class EventTeamUser {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'event_team_id' })
  eventTeamId: number;

  @Column({ name: 'user_id' })
  userId: number;

  @Column({
    name: 'added_on',
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
    nullable: true,
  })
  addedOn: Date;

  @Column({ name: 'enabled', default: true })
  enabled: boolean;

  @Column({ name: 'created_by_user_id', default: 0 })
  createdByUserId: number;

  @CreateDateColumn({
    name: 'created_date',
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
  })
  createdDate: Date;

  @Column({ name: 'updated_by_user_id', default: 0 })
  updatedByUserId: number;

  @UpdateDateColumn({
    name: 'updated_date',
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
  })
  updatedDate: Date;

  @ManyToOne(() => EventTeam)
  @JoinColumn({ name: 'event_team_id', referencedColumnName: 'id' })
  eventTeam: EventTeam;

  @ManyToOne(() => User)
  @JoinColumn({ name: 'user_id', referencedColumnName: 'id' })
  user: User;

  @AfterInsert()
  logInsert() {
    Logger.log(`Inserted Event Team User with id ${this.id}`);
  }

  @AfterUpdate()
  logUpdate() {
    Logger.log(`Updated Event Team User with id ${this.id}`);
  }

  @AfterRemove()
  logRemove() {
    Logger.log('Removed Event Team User');
  }
}
