import { BadRequestException, Injectable, Logger, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindOneOptions, Repository } from 'typeorm';
import { CreateEventTeamUserDto } from './dto/create-event-team-user.dto';
import { EventTeamUsersQuery } from './dto/event-team-users-query';
import { UpdateEventTeamUserDto } from './dto/update-event-team-user.dto';
import { EventTeamUser } from './entities/event-team-user.entity';
import { User } from '../users/entities/user.entity';
import { EventTeamsService } from '../event-teams/event-teams.service';

@Injectable()
export class EventTeamUsersService {
  constructor(
    @InjectRepository(EventTeamUser)
    private repo: Repository<EventTeamUser>,
    private eventTeamsService: EventTeamsService,
  ) {}

  async canModifyTeamUser(user: User, teamUserId: number, autoError = true): Promise<boolean> {
    const teamUser = await this.repo.findOne(teamUserId);
    return await this.eventTeamsService.userCanModifyTeam(user, teamUser.eventTeamId, autoError);
  }

  async create(createEventTeamUserDto: CreateEventTeamUserDto, currentUserId): Promise<EventTeamUser> {
    const eventTeamUser = this.repo.create({
      ...createEventTeamUserDto,
      createdByUserId: currentUserId,
      updatedByUserId: currentUserId,
    });
    await this.repo.save(eventTeamUser);
    return eventTeamUser;
  }

  findAll(params?: EventTeamUsersQuery): Promise<EventTeamUser[]> {
    let query = this.repo.createQueryBuilder('event_teams_x_users');
    query = params.eventTeamId
      ? query.andWhere('event_teams_x_users.eventTeamId = :eventTeamId', {
          eventTeamId: params.eventTeamId,
        })
      : query;
    query = params.userId
      ? query.andWhere('event_teams_x_users.userId = :userId', {
          userId: params.userId,
        })
      : query;
    query = params.addedOn
      ? query.andWhere('event_teams_x_users.addedOn = :addedOn', {
          addedOn: params.addedOn,
        })
      : query;
    // include user details for members
    query.leftJoinAndSelect('event_teams_x_users.user', 'user');
    return query.getMany();
  }

  async findOne(options: FindOneOptions, autoError = true): Promise<EventTeamUser | undefined> {
    // todo check if user is an admin, they can do anything!
    const user = await this.repo.findOne(options);
    if (!user && autoError) {
      throw new NotFoundException('Event Team User not found');
    }
    return user;
  }

  async findOneById(id: number): Promise<EventTeamUser> {
    if (!id) {
      throw new BadRequestException();
    }
    const eventTeamUser = await this.repo.findOne(id);
    if (!eventTeamUser) {
      throw new NotFoundException('Event Team User not found');
    }
    return eventTeamUser;
  }

  async update(id: number, updateEventTeamUserDto: UpdateEventTeamUserDto, currentUserId = 0): Promise<EventTeamUser> {
    const eventTeamUser = await this.findOneById(id);
    Object.assign(updateEventTeamUserDto, { updatedByUserId: currentUserId });
    Object.assign(eventTeamUser, updateEventTeamUserDto);
    await this.repo.save(eventTeamUser);
    return eventTeamUser;
  }

  async memberCount(eventTeamId: number) {
    return await this.repo.count({ where: { eventTeamId } });
  }

  async remove(id: number): Promise<EventTeamUser> {
    const eventTeamUser = await this.findOneById(id);
    await this.repo.remove(eventTeamUser);
    // check for an empty team and delete it
    if ((await this.memberCount(eventTeamUser.eventTeamId)) === 0) {
      Logger.debug('removing empty team');
      await this.eventTeamsService.remove(eventTeamUser.eventTeamId);
    }
    return eventTeamUser;
  }

  async removeByTeamId(eventTeamId: number): Promise<void> {
    const users = await this.repo.find({
      where: { eventTeamId },
    });
    await this.repo.remove(users);
  }
}
