import { faker } from '@faker-js/faker';
import { BadRequestException, NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { createFakeEventTeamUser } from '../utils/test-helpers/fakes/event-team-user-faker';
import { EventTeamUsersQuery } from './dto/event-team-users-query';
import { EventTeamUser } from './entities/event-team-user.entity';
import { EventTeamUsersService } from './event-team-users.service';

const currentUserId = faker.datatype.number();

const testEventTeamUser = createFakeEventTeamUser({ currentUserId });

const eventTeamUsersQueryDto = new EventTeamUsersQuery();

describe('EventTeamUsersService', () => {
  let service: EventTeamUsersService;
  let repo: Repository<EventTeamUser>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        EventTeamUsersService,
        {
          provide: getRepositoryToken(EventTeamUser),
          useValue: {
            create: jest.fn().mockReturnValue(testEventTeamUser),
            save: jest.fn(),
            findAll: jest.fn().mockResolvedValue([testEventTeamUser]),
            createQueryBuilder: jest.fn(() => ({
              where: jest.fn().mockReturnThis(),
              andWhere: jest.fn().mockReturnThis(),
              getMany: jest.fn().mockResolvedValue([testEventTeamUser]),
            })),
            findOne: jest.fn().mockResolvedValue(testEventTeamUser),
            update: jest.fn().mockResolvedValue(true),
            remove: jest.fn().mockResolvedValue(true),
          },
        },
      ],
    }).compile();

    service = module.get<EventTeamUsersService>(EventTeamUsersService);
    repo = module.get<Repository<EventTeamUser>>(getRepositoryToken(EventTeamUser));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should create a new event team user when provided with valid createEventTeamUserDto', async () => {
    const createEventTeamUserDto = {
      eventTeamId: testEventTeamUser.eventTeamId,
      userId: testEventTeamUser.userId,
    };

    const eventTeamUser = await service.create(createEventTeamUserDto, currentUserId);

    expect(eventTeamUser).toEqual(testEventTeamUser);
    expect(repo.create).toBeCalledTimes(1);
    expect(repo.create).toBeCalledWith({
      ...createEventTeamUserDto,
      createdByUserId: currentUserId,
      updatedByUserId: currentUserId,
    });
    expect(repo.save).toBeCalledTimes(1);
  });

  it('should find all event team users', async () => {
    const eventTeamUsers = await service.findAll(eventTeamUsersQueryDto);

    expect(eventTeamUsers).toEqual([testEventTeamUser]);
  });

  it('should find one event team user', async () => {
    const eventTeamUser = await service.findOneById(faker.datatype.number());

    expect(eventTeamUser).toEqual(testEventTeamUser);
  });

  it('should throw bad request exception on findOne if id not provided', async () => {
    await expect(() => service.findOneById(null)).rejects.toThrow(BadRequestException);
  });

  it('should throw not found exception on findOne if event team user not found', async () => {
    const eventTeamUserId = faker.datatype.number();
    const repoSpy = jest.spyOn(repo, 'findOne').mockReturnValue(undefined);

    await expect(() => service.findOneById(eventTeamUserId)).rejects.toThrow(
      new NotFoundException('Event Team User not found'),
    );
    expect(repoSpy).toBeCalledTimes(1);
    expect(repoSpy).toBeCalledWith(eventTeamUserId);
  });

  it('should update event team user', async () => {
    const eventTeamUserId = faker.datatype.number();
    const updateEventTeamUserDto = { addedOn: faker.datatype.datetime() };

    const eventTeamUser = await service.update(eventTeamUserId, updateEventTeamUserDto, currentUserId);

    expect(eventTeamUser).toEqual({
      ...testEventTeamUser,
      ...updateEventTeamUserDto,
    });
    expect(repo.findOne).toBeCalledTimes(1);
    expect(repo.findOne).toBeCalledWith(eventTeamUserId);
    expect(repo.save).toBeCalledTimes(1);
  });

  it('should remove event team user', async () => {
    const eventTeamUserId = faker.datatype.number();

    const eventTeamUser = await service.remove(eventTeamUserId);

    expect(eventTeamUser).toEqual(testEventTeamUser);
  });
});
