import { ClassSerializerInterceptor, ValidationPipe, VersioningType } from '@nestjs/common';
import { NestFactory, Reflector } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { useContainer } from 'class-validator';
import { AppModule } from './app.module';

const port = process.env.PORT || 5000;

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  useContainer(app.select(AppModule), { fallbackOnErrors: true });

  app.enableCors();
  app.enableVersioning({
    defaultVersion: '1',
    type: VersioningType.URI,
  });
  app.useGlobalInterceptors(new ClassSerializerInterceptor(app.get(Reflector)));
  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
      transformOptions: { enableImplicitConversion: true },
      forbidNonWhitelisted: true,
    }),
  );

  const swaggerConfig = new DocumentBuilder()
    .setTitle('Tracking API')
    .setDescription('Central API for tracking users across the entire C2Games platform')
    .addBearerAuth()
    .addTag('Achievements')
    .addTag('Auth')
    .addTag('Badges')
    .addTag('Challenges')
    .addTag('Challenge Milestones')
    .addTag('Event Checklist Items')
    .addTag('Event Team Event Checklist Items')
    .addTag('Event Team Users')
    .addTag('Event Teams')
    .addTag('Events')
    .addTag('Intercom')
    .addTag('Messages')
    .addTag('Users')
    .addTag('User Achievements')
    .addTag('User Badges')
    .addTag('User Challenge Milestones')
    .addTag('User Checklist Items')
    .addTag('User Messages')
    .addTag('User User Checklist Items')
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, swaggerConfig);
  SwaggerModule.setup('api', app, document);

  await app.listen(port);
}
bootstrap();
