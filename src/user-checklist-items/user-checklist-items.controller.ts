import { Body, Controller, Delete, Get, Param, Patch, Post, Query, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiCreatedResponse, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { UserRoles } from 'src/auth/decorators/roles.decorator';
import { UserRole } from 'src/auth/decorators/user-role.enum';
import { UserRolesGuard } from 'src/auth/guards/user-roles.guard';
import { CurrentUser } from '../users/decorators/current-user.decorator';
import { User } from '../users/entities/user.entity';
import { CreateUserChecklistItemDto } from './dto/create-user-checklist-item.dto';
import { GetUserChecklistItemRequestParamsDto } from './dto/get-user-checklist-item-request-params.dto';
import { UpdateUserChecklistItemDto } from './dto/update-user-checklist-item.dto';
import { UserChecklistItemsService } from './user-checklist-items.service';

@ApiTags('User Checklist Items')
@ApiBearerAuth()
@Controller('user-checklist-items')
@UseGuards(UserRolesGuard)
export class UserChecklistItemsController {
  constructor(private readonly userChecklistItemsService: UserChecklistItemsService) {}

  @Post()
  @ApiOperation({ summary: 'Create user checklist item' })
  @ApiCreatedResponse({ description: 'User checklist item created' })
  @UserRoles(UserRole.AwardAdmin)
  create(@Body() createUserChecklistItemDto: CreateUserChecklistItemDto, @CurrentUser() currentUser: User) {
    return this.userChecklistItemsService.create(createUserChecklistItemDto, currentUser.id);
  }

  @Get()
  @ApiOperation({ summary: 'Find all user checklist items' })
  @ApiOkResponse({ description: 'User checklist items found' })
  findAll(@Query() params?: GetUserChecklistItemRequestParamsDto) {
    return this.userChecklistItemsService.findAll(params);
  }

  @Get(':id')
  @ApiOperation({ summary: 'Find user checklist item by ID' })
  @ApiOkResponse({ description: 'User checklist item found' })
  findOne(@Param('id') id: string) {
    return this.userChecklistItemsService.findOne(+id);
  }

  @Patch(':id')
  @ApiOperation({ summary: 'Update user checklist item' })
  @ApiOkResponse({ description: 'User checklist item updated' })
  @UserRoles(UserRole.AwardAdmin)
  update(
    @Param('id') id: string,
    @Body() updateUserChecklistItemDto: UpdateUserChecklistItemDto,
    @CurrentUser() currentUser: User,
  ) {
    return this.userChecklistItemsService.update(+id, updateUserChecklistItemDto, currentUser.id);
  }

  @Delete(':id')
  @ApiOperation({ summary: 'Remove user checklist item' })
  @ApiOkResponse({ description: 'User checklist item removed' })
  @UserRoles(UserRole.AwardAdmin)
  remove(@Param('id') id: string) {
    return this.userChecklistItemsService.remove(+id);
  }
}
