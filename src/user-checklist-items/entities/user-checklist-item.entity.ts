import { Logger } from '@nestjs/common';
import {
  AfterInsert,
  AfterRemove,
  AfterUpdate,
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { UserUserChecklistItem } from '../../user-user-checklist-items/entities/user-user-checklist-item.entity';

@Entity({ name: 'user_checklist_items' })
export class UserChecklistItem {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'title' })
  title: string;

  @Column({ name: 'description' })
  description: string;

  @Column({ name: 'enabled', default: true })
  enabled: boolean;

  @Column({ name: 'created_by_user_id', default: 0 })
  createdByUserId: number;

  @CreateDateColumn({
    name: 'created_date',
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
  })
  createdDate: Date;

  @Column({ name: 'updated_by_user_id', default: 0 })
  updatedByUserId: number;

  @UpdateDateColumn({
    name: 'updated_date',
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
  })
  updatedDate: Date;

  @OneToMany(() => UserUserChecklistItem, (userUserChecklistItem) => userUserChecklistItem.userChecklistItem)
  userUserChecklistItems: UserUserChecklistItem[];

  @AfterInsert()
  logInsert() {
    Logger.log(`Inserted User Checklist Item with id ${this.id}`);
  }

  @AfterUpdate()
  logUpdate() {
    Logger.log(`Updated User Checklist Item with id ${this.id}`);
  }

  @AfterRemove()
  logRemove() {
    Logger.log('Removed User Checklist Item');
  }
}
