import { Injectable } from '@nestjs/common';
import { ValidatorConstraint, ValidatorConstraintInterface } from 'class-validator';
import { UserChecklistItemsService } from '../user-checklist-items.service';

@ValidatorConstraint({ name: 'UserChecklistItemExists', async: true })
@Injectable()
export class UserChecklistItemExistsRule implements ValidatorConstraintInterface {
  constructor(private readonly userChecklistItemsService: UserChecklistItemsService) {}

  async validate(id: number) {
    try {
      await this.userChecklistItemsService.findOne(id);
    } catch (error) {
      return false;
    }
    return true;
  }

  defaultMessage() {
    return `User Checklist Item does not exist`;
  }
}
