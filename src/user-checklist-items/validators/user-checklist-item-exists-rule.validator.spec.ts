import { NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { createFakeUserChecklistItem } from '../../utils/test-helpers/fakes/user-checklist-item-faker';
import { UserChecklistItemsService } from '../user-checklist-items.service';
import { UserChecklistItemExistsRule } from './user-checklist-item-exists-rule.validator';

const fakeUserChecklistItem = createFakeUserChecklistItem();

describe('UserChecklistItemExistsRule', () => {
  let userChecklistItemExistsRule: UserChecklistItemExistsRule;
  let service: UserChecklistItemsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UserChecklistItemExistsRule,
        {
          provide: UserChecklistItemsService,
          useValue: {
            findOne: jest.fn().mockImplementation((id: number) => Promise.resolve(fakeUserChecklistItem)),
          },
        },
      ],
    }).compile();

    userChecklistItemExistsRule = module.get<UserChecklistItemExistsRule>(UserChecklistItemExistsRule);
    service = module.get<UserChecklistItemsService>(UserChecklistItemsService);
  });

  it('should return true when userChecklistItem exists', async () => {
    expect(userChecklistItemExistsRule.validate(fakeUserChecklistItem.id)).resolves.toEqual(true);
    expect(service.findOne).toBeCalledTimes(1);
    expect(service.findOne).toBeCalledWith(fakeUserChecklistItem.id);
  });

  it('should return false when user checklist item does not exist', async () => {
    const findOneSpy = jest.spyOn(service, 'findOne').mockRejectedValueOnce(NotFoundException);
    expect(userChecklistItemExistsRule.validate(fakeUserChecklistItem.id)).resolves.toEqual(false);
    expect(findOneSpy).toBeCalledTimes(1);
    expect(findOneSpy).toBeCalledWith(fakeUserChecklistItem.id);
  });

  it('should return correct default message', () => {
    expect(userChecklistItemExistsRule.defaultMessage()).toEqual('User Checklist Item does not exist');
  });
});
