import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserChecklistItemDto } from './dto/create-user-checklist-item.dto';
import { GetUserChecklistItemRequestParamsDto } from './dto/get-user-checklist-item-request-params.dto';
import { UpdateUserChecklistItemDto } from './dto/update-user-checklist-item.dto';
import { UserChecklistItem } from './entities/user-checklist-item.entity';

@Injectable()
export class UserChecklistItemsService {
  constructor(
    @InjectRepository(UserChecklistItem)
    private repo: Repository<UserChecklistItem>,
  ) {}

  async create(createUserChecklistItemDto: CreateUserChecklistItemDto, currentUserId = 0) {
    const userChecklistItem = this.repo.create({
      ...createUserChecklistItemDto,
      createdByUserId: currentUserId,
      updatedByUserId: currentUserId,
    });
    await this.repo.save(userChecklistItem);
    return userChecklistItem;
  }

  findAll(params?: GetUserChecklistItemRequestParamsDto) {
    let query = this.repo.createQueryBuilder('userChecklistItems').where('1=1');
    query = params.title
      ? query.andWhere('userChecklistItems.title = :title', {
          title: params.title,
        })
      : query;
    query = params.description
      ? query.andWhere('userChecklistItems.description = :description', {
          description: params.description,
        })
      : query;
    return query.getMany();
  }

  async findOne(id: number) {
    if (!id) {
      throw new BadRequestException();
    }
    const userChecklistItem = await this.repo.findOne(id);
    if (!userChecklistItem) {
      throw new NotFoundException('User Checklist Item not found');
    }
    return userChecklistItem;
  }

  async update(id: number, updateUserChecklistItemDto: UpdateUserChecklistItemDto, currentUserId = 0) {
    const userChecklistItem = await this.findOne(id);
    Object.assign(updateUserChecklistItemDto, {
      updatedByUserId: currentUserId,
    });
    Object.assign(userChecklistItem, updateUserChecklistItemDto);
    await this.repo.save(userChecklistItem);
    return userChecklistItem;
  }

  async remove(id: number) {
    const userChecklistItem = await this.findOne(id);
    await this.repo.remove(userChecklistItem);
    return userChecklistItem;
  }
}
