import { faker } from '@faker-js/faker';
import { BadRequestException, NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { createFakeUserChecklistItem } from '../utils/test-helpers/fakes/user-checklist-item-faker';
import { UserChecklistItemsService } from './user-checklist-items.service';
import { GetUserChecklistItemRequestParamsDto } from './dto/get-user-checklist-item-request-params.dto';
import { UserChecklistItem } from './entities/user-checklist-item.entity';

const currentUserId = faker.datatype.number();

const testUserChecklistItem = createFakeUserChecklistItem(currentUserId);

const getUserChecklistItemRequestParamsDto = new GetUserChecklistItemRequestParamsDto();

describe('UserChecklistItemsService', () => {
  let service: UserChecklistItemsService;
  let repo: Repository<UserChecklistItem>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UserChecklistItemsService,
        {
          provide: getRepositoryToken(UserChecklistItem),
          useValue: {
            create: jest.fn().mockReturnValue(testUserChecklistItem),
            save: jest.fn(),
            findAll: jest.fn().mockResolvedValue([testUserChecklistItem]),
            createQueryBuilder: jest.fn(() => ({
              where: jest.fn().mockReturnThis(),
              andWhere: jest.fn().mockReturnThis(),
              getMany: jest.fn().mockResolvedValue([testUserChecklistItem]),
            })),
            findOne: jest.fn().mockResolvedValue(testUserChecklistItem),
            update: jest.fn().mockResolvedValue(true),
            remove: jest.fn().mockResolvedValue(true),
          },
        },
      ],
    }).compile();

    service = module.get<UserChecklistItemsService>(UserChecklistItemsService);
    repo = module.get<Repository<UserChecklistItem>>(getRepositoryToken(UserChecklistItem));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should create a new user checklist item when provided with valid createUserChecklistItemDto', async () => {
    const createUserChecklistItemDto = {
      title: testUserChecklistItem.title,
      description: testUserChecklistItem.description,
    };

    const userChecklistItem = await service.create(createUserChecklistItemDto, currentUserId);

    expect(userChecklistItem).toEqual(testUserChecklistItem);
    expect(repo.create).toBeCalledTimes(1);
    expect(repo.create).toBeCalledWith({
      ...createUserChecklistItemDto,
      createdByUserId: currentUserId,
      updatedByUserId: currentUserId,
    });
    expect(repo.save).toBeCalledTimes(1);
  });

  it('should find all user checklist items', async () => {
    const userChecklistItems = await service.findAll(getUserChecklistItemRequestParamsDto);

    expect(userChecklistItems).toEqual([testUserChecklistItem]);
  });

  it('should find one user checklist item', async () => {
    const userChecklistItem = await service.findOne(faker.datatype.number());

    expect(userChecklistItem).toEqual(testUserChecklistItem);
  });

  it('should throw bad request exception on findOne if id not provided', async () => {
    await expect(() => service.findOne(null)).rejects.toThrow(BadRequestException);
  });

  it('should throw not found exception on findOne if user checklist item not found', async () => {
    const userChecklistItemId = faker.datatype.number();
    const repoSpy = jest.spyOn(repo, 'findOne').mockReturnValue(undefined);

    await expect(() => service.findOne(userChecklistItemId)).rejects.toThrow(
      new NotFoundException('User Checklist Item not found'),
    );
    expect(repoSpy).toBeCalledTimes(1);
    expect(repoSpy).toBeCalledWith(userChecklistItemId);
  });

  it('should update user checklist item', async () => {
    const userChecklistItemId = faker.datatype.number();
    const updateUserChecklistItemDto = {
      title: 'This title has been updated',
    };

    const userChecklistItem = await service.update(userChecklistItemId, updateUserChecklistItemDto, currentUserId);

    expect(userChecklistItem).toEqual({
      ...testUserChecklistItem,
      ...updateUserChecklistItemDto,
    });
    expect(repo.findOne).toBeCalledTimes(1);
    expect(repo.findOne).toBeCalledWith(userChecklistItemId);
    expect(repo.save).toBeCalledTimes(1);
  });

  it('should remove user checklist item', async () => {
    const userChecklistItemId = faker.datatype.number();

    const userChecklistItem = await service.remove(userChecklistItemId);

    expect(userChecklistItem).toEqual(testUserChecklistItem);
  });
});
