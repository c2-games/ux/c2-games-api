import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class CreateUserChecklistItemDto {
  @ApiProperty({
    type: 'string',
    required: true,
    example: 'User Checklist Item Title',
  })
  @IsString()
  title: string;

  @ApiProperty({
    type: 'string',
    required: true,
    example: 'This is a user checklist item description',
  })
  @IsString()
  description: string;
}
