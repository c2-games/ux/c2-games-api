import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

export class GetUserChecklistItemRequestParamsDto {
  @ApiProperty({
    type: 'string',
    required: false,
    example: 'User Checklist Item Title',
  })
  @IsString()
  @IsOptional()
  title?: string;

  @ApiProperty({
    type: 'string',
    required: false,
    example: 'This is a user checklist item description',
  })
  @IsString()
  @IsOptional()
  description?: string;
}
