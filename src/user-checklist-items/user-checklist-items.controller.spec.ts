import { faker } from '@faker-js/faker';
import { Test, TestingModule } from '@nestjs/testing';
import { KeycloakModule } from '../keycloak/keycloak.module';
import { createFakeUserChecklistItem } from '../utils/test-helpers/fakes/user-checklist-item-faker';
import { createFakeUser } from '../utils/test-helpers/fakes/user-faker';
import { CreateUserChecklistItemDto } from './dto/create-user-checklist-item.dto';
import { UpdateUserChecklistItemDto } from './dto/update-user-checklist-item.dto';
import { UserChecklistItemsController } from './user-checklist-items.controller';
import { UserChecklistItemsService } from './user-checklist-items.service';

const currentUserId = faker.datatype.number();

const fakeUserChecklistItem = createFakeUserChecklistItem(currentUserId);
const fakeUser = createFakeUser(currentUserId);

describe('UserChecklistItemsController', () => {
  let controller: UserChecklistItemsController;
  let service: UserChecklistItemsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [KeycloakModule],
      controllers: [UserChecklistItemsController],
      providers: [
        {
          provide: UserChecklistItemsService,
          useValue: {
            create: jest
              .fn()
              .mockImplementation((userChecklistItem: CreateUserChecklistItemDto, currentUserId: number) =>
                Promise.resolve({
                  ...fakeUserChecklistItem,
                  ...userChecklistItem,
                }),
              ),
            findAll: jest.fn().mockResolvedValue([fakeUserChecklistItem]),
            findOne: jest.fn().mockImplementation((id: number) => Promise.resolve({ ...fakeUserChecklistItem, id })),
            update: jest
              .fn()
              .mockImplementation((id: number, userChecklistItem: UpdateUserChecklistItemDto, currentUserId: number) =>
                Promise.resolve({ ...fakeUserChecklistItem, id }),
              ),
            remove: jest.fn().mockResolvedValue(fakeUserChecklistItem),
          },
        },
      ],
    }).compile();

    controller = module.get<UserChecklistItemsController>(UserChecklistItemsController);
    service = module.get<UserChecklistItemsService>(UserChecklistItemsService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should create user checklist item', async () => {
    const createUserChecklistItemDto: CreateUserChecklistItemDto = {
      title: fakeUserChecklistItem.title,
      description: fakeUserChecklistItem.description,
    };
    const userChecklistItem = await controller.create(createUserChecklistItemDto, fakeUser);

    expect(userChecklistItem).toEqual({ ...fakeUserChecklistItem });
    expect(service.create).toBeCalledTimes(1);
    expect(service.create).toBeCalledWith(createUserChecklistItemDto, fakeUser.id);
  });

  it('should find all user checklist items', async () => {
    const userChecklistItems = await controller.findAll();

    expect(userChecklistItems).toEqual([fakeUserChecklistItem]);
    expect(service.findAll).toBeCalledTimes(1);
  });

  it('should find one user checklist item by id', async () => {
    const userChecklistItem = await controller.findOne(`${fakeUserChecklistItem.id}`);

    expect(userChecklistItem).toEqual(fakeUserChecklistItem);
    expect(service.findOne).toBeCalledTimes(1);
    expect(service.findOne).toBeCalledWith(fakeUserChecklistItem.id);
  });

  it('should update user checklist item', async () => {
    const updateUserChecklistItemDto = {
      title: faker.datatype.string(),
    };
    const userChecklistItem = await controller.update(
      `${fakeUserChecklistItem.id}`,
      updateUserChecklistItemDto,
      fakeUser,
    );

    expect(userChecklistItem).toEqual(fakeUserChecklistItem);
    expect(service.update).toBeCalledTimes(1);
    expect(service.update).toBeCalledWith(fakeUserChecklistItem.id, updateUserChecklistItemDto, fakeUser.id);
  });

  it('should remove user checklist item', async () => {
    const userChecklistItem = await controller.remove(`${fakeUserChecklistItem.id}`);

    expect(userChecklistItem).toEqual(fakeUserChecklistItem);
    expect(service.remove).toBeCalledTimes(1);
    expect(service.remove).toBeCalledWith(fakeUserChecklistItem.id);
  });
});
