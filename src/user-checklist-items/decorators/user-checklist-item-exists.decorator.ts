import { registerDecorator, ValidationOptions } from 'class-validator';
import { UserChecklistItemExistsRule } from '../validators/user-checklist-item-exists-rule.validator';

export function UserChecklistItemExists(validationOptions?: ValidationOptions) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      name: 'UserChecklistItemExists',
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: UserChecklistItemExistsRule,
    });
  };
}
