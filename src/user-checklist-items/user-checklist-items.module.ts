import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { KeycloakModule } from '../keycloak/keycloak.module';
import { UserChecklistItem } from './entities/user-checklist-item.entity';
import { UserChecklistItemsController } from './user-checklist-items.controller';
import { UserChecklistItemsService } from './user-checklist-items.service';
import { UserChecklistItemExistsRule } from './validators/user-checklist-item-exists-rule.validator';

@Module({
  imports: [TypeOrmModule.forFeature([UserChecklistItem]), KeycloakModule],
  controllers: [UserChecklistItemsController],
  providers: [UserChecklistItemsService, UserChecklistItemExistsRule],
})
export class UserChecklistItemsModule {}
