FROM node:16

WORKDIR /usr/src/app

COPY package*.json ./
# todo get rid of --legacy-peer-deps
RUN npm install --legacy-peer-deps

COPY tsconfig*.json ./
COPY src src
RUN npm run build

CMD npm run start:prod
